#include <stdlib.h>
#include <iostream>
#include <cmath>

#include <QtCore/QFile>
#include <QtCore/QRegularExpression>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QTextStream>

//#include <SgMJD.h>


#include <SgVersion.h>
SgVersion                       drVersion("test", 0, 0, 1, "Banzai", SgMJD(2020, 9, 14, 14, 32));
SgVersion                      *driverVersion=&drVersion;

char                            progname[80];
int                             msglev=2;

//
//
//
//
int main(int argc, char **argv)
{
 QRegularExpression     reYy("\\s+(\\d{4})\\s+MULTI-AGENCY\\s+(?:INTENSIVES\\s+|VGOS\\s+|)SCHEDULE\\s*",
                          QRegularExpression::CaseInsensitiveOption);
 QRegularExpressionMatch
                        match;
 QString                fileName("");
 QString                str("");
 QFile                  f;
 QString                yy, name, code, date, stations, dbcCode, scheduledBy, correlatedBy, submittedBy;


 if (argc>1)
 {
    fileName = argv[1];
    f.setFileName(fileName);
    if (!f.exists())
    {
      std::cout << "file \"" << qPrintable(fileName) << "\" does not exist\n";
      return 1;
    };
 }
 else
 {
    std::cout << "say a file name\n";
    return 1;
 };

// std::cout << "Trying to read [" << qPrintable(fileName) << "] file\n";

 if (f.open(QFile::ReadOnly))
 {
   QTextStream                 s(&f);
   while (!s.atEnd())
   {
     str = s.readLine();

     if ((match=reYy.match(str)).hasMatch())
     {
       // std::cout << " ++ get match: " << qPrintable(match.captured(1)) << "\n";
       yy = match.captured(1).right(2);
     };

     if (str.size()>16)
     {
       if (str.at(0) == QChar('|') &&
           str.at(str.size()-1) == QChar('|') &&
           str.count(QChar('|')) == 16)
       {
         QStringList fields=str.split("|");
         name         = fields.at(1).simplified();
         code         = fields.at(2).simplified();
         date         = fields.at(3).simplified();
         stations     = fields.at(7).simplified();
         dbcCode      = fields.at(12).simplified();
         scheduledBy  = fields.at(8).simplified();
         correlatedBy = fields.at(9).simplified();
         submittedBy  = fields.at(13).simplified();
         if (name.size() && code.size() && dbcCode.size() && date.size())
         {
           str = yy + date + dbcCode;
           std::cout << qPrintable(str) << "\n";
         };
       };
     };
   };
   f.close();
   s.setDevice(NULL);
 };


 return 0;
}


