var classSgAgvDatumString =
[
    [ "SgAgvDatumString", "classSgAgvDatumString.html#a9feb2f6f437a8b32759614cd4c5a3d54", null ],
    [ "SgAgvDatumString", "classSgAgvDatumString.html#a78f46e531ba49eee5659364370ec9015", null ],
    [ "~SgAgvDatumString", "classSgAgvDatumString.html#ae56a1f673432ed15ff335696249eb73d", null ],
    [ "allocateSpace", "classSgAgvDatumString.html#a3dcfef3a4ec216fac2a54e8edfae2e98", null ],
    [ "className", "classSgAgvDatumString.html#a23db2673625dfe7f3f7f2570205218da", null ],
    [ "descriptor", "classSgAgvDatumString.html#ac12ef7b61f758b15f991461ad02b4695", null ],
    [ "freeSpace", "classSgAgvDatumString.html#a913a6ebcc882406775a03ade97b49151", null ],
    [ "getValue", "classSgAgvDatumString.html#a11402222e51ea8518f10a8d1df3b7a92", null ],
    [ "isAllocated", "classSgAgvDatumString.html#ac56099ce39e5684cfd0aadca80bc1c10", null ],
    [ "isEmpty", "classSgAgvDatumString.html#a167ff63b30b81b474518e11ff8dcecba", null ],
    [ "value", "classSgAgvDatumString.html#a29bcb4ad5f07425b23c736af0cb2272a", null ],
    [ "data_", "classSgAgvDatumString.html#ac8c754c4d706050cfe1bbb375f4cd7e0", null ],
    [ "descriptor_", "classSgAgvDatumString.html#ae0329e30bbe748ba4735f6ed356da7ff", null ]
];