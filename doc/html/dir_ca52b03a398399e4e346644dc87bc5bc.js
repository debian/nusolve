var dir_ca52b03a398399e4e346644dc87bc5bc =
[
    [ "VcCalc2SessionIfc.cpp", "VcCalc2SessionIfc_8cpp.html", "VcCalc2SessionIfc_8cpp" ],
    [ "VcCalc2SessionIfc.h", "VcCalc2SessionIfc_8h.html", "VcCalc2SessionIfc_8h" ],
    [ "VcSetup.cpp", "VcSetup_8cpp.html", null ],
    [ "VcSetup.h", "VcSetup_8h.html", [
      [ "VcSetup", "classVcSetup.html", "classVcSetup" ]
    ] ],
    [ "VcStartupWizard.cpp", "VcStartupWizard_8cpp.html", null ],
    [ "VcStartupWizard.h", "VcStartupWizard_8h.html", [
      [ "VcWizardPage", "classVcWizardPage.html", "classVcWizardPage" ],
      [ "VcStartupWizard", "classVcStartupWizard.html", "classVcStartupWizard" ]
    ] ],
    [ "VcStartupWizard.moc.cpp", "VcStartupWizard_8moc_8cpp.html", "VcStartupWizard_8moc_8cpp" ],
    [ "VcVersion.cpp", "VcVersion_8cpp.html", "VcVersion_8cpp" ],
    [ "vgosDbCalc.cpp", "vgosDbCalc_8cpp.html", "vgosDbCalc_8cpp" ],
    [ "vgosDbCalc.h", "vgosDbCalc_8h.html", "vgosDbCalc_8h" ],
    [ "vgosDbCalc_resources.cpp", "vgosDbCalc__resources_8cpp.html", "vgosDbCalc__resources_8cpp" ],
    [ "vgosDbCalcSupport.cpp", "vgosDbCalcSupport_8cpp.html", "vgosDbCalcSupport_8cpp" ]
];