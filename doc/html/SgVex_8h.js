var SgVex_8h =
[
    [ "SgVexParameter", "classSgVexParameter.html", "classSgVexParameter" ],
    [ "SgVexRefSatement", "classSgVexRefSatement.html", "classSgVexRefSatement" ],
    [ "SgVexLiteralBlock", "classSgVexLiteralBlock.html", "classSgVexLiteralBlock" ],
    [ "SgVexDefBlock", "classSgVexDefBlock.html", "classSgVexDefBlock" ],
    [ "SgVexScanBlock", "classSgVexScanBlock.html", "classSgVexScanBlock" ],
    [ "SgVexSection", "classSgVexSection.html", "classSgVexSection" ],
    [ "SgVexFile", "classSgVexFile.html", "classSgVexFile" ],
    [ "IfSetup", "classSgVexFile_1_1IfSetup.html", "classSgVexFile_1_1IfSetup" ],
    [ "BbcSetup", "classSgVexFile_1_1BbcSetup.html", "classSgVexFile_1_1BbcSetup" ],
    [ "FreqSetup", "classSgVexFile_1_1FreqSetup.html", "classSgVexFile_1_1FreqSetup" ],
    [ "StationSetup", "classSgVexFile_1_1StationSetup.html", "classSgVexFile_1_1StationSetup" ],
    [ "SgChannelPolarization", "SgVex_8h.html#a12c1fe2d25dd592c7d495fca02b5919d", [
      [ "CP_UNDEF", "SgVex_8h.html#a12c1fe2d25dd592c7d495fca02b5919da3dce692207deb4e44398576e8981d17e", null ],
      [ "CP_RightCP", "SgVex_8h.html#a12c1fe2d25dd592c7d495fca02b5919dabe14aa2e4b4dd1b99c694ad70d996696", null ],
      [ "CP_LeftCP", "SgVex_8h.html#a12c1fe2d25dd592c7d495fca02b5919daf4e9a674925aac98ffb6331683a33531", null ],
      [ "CP_HorizontalLP", "SgVex_8h.html#a12c1fe2d25dd592c7d495fca02b5919dac7c9170eca502017659b7abbaedf2e3c", null ],
      [ "CP_VerticalLP", "SgVex_8h.html#a12c1fe2d25dd592c7d495fca02b5919da9c6c4abf9b5f836f45c97563508edeaf", null ]
    ] ],
    [ "SgChannelSideBand", "SgVex_8h.html#a9d39dcbd0d5e7733ae2f048769183031", [
      [ "CSB_UNDEF", "SgVex_8h.html#a9d39dcbd0d5e7733ae2f048769183031afa6f4cef53c398595fee6e9ee9b3e940", null ],
      [ "CSB_LSB", "SgVex_8h.html#a9d39dcbd0d5e7733ae2f048769183031ae83395e6972a33e5fcc5720649a9c825", null ],
      [ "CSB_USB", "SgVex_8h.html#a9d39dcbd0d5e7733ae2f048769183031aadb7f2635880029681aaf7e6b956e9c1", null ],
      [ "CSB_DUAL", "SgVex_8h.html#a9d39dcbd0d5e7733ae2f048769183031a15e42a497286c902fff2e7dd87b797f7", null ]
    ] ],
    [ "polarization2Str", "SgVex_8h.html#add1d559b57a092996ae453f2a6fcf342", null ],
    [ "sideBand2Str", "SgVex_8h.html#a9b44b0cc7bd3859290d9d599b0574969", null ]
];