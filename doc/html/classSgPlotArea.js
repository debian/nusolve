var classSgPlotArea =
[
    [ "SetsOfData", "classSgPlotArea.html#a0b6707b81bb914e8d09246ebcd0eb2fe", [
      [ "SOD_ALL", "classSgPlotArea.html#a0b6707b81bb914e8d09246ebcd0eb2fead5d1118aa56e7a7b425e572939aec3e3", null ],
      [ "SOD_USABLE", "classSgPlotArea.html#a0b6707b81bb914e8d09246ebcd0eb2fea9da9e56a45340f83091421ef2d9f5334", null ],
      [ "SOD_PROCESSED", "classSgPlotArea.html#a0b6707b81bb914e8d09246ebcd0eb2fea9af2e46589a22405e788dbb08db9e500", null ]
    ] ],
    [ "UserMode", "classSgPlotArea.html#a0e9e74d4dcb0f8af35e3f44e19d86293", [
      [ "UserMode_DEFAULT", "classSgPlotArea.html#a0e9e74d4dcb0f8af35e3f44e19d86293aef8dcd210ea88401f137243388f93221", null ],
      [ "UserMode_SCROLLING", "classSgPlotArea.html#a0e9e74d4dcb0f8af35e3f44e19d86293afa7822434a5ae04554d6ef5fd2467d2c", null ],
      [ "UserMode_INQUIRING", "classSgPlotArea.html#a0e9e74d4dcb0f8af35e3f44e19d86293a079681b78d1456be46eeebd846557eab", null ],
      [ "UserMode_MEASURING", "classSgPlotArea.html#a0e9e74d4dcb0f8af35e3f44e19d86293a6249d32e999bad92abad6a31920e2409", null ],
      [ "UserMode_RERANGING", "classSgPlotArea.html#a0e9e74d4dcb0f8af35e3f44e19d86293a78bfd0ad5ead695ff59f85dadd0059d1", null ],
      [ "UserMode_QUERYING", "classSgPlotArea.html#a0e9e74d4dcb0f8af35e3f44e19d86293a8852695e045bf5a9e2d7221fea27ef25", null ],
      [ "UserMode_SELECTING", "classSgPlotArea.html#a0e9e74d4dcb0f8af35e3f44e19d86293a65a89100866afd8322ad1819094fa04e", null ],
      [ "UserMode_DESELECTING", "classSgPlotArea.html#a0e9e74d4dcb0f8af35e3f44e19d86293a2e3410822e6457255c86afd44b695a52", null ]
    ] ],
    [ "SgPlotArea", "classSgPlotArea.html#ae7ecb7beb3bbfb168d40d54e09251e12", null ],
    [ "~SgPlotArea", "classSgPlotArea.html#a9391296fa98a0e1673afd08918c632ea", null ],
    [ "calcLimits", "classSgPlotArea.html#ae89d712fef3ff041ced1245203992edc", null ],
    [ "calcTransforms", "classSgPlotArea.html#ab7a28453a09146a9a67ccab661d2b63a", null ],
    [ "calcX", "classSgPlotArea.html#ad247f782db8b914f05efbeaa4b5cc54a", null ],
    [ "calcY", "classSgPlotArea.html#abeced40881448e780c43207c95d55fba", null ],
    [ "className", "classSgPlotArea.html#acd3dbfc05c34aeb67e8e96a5d338947d", null ],
    [ "dataChanged", "classSgPlotArea.html#a87df419d968ad07225457776d941824b", null ],
    [ "defineAreas", "classSgPlotArea.html#a1ad3cfbc7fc58d372e5403810e3c0147", null ],
    [ "drawData", "classSgPlotArea.html#a6abf0246b3de4a29c45ae7d5648562d9", null ],
    [ "drawFrames", "classSgPlotArea.html#a3613f2e4891bb2a521415d2a30acaa84", null ],
    [ "drawPointInfo", "classSgPlotArea.html#a50d5e72788e2ae092772aeb08415e53d", null ],
    [ "drawPointSelector", "classSgPlotArea.html#ad0932c4f0c59b499e40116412f23c6a3", null ],
    [ "drawRangeSelector", "classSgPlotArea.html#a39689fbf6f0a59de0f2bf16d69178766", null ],
    [ "drawRuler", "classSgPlotArea.html#ab71e05febbc5262ce5d618dab33e65ce", null ],
    [ "drawWholePlot", "classSgPlotArea.html#ae30b89bbfe22e85e33fc4621f9b753e4", null ],
    [ "drawXmjdTics", "classSgPlotArea.html#a515c6b53459183058bec8658eb128404", null ],
    [ "drawXTics", "classSgPlotArea.html#aae8b9be3106171da3dd1d2486394388a", null ],
    [ "drawYTics", "classSgPlotArea.html#a8be9e098c0f6955d89134e43d9a79c7a", null ],
    [ "getRangeLimits", "classSgPlotArea.html#a50a0239448fa7c0c3c2b8bd6465de5ba", null ],
    [ "getRulerFromPoint", "classSgPlotArea.html#a2f4945c45729beace4ba8f704c996b85", null ],
    [ "getRulerToPoint", "classSgPlotArea.html#a78b114e62b5a5805215d2cb9ffa52afb", null ],
    [ "getRulerToPointPrev", "classSgPlotArea.html#aa815637decfa19b4ea31737d6e51844f", null ],
    [ "getSets2plot", "classSgPlotArea.html#ab2b95d289a299c8b9d600be6f179668a", null ],
    [ "getUserMode", "classSgPlotArea.html#aad4fb4bf650aaa769fd9c4e3a10b481b", null ],
    [ "getXColumn", "classSgPlotArea.html#a8cfe5f17dae925a432fa39d2ca7b5600", null ],
    [ "getYColumn", "classSgPlotArea.html#aca34776aec3396c74f0e08eb6e3c2766", null ],
    [ "height", "classSgPlotArea.html#a360df6e847cbe68016aaeb2dcfc43034", null ],
    [ "initBranchPens", "classSgPlotArea.html#a8a74149e8f76664ad128dc0e28214107", null ],
    [ "isXTicsMJD", "classSgPlotArea.html#afc138398c4df350308ff989843220a34", null ],
    [ "output4Files", "classSgPlotArea.html#a579ca0032926464b63d4b3f7d0faa296", null ],
    [ "output4Print", "classSgPlotArea.html#a3c569759a57ba67f21af34de97af1302", null ],
    [ "paintEvent", "classSgPlotArea.html#ad6b41de7a4044bf82ffcc6142df4e502", null ],
    [ "queryPoint", "classSgPlotArea.html#ab9c7a9c09a887809254732ba26822511", null ],
    [ "resizeEvent", "classSgPlotArea.html#a4c201223562e54db3e6e7f2d410c3712", null ],
    [ "reverseCalcX", "classSgPlotArea.html#afeeded3e792b3b06534f409350d112cd", null ],
    [ "reverseCalcY", "classSgPlotArea.html#a6725a0c79146ca4d4e9c167ff0340e97", null ],
    [ "setBPHuePhase", "classSgPlotArea.html#aa026115ebcf6dd533204b5fd4b2ecfba", null ],
    [ "setBPSaturation", "classSgPlotArea.html#a33bfa073bd09816041e23fc4a7bb13c7", null ],
    [ "setBPValue", "classSgPlotArea.html#a704d9b4bcb978505127aee6a82ba9afd", null ],
    [ "setBranchColors", "classSgPlotArea.html#aea5b722cf543198e713077099e96b6a0", null ],
    [ "setHave2HasZero", "classSgPlotArea.html#a6b9c0c86182a87cff3fa06a87cceda7a", null ],
    [ "setIsLimitsOnVisible", "classSgPlotArea.html#ab51fb187fdbda0cc91d189ffa508b658", null ],
    [ "setIsPlotErrBars", "classSgPlotArea.html#a436c0f16a22778c197aa81d205eefbb5", null ],
    [ "setIsPlotImpulses", "classSgPlotArea.html#a6009fc0f97a955e827d20c2023c0ec7a", null ],
    [ "setIsPlotLines", "classSgPlotArea.html#a9a7453f045a3f5ec0382538451da0f22", null ],
    [ "setIsPlotPoints", "classSgPlotArea.html#a3607ff68016dbd933f5d9eeeeb470163", null ],
    [ "setIsRangeSymmetrical", "classSgPlotArea.html#a477f81381f0c15211030296e5e6aa98f", null ],
    [ "setIsStdVar", "classSgPlotArea.html#ae4fbfb758591f89e84acd16f4079fead", null ],
    [ "setRangeLimits", "classSgPlotArea.html#ae05325f7e1b84c6af064faf380625c6f", null ],
    [ "setRulerFromPoint", "classSgPlotArea.html#a1d663ad92471b54ef2608fd7a6a71e90", null ],
    [ "setRulerToPoint", "classSgPlotArea.html#acf91cce6891f6a7f00610f7f3f950c52", null ],
    [ "setSets2plot", "classSgPlotArea.html#a0ac6967dea5830ae760bcc6ced85de72", null ],
    [ "setUserDefinedRanges", "classSgPlotArea.html#a47f46f1899a9a5149fa4066789e92674", null ],
    [ "setUserMode", "classSgPlotArea.html#a1bec27b62b0b483b3c22cd51af261332", null ],
    [ "setVisibleHeight", "classSgPlotArea.html#aed01c72167ad2902ed1852283e9acd29", null ],
    [ "setVisibleWidth", "classSgPlotArea.html#aac16c4913ed3ca6044f41742217a8994", null ],
    [ "setXColumn", "classSgPlotArea.html#a6343cfb892b5e90fc970a22fc33da525", null ],
    [ "setYColumn", "classSgPlotArea.html#a808f85f6efcf129a128041e90ac7a2f1", null ],
    [ "unsetUserDefinedRanges", "classSgPlotArea.html#a93fa0357520150ccc3a2f23ad4064ff6", null ],
    [ "width", "classSgPlotArea.html#afaeac433b441e4824699d5e2f9c22f7b", null ],
    [ "SgPlot", "classSgPlotArea.html#ad9fb9bf35687c8431b08d1e029210a4f", null ],
    [ "barPen_", "classSgPlotArea.html#afc121f4bfae091bdecfd8a43ddccc8c8", null ],
    [ "bpHuePhase_", "classSgPlotArea.html#a242e390e733653fe02ea1cc3c71013b9", null ],
    [ "bpSaturation_", "classSgPlotArea.html#ab9072086372a73f9fd2632191a1ab222", null ],
    [ "bpValue_", "classSgPlotArea.html#a3c052d74df566393f3476be2b5334401", null ],
    [ "branchBrushes_", "classSgPlotArea.html#affd02904c4d34ea9e35e9ed1cb7a0702", null ],
    [ "branchPens_", "classSgPlotArea.html#a8ea18e23bd952bed51a8bda078138685", null ],
    [ "branchSelectedBrushes_", "classSgPlotArea.html#a439f816f2d99b92aca0913839507d375", null ],
    [ "branchSelectedPens_", "classSgPlotArea.html#a6bb8a4699185530e1e857fba7d984a03", null ],
    [ "cursorDefault_", "classSgPlotArea.html#a83d7e6f4618dc284261cd17289725e20", null ],
    [ "cursorMeasuring_", "classSgPlotArea.html#a3f4b121a14f03a971844ea104d2fdeed", null ],
    [ "cursorScrolling_", "classSgPlotArea.html#a385e95c64ee9d47481f2b35572b65724", null ],
    [ "ddr_", "classSgPlotArea.html#abeae83194528fe5c5090ba461111f876", null ],
    [ "f_Ax_", "classSgPlotArea.html#a6c8cd639ad54ca91f030e8f8f8acdf1a", null ],
    [ "f_Ay_", "classSgPlotArea.html#a35e30727dde4b47f871496ed29858ced", null ],
    [ "f_Bx_", "classSgPlotArea.html#ae65cbd1632e8edb1709df89412ee1271", null ],
    [ "f_By_", "classSgPlotArea.html#a6af984dc138ae472131f8d7431e7d026", null ],
    [ "framePen_", "classSgPlotArea.html#a4550cd9fc9d343b43d0f5bab39194391", null ],
    [ "have2HasZero_", "classSgPlotArea.html#ae7a14913cfd167c432da218168ed97f3", null ],
    [ "height_", "classSgPlotArea.html#ab27e36e5c62ad7445f25302d18b96c8e", null ],
    [ "ignoredBrush_", "classSgPlotArea.html#af49c292c689bd39c85af1311b9cc3929", null ],
    [ "ignoredPen_", "classSgPlotArea.html#aa5f16f729b1eae1082de5f4a1ce6fdec", null ],
    [ "isLimitsOnVisible_", "classSgPlotArea.html#ae39f02c978f49f1389364eb49e437f5c", null ],
    [ "isPlotErrBars_", "classSgPlotArea.html#aeeefa5fa2d2d297fac00f59571c243f7", null ],
    [ "isPlotImpulses_", "classSgPlotArea.html#a2a7a5f9c9a104a27203ebefda2f2ad73", null ],
    [ "isPlotLines_", "classSgPlotArea.html#a8590e6fbf437e565d0ec2ac822bdbded", null ],
    [ "isPlotPoints_", "classSgPlotArea.html#a04406c25210fd675db3b6f4e929c61a2", null ],
    [ "isRangeSymmetrical_", "classSgPlotArea.html#a05424b37e8360eabff9a6dcf8574e7a9", null ],
    [ "isStdVar_", "classSgPlotArea.html#aafbc43257a8f5546c83953bfec12366c", null ],
    [ "isXTicsBiased_", "classSgPlotArea.html#a56a01f71a790a019c483c90e040d800a", null ],
    [ "isXTicsMJD_", "classSgPlotArea.html#a2b8e753ffcabdf589df91a77bdfcdfe0", null ],
    [ "isYTicsBiased_", "classSgPlotArea.html#a5ceca900d9cbf9bcaad1e6e5d1517102", null ],
    [ "labelsHeight_", "classSgPlotArea.html#a61a771e3ad0c3819dc29f49e35bdf1e9", null ],
    [ "maxX_", "classSgPlotArea.html#aa17e381bdae99e467406807a3439b490", null ],
    [ "maxY_", "classSgPlotArea.html#a039d76fa52fa6b14b19cc3df8cb05ac6", null ],
    [ "minX_", "classSgPlotArea.html#af1cd8e78ed1743bbbe2434897a7d9020", null ],
    [ "minY_", "classSgPlotArea.html#a53c9b7b2076f7a8b0a9cd5028d233eeb", null ],
    [ "numOfXTics_", "classSgPlotArea.html#acd63aa9a8d960bbbd937d19c5821636d", null ],
    [ "numOfYTics_", "classSgPlotArea.html#a8634fb9e0f6647362e1af291260a5575", null ],
    [ "plotCarrier_", "classSgPlotArea.html#acae09740b64be92f794e66047c7e4d06", null ],
    [ "radius_", "classSgPlotArea.html#af3432c9a36d788520c17533f5b0d81e3", null ],
    [ "rangeLimits_", "classSgPlotArea.html#a44ed0514680ff44453a981b629dc25ea", null ],
    [ "rightMargin_", "classSgPlotArea.html#a66ea0cdd6a03e6e680333c8282a131a0", null ],
    [ "rulerBrush_", "classSgPlotArea.html#a7e5e01e15e3caa393ad34d2fa27e043a", null ],
    [ "rulerFromPoint_", "classSgPlotArea.html#a484a1ab962a152122d802fdc6bbbbda3", null ],
    [ "rulerPen_", "classSgPlotArea.html#a05186556b031cec93a3f15707abb0f1e", null ],
    [ "rulerToPoint_", "classSgPlotArea.html#a853ee01e43e1427efaedafc79071d020", null ],
    [ "rulerToPointPrev_", "classSgPlotArea.html#a274f246ff3d00dafcb354df7ec9831a4", null ],
    [ "sets2plot_", "classSgPlotArea.html#a02d9a5c2206b4a4a9b9b64505330c745", null ],
    [ "ticLinesPen_", "classSgPlotArea.html#ad64355ee260551ccbecdd5df13471068", null ],
    [ "titleWidth_", "classSgPlotArea.html#aeedcb0f5b2af5cf389a5d6e1e51cf579", null ],
    [ "upMargin_", "classSgPlotArea.html#a8b10341554e12793cd83ffe590d6c329", null ],
    [ "userDefinedMaxX_", "classSgPlotArea.html#aef9b43b5c9f842a8bbc15a4edb7e1a61", null ],
    [ "userDefinedMaxY_", "classSgPlotArea.html#a571c9fc901a7f028b4bb63f51cfde488", null ],
    [ "userDefinedMinX_", "classSgPlotArea.html#a235b6e9957577c324482788ef824a5b6", null ],
    [ "userDefinedMinY_", "classSgPlotArea.html#ab595147b10898bd5e93e72f236879599", null ],
    [ "userMode_", "classSgPlotArea.html#afba7e6ca4b0544893f72c2de7940a2fe", null ],
    [ "useUserDefinedRanges_", "classSgPlotArea.html#ad4479e5bc31e25b23c4805d62c38d5cb", null ],
    [ "visibleHeight_", "classSgPlotArea.html#a09847a9169afae38f6908502effe52c0", null ],
    [ "visibleWidth_", "classSgPlotArea.html#ad88188d85840ab18e4e56e1af91001ad", null ],
    [ "width_", "classSgPlotArea.html#a3bfd34dc43251a210a8198b07f4eb4c8", null ],
    [ "xColumn_", "classSgPlotArea.html#a6cdf80721ade8b73d8f183ef98f6a753", null ],
    [ "xDataBegin_", "classSgPlotArea.html#a9afe4c44a2a23730774bfd8b3f7bb4fd", null ],
    [ "xDataEnd_", "classSgPlotArea.html#a35ba76f4e9494d3ad7bd62200dffb92f", null ],
    [ "xFrameBegin_", "classSgPlotArea.html#a7c9ab86845045e878683933a831cc82a", null ],
    [ "xFrameEnd_", "classSgPlotArea.html#ac91e1577918306bec2a6050975549e0b", null ],
    [ "xLabel4Unknown_", "classSgPlotArea.html#a67dc838bbf3835f9eebc70b630bde5bd", null ],
    [ "xLabel_", "classSgPlotArea.html#a8ff6513208fdeeec65a0f817c24e1fc5", null ],
    [ "xLabelWidth_", "classSgPlotArea.html#a5eb035c730343e09d645fdabeb1c5879", null ],
    [ "xMargins_", "classSgPlotArea.html#a8cdb2bb8d625528bb8608c91c6ddec3c", null ],
    [ "xStepP_", "classSgPlotArea.html#aad2842af322a3c738e9bbdd27939c6c7", null ],
    [ "xTicsBias_", "classSgPlotArea.html#a950e8282241b72eb488b880ff57d6e96", null ],
    [ "xTicsMJD_", "classSgPlotArea.html#a73095dc65e8f364b1a85e4f19feb2450", null ],
    [ "xTicsStep_", "classSgPlotArea.html#a3b805d9c6858a53c96f2b676630b5496", null ],
    [ "xTicsWidth_", "classSgPlotArea.html#a72ed9012a7783b9cecf8dde6b41c35c5", null ],
    [ "yColumn_", "classSgPlotArea.html#ac4679053929c8c1c1a8f0d561bd1cce6", null ],
    [ "yDataBegin_", "classSgPlotArea.html#abfe6340c1dc5484cae3ee9c9040ae461", null ],
    [ "yDataEnd_", "classSgPlotArea.html#a7fc795014d84ce7a581984f0b7023daf", null ],
    [ "yFrameBegin_", "classSgPlotArea.html#a3325b8623a6ed76225bbde6527f21413", null ],
    [ "yFrameEnd_", "classSgPlotArea.html#ae3d2941c3b1aa5c14551b9ff12d0b739", null ],
    [ "yLabel4Unknown_", "classSgPlotArea.html#a176058b045508cf64f01519cff835e28", null ],
    [ "yLabel_", "classSgPlotArea.html#a0f2c7b72df56891bc448fe5a316b5859", null ],
    [ "yLabelHeight_", "classSgPlotArea.html#a529866b804397c038995df36d19c41b3", null ],
    [ "yLabelWidth_", "classSgPlotArea.html#a5d703ef47c2d41c693b312127a8a5082", null ],
    [ "yMargins_", "classSgPlotArea.html#ae63a84a71fae9f183e4481502c1f7b43", null ],
    [ "yStepP_", "classSgPlotArea.html#a89f58c1867b3f6a2f916761a316cd814", null ],
    [ "yTicsBias_", "classSgPlotArea.html#a8466b75c93cd7b0d4e020c08777f1bf8", null ],
    [ "yTicsStep_", "classSgPlotArea.html#abb217b5e99d02309cf29ed72d0fc9b14", null ],
    [ "yTicsWidth_", "classSgPlotArea.html#ac407bf51d864b5c63bd19a15fad053ef", null ],
    [ "zeroPen_", "classSgPlotArea.html#a4e6a99d94b6eaa2883e970beed91c74e", null ]
];