var classSgVlbiSessionInfo =
[
    [ "Attributes", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46", [
      [ "Attr_NOT_VALID", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46af7aa2497e4101c17ab7abdb6b4b3920e", null ],
      [ "Attr_HAS_AUX_OBS", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46ab7162a405f5c183dbbaacf081c73df50", null ],
      [ "Attr_PRE_PROCESSED", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46a636c2f772b51a7998d2a97e5673e0ae2", null ],
      [ "Attr_HAS_CLOCK_BREAKS", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46ad2f444df920b1357a52664d1bbb6ba43", null ],
      [ "Attr_HAS_DTEC", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46a8f1aa11deb7fc7de72fa08541db39246", null ],
      [ "Attr_HAS_CALC_DATA", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46a8683c919028442bdfbe5a34ee8c9070a", null ],
      [ "Attr_HAS_IONO_CORR", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46abae9d5fa8ca0f6e9f0c77e34abc692e4", null ],
      [ "Attr_HAS_WEIGHTS", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46abbd21cdcda9371cbfc7d0f6c1215600a", null ],
      [ "Attr_FF_CREATED", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46aa689b28153e54424a200d02dba6950c2", null ],
      [ "Attr_FF_CALC_DATA_MODIFIED", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46a6d9f576a6bcab88e105f7f70403ed39b", null ],
      [ "Attr_FF_AUX_OBS_MODIFIED", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46a2297ce1d02f577c78cfb41f8a6b5f632", null ],
      [ "Attr_FF_OUTLIERS_PROCESSED", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46a8560223029c42e2ef2710272fd345d14", null ],
      [ "Attr_FF_AMBIGS_RESOLVED", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46aa2d14ff4574f9b4eda72173a2aad6cf0", null ],
      [ "Attr_FF_WEIGHTS_CORRECTED", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46a4ddaa66cafff84e32a97eec263c25259", null ],
      [ "Attr_FF_ION_C_CALCULATED", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46a65207922a8ec0aeac83248815b5674e5", null ],
      [ "Attr_FF_ECC_UPDATED", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46ab4294f108e2d7c84c3d0ebedd1d59974", null ],
      [ "Attr_FF_AUTOPROCESSED", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46ab4b3af85893e8ac6d57e3a33ad61f9a4", null ],
      [ "Attr_FF_EDIT_INFO_MODIFIED", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46a45e76027600eeebd877080c9d88a77b1", null ],
      [ "Attr_FF_STN_PWL_MODIFIED", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46a9bbde2bbacc64eee9c392019c7284a2e", null ],
      [ "Attr_FF_PHASE_DEL_USED", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46a84ca828216c9a36a374946e661f5aeca", null ],
      [ "Attr_REF_CLOCKS_ADJUSTED", "classSgVlbiSessionInfo.html#a6233994c652044a3914e85f8a1086f46a5de5a0a0de4df9e1e1a5b525266d4604", null ]
    ] ],
    [ "CorrelatorPostProcSoftware", "classSgVlbiSessionInfo.html#a7962e4996a7643ddae0f288fc645d9ac", [
      [ "CPPS_HOPS", "classSgVlbiSessionInfo.html#a7962e4996a7643ddae0f288fc645d9acaf294e67ba582706a7ee08b8758315438", null ],
      [ "CPPS_PIMA", "classSgVlbiSessionInfo.html#a7962e4996a7643ddae0f288fc645d9aca923edf19ad9b15ed319f165c0908b43c", null ],
      [ "CPPS_C5PP", "classSgVlbiSessionInfo.html#a7962e4996a7643ddae0f288fc645d9aca0fa9d5ff3602999a9fc5b617dd6ddf95", null ],
      [ "CPPS_UNKNOWN", "classSgVlbiSessionInfo.html#a7962e4996a7643ddae0f288fc645d9aca3e13c6c8db089266a96c9b08b201ac03", null ]
    ] ],
    [ "OriginType", "classSgVlbiSessionInfo.html#a4d5daf983ac44253967ac50f3be1a7e2", [
      [ "OT_DBH", "classSgVlbiSessionInfo.html#a4d5daf983ac44253967ac50f3be1a7e2a0476407ff73fa286896f7d4c1584a9c8", null ],
      [ "OT_NGS", "classSgVlbiSessionInfo.html#a4d5daf983ac44253967ac50f3be1a7e2a7a4ab711818f7c2129121b64b521f68c", null ],
      [ "OT_VDB", "classSgVlbiSessionInfo.html#a4d5daf983ac44253967ac50f3be1a7e2a7a467482ef2bc55fc7d215a6bba44a47", null ],
      [ "OT_MK4", "classSgVlbiSessionInfo.html#a4d5daf983ac44253967ac50f3be1a7e2a4134d0be3618b1cc60595d7d94e897d7", null ],
      [ "OT_KOMB", "classSgVlbiSessionInfo.html#a4d5daf983ac44253967ac50f3be1a7e2aacd3f9bdc7b1f9007c83259837a7ccce", null ],
      [ "OT_AGV", "classSgVlbiSessionInfo.html#a4d5daf983ac44253967ac50f3be1a7e2a4fa65c686a662543f668f7422729ce52", null ],
      [ "OT_UNKNOWN", "classSgVlbiSessionInfo.html#a4d5daf983ac44253967ac50f3be1a7e2af531d46c4163790eb12019d743ef784f", null ]
    ] ],
    [ "SgVlbiSessionInfo", "classSgVlbiSessionInfo.html#a8958b79fd94eb566eecdf7e80bef3919", null ],
    [ "SgVlbiSessionInfo", "classSgVlbiSessionInfo.html#aeee052ff2f78c4306e938208d666ce05", null ],
    [ "~SgVlbiSessionInfo", "classSgVlbiSessionInfo.html#a641d13c7108a3d2281fa814053caf0b7", null ],
    [ "addDelayWRMS", "classSgVlbiSessionInfo.html#afa5609ce58324d6ed5650c390755635f", null ],
    [ "addRateWRMS", "classSgVlbiSessionInfo.html#a52f0be90be703cb56d68d13cc84bbaea", null ],
    [ "calcTMean", "classSgVlbiSessionInfo.html#a36b34d9d23793fa0031d161dbc2b9426", null ],
    [ "className", "classSgVlbiSessionInfo.html#af45af95831982619f034c56c9a2dc9ba", null ],
    [ "clearRMSs", "classSgVlbiSessionInfo.html#a0c1a77eea75d954baf00c06afd6f6fd9", null ],
    [ "delayWRMS", "classSgVlbiSessionInfo.html#a962bb4a12e0d82e69fb310cc21f61ab9", null ],
    [ "getCorrelatorName", "classSgVlbiSessionInfo.html#a72d14c360a2fd179859c5ceabac18609", null ],
    [ "getCorrelatorType", "classSgVlbiSessionInfo.html#a06c0e627b6176ab036ad233527bfd995", null ],
    [ "getCppsSoft", "classSgVlbiSessionInfo.html#aac49cf07ebd84c52ece5d57f15fabbd0", null ],
    [ "getDescription", "classSgVlbiSessionInfo.html#a8ad336fa9e10af9b41fa190083dada21", null ],
    [ "getExperimentSerialNumber", "classSgVlbiSessionInfo.html#a822fb78c6d5ec0719f0c6f474c3a3626", null ],
    [ "getName", "classSgVlbiSessionInfo.html#a0924be8127bcc2a83a1d9779dc2158ba", null ],
    [ "getNetworkID", "classSgVlbiSessionInfo.html#a0e90e8ba2fb8943ef0c0722968de8810", null ],
    [ "getNetworkSuffix", "classSgVlbiSessionInfo.html#a9a97f5e1df62164208e7be2e383c4c30", null ],
    [ "getNumOfBaselines", "classSgVlbiSessionInfo.html#a6ec7c870fe786e9de7f0f25dcba54d0f", null ],
    [ "getNumOfObservations", "classSgVlbiSessionInfo.html#a9f58f859125602a9508c896fe06f6ade", null ],
    [ "getNumOfSources", "classSgVlbiSessionInfo.html#a9c55fbcbfa0a7682baec92aa8d0dba63", null ],
    [ "getNumOfStations", "classSgVlbiSessionInfo.html#a2d8798515353d41ad2f0a621cf509a47", null ],
    [ "getOfficialName", "classSgVlbiSessionInfo.html#a770114b6fec743023da03667d002e274", null ],
    [ "getOriginType", "classSgVlbiSessionInfo.html#a6b6f48c856ba8209beec748d8a117ca4", null ],
    [ "getPiAgencyName", "classSgVlbiSessionInfo.html#a8867d6cc0cc049b0dff77db2244e9c2a", null ],
    [ "getRecordingMode", "classSgVlbiSessionInfo.html#af3f6813ec07903d1955bc92c21faee3b", null ],
    [ "getSchedulerName", "classSgVlbiSessionInfo.html#a95f4b9cc08aa900d478741dc7c99f730", null ],
    [ "getSessionCode", "classSgVlbiSessionInfo.html#ac05153fe45827616db0561fe3480d36d", null ],
    [ "getSubmitterName", "classSgVlbiSessionInfo.html#a2c84362b3466e123292ef00039f7a83a", null ],
    [ "getTCreation", "classSgVlbiSessionInfo.html#ad4c8b286a2daa7d713cdcb6b00134328", null ],
    [ "getTFinis", "classSgVlbiSessionInfo.html#a08c448e5899bd495127cf055c6126d98", null ],
    [ "getTMean", "classSgVlbiSessionInfo.html#ac06a8aa8d788194af0559523d275805a", null ],
    [ "getTStart", "classSgVlbiSessionInfo.html#a0f0b361a10c542ce1cbeddc07f982925", null ],
    [ "getUserFlag", "classSgVlbiSessionInfo.html#a36aa5230ffd2647f590aa6faeb7412d3", null ],
    [ "guessNetworkId", "classSgVlbiSessionInfo.html#ac0a1ef747cabbdd651847f6542f509fe", null ],
    [ "operator=", "classSgVlbiSessionInfo.html#a4c3f11c098e83e7acd0cb0143d5a2370", null ],
    [ "rateWRMS", "classSgVlbiSessionInfo.html#aef2ec8d2489ad90eca13460d9a39b24e", null ],
    [ "setCorrelatorName", "classSgVlbiSessionInfo.html#abfc634e543fb531a7c7bcae4245ec60a", null ],
    [ "setCorrelatorType", "classSgVlbiSessionInfo.html#a559e2d951f32774585e589968b1d6756", null ],
    [ "setCppsSoft", "classSgVlbiSessionInfo.html#ae7343040f078537e5a2262b717994492", null ],
    [ "setDescription", "classSgVlbiSessionInfo.html#a3e1eb26e7a036da3efb90d48bca94171", null ],
    [ "setExperimentSerialNumber", "classSgVlbiSessionInfo.html#a86b3516e47f087109b6a709cb4288d92", null ],
    [ "setName", "classSgVlbiSessionInfo.html#a513e62a03cd8ee52708034016a49259f", null ],
    [ "setNetworkID", "classSgVlbiSessionInfo.html#a28c2cec429296040713e3e9cd68c11f1", null ],
    [ "setNetworkSuffix", "classSgVlbiSessionInfo.html#a9318a9df7b60e0ff68f0fd48e9b7f4f2", null ],
    [ "setNumOfBaselines", "classSgVlbiSessionInfo.html#ab222b895749354b622e4c61f1adb8d5d", null ],
    [ "setNumOfObservations", "classSgVlbiSessionInfo.html#a93e626684145d79bb60273dfc32c040a", null ],
    [ "setNumOfSources", "classSgVlbiSessionInfo.html#a90b79995aae21b54cbba1010bdb9650c", null ],
    [ "setNumOfStations", "classSgVlbiSessionInfo.html#a87ef627d7b99f13664eeac2cad2aede8", null ],
    [ "setOfficialName", "classSgVlbiSessionInfo.html#a0c7a8a2e96f6980f207af4928040e839", null ],
    [ "setOriginType", "classSgVlbiSessionInfo.html#ad4204f74936e8b4b68f04f5bc3a7a31f", null ],
    [ "setPiAgencyName", "classSgVlbiSessionInfo.html#aca55dac3c2ce67363c7753a2b4f79b70", null ],
    [ "setRecordingMode", "classSgVlbiSessionInfo.html#a16c41b68dbc0724a5b45611d287c3011", null ],
    [ "setSchedulerName", "classSgVlbiSessionInfo.html#a10340d4f1330fa11d35323b2f968ca10", null ],
    [ "setSessionCode", "classSgVlbiSessionInfo.html#a320bfd115eaffd1a86641ec6297364b4", null ],
    [ "setSubmitterName", "classSgVlbiSessionInfo.html#abed990b3afa4b0ebbf33e90f3ea8e327", null ],
    [ "setTCreation", "classSgVlbiSessionInfo.html#a62bd0b8f4e8d0d1467ce9fcf0a469346", null ],
    [ "setTFinis", "classSgVlbiSessionInfo.html#a54394dbd1fe6883bdceb9010c6f9f8ca", null ],
    [ "setTStart", "classSgVlbiSessionInfo.html#ac377a899abb316a87ccf50ff0ef49483", null ],
    [ "setUserFlag", "classSgVlbiSessionInfo.html#a5eef350c76638e49a35bce1641b9b2a5", null ],
    [ "correlatorName_", "classSgVlbiSessionInfo.html#ace8de411d7cc1a43ffc1dbc485698774", null ],
    [ "correlatorType_", "classSgVlbiSessionInfo.html#a7b9f7180e557d75fe6ddc7fbd9c6c683", null ],
    [ "cppsSoft_", "classSgVlbiSessionInfo.html#a58f492aafc35157eab10086a92831b28", null ],
    [ "delaySumRMS2_", "classSgVlbiSessionInfo.html#aed2b34af1eb751b3ed46463954d2c101", null ],
    [ "delaySumW_", "classSgVlbiSessionInfo.html#afad4ca55cb6daacfbac13b1607341583", null ],
    [ "description_", "classSgVlbiSessionInfo.html#ac4113549e458aeec52578c0aa1389195", null ],
    [ "experimentSerialNumber_", "classSgVlbiSessionInfo.html#a9e9008068a1469276bd7d4f4146a9721", null ],
    [ "name_", "classSgVlbiSessionInfo.html#af9f35b34266d3bae312d5bf1d12cf7d9", null ],
    [ "networkID_", "classSgVlbiSessionInfo.html#a784a2c90176ec177428e99c0a19db883", null ],
    [ "networkSuffix_", "classSgVlbiSessionInfo.html#a7e66772fdb6d0ec90f3e90573b46aaf8", null ],
    [ "numOfBaselines_", "classSgVlbiSessionInfo.html#ab740c96e5973db570e5c7315369507ab", null ],
    [ "numOfObservations_", "classSgVlbiSessionInfo.html#ad9a18029e87b9cce6452a2003244e0b4", null ],
    [ "numOfSources_", "classSgVlbiSessionInfo.html#acf111c5f2dc88af9dd739b1f1e8ed9dd", null ],
    [ "numOfStations_", "classSgVlbiSessionInfo.html#a0ba16f174952881e925eede18fb09d67", null ],
    [ "officialName_", "classSgVlbiSessionInfo.html#a54e9b45ceb8ba8cf75cc337ef3279237", null ],
    [ "originType_", "classSgVlbiSessionInfo.html#ad2e4e371e16b369bd9e9a2a183454d3d", null ],
    [ "piAgencyName_", "classSgVlbiSessionInfo.html#ac135010cd25292bec180392ec48ada09", null ],
    [ "rateSumRMS2_", "classSgVlbiSessionInfo.html#a8ede1e74fe79f89844f4f7b7273980ce", null ],
    [ "rateSumW_", "classSgVlbiSessionInfo.html#a253ec85b486a886db3608730bd6410d8", null ],
    [ "recordingMode_", "classSgVlbiSessionInfo.html#a4c74feee88a3146380885e8079d82447", null ],
    [ "schedulerName_", "classSgVlbiSessionInfo.html#ac4977245cc1d7cbc97600651e27eb890", null ],
    [ "sessionCode_", "classSgVlbiSessionInfo.html#a9dbcecd1dd97c98a3f08832a5f545981", null ],
    [ "submitterName_", "classSgVlbiSessionInfo.html#af4adac43d8c842bd93e411538a2350af", null ],
    [ "tCreation_", "classSgVlbiSessionInfo.html#ad92b85c8bd8daafc2b1a5cef66224969", null ],
    [ "tFinis_", "classSgVlbiSessionInfo.html#a6532d75b743170964e54ca9ff46eb72c", null ],
    [ "tMean_", "classSgVlbiSessionInfo.html#ab47d751ac33554570ef8dcfcea780f54", null ],
    [ "tStart_", "classSgVlbiSessionInfo.html#afa7f440e1dc616d83239d8edcab927ff", null ],
    [ "userFlag_", "classSgVlbiSessionInfo.html#a4052c619d747ec77b5986221027d8da4", null ]
];