var SgTidalUt1_8h =
[
    [ "SgTidalUt1", "classSgTidalUt1.html", "classSgTidalUt1" ],
    [ "SgUt1TidalTableEntry_Simplified", "structSgUt1TidalTableEntry__Simplified.html", "structSgUt1TidalTableEntry__Simplified" ],
    [ "SgUt1TidalTableEntry_Standard", "structSgUt1TidalTableEntry__Standard.html", "structSgUt1TidalTableEntry__Standard" ],
    [ "SgUt1TidalTableEntry_Clipped", "structSgUt1TidalTableEntry__Clipped.html", "structSgUt1TidalTableEntry__Clipped" ],
    [ "numOfRecs_Ut1cm", "SgTidalUt1_8h.html#aa491ac7424ebee5f5ed10110978cc5e7", null ],
    [ "numOfRecs_Ut1s2k", "SgTidalUt1_8h.html#a56942efd31edd81b2801e6c733084774", null ],
    [ "numOfRecs_Ut1s82", "SgTidalUt1_8h.html#a4baf8c434e6f190a89c5759af5c80c38", null ],
    [ "numOfRecs_Ut1s83", "SgTidalUt1_8h.html#a0589f4893d73fc7377dc767d6f4b95ad", null ],
    [ "ut1Ttable_Ut1cm", "SgTidalUt1_8h.html#a90ab24388eba41a1f408e08050931c4e", null ],
    [ "ut1Ttable_Ut1s2k", "SgTidalUt1_8h.html#aa17e67419a3680abcc2c44d6d2a81937", null ],
    [ "ut1Ttable_Ut1s82", "SgTidalUt1_8h.html#a76f87eeade6e88bab4472bff82f3f542", null ],
    [ "ut1Ttable_Ut1s83", "SgTidalUt1_8h.html#a82cb1a6f907760c751ef69259e101f50", null ]
];