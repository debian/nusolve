var vgosDbCalc_8cpp =
[
    [ "vdbcOptions", "structvdbcOptions.html", "structvdbcOptions" ],
    [ "checkAprioriFiles", "vgosDbCalc_8cpp.html#abf00f8abb313d99b807d2f31417bfad8", null ],
    [ "createApplication", "vgosDbCalc_8cpp.html#a1e277db66c8cb8550e8a8e95aadd39b9", null ],
    [ "createCalcExtFile", "vgosDbCalc_8cpp.html#ac42659301cb8e02a8ce21cf17a41e8af", null ],
    [ "createCalcOnnFile", "vgosDbCalc_8cpp.html#af4aee018822e70c44c803df6843ebcf0", null ],
    [ "loadSettings", "vgosDbCalc_8cpp.html#a14a7ab7317188b16a1408cc229442326", null ],
    [ "main", "vgosDbCalc_8cpp.html#a3c04138a5bfe5d72780bb7e82a18e627", null ],
    [ "origAppName", "vgosDbCalc_8cpp.html#ae5a26cc7c82b474fffb651f72a83b7fa", null ],
    [ "origDmnName", "vgosDbCalc_8cpp.html#a47944d034c8a3b75a3cbdee661d327a1", null ],
    [ "origOrgName", "vgosDbCalc_8cpp.html#a2683c1afbf4c9afd2ea595468cea145e", null ],
    [ "parse_opt", "vgosDbCalc_8cpp.html#aaf7bc24f3891f0c63a6043f4dc2ab311", null ],
    [ "saveSettings", "vgosDbCalc_8cpp.html#af3aa4a734276555216af641f7c564e40", null ],
    [ "argp_program_bug_address", "vgosDbCalc_8cpp.html#aaa037e59f26a80a8a2e35e6f2364004d", null ],
    [ "msglev", "vgosDbCalc_8cpp.html#ac4693c7c31653d6304e6b816726f4482", null ],
    [ "progname", "vgosDbCalc_8cpp.html#a32677a3383181e49438f53bb4beea107", null ],
    [ "setup", "vgosDbCalc_8cpp.html#afd0495a564c3de8108cb30f7ef65a43f", null ]
];