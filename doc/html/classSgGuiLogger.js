var classSgGuiLogger =
[
    [ "SgGuiLogger", "classSgGuiLogger.html#a263fbd068611286e4a11e209c012b453", null ],
    [ "~SgGuiLogger", "classSgGuiLogger.html#a3d0b2ef139b036c19e6375d9b6800c2a", null ],
    [ "ClassName", "classSgGuiLogger.html#a2773025fe0bb64f6101ec432b6d2e5a8", null ],
    [ "clearSpool", "classSgGuiLogger.html#adeea624b6fa59905ed4f79a02d14a143", null ],
    [ "makeOutput", "classSgGuiLogger.html#abf01d5c3d802bdbcb4f7457bbe37cd7d", null ],
    [ "dbgFormat", "classSgGuiLogger.html#a4a0085c30858307155e9129835dd609d", null ],
    [ "errFormat", "classSgGuiLogger.html#a27bc162af58f1e81dd96ec19bab3151d", null ],
    [ "infFormat", "classSgGuiLogger.html#ad3c10df81bbd9df53cc2601cae1466f4", null ],
    [ "wrnFormat", "classSgGuiLogger.html#a47db5b4f95671d4a0fbc7d87ea3e2159", null ]
];