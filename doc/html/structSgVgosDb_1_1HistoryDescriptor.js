var structSgVgosDb_1_1HistoryDescriptor =
[
    [ "HistoryDescriptor", "structSgVgosDb_1_1HistoryDescriptor.html#af12f9fcd9d2b2b09cce618da7fe870de", null ],
    [ "creator_", "structSgVgosDb_1_1HistoryDescriptor.html#a6c0d515d3d33f3d2f500811d4201f99a", null ],
    [ "defaultDir_", "structSgVgosDb_1_1HistoryDescriptor.html#a327feb2217198d8722c37b6d1eef0a46", null ],
    [ "epochOfCreation_", "structSgVgosDb_1_1HistoryDescriptor.html#affa773c00ad8da920bba318200eef3e8", null ],
    [ "historyFileName_", "structSgVgosDb_1_1HistoryDescriptor.html#ab78ef948836a812817ad4e80f29c07bf", null ],
    [ "inputWrapperFileName_", "structSgVgosDb_1_1HistoryDescriptor.html#a9a8d128ae1a584f686014b77aa57b655", null ],
    [ "isMk3Compatible_", "structSgVgosDb_1_1HistoryDescriptor.html#afd44c4221b0c5cfd357c5b3eeaf250a2", null ],
    [ "processName_", "structSgVgosDb_1_1HistoryDescriptor.html#abd708124b7809ac5f18e0eb11ec298ea", null ],
    [ "version_", "structSgVgosDb_1_1HistoryDescriptor.html#a127c6ee5e7d306cd780f74d117a389b3", null ]
];