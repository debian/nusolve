var classSgNetworkStations =
[
    [ "SgNetworkStations", "classSgNetworkStations.html#a958fbad7ab0850cbddb091e528ad4c6d", null ],
    [ "~SgNetworkStations", "classSgNetworkStations.html#a25ae49cfe614910a21f8c1b76d6fbac0", null ],
    [ "className", "classSgNetworkStations.html#aaa351d9c3af5edd43939d6baa0ab6abe", null ],
    [ "getFileName", "classSgNetworkStations.html#ae1b2e18c30361c3263f817b02ab86ce5", null ],
    [ "getPath2file", "classSgNetworkStations.html#ad65d6ec7414774c7f6506ca2665a6461", null ],
    [ "isOk", "classSgNetworkStations.html#a285d0174e7f920e0f7c90d3e326a609c", null ],
    [ "lookUp", "classSgNetworkStations.html#a412d1739ee47e1fb043ae3effde0eb61", null ],
    [ "readFile", "classSgNetworkStations.html#a822b4103d8859b7d50a47cb2a1ec2880", null ],
    [ "recsById", "classSgNetworkStations.html#a3d1a67d5f5647f50133f7544a68a5752", null ],
    [ "recsByName", "classSgNetworkStations.html#ad0ec3a95c6bead0df3a68dc4804cc0f9", null ],
    [ "setFileName", "classSgNetworkStations.html#a8fa2ea0e0e9aadf604a457c5ae98d26e", null ],
    [ "setPath2file", "classSgNetworkStations.html#a4f03b28ee9591df4c5760c1b519ad36b", null ],
    [ "fileName_", "classSgNetworkStations.html#ad9cfd40dbc015e8bdc1fa3cc77099659", null ],
    [ "isOk_", "classSgNetworkStations.html#a36d5c89ebfc532fffc97a9c5d98ff6c8", null ],
    [ "path2file_", "classSgNetworkStations.html#a7960f6f9578a349e9a33e167d747ae2a", null ],
    [ "recById_", "classSgNetworkStations.html#ad2a9d8371198040b4d470ae21afd29de", null ],
    [ "recByName_", "classSgNetworkStations.html#ad3db450a65612ebf29280d507faf55f2", null ]
];