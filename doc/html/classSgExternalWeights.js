var classSgExternalWeights =
[
    [ "SgExternalWeights", "classSgExternalWeights.html#aa39c6c0e5dde08ddfbe684d121367834", null ],
    [ "SgExternalWeights", "classSgExternalWeights.html#a2f0a9973c02eebcc8b73f9e361f17799", null ],
    [ "~SgExternalWeights", "classSgExternalWeights.html#ab1dc5327395f65b0298a2759ff2ea014", null ],
    [ "className", "classSgExternalWeights.html#ae6f33a1de001125637057f699aa410c0", null ],
    [ "getFileName", "classSgExternalWeights.html#a5153eeda860d7141db1aa649f936bbd7", null ],
    [ "getSessionName", "classSgExternalWeights.html#ad909a42a2d38b093751059bdef249b3e", null ],
    [ "isOk", "classSgExternalWeights.html#a2e509f8b88f3efd372ccd86c0595d4e1", null ],
    [ "readFile", "classSgExternalWeights.html#a02cb5d36a4adb644a8735e548255ec1b", null ],
    [ "setSessionName", "classSgExternalWeights.html#ad8ae1ef45312562c1ba4fc55733229c7", null ],
    [ "setupExternalWeights", "classSgExternalWeights.html#aea343b17e796d52a62602933bd412710", null ],
    [ "fileName_", "classSgExternalWeights.html#a64e4a03cab034d634769db09307f85f3", null ],
    [ "isOk_", "classSgExternalWeights.html#a7c735cf295bdf443da577e15725d947f", null ],
    [ "sessionName_", "classSgExternalWeights.html#a60a6a584e71c990f837435f497fb16e2", null ],
    [ "weights_", "classSgExternalWeights.html#a79753d94fb818e7c3c8f93d65258a01c", null ]
];