var classSgAPrioriRec =
[
    [ "SgAPrioriRec", "classSgAPrioriRec.html#a323e65d634a74a7d6e7eca7420d46248", null ],
    [ "~SgAPrioriRec", "classSgAPrioriRec.html#a263707cc15fb9539399f794e37e506c6", null ],
    [ "className", "classSgAPrioriRec.html#aa738ef55963d40fe1f05364f5ca0f8ea", null ],
    [ "getComments", "classSgAPrioriRec.html#a85ce8bf293e062a9f9852d7fdcda2130", null ],
    [ "getKey", "classSgAPrioriRec.html#a456ba9f3d26fdad505a3f71326a3ca4f", null ],
    [ "getTsince", "classSgAPrioriRec.html#a8a2a1b661adcc63aa15b1b9d0015b14a", null ],
    [ "setComments", "classSgAPrioriRec.html#ac0b70bb31e8b70a8c32208d82f37b8ba", null ],
    [ "setKey", "classSgAPrioriRec.html#af27706e42f33155e19970a0f1197e641", null ],
    [ "setTsince", "classSgAPrioriRec.html#aed45db90930b3eaebceacce5f77f9be4", null ],
    [ "comments_", "classSgAPrioriRec.html#ae5de494f11a4f1bfbe639fcdce1f12e2", null ],
    [ "key_", "classSgAPrioriRec.html#a32ac82b5c71a9e685c614df8eee1a0ca", null ],
    [ "tSince_", "classSgAPrioriRec.html#ae40924c12857a54858a05ee9d7c4e607", null ]
];