var classSgTidalUt1 =
[
    [ "UT1TideContentType", "classSgTidalUt1.html#a1596f8f0d03ee8e379e756bcd32155e2", [
      [ "CT_FULL", "classSgTidalUt1.html#a1596f8f0d03ee8e379e756bcd32155e2ac38abdbbc8fc3792c4f0c591339c6a3f", null ],
      [ "CT_SHORT_TERMS_REMOVED", "classSgTidalUt1.html#a1596f8f0d03ee8e379e756bcd32155e2a9d0fdbf8735eb6def95db159310aa82c", null ],
      [ "CT_ALL_TERMS_REMOVED", "classSgTidalUt1.html#a1596f8f0d03ee8e379e756bcd32155e2a12da9a51c9b5925b256c8b015a0be1c9", null ]
    ] ],
    [ "SgTidalUt1", "classSgTidalUt1.html#a340e50d72f3dc0f2b7eeefcb81222b55", null ],
    [ "~SgTidalUt1", "classSgTidalUt1.html#ab76d9fbcb2e723ca03628942ee880f31", null ],
    [ "calc", "classSgTidalUt1.html#a556592b994f350b201d183a7ffb9723a", null ],
    [ "className", "classSgTidalUt1.html#a6bd36973081f16d9ea3ad72bc8386462", null ],
    [ "calcUT1_UTC", "classSgTidalUt1.html#a890c256c3500c457c9b45cabdcb17fe3", null ],
    [ "calcVersionValue_", "classSgTidalUt1.html#a98b14e39ffb2af5070163d0bd84ec304", null ],
    [ "fundArgs", "classSgTidalUt1.html#a634f29d8ab389abab161ed3f08f0c78f", null ],
    [ "tideContent_", "classSgTidalUt1.html#a25a4367157e3f4526142e9144ddfccd3", null ]
];