var vgosDbProcLogs_8cpp =
[
    [ "vdbpOptions", "structvdbpOptions.html", "structvdbpOptions" ],
    [ "createApplication", "vgosDbProcLogs_8cpp.html#a1e277db66c8cb8550e8a8e95aadd39b9", null ],
    [ "loadSettings", "vgosDbProcLogs_8cpp.html#a14a7ab7317188b16a1408cc229442326", null ],
    [ "main", "vgosDbProcLogs_8cpp.html#a3c04138a5bfe5d72780bb7e82a18e627", null ],
    [ "origAppName", "vgosDbProcLogs_8cpp.html#a09c9c2d159d8301dd2862adda5f374fe", null ],
    [ "origDmnName", "vgosDbProcLogs_8cpp.html#a47944d034c8a3b75a3cbdee661d327a1", null ],
    [ "origOrgName", "vgosDbProcLogs_8cpp.html#a2683c1afbf4c9afd2ea595468cea145e", null ],
    [ "parse_opt", "vgosDbProcLogs_8cpp.html#aaf7bc24f3891f0c63a6043f4dc2ab311", null ],
    [ "saveSettings", "vgosDbProcLogs_8cpp.html#af3aa4a734276555216af641f7c564e40", null ],
    [ "argp_program_bug_address", "vgosDbProcLogs_8cpp.html#aaa037e59f26a80a8a2e35e6f2364004d", null ],
    [ "msglev", "vgosDbProcLogs_8cpp.html#ac4693c7c31653d6304e6b816726f4482", null ],
    [ "progname", "vgosDbProcLogs_8cpp.html#a32677a3383181e49438f53bb4beea107", null ],
    [ "setup", "vgosDbProcLogs_8cpp.html#adde155780c4b994cd8d65c6124f16ab6", null ]
];