var classSgVexFile_1_1StationSetup =
[
    [ "StationSetup", "classSgVexFile_1_1StationSetup.html#a4dc93f3bbb83055fc14702789bc239c3", null ],
    [ "StationSetup", "classSgVexFile_1_1StationSetup.html#a4a25fe8fb0b8ebc5424bca9c83b2faca", null ],
    [ "~StationSetup", "classSgVexFile_1_1StationSetup.html#a64de2c24e31ed3957041dc433d81cf0f", null ],
    [ "bbcs", "classSgVexFile_1_1StationSetup.html#ad5b973d71e6638d5462a036719860e2f", null ],
    [ "freqs", "classSgVexFile_1_1StationSetup.html#a4b3ca61aa47b77ef9d8de4bc2b5df23f", null ],
    [ "getBbcs", "classSgVexFile_1_1StationSetup.html#aa9fa785010fc97a974867c360deb0e1a", null ],
    [ "getFreqs", "classSgVexFile_1_1StationSetup.html#a7e09ddef6fc7e92bcc6df2d1cd66f6a8", null ],
    [ "getIfs", "classSgVexFile_1_1StationSetup.html#afb77897b31a8727e49451f29eff74aa3", null ],
    [ "ifs", "classSgVexFile_1_1StationSetup.html#a14b83c51357e107a400e8f63f341fce9", null ],
    [ "bbcs_", "classSgVexFile_1_1StationSetup.html#af32b274a11e3fe7bdfb3554ac56bd782", null ],
    [ "freqs_", "classSgVexFile_1_1StationSetup.html#aeebd476fca3b6d1fcb658abe5ff084e3", null ],
    [ "ifs_", "classSgVexFile_1_1StationSetup.html#ad25e7badae74e65584e408c7264786ac", null ]
];