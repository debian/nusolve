var SgIoAgv_8h =
[
    [ "SgAgvDatumDescriptor", "classSgAgvDatumDescriptor.html", "classSgAgvDatumDescriptor" ],
    [ "SgAgvDatum", "classSgAgvDatum.html", "classSgAgvDatum" ],
    [ "SgAgvDatumString", "classSgAgvDatumString.html", "classSgAgvDatumString" ],
    [ "SgAgvRecord", "classSgAgvRecord.html", "classSgAgvRecord" ],
    [ "SgAgvSection", "classSgAgvSection.html", "classSgAgvSection" ],
    [ "SgAgvFileSection", "classSgAgvFileSection.html", "classSgAgvFileSection" ],
    [ "SgAgvPreaSection", "classSgAgvPreaSection.html", "classSgAgvPreaSection" ],
    [ "SgAgvTextSection", "classSgAgvTextSection.html", "classSgAgvTextSection" ],
    [ "SgAgvTocsSection", "classSgAgvTocsSection.html", "classSgAgvTocsSection" ],
    [ "SgAgvDataSection", "classSgAgvDataSection.html", "classSgAgvDataSection" ],
    [ "SgAgvChunk", "classSgAgvChunk.html", "classSgAgvChunk" ],
    [ "AgvContentStyle", "SgIoAgv_8h.html#aecb3c527a1eafa7a89bc1441b7eacf8a", [
      [ "ACS_NONE", "SgIoAgv_8h.html#aecb3c527a1eafa7a89bc1441b7eacf8aa84691a942de1d999bc4c841637ac9f44", null ],
      [ "ACS_NATIVE", "SgIoAgv_8h.html#aecb3c527a1eafa7a89bc1441b7eacf8aa4824e25cc38363fcaa86c4227a2dd112", null ],
      [ "ACS_GVH_DBH", "SgIoAgv_8h.html#aecb3c527a1eafa7a89bc1441b7eacf8aaf8679b34aefd133fbb39c8f417929d1c", null ],
      [ "ACS_GVH_VDB", "SgIoAgv_8h.html#aecb3c527a1eafa7a89bc1441b7eacf8aa81d4f8606b65a9f8c8d7ff1b621233da", null ],
      [ "ACS_GVH_PIMA", "SgIoAgv_8h.html#aecb3c527a1eafa7a89bc1441b7eacf8aa86282152b5cd7decef8ce8f65e8e3165", null ],
      [ "ACS_GVH", "SgIoAgv_8h.html#aecb3c527a1eafa7a89bc1441b7eacf8aaf2b1aeb5b899616e9afd6f05ea72b328", null ],
      [ "ACS_ANY", "SgIoAgv_8h.html#aecb3c527a1eafa7a89bc1441b7eacf8aa27db2ef896f8d17201992fc96887d160", null ]
    ] ],
    [ "AgvDataScope", "SgIoAgv_8h.html#a115d7b895452a34272d4ee58360fc4ff", [
      [ "ADS_NONE", "SgIoAgv_8h.html#a115d7b895452a34272d4ee58360fc4ffa1732286d90a478b23ae3a4717e345129", null ],
      [ "ADS_SESSION", "SgIoAgv_8h.html#a115d7b895452a34272d4ee58360fc4ffa11a0ac829dc455cc06f0f9e4e605ea3c", null ],
      [ "ADS_SCAN", "SgIoAgv_8h.html#a115d7b895452a34272d4ee58360fc4ffa6878a8ab740453676083ac7878e629b5", null ],
      [ "ADS_STATION", "SgIoAgv_8h.html#a115d7b895452a34272d4ee58360fc4ffa7434c336ebe9ffb80127de2fbf4a39e5", null ],
      [ "ADS_BASELINE", "SgIoAgv_8h.html#a115d7b895452a34272d4ee58360fc4ffa6869334a5a440d56ade424f60278a1c0", null ]
    ] ],
    [ "AgvDataType", "SgIoAgv_8h.html#a21777ac39a992a271faab4a6650e2ad3", [
      [ "ADT_NONE", "SgIoAgv_8h.html#a21777ac39a992a271faab4a6650e2ad3a70cbb00bd69433d6210303db5ed8f430", null ],
      [ "ADT_CHAR", "SgIoAgv_8h.html#a21777ac39a992a271faab4a6650e2ad3a22839393db5668ea955b00118db2a527", null ],
      [ "ADT_I2", "SgIoAgv_8h.html#a21777ac39a992a271faab4a6650e2ad3ad94803970aff2d43b18c435cb6f032c4", null ],
      [ "ADT_I4", "SgIoAgv_8h.html#a21777ac39a992a271faab4a6650e2ad3aeccf6f6af3d25723d666d60d0b5b2dda", null ],
      [ "ADT_I8", "SgIoAgv_8h.html#a21777ac39a992a271faab4a6650e2ad3a0e8ad998888f6c1c39f5aec33fad0d43", null ],
      [ "ADT_R4", "SgIoAgv_8h.html#a21777ac39a992a271faab4a6650e2ad3a3dc1a6a681fb7c1d2a27ad62ee6fe127", null ],
      [ "ADT_R8", "SgIoAgv_8h.html#a21777ac39a992a271faab4a6650e2ad3a62d23b45ede421b54eeaa11ce71bad02", null ]
    ] ]
];