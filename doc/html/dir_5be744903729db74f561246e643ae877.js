var dir_5be744903729db74f561246e643ae877 =
[
    [ "NsBrowseNotUsedObsDialog.cpp", "NsBrowseNotUsedObsDialog_8cpp.html", null ],
    [ "NsBrowseNotUsedObsDialog.h", "NsBrowseNotUsedObsDialog_8h.html", [
      [ "NsNotUsedObsInfo", "classNsNotUsedObsInfo.html", "classNsNotUsedObsInfo" ],
      [ "NsDeselectedObsInfo", "classNsDeselectedObsInfo.html", "classNsDeselectedObsInfo" ],
      [ "NsQTreeWidgetItem", "classNsQTreeWidgetItem.html", "classNsQTreeWidgetItem" ],
      [ "NsBrowseNotUsedObsDialog", "classNsBrowseNotUsedObsDialog.html", "classNsBrowseNotUsedObsDialog" ]
    ] ],
    [ "NsBrowseNotUsedObsDialog.moc.cpp", "NsBrowseNotUsedObsDialog_8moc_8cpp.html", "NsBrowseNotUsedObsDialog_8moc_8cpp" ],
    [ "NsMainWindow.cpp", "NsMainWindow_8cpp.html", "NsMainWindow_8cpp" ],
    [ "NsMainWindow.h", "NsMainWindow_8h.html", "NsMainWindow_8h" ],
    [ "NsMainWindow.moc.cpp", "NsMainWindow_8moc_8cpp.html", "NsMainWindow_8moc_8cpp" ],
    [ "NsScrPrx4Logger.cpp", "NsScrPrx4Logger_8cpp.html", "NsScrPrx4Logger_8cpp" ],
    [ "NsScrPrx4Logger.h", "NsScrPrx4Logger_8h.html", "NsScrPrx4Logger_8h" ],
    [ "NsScrPrx4Logger.moc.cpp", "NsScrPrx4Logger_8moc_8cpp.html", "NsScrPrx4Logger_8moc_8cpp" ],
    [ "NsScrPrx4Observation.cpp", "NsScrPrx4Observation_8cpp.html", null ],
    [ "NsScrPrx4Observation.h", "NsScrPrx4Observation_8h.html", [
      [ "NsScrPrx4VlbiObservation", "classNsScrPrx4VlbiObservation.html", "classNsScrPrx4VlbiObservation" ],
      [ "NsScrPrx4VlbiAuxObservation", "classNsScrPrx4VlbiAuxObservation.html", "classNsScrPrx4VlbiAuxObservation" ]
    ] ],
    [ "NsScrPrx4Observation.moc.cpp", "NsScrPrx4Observation_8moc_8cpp.html", "NsScrPrx4Observation_8moc_8cpp" ],
    [ "NsScrPrx4ParametersDescriptor.cpp", "NsScrPrx4ParametersDescriptor_8cpp.html", "NsScrPrx4ParametersDescriptor_8cpp" ],
    [ "NsScrPrx4ParametersDescriptor.h", "NsScrPrx4ParametersDescriptor_8h.html", "NsScrPrx4ParametersDescriptor_8h" ],
    [ "NsScrPrx4ParametersDescriptor.moc.cpp", "NsScrPrx4ParametersDescriptor_8moc_8cpp.html", "NsScrPrx4ParametersDescriptor_8moc_8cpp" ],
    [ "NsScrPrx4Session.cpp", "NsScrPrx4Session_8cpp.html", null ],
    [ "NsScrPrx4Session.h", "NsScrPrx4Session_8h.html", [
      [ "NsScrPrx4Object", "classNsScrPrx4Object.html", "classNsScrPrx4Object" ],
      [ "NsScrPrx4Band", "classNsScrPrx4Band.html", "classNsScrPrx4Band" ],
      [ "NsScrPrx4Station", "classNsScrPrx4Station.html", "classNsScrPrx4Station" ],
      [ "NsScrPrx4Baseline", "classNsScrPrx4Baseline.html", "classNsScrPrx4Baseline" ],
      [ "NsScrPrx4Source", "classNsScrPrx4Source.html", "classNsScrPrx4Source" ],
      [ "NsScrPrx4Session", "classNsScrPrx4Session.html", "classNsScrPrx4Session" ]
    ] ],
    [ "NsScrPrx4Session.moc.cpp", "NsScrPrx4Session_8moc_8cpp.html", "NsScrPrx4Session_8moc_8cpp" ],
    [ "NsScrPrx4SessionHandler.cpp", "NsScrPrx4SessionHandler_8cpp.html", "NsScrPrx4SessionHandler_8cpp" ],
    [ "NsScrPrx4SessionHandler.h", "NsScrPrx4SessionHandler_8h.html", [
      [ "NsScrPrx4SessionHandler", "classNsScrPrx4SessionHandler.html", "classNsScrPrx4SessionHandler" ]
    ] ],
    [ "NsScrPrx4SessionHandler.moc.cpp", "NsScrPrx4SessionHandler_8moc_8cpp.html", "NsScrPrx4SessionHandler_8moc_8cpp" ],
    [ "NsScrPrx4Setup.cpp", "NsScrPrx4Setup_8cpp.html", null ],
    [ "NsScrPrx4Setup.h", "NsScrPrx4Setup_8h.html", [
      [ "NsScrPrx4Setup", "classNsScrPrx4Setup.html", "classNsScrPrx4Setup" ]
    ] ],
    [ "NsScrPrx4Setup.moc.cpp", "NsScrPrx4Setup_8moc_8cpp.html", "NsScrPrx4Setup_8moc_8cpp" ],
    [ "NsScrPrx4TaskConfig.cpp", "NsScrPrx4TaskConfig_8cpp.html", "NsScrPrx4TaskConfig_8cpp" ],
    [ "NsScrPrx4TaskConfig.h", "NsScrPrx4TaskConfig_8h.html", "NsScrPrx4TaskConfig_8h" ],
    [ "NsScrPrx4TaskConfig.moc.cpp", "NsScrPrx4TaskConfig_8moc_8cpp.html", "NsScrPrx4TaskConfig_8moc_8cpp" ],
    [ "NsScrSupport.cpp", "NsScrSupport_8cpp.html", "NsScrSupport_8cpp" ],
    [ "NsScrSupport.h", "NsScrSupport_8h.html", "NsScrSupport_8h" ],
    [ "NsSessionEditDialog.cpp", "NsSessionEditDialog_8cpp.html", "NsSessionEditDialog_8cpp" ],
    [ "NsSessionEditDialog.h", "NsSessionEditDialog_8h.html", [
      [ "NsSessionEditDialog", "classNsSessionEditDialog.html", "classNsSessionEditDialog" ]
    ] ],
    [ "NsSessionEditDialog.moc.cpp", "NsSessionEditDialog_8moc_8cpp.html", "NsSessionEditDialog_8moc_8cpp" ],
    [ "NsSessionHandler.cpp", "NsSessionHandler_8cpp.html", null ],
    [ "NsSessionHandler.h", "NsSessionHandler_8h.html", [
      [ "NsSessionHandler", "classNsSessionHandler.html", "classNsSessionHandler" ]
    ] ],
    [ "NsSessionNameDialog.cpp", "NsSessionNameDialog_8cpp.html", null ],
    [ "NsSessionNameDialog.h", "NsSessionNameDialog_8h.html", [
      [ "NsSessionNameDialog", "classNsSessionNameDialog.html", "classNsSessionNameDialog" ]
    ] ],
    [ "NsSessionNameDialog.moc.cpp", "NsSessionNameDialog_8moc_8cpp.html", "NsSessionNameDialog_8moc_8cpp" ],
    [ "NsSetup.cpp", "NsSetup_8cpp.html", null ],
    [ "NsSetup.h", "NsSetup_8h.html", [
      [ "NsSetup", "classNsSetup.html", "classNsSetup" ]
    ] ],
    [ "NsSetupDialog.cpp", "NsSetupDialog_8cpp.html", null ],
    [ "NsSetupDialog.h", "NsSetupDialog_8h.html", [
      [ "NsSetupDialog", "classNsSetupDialog.html", "classNsSetupDialog" ]
    ] ],
    [ "NsSetupDialog.moc.cpp", "NsSetupDialog_8moc_8cpp.html", "NsSetupDialog_8moc_8cpp" ],
    [ "NsStartupWizard.cpp", "NsStartupWizard_8cpp.html", null ],
    [ "NsStartupWizard.h", "NsStartupWizard_8h.html", [
      [ "NsWizardPage", "classNsWizardPage.html", "classNsWizardPage" ],
      [ "NsStartupWizard", "classNsStartupWizard.html", "classNsStartupWizard" ]
    ] ],
    [ "NsStartupWizard.moc.cpp", "NsStartupWizard_8moc_8cpp.html", "NsStartupWizard_8moc_8cpp" ],
    [ "NsTestDialog.cpp", "NsTestDialog_8cpp.html", null ],
    [ "NsTestDialog.h", "NsTestDialog_8h.html", [
      [ "NsTestDialog", "classNsTestDialog.html", "classNsTestDialog" ]
    ] ],
    [ "NsTestDialog.moc.cpp", "NsTestDialog_8moc_8cpp.html", "NsTestDialog_8moc_8cpp" ],
    [ "NsTestFour1Dialog.cpp", "NsTestFour1Dialog_8cpp.html", null ],
    [ "NsTestFour1Dialog.h", "NsTestFour1Dialog_8h.html", [
      [ "NsTestFour1Dialog", "classNsTestFour1Dialog.html", "classNsTestFour1Dialog" ]
    ] ],
    [ "NsTestFour1Dialog.moc.cpp", "NsTestFour1Dialog_8moc_8cpp.html", "NsTestFour1Dialog_8moc_8cpp" ],
    [ "NsVersion.cpp", "NsVersion_8cpp.html", "NsVersion_8cpp" ],
    [ "nuSolve.cpp", "nuSolve_8cpp.html", "nuSolve_8cpp" ],
    [ "nuSolve.h", "nuSolve_8h.html", "nuSolve_8h" ],
    [ "nuSolve_resources.cpp", "nuSolve__resources_8cpp.html", "nuSolve__resources_8cpp" ]
];