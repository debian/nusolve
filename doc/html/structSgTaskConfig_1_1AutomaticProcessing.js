var structSgTaskConfig_1_1AutomaticProcessing =
[
    [ "FinalSolution", "structSgTaskConfig_1_1AutomaticProcessing.html#a8c97c15ea62d8f2b9a373eb3f3fb5e2a", [
      [ "FS_BASELINE", "structSgTaskConfig_1_1AutomaticProcessing.html#a8c97c15ea62d8f2b9a373eb3f3fb5e2aa45077794221954300f6fcc6b9f64fad6", null ],
      [ "FS_UT1", "structSgTaskConfig_1_1AutomaticProcessing.html#a8c97c15ea62d8f2b9a373eb3f3fb5e2aa71e2224057e586a9aaee6c5ea9756762", null ]
    ] ],
    [ "AutomaticProcessing", "structSgTaskConfig_1_1AutomaticProcessing.html#a79444d6cc0db92779fcbaf57fc673207", null ],
    [ "AutomaticProcessing", "structSgTaskConfig_1_1AutomaticProcessing.html#a29220dd549fbe0ad3f6d35262dee1a54", null ],
    [ "~AutomaticProcessing", "structSgTaskConfig_1_1AutomaticProcessing.html#ac541c115face75a1e94a3063e9692df2", null ],
    [ "operator=", "structSgTaskConfig_1_1AutomaticProcessing.html#a6a98f69da36b6dccca2760fd40309c15", null ],
    [ "doAmbigResolution_", "structSgTaskConfig_1_1AutomaticProcessing.html#a3362f490c64434c8760538ef2d45741c", null ],
    [ "doClockBreaksDetection_", "structSgTaskConfig_1_1AutomaticProcessing.html#a5946afd09bc55e6b580b971e20b87fea", null ],
    [ "doIonoCorrection4All_", "structSgTaskConfig_1_1AutomaticProcessing.html#ade49f994c0268a5b44372b4b0ac9f0cf", null ],
    [ "doIonoCorrection4SBD_", "structSgTaskConfig_1_1AutomaticProcessing.html#ab43dc331fe65970b35ff5cf12323aecf", null ],
    [ "doOutliers_", "structSgTaskConfig_1_1AutomaticProcessing.html#adec3ba387f942eb5673a42a5f050bcf0", null ],
    [ "doReportNotUsedData_", "structSgTaskConfig_1_1AutomaticProcessing.html#afcfb1ecd970c2267de5bd2bbda761d3d", null ],
    [ "doSessionSetup_", "structSgTaskConfig_1_1AutomaticProcessing.html#a33267a4db7d374baed0425e1a2a6b799", null ],
    [ "doWeights_", "structSgTaskConfig_1_1AutomaticProcessing.html#a163ec171d9a79d3d4a26b20fd303404e", null ],
    [ "finalSolution_", "structSgTaskConfig_1_1AutomaticProcessing.html#abaa021c3faef95095e89ae9ef45071c2", null ]
];