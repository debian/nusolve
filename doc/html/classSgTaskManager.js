var classSgTaskManager =
[
    [ "SgTaskManager", "classSgTaskManager.html#a0b02e4528339f4d9b9a185374b28c647", null ],
    [ "SgTaskManager", "classSgTaskManager.html#aeffc52672f3e0368e02499c02dc475a4", null ],
    [ "~SgTaskManager", "classSgTaskManager.html#a62f3ec4176dbccea2248e762a159c1da", null ],
    [ "arcParameters", "classSgTaskManager.html#ac2c950696bf7ebbe861a9642c8891439", null ],
    [ "className", "classSgTaskManager.html#acb202ae54c72f8693af82f3a9426e444", null ],
    [ "constrainClocks", "classSgTaskManager.html#aad3aa3f6032a2072e82359634c0eeb62", null ],
    [ "constrainSourcesPositions", "classSgTaskManager.html#ac64a3e704cf634d6eec2cd2d7d1339bc", null ],
    [ "constrainSourcesTmp", "classSgTaskManager.html#aed3e596a1ede1680e306b3b2200b9cf3", null ],
    [ "constrainStcPositionsNNR", "classSgTaskManager.html#a860a736ddf1b218b806aa18da274deec", null ],
    [ "constrainStcPositionsNNT", "classSgTaskManager.html#a91251ff51a28e9b41cef39ffd62b9cd7", null ],
    [ "constrainTroposphericParameters", "classSgTaskManager.html#a538d4502f08a019207b6a023e247be5b", null ],
    [ "currentSession", "classSgTaskManager.html#ae5a69c3684ae106610596d854ed421e6", null ],
    [ "currentSessionInfo", "classSgTaskManager.html#a546f551bb74405a088bc50e3149fe523", null ],
    [ "estimator", "classSgTaskManager.html#a0c395494f1569ea39e6b2643634bcbad", null ],
    [ "fillParameterList", "classSgTaskManager.html#af99263aa7558fd844009e2f714aa72de", null ],
    [ "finisRun", "classSgTaskManager.html#af16a9003f14377ad4c84958c75b7414f", null ],
    [ "getFinisRunEpoch", "classSgTaskManager.html#ab515cc0d3e5d41443940521acd19bcfd", null ],
    [ "getIsTaskOwner", "classSgTaskManager.html#a9d33150b8de80496874c9ff82f6cf354", null ],
    [ "getNumOfConstraints", "classSgTaskManager.html#a8ebda1e5ee965513818441cb33304d2d", null ],
    [ "getNumOfParameters", "classSgTaskManager.html#a73107a9dd0c07eb6137a0566c9ef30c9", null ],
    [ "getStartRunEpoch", "classSgTaskManager.html#a7698b023abd79da01d863d0b206805c1", null ],
    [ "getTask", "classSgTaskManager.html#adc078d5c529a7b89a502cb13ae7f5826", null ],
    [ "getTFinis", "classSgTaskManager.html#a108320fcaed39f12691edb0e49de42d9", null ],
    [ "getTRefer", "classSgTaskManager.html#a293eb48f3bd9404e247a359c8cc2534c", null ],
    [ "getTStart", "classSgTaskManager.html#ac626823af80b739faac5d0b28d541cf6", null ],
    [ "globalParameters", "classSgTaskManager.html#a816c8b0d14e988c88d580d0e3f459d51", null ],
    [ "loadVlbiSession", "classSgTaskManager.html#a9e64f96a9e122353dc5c2cdbab961d9a", null ],
    [ "localParameters", "classSgTaskManager.html#ad7033d413b477aa81a64cb8d05aaa6f4", null ],
    [ "prepare4Run", "classSgTaskManager.html#ad33441b3216b44b466f36e61798c27c5", null ],
    [ "pwlParameters", "classSgTaskManager.html#aeb1524bbda0a5784490e47bba540c130", null ],
    [ "refraction", "classSgTaskManager.html#a3524302786da023384d9f395a516319f", null ],
    [ "run", "classSgTaskManager.html#a7a68b09e513404a78548503ad6a79c01", null ],
    [ "saveVlbiSession", "classSgTaskManager.html#ad944c2d5b4f09b0b6ecb27c31c98fed0", null ],
    [ "setHave2InteractWithGui", "classSgTaskManager.html#ab31ec6be8e4c4ab3f456c3479b222bdb", null ],
    [ "setIsTaskOwner", "classSgTaskManager.html#aeb981f790448a302f91a644de71f86ce", null ],
    [ "setLongOperationMessage", "classSgTaskManager.html#a61bad0ba8bcf94e3f3128ff8a8befa5a", null ],
    [ "setLongOperationProgress", "classSgTaskManager.html#ae915d7d8bd36622ae9865a5a9dc43315", null ],
    [ "setLongOperationShowStats", "classSgTaskManager.html#ac7d099295365ebc811f6d36be443b05f", null ],
    [ "setLongOperationStart", "classSgTaskManager.html#a128c2e43be46ffd0076741a8100539ca", null ],
    [ "setLongOperationStop", "classSgTaskManager.html#a6570ccb90126e5f886c8c98d39e78732", null ],
    [ "setObservations", "classSgTaskManager.html#a695a8c22f1f912bc38a504f43f35daa7", null ],
    [ "setTask", "classSgTaskManager.html#ae52a8702609dda9431b70acc5b97292f", null ],
    [ "stochasticParameters", "classSgTaskManager.html#a5561eeb36b2a13accea2dd8eb6839a4f", null ],
    [ "updateParamaterLists", "classSgTaskManager.html#a00208adc4b66ce7766943c0bc9f7364a", null ],
    [ "arcParameters_", "classSgTaskManager.html#a15423d532c310b5818582fe359f94d23", null ],
    [ "currentSession_", "classSgTaskManager.html#a8080b31bf7e5d8d02f5a009eb1d1fe37", null ],
    [ "currentSessionInfo_", "classSgTaskManager.html#a8fe863603947d8c51e4ab2d537a918db", null ],
    [ "estimator_", "classSgTaskManager.html#a5ac742518d73a83c2a20d52cdc3ecb6d", null ],
    [ "finisRunEpoch_", "classSgTaskManager.html#ad3a6efc78cc7c3652c63916f4333d4a3", null ],
    [ "globalParameters_", "classSgTaskManager.html#ab1bc0734dedd498199c9114bd462d32f", null ],
    [ "have2InteractWithGui_", "classSgTaskManager.html#adde9258b521bdf20e6a31c740cd9a1c8", null ],
    [ "isObsListOwner_", "classSgTaskManager.html#a9a764a9e6c2f4a383b4748cfbd12340a", null ],
    [ "isTaskOwner_", "classSgTaskManager.html#aad2d5f46acf3b8fa3e6bd99bd8f40f82", null ],
    [ "localParameters_", "classSgTaskManager.html#a12d3c3c35520bca03af84d1951f617c4", null ],
    [ "longOperationMessage_", "classSgTaskManager.html#a2746bfd9c44c123c3e3297ab10e8d271", null ],
    [ "longOperationProgress_", "classSgTaskManager.html#a590c333dc6400944ace80d150f921b91", null ],
    [ "longOperationShowStats_", "classSgTaskManager.html#a603a0225986f370ae6d093e5bbff7a04", null ],
    [ "longOperationStart_", "classSgTaskManager.html#a3eb6266e1128c5a395d4e72a465bbeb4", null ],
    [ "longOperationStop_", "classSgTaskManager.html#a44ced09d4ada1100aa63054e099a587e", null ],
    [ "numOfConstraints_", "classSgTaskManager.html#adbf4c848e6c647d3dc8273433ac0d64f", null ],
    [ "numOfParameters_", "classSgTaskManager.html#a6ba97b0fc1078b385752a22c0243d9b6", null ],
    [ "observations_", "classSgTaskManager.html#a0ce0a8bf77ec903fd3321286aead40a3", null ],
    [ "pwlParameters_", "classSgTaskManager.html#aa46fb10b7f24c920cf9a77dc1611904f", null ],
    [ "refraction_", "classSgTaskManager.html#ad14e3db8faad7e0d9b94b38f0322c7db", null ],
    [ "sessions_", "classSgTaskManager.html#a35a3677cb0e250284ee96e4484a06882", null ],
    [ "startRunEpoch_", "classSgTaskManager.html#a0ae9bd3f3de13db7014ffb06a116e830", null ],
    [ "stochasticParameters_", "classSgTaskManager.html#ad9bfc9f36af7b86d6343405423b59006", null ],
    [ "task_", "classSgTaskManager.html#a0a2ec3b7adc5907df7cc6096259f00c1", null ],
    [ "tFinis_", "classSgTaskManager.html#a4750c10193627d1573a028e3abd0324c", null ],
    [ "tRefer_", "classSgTaskManager.html#a059fce496a1e3d3db75e5d22b4fc8736", null ],
    [ "tStart_", "classSgTaskManager.html#a095dad72d5a31217947e70478b3f631d", null ]
];