var classSgAgvTextSection =
[
    [ "SgAgvTextSection", "classSgAgvTextSection.html#a3c03cfa4e595d93bc000b87027061bbb", null ],
    [ "SgAgvTextSection", "classSgAgvTextSection.html#a0f0d9c466db0dffc20067454fe863005", null ],
    [ "~SgAgvTextSection", "classSgAgvTextSection.html#a42de88bdcea77edf680e2c25be407e05", null ],
    [ "className", "classSgAgvTextSection.html#ab6e1dc9a00afc021df4f4d7b590ed11c", null ],
    [ "exportData", "classSgAgvTextSection.html#a88802cb757f8f1c627b8c7a3581cfbc5", null ],
    [ "fillDataStructures", "classSgAgvTextSection.html#aa164ff76e5761764f02559b55b46bc4f", null ],
    [ "history", "classSgAgvTextSection.html#a23527ae5890825554b146360bffadf1b", null ],
    [ "history", "classSgAgvTextSection.html#aae93678641bb29a423f8cab573757af8", null ],
    [ "importData", "classSgAgvTextSection.html#a9d057acbc1790ba1cb14c86029045711", null ],
    [ "parseChapterInitString", "classSgAgvTextSection.html#acf0a5d45e16c0b8ca459db10f73bf2a4", null ],
    [ "headerByIdx_", "classSgAgvTextSection.html#a3bd466d23199f75bd8b2100be27b4a0a", null ],
    [ "histEpoch_", "classSgAgvTextSection.html#a5daa771c885477720676fd75db4ac190", null ],
    [ "history_", "classSgAgvTextSection.html#a2e0e53895dae9f0979622ad1d274320c", null ],
    [ "histVer_", "classSgAgvTextSection.html#a8006bb55d65ea1f8dca71a9e0f4ad8c6", null ],
    [ "maxLenByIdx_", "classSgAgvTextSection.html#a558e3781e6fa309365b9c1adc45096d7", null ],
    [ "numByIdx_", "classSgAgvTextSection.html#ae1b1656733f5eefb906bbacedf0a8f50", null ]
];