var classSgMasterRecord =
[
    [ "SgMasterRecord", "classSgMasterRecord.html#ad910925b28e785758f406bdb03f8bfa0", null ],
    [ "~SgMasterRecord", "classSgMasterRecord.html#a8da566ecc76f9c3d9dbdcb5a6b32e366", null ],
    [ "className", "classSgMasterRecord.html#a27b0b093e7dc1ef2ed5e41c2f96fe348", null ],
    [ "findRecordByCode", "classSgMasterRecord.html#a33b65f3afd38c18141b57e5a6ec5ec09", null ],
    [ "findRecordByName", "classSgMasterRecord.html#ab1156454d21d292851433659b3a58b09", null ],
    [ "getCode", "classSgMasterRecord.html#aa71f86151ecdf4c1f9228d90c8e9c9e0", null ],
    [ "getCorrelatedBy", "classSgMasterRecord.html#a0a4c5ee360500270da3c77451e01772f", null ],
    [ "getDate", "classSgMasterRecord.html#a13d2a8b5cb67484eb2f0f66cc7cfaba6", null ],
    [ "getDbcCode", "classSgMasterRecord.html#ac8bc8398b4239f805bcb41901d505c91", null ],
    [ "getName", "classSgMasterRecord.html#a5b7faf562d59cb8ce90e59bcb16e8575", null ],
    [ "getScheduledBy", "classSgMasterRecord.html#a5400e14cabaa5d8c6babff4d65865eeb", null ],
    [ "getStations", "classSgMasterRecord.html#a5864decf5754a439f151bdcf8e8e2ded", null ],
    [ "getSubmittedBy", "classSgMasterRecord.html#acf060cd3d03ccf38cf88da5e732f2697", null ],
    [ "isValid", "classSgMasterRecord.html#a495ddf74e5ebb43038c1bf0d89e489e5", null ],
    [ "lookupRecordByCode", "classSgMasterRecord.html#a3125f23c396850ef004de98d9af56ef6", null ],
    [ "lookupRecordByName", "classSgMasterRecord.html#ad60823b218e3adf59382244d60bccaed", null ],
    [ "operator=", "classSgMasterRecord.html#aabb867dad956681226144191dc617bfc", null ],
    [ "parseString", "classSgMasterRecord.html#aad31c5b5ce096c790217d8ca302945ea", null ],
    [ "setCode", "classSgMasterRecord.html#a699f19a9ecef6830707aecd87e7c84c7", null ],
    [ "setCorrelatedBy", "classSgMasterRecord.html#a17dcbd097f88e504e792f5eafb0865ea", null ],
    [ "setDate", "classSgMasterRecord.html#ad22e5a269ca9907785a356b3f08ddee6", null ],
    [ "setDbcCode", "classSgMasterRecord.html#a43c284e4ff58fad376e5fb018a724cc5", null ],
    [ "setName", "classSgMasterRecord.html#a3bb3834dba621c06c2b993ae2ceeb152", null ],
    [ "setScheduledBy", "classSgMasterRecord.html#afa318a4302971bb781f0e307ca2d2552", null ],
    [ "setStations", "classSgMasterRecord.html#af75bacd9f5ac236d61b67dabf138c2ed", null ],
    [ "setSubmittedBy", "classSgMasterRecord.html#aecc4901cbc38b393733bed6057b933d8", null ],
    [ "code_", "classSgMasterRecord.html#a3c9eeb297ac6d29638284778303d0617", null ],
    [ "correlatedBy_", "classSgMasterRecord.html#a9be991037781ffd612b69880152439a4", null ],
    [ "date_", "classSgMasterRecord.html#a364f179ffa078137aad55bcda97aa354", null ],
    [ "dbcCode_", "classSgMasterRecord.html#a04ef8212c0b75d37b5d9b350c49bd31f", null ],
    [ "isValid_", "classSgMasterRecord.html#a5076865522220a86c749a8459077de3f", null ],
    [ "name_", "classSgMasterRecord.html#a5621f5ab3aaf5d15c9060c1b8ad2a685", null ],
    [ "scheduledBy_", "classSgMasterRecord.html#a90fd13f0f625106d13217509d7044bee", null ],
    [ "stations_", "classSgMasterRecord.html#a4eeea52136c92b0d735c255be063c8dc", null ],
    [ "submittedBy_", "classSgMasterRecord.html#acd9a606fc4aed95c1f4debbaf5af1b84", null ]
];