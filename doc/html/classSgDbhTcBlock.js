var classSgDbhTcBlock =
[
    [ "SgDbhTcBlock", "classSgDbhTcBlock.html#a1df0ee5ff4a46d499abab6c4f56f6591", null ],
    [ "~SgDbhTcBlock", "classSgDbhTcBlock.html#ac7c45bc46830015dc443df382776978b", null ],
    [ "className", "classSgDbhTcBlock.html#a9db7c75e4b2d50c5995d5e93bfa50041", null ],
    [ "descriptorByLCode", "classSgDbhTcBlock.html#a3b53eadb3c2f5c5a6d8095f8a734392a", null ],
    [ "dump", "classSgDbhTcBlock.html#a9954c378412fe36c799ff55fc9dbd1ac", null ],
    [ "isLast", "classSgDbhTcBlock.html#a249a403e0cb097a551faf986e5b3029e", null ],
    [ "isOk", "classSgDbhTcBlock.html#a63fe2f7435596219bb7e3228a107565f", null ],
    [ "listOfTeBlocks", "classSgDbhTcBlock.html#a268aab7dc14177086bd83298aeea709e", null ],
    [ "numTeBlocks", "classSgDbhTcBlock.html#a0144b18773521793ecf517c45bb1d348", null ],
    [ "operator=", "classSgDbhTcBlock.html#aeeb4e3f68478c09156a8770dd6d0c2f7", null ],
    [ "recTc", "classSgDbhTcBlock.html#ad580d7a19a6ca2c3c09296079341dabe", null ],
    [ "tocType", "classSgDbhTcBlock.html#ae916da72a3ea61bec8475a913186f8f8", null ],
    [ "operator<<", "classSgDbhTcBlock.html#a7aa160d145675abcc2464f62d5c0365e", null ],
    [ "operator>>", "classSgDbhTcBlock.html#a4a20eac0b48943814abb62b8ce0ec6e0", null ],
    [ "SgDbhFormat", "classSgDbhTcBlock.html#aff72bbbfd57b5ccfd2bff627e362778f", null ],
    [ "descriptorByLCode_", "classSgDbhTcBlock.html#a8710eaae3696f678f4d6856fb2909f3c", null ],
    [ "isOK_", "classSgDbhTcBlock.html#aac183648cb1545bd04af61b2980f4d77", null ],
    [ "listOfTeBlocks_", "classSgDbhTcBlock.html#a426e21a39fc00208b8e2be7ead0e86d9", null ],
    [ "recTc_", "classSgDbhTcBlock.html#a76fd6cb4f8743b1853b292568f39b810", null ]
];