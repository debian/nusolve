var classSgAttribute =
[
    [ "SgAttribute", "classSgAttribute.html#afc4c8b0cf3de14fceb0c9cb83c3c87bc", null ],
    [ "SgAttribute", "classSgAttribute.html#aaffa19e0b74bf3270a140ba76b115fe2", null ],
    [ "~SgAttribute", "classSgAttribute.html#ad42a247b85ff1d80290fa92b934c706d", null ],
    [ "addAttr", "classSgAttribute.html#add62ba2a1e235fdd3eedc83efac52f36", null ],
    [ "assignAttr", "classSgAttribute.html#a304e367caee1b360be97a4f3034dd1d3", null ],
    [ "className", "classSgAttribute.html#a25d168730813d577954074cdb863d6d7", null ],
    [ "clearAll", "classSgAttribute.html#ab93d2646bea7104aab82c53d63252945", null ],
    [ "delAttr", "classSgAttribute.html#ad01ffb23e74bf1ce8cb2c94b6d6996e4", null ],
    [ "getAttributes", "classSgAttribute.html#ac2ccbda22f698e11bf053e0079b00bd4", null ],
    [ "isAttr", "classSgAttribute.html#a018edfc77ef0786b6cc87685f996c0cc", null ],
    [ "operator=", "classSgAttribute.html#ab1adcc425144b37647d6c44b2dd4e049", null ],
    [ "setAttributes", "classSgAttribute.html#a34b74d4313808ad2bb6eadd6ba29e2d3", null ],
    [ "xorAttr", "classSgAttribute.html#a1624636fb0c2bf25b6639aed8b2f87c2", null ],
    [ "attributes_", "classSgAttribute.html#adbbaa996179b73048f0d76d4760dfe14", null ]
];