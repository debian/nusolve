var classSgTraklReading =
[
    [ "SgTraklReading", "classSgTraklReading.html#a8b57efe0dd7ed9de95c9c8c8234adefd", null ],
    [ "SgTraklReading", "classSgTraklReading.html#a9e4b9fcb5d2258e027e11b40ce8aa5a8", null ],
    [ "SgTraklReading", "classSgTraklReading.html#af655a759318629d7cb393b7272f665ab", null ],
    [ "~SgTraklReading", "classSgTraklReading.html#ac34aefa386cced6293f358cdd6b318d1", null ],
    [ "className", "classSgTraklReading.html#a107f607d0cbc824be193a6b658fd0faa", null ],
    [ "getAz", "classSgTraklReading.html#a76256510a1ae2a11b4305d2a27beb3fc", null ],
    [ "getAzv", "classSgTraklReading.html#a00b2cbc0581660ae8ef2d583a91a72d2", null ],
    [ "getEl", "classSgTraklReading.html#a525e0d0bb3b4e9861b4228675f7844e8", null ],
    [ "getElv", "classSgTraklReading.html#af95fc69ad3e58c6aa1d841677069238f", null ],
    [ "getT", "classSgTraklReading.html#adcebc22ddb929f907d0a38cedaaadebc", null ],
    [ "setAz", "classSgTraklReading.html#a89cd9eb4673e7ee1d130dbc45ed47b8a", null ],
    [ "setAzv", "classSgTraklReading.html#a83bd8040ed9d403079257309ec3d0331", null ],
    [ "setEl", "classSgTraklReading.html#a974be5124b24f553f2d683c42ee00093", null ],
    [ "setElv", "classSgTraklReading.html#a501793e8ee5c25e5588cb56ce67fab3f", null ],
    [ "setT", "classSgTraklReading.html#a2c580e70be9e15785d74bd9ca65f78c0", null ],
    [ "az_", "classSgTraklReading.html#a5ae46f7aef6ecd791852b2e53785da0e", null ],
    [ "azv_", "classSgTraklReading.html#a92976b146b3027363f34d7c26f0b12f4", null ],
    [ "el_", "classSgTraklReading.html#ae40a7edf9d9ad2c4f8c5ce3213dfa8ba", null ],
    [ "elv_", "classSgTraklReading.html#a92500775bbb23b4da3e6530b323fe285", null ],
    [ "t_", "classSgTraklReading.html#a298fe0486289974724283c47f6bd3972", null ]
];