var classSgDbbc3TpReading =
[
    [ "TpRecord", "classSgDbbc3TpReading_1_1TpRecord.html", "classSgDbbc3TpReading_1_1TpRecord" ],
    [ "SgDbbc3TpReading", "classSgDbbc3TpReading.html#af2185b1189e56afeca151767265e96fa", null ],
    [ "SgDbbc3TpReading", "classSgDbbc3TpReading.html#af58ea2b7fe3d2fb5ba633820ec0ea7e0", null ],
    [ "~SgDbbc3TpReading", "classSgDbbc3TpReading.html#a7cc0a44c764ee0d300cc19d8d30b677d", null ],
    [ "addRecord", "classSgDbbc3TpReading.html#a09d2e43fffbee075b9adad648f1121e2", null ],
    [ "className", "classSgDbbc3TpReading.html#a1f355ff03a9f9306ceb4673ae50ce732", null ],
    [ "getIsDataOn", "classSgDbbc3TpReading.html#a3f43f89168f501ec9babbc3417bf9c52", null ],
    [ "getIsOnSource", "classSgDbbc3TpReading.html#a7c2482bb780eb92c791de50d25fde862", null ],
    [ "getOsRec", "classSgDbbc3TpReading.html#a69a3d87e717fd5400c42ec70bc39b089", null ],
    [ "getT", "classSgDbbc3TpReading.html#a7d76756e6193928612113af9b2aa52bd", null ],
    [ "getTpBySensor", "classSgDbbc3TpReading.html#acaf9f6df201aa189b852a59b350c009b", null ],
    [ "osRec", "classSgDbbc3TpReading.html#aa9c98fe5e3f2871d8943f43926c2fba3", null ],
    [ "setIsDataOn", "classSgDbbc3TpReading.html#ad371dae23acd7be636fcf33489010c74", null ],
    [ "setIsOnSource", "classSgDbbc3TpReading.html#a8dc0d0b33ebca65d3dd6be4a60c173b1", null ],
    [ "setOsRec", "classSgDbbc3TpReading.html#a4f6cbfca6acf8fde3de9c19453b8cee8", null ],
    [ "setT", "classSgDbbc3TpReading.html#a927e13df3c123d139c4811f6ee2e65c9", null ],
    [ "tpBySensor", "classSgDbbc3TpReading.html#a33ac34a3a9278a163d4951cff5cc1541", null ],
    [ "isDataOn_", "classSgDbbc3TpReading.html#ab3c3173526950d380aa0b7bbca2b0535", null ],
    [ "isOnSource_", "classSgDbbc3TpReading.html#a25e6b1b8c016e9da10d466a7949744ec", null ],
    [ "osRec_", "classSgDbbc3TpReading.html#acc00136184088936466f9eee7d93296d", null ],
    [ "t_", "classSgDbbc3TpReading.html#a0eb3d539376d88d6f88e49a65e7f820d", null ],
    [ "tpBySensor_", "classSgDbbc3TpReading.html#adb7b6a2e47d7d58af4c8180540f77b35", null ]
];