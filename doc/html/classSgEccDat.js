var classSgEccDat =
[
    [ "SgEccDat", "classSgEccDat.html#ac0a6969672ff41f61f706cdd56a39524", null ],
    [ "~SgEccDat", "classSgEccDat.html#a122662c086fa8b9a7d1d6923ca3aa947", null ],
    [ "className", "classSgEccDat.html#a265308e94d54d6089f5d958be3f6d3a5", null ],
    [ "getFileName", "classSgEccDat.html#a4e2b752a16c3a948176bb933abee56ac", null ],
    [ "getPath2File", "classSgEccDat.html#a276bc6c8663ae2b3159e35f25d660246", null ],
    [ "importEccFile", "classSgEccDat.html#addae0c0874e73cca2ee491cd34de1363", null ],
    [ "lookupRecord", "classSgEccDat.html#a1c06b6f9cfeb34aa01ae321906a752f7", null ],
    [ "setFileName", "classSgEccDat.html#a8b093721214b8bcacbae115555bcd252", null ],
    [ "setPath2File", "classSgEccDat.html#a71a3b20fc84b75d6937b32f64fdb93da", null ],
    [ "fileName_", "classSgEccDat.html#ad712eaf3abd88d044bbaea588f157a8c", null ],
    [ "path2File_", "classSgEccDat.html#a26a15b293535876e3bb49d79606e8084", null ],
    [ "siteByName_", "classSgEccDat.html#ad36f46b14ed588e2e612abc823417ec0", null ]
];