var structvdbmOptions =
[
    [ "altCorrelatorName", "structvdbmOptions.html#a42e3b62981f482fdab632c1a4b69c35e", null ],
    [ "altDatabaseName", "structvdbmOptions.html#a42e255dfcf16a270889aa9f216491692", null ],
    [ "altExpSerialNumber", "structvdbmOptions.html#ad9644e73abaa2b165e3715c58234a585", null ],
    [ "altOutputDir", "structvdbmOptions.html#aaae6b827fe0e43b42c8a3e4229eadf39", null ],
    [ "altSetupAppName", "structvdbmOptions.html#abd67dbb4f8e2a90ae6d8addd449232a3", null ],
    [ "altSetupName", "structvdbmOptions.html#ae3f520eed5bd37054c4d072cbac57d32", null ],
    [ "correlatorReportFileName", "structvdbmOptions.html#ad3eda242eeddec0c0c3d2116f3148300", null ],
    [ "fringeErrorCodes2Skip", "structvdbmOptions.html#abf4c40ae2f6f669ea48cac903ca0060e", null ],
    [ "have2ForceWizard", "structvdbmOptions.html#ab472f67dc247de3170aa7cb196e7aeb2", null ],
    [ "have2SaveAltOutputDir", "structvdbmOptions.html#a6c2bf4f10bfccd36a53db77ecc6267b4", null ],
    [ "have2UseAltSetup", "structvdbmOptions.html#a858ad3f4c9ba24c924e885d990a9c45d", null ],
    [ "inputArg", "structvdbmOptions.html#a5a48e09f24eb4006938de24b4e645b8e", null ],
    [ "isDryRun", "structvdbmOptions.html#a09fec310e5ef889ebb4b1bf411ed10bd", null ],
    [ "mapFileName", "structvdbmOptions.html#aaf6e7113d31e37e60b0a14c8b78b3805", null ],
    [ "need2correctRefClocks", "structvdbmOptions.html#a2b2cb2e1d2fee0bd5ec7c6f906f44114", null ],
    [ "settings", "structvdbmOptions.html#aa86bd7811d282fb0ba710470c5f1f3cb", null ],
    [ "shouldInvokeSystemWideWizard", "structvdbmOptions.html#a50c9e982e18991510a7739a46b741dbc", null ],
    [ "useStdLocale", "structvdbmOptions.html#af2637bc01af8a8e7dec44ef154d20c21", null ]
];