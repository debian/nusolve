var classSgVlbiBaselineInfo =
[
    [ "Attributes", "classSgVlbiBaselineInfo.html#a0db5bdc667c34dbd52e3a89f8aa3f108", [
      [ "Attr_NOT_VALID", "classSgVlbiBaselineInfo.html#a0db5bdc667c34dbd52e3a89f8aa3f108a8ec1a3f023fd0b1e1627f211bc301ffa", null ],
      [ "Attr_ESTIMATE_CLOCKS", "classSgVlbiBaselineInfo.html#a0db5bdc667c34dbd52e3a89f8aa3f108a5b88563ddc4595677b355d1c19d65ef1", null ],
      [ "Attr_BIND_TROPOSPHERE", "classSgVlbiBaselineInfo.html#a0db5bdc667c34dbd52e3a89f8aa3f108ac9afa4d39311036a54f78b5d2ceedb08", null ],
      [ "Attr_USE_IONO4GRD", "classSgVlbiBaselineInfo.html#a0db5bdc667c34dbd52e3a89f8aa3f108a28fce28b8dc65578a1eaf68dcb422122", null ],
      [ "Attr_USE_IONO4PHD", "classSgVlbiBaselineInfo.html#a0db5bdc667c34dbd52e3a89f8aa3f108a37746c99464b7cda1b2d6bc64a626be7", null ],
      [ "Attr_SKIP_WC", "classSgVlbiBaselineInfo.html#a0db5bdc667c34dbd52e3a89f8aa3f108aaa75baefc4b8af4d63d5e99b2b5443af", null ]
    ] ],
    [ "SgVlbiBaselineInfo", "classSgVlbiBaselineInfo.html#adba2209e673f2d14b9b4577c2c289fd3", null ],
    [ "~SgVlbiBaselineInfo", "classSgVlbiBaselineInfo.html#ad03a20b5d3bec09d2c60f10e1a94faa9", null ],
    [ "calculateClockF1", "classSgVlbiBaselineInfo.html#a8890982ed427aea7eaf773f47087321c", null ],
    [ "cbIndicator", "classSgVlbiBaselineInfo.html#aa49b363864281d8de14f38cc6bd8265e", null ],
    [ "className", "classSgVlbiBaselineInfo.html#a8dc5a3ee7215b62e47349382f18aca29", null ],
    [ "createParameters", "classSgVlbiBaselineInfo.html#ab6d9f85d9d3ef2e75b5671aa8aacbd92", null ],
    [ "dClock", "classSgVlbiBaselineInfo.html#afbcc891b2b37ba2e65411226596eb9a4", null ],
    [ "dClockSigma", "classSgVlbiBaselineInfo.html#a55e923cc5301c625ea1414fb1cbe8199", null ],
    [ "evaluateCBIndicator", "classSgVlbiBaselineInfo.html#a25f8d00e024e5c40ace478d7aace7d9c", null ],
    [ "evaluateMeanGrDelResiduals", "classSgVlbiBaselineInfo.html#a6ce13342f159456a4783f50ca66ea339", null ],
    [ "getAuxSign", "classSgVlbiBaselineInfo.html#ac9f4fcb1f30ec12b0a13682c75804daa", null ],
    [ "getCbdTotalWrms", "classSgVlbiBaselineInfo.html#aa017fe24df188ba34c222cc5e2739944", null ],
    [ "getLength", "classSgVlbiBaselineInfo.html#acad51c2fe200d1c6044d30a67e41f3c6", null ],
    [ "getMeanGrDelResiduals", "classSgVlbiBaselineInfo.html#a56e1c73cdaf8a99e6105994f08e72da7", null ],
    [ "getMeanGrDelResidualsSigma", "classSgVlbiBaselineInfo.html#ab4dbbc9ef190236a1ab7293920e2ca9e", null ],
    [ "getTypicalAmbigSpacing", "classSgVlbiBaselineInfo.html#aa0a7a5be5ac219223f218c34e6d050d0", null ],
    [ "getTypicalGrdAmbigSpacing", "classSgVlbiBaselineInfo.html#affc9daf30a16006aa9800b47fc0c9f22", null ],
    [ "getTypicalNumOfChannels", "classSgVlbiBaselineInfo.html#a23e1b617e0cc89e2c6684317c7c72d26", null ],
    [ "getTypicalPhdAmbigSpacing", "classSgVlbiBaselineInfo.html#a9d6c7f0de4ffb536506419a59e0762bc", null ],
    [ "loadIntermediateResults", "classSgVlbiBaselineInfo.html#a85b04c62d395420bab644582f5b56a6c", null ],
    [ "numOfChanByCount", "classSgVlbiBaselineInfo.html#ab51c812608fc8a87be7c9b986d13019f", null ],
    [ "observables", "classSgVlbiBaselineInfo.html#a7a5d8fb8590df2330a0c0534f5efafc6", null ],
    [ "pAux", "classSgVlbiBaselineInfo.html#a33bf0ffa3de02bc71166b1f770eed527", null ],
    [ "pBx", "classSgVlbiBaselineInfo.html#afbddd135ada580b9e352926f5ccfc822", null ],
    [ "pBy", "classSgVlbiBaselineInfo.html#ab17d8e7401890348c79f4bbf1ae77612", null ],
    [ "pBz", "classSgVlbiBaselineInfo.html#a9c0c7620c8d891941657fbf0931f6ae1", null ],
    [ "pClock", "classSgVlbiBaselineInfo.html#a85f68953079a3b4ef16ee9502428ff0d", null ],
    [ "releaseParameters", "classSgVlbiBaselineInfo.html#a575477e255b0fe110193b4250c89d139", null ],
    [ "resetAllEditings", "classSgVlbiBaselineInfo.html#a095fd6bf6bcd53b981f6f4d524c79eb3", null ],
    [ "saveIntermediateResults", "classSgVlbiBaselineInfo.html#a93aec2627b8ea393ebc3f44363f5ea15", null ],
    [ "scan4Ambiguities", "classSgVlbiBaselineInfo.html#a3b532f90329104ff3734d626955553a1", null ],
    [ "scan4Ambiguities_m1", "classSgVlbiBaselineInfo.html#a6c9cab5f2aa876959eba00fa0b70f0e2", null ],
    [ "scan4Ambiguities_m2", "classSgVlbiBaselineInfo.html#ace50739ba2ef76fed1ef6c2cbab4fd7d", null ],
    [ "selfCheck", "classSgVlbiBaselineInfo.html#ae429f539805212e63b9112bcdfd2a460", null ],
    [ "setAuxSign", "classSgVlbiBaselineInfo.html#aba755e288c41f7da297d7329b6f7837f", null ],
    [ "setGrdAmbiguities2min", "classSgVlbiBaselineInfo.html#af8c63d425718c96f4376be12383e6dd7", null ],
    [ "setLength", "classSgVlbiBaselineInfo.html#a069f3de33907ee9ec4d4141f30817305", null ],
    [ "shiftAmbiguities", "classSgVlbiBaselineInfo.html#af4e5bb6e40abcd779cc8ea3bf3886256", null ],
    [ "stn_1", "classSgVlbiBaselineInfo.html#a485f4a97b7e2830101ff90cd63968469", null ],
    [ "stn_2", "classSgVlbiBaselineInfo.html#a0a079116db2cb642afeb6d756334a876", null ],
    [ "strGrdAmbigsStat", "classSgVlbiBaselineInfo.html#af0d6789e3293c3bc50f869e6700834a9", null ],
    [ "strPhdAmbigsStat", "classSgVlbiBaselineInfo.html#a762f6e4e9da8e3242a096e3c1e7ef976", null ],
    [ "auxSign_", "classSgVlbiBaselineInfo.html#ac08b52f79d1a0a3a8234d9af66d277ea", null ],
    [ "cbd_total_wrms_", "classSgVlbiBaselineInfo.html#a0cfe60ef801fd032cb8ae3a6cea4eefe", null ],
    [ "cbIndicator_", "classSgVlbiBaselineInfo.html#a4db21789ec809338b8b91726177150d0", null ],
    [ "dClock_", "classSgVlbiBaselineInfo.html#a26b40c6796e1a620bad68e2642c991b0", null ],
    [ "dClockSigma_", "classSgVlbiBaselineInfo.html#ac60c0b4162c76a7639fea17ef7d3665d", null ],
    [ "grdAmbigsBySpacing_", "classSgVlbiBaselineInfo.html#a42cf5cf53c5da4ae7ce9fe0223779700", null ],
    [ "length_", "classSgVlbiBaselineInfo.html#ac37b0ae490e001e05f3e80f14565c4a0", null ],
    [ "meanGrDelResiduals_", "classSgVlbiBaselineInfo.html#a9deed8362b68ba0ee2a9db5efcb885de", null ],
    [ "meanGrDelResidualsSigma_", "classSgVlbiBaselineInfo.html#a1132d981612c34af21607033cef4d08c", null ],
    [ "numOfChanByCount_", "classSgVlbiBaselineInfo.html#a26667bf9ba768ab7bac089d24d5cf6a9", null ],
    [ "observables_", "classSgVlbiBaselineInfo.html#a0f53b72cad759a0b59aae41e1db7505e", null ],
    [ "pAux_", "classSgVlbiBaselineInfo.html#adad373efef37cea3303e6f4ce513a414", null ],
    [ "pBx_", "classSgVlbiBaselineInfo.html#a7ed2628e12eba7b842db3049c3d83f3b", null ],
    [ "pBy_", "classSgVlbiBaselineInfo.html#a9cf3afd2bb49a41e4d1065b9afdc4761", null ],
    [ "pBz_", "classSgVlbiBaselineInfo.html#aa7b698956564ee6159bca3e85eab4131", null ],
    [ "pClock_", "classSgVlbiBaselineInfo.html#ab35c3ecd83f963018b16bbab6470bdc9", null ],
    [ "phdAmbigsBySpacing_", "classSgVlbiBaselineInfo.html#ad48752ad1851e78987cce6026004b25c", null ],
    [ "strGrdAmbigsStat_", "classSgVlbiBaselineInfo.html#aae994f0f3864d17c37b599576c243eba", null ],
    [ "strPhdAmbigsStat_", "classSgVlbiBaselineInfo.html#a8d652a87b31bdf4482936e36389fa190", null ],
    [ "typicalGrdAmbigSpacing_", "classSgVlbiBaselineInfo.html#a2617bbd94c73e0e7040be694181a0eee", null ],
    [ "typicalNumOfCannels_", "classSgVlbiBaselineInfo.html#a14d27d4871fe2d8e4f5bda11ee1565fb", null ],
    [ "typicalPhdAmbigSpacing_", "classSgVlbiBaselineInfo.html#a43473a61f52500997842e70512210025", null ]
];