var classSgGuiLoggerConfig =
[
    [ "SgGuiLoggerConfig", "classSgGuiLoggerConfig.html#a1cf51ed5b436d5cc8d6823e9edbc8a4d", null ],
    [ "~SgGuiLoggerConfig", "classSgGuiLoggerConfig.html#ade11c2585c3ada36bc8d94e9f3f5904b", null ],
    [ "acquireData", "classSgGuiLoggerConfig.html#aea7b2397f96e0126d2fd5d05d69829eb", null ],
    [ "className", "classSgGuiLoggerConfig.html#abcf71adc599513bf7af1422824e37da8", null ],
    [ "cbFullDate_", "classSgGuiLoggerConfig.html#a8df2a13cc7866033050d02076e4dd85f", null ],
    [ "cbLLevel_", "classSgGuiLoggerConfig.html#ae7a04427be6186725412146c25597040", null ],
    [ "cbLogStoreInFile_", "classSgGuiLoggerConfig.html#a755d84228e5d7797485e9d1940a25aa6", null ],
    [ "cbLogTimeLabel_", "classSgGuiLoggerConfig.html#a85d63a7a4e8c8954febbf4c043018dc0", null ],
    [ "eLogFileName_", "classSgGuiLoggerConfig.html#a476b1ab207381235cd9694cc55207e45", null ],
    [ "sLogCapacity_", "classSgGuiLoggerConfig.html#af7321ba8c0f3354a42f977ed2755d1fc", null ]
];