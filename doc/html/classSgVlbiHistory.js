var classSgVlbiHistory =
[
    [ "SgVlbiHistory", "classSgVlbiHistory.html#a03922caecb1bb2fed038e4403c347e6f", null ],
    [ "~SgVlbiHistory", "classSgVlbiHistory.html#a7eb99d059f1adf3566a950b82d4af4df", null ],
    [ "addHistoryRecord", "classSgVlbiHistory.html#acc438f380cb638e34d09a772da5ec2fb", null ],
    [ "className", "classSgVlbiHistory.html#a067e422e461a6fc9fa79f79bb4efc91f", null ],
    [ "export2DbhHistoryBlock", "classSgVlbiHistory.html#ad22c453ef6bd1c23334ae33320526289", null ],
    [ "getFirstRecordFromUser", "classSgVlbiHistory.html#a72d4115eb92b9d943405f9a118d7272f", null ],
    [ "importDbhHistoryBlock", "classSgVlbiHistory.html#a97a8e2915f2a33dbc644e31714424fa2", null ],
    [ "setFirstRecordFromUser", "classSgVlbiHistory.html#ad9adc824a75b0e1986fd77f212b57e28", null ],
    [ "firstRecordFromUser_", "classSgVlbiHistory.html#a83700fd771746abefe6f05897c831803", null ]
];