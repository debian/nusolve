var classSgBreakModel =
[
    [ "SgBreakModel", "classSgBreakModel.html#a4435ac03bb2d6f54d20671b5bb1194cd", null ],
    [ "~SgBreakModel", "classSgBreakModel.html#ae995500f779617124a8d9c3ee71d111c", null ],
    [ "addBreak", "classSgBreakModel.html#a4da2b5608e61a33c37fc8d01821a04b9", null ],
    [ "addBreak", "classSgBreakModel.html#a8e02a07c982b58199192b9707fc4535f", null ],
    [ "className", "classSgBreakModel.html#a88d1414877792512ec43c4ca3961c5a6", null ],
    [ "createParameters", "classSgBreakModel.html#a2bee4b7cf8065c25743fe2e53cba5448", null ],
    [ "delBreak", "classSgBreakModel.html#a28788bbafb582a5718c69840f5c81219", null ],
    [ "delBreak", "classSgBreakModel.html#ac0cd47643f2b1bae0b12161f628c3d37", null ],
    [ "getT0", "classSgBreakModel.html#a79c74695a612c69d32439f30f7bff6cf", null ],
    [ "loadIntermediateResults", "classSgBreakModel.html#a62349824bf86617bbe7ad043311a20da", null ],
    [ "operator=", "classSgBreakModel.html#a0941086153bb6595a4bf5e37cc338ede", null ],
    [ "propagatePartials", "classSgBreakModel.html#ab2d9440deb9c5aebb5ce5c528c1c3b16", null ],
    [ "propagatePartials4rates", "classSgBreakModel.html#a969541b9fba1599dc981af6f55f71fed", null ],
    [ "rate", "classSgBreakModel.html#a2d5f4ed172a660b473896937fc4a831f", null ],
    [ "releaseParameters", "classSgBreakModel.html#aba5fe8ee9aca82b3c4e2156ed2d864d1", null ],
    [ "resetAllEditings", "classSgBreakModel.html#ad2053f88ed663d61d202761ea7307a4b", null ],
    [ "saveIntermediateResults", "classSgBreakModel.html#afb13ccfd20351e789f21ab10f8d2f914", null ],
    [ "setT0", "classSgBreakModel.html#a0453fdb3e962e66b13b85ebbe56ac443", null ],
    [ "sortEvents", "classSgBreakModel.html#aba80ac8ed9d570f143de7de845e6163a", null ],
    [ "value", "classSgBreakModel.html#a26c7a291d6965b953e52065f4cf32ae6", null ],
    [ "t0_", "classSgBreakModel.html#a5f0c74f62e413eaef4f5486b9d3be9d7", null ]
];