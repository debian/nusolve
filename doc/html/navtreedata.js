/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "General Purpose Geodetic Library", "index.html", [
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", "functions_eval" ],
        [ "Properties", "functions_prop.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", "globals_vars" ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"NsBrowseNotUsedObsDialog_8cpp.html",
"NsScrPrx4TaskConfig_8h.html#a390c194bc3385b1cd568d0871df64c3c",
"SgDbhFormat_8cpp.html#af789efe855d149217366581c2fafd573",
"SgIdentities_8h_source.html",
"SgIoAgvDriverDds_8cpp.html#a9ee3659242a8ceb05ab863b0609b14bd",
"SgIoAgvDriver_8h.html#a3b79574a1889bb3253e8256a6a6e3e9e",
"SgIoAgvDriver_8h.html#ae760f1fe8468a9aad00858f271aad258",
"SgOceanLoad_8h.html",
"SgVgosDbLoadObs_8cpp.html#a30bf690bca924e6680934a688821c7d5",
"SgVgosDbLoadSession_8cpp.html#a3e3633c84b076eae446e1c594926a7e0",
"SgVgosDb_8h.html#a16ce71827b0591350078eceac41d1d0e",
"SgVgosDb_8h.html#aa5391ddf5e68db439b6774e236a519c2",
"SgVlbiSessionIoDbh_8cpp.html#a080ce7391e2ff18a0067e54b1e6eef2c",
"SgVlbiSessionIoDbh_8cpp.html#a87b5ced14f14ca39add3da53d10c9cec",
"SgVlbiSessionIoDbh_8cpp.html#afa8fa57df3fdb3509e5015c4cf0d1f0f",
"classNsMainWindow.html#a2b119d00bcbc972c049082b9f9623d03",
"classNsScrPrx4ParametersDescriptor.html#ad57fd7421d7f8451167ed5e49e22b346",
"classNsScrPrx4Source.html#a86b8ee5d0b30a47bd3edb3444eaa6736",
"classNsScrPrx4VlbiObservation.html#a80e06a1ad2b323e971c95ada20d8dea6",
"classNsSetup.html#a3393989d22a93f8f1d937b9cc7cd6a3c",
"classSg3dMatrix.html#aeb0b076e2ad26066f43582dc9e2514fa",
"classSgAgvDriver.html#a0e5c4d93bb3b97ca77df5666622aeb93",
"classSgAgvDriver.html#a8a4120711c027ded23b27186de4b262d",
"classSgAgvPreaSection.html#aadebccdf5d7a7b93a1ebcdf69ced943f",
"classSgChannelSkeded_1_1StnCfg.html#a60c33dd1de9a49c2383d281fbdf3ebe6",
"classSgDbhImage.html#aabf55c50807103e70699549e6a4c6b6b",
"classSgDbhTeBlock.html#a57a7dc4b6b8bbb8b4e9f45554751b99a",
"classSgExternalEopFile.html#a39110ac9969fbfaeb9912f1ae0529bbd",
"classSgGuiTaskConfig.html#a7fd2e0912cc76392c78cb6e79f201234",
"classSgGuiVlbiSrcStrModelEditor.html#a854d50348eeadcb37e6931092af353b0",
"classSgKombBd02Record.html#a1ceb3133d0ef593126f9208bbae6867b",
"classSgKombOb03Record.html#adc406eaab51c17ab646d76c5b4595fdb",
"classSgMasterRecord.html#aa71f86151ecdf4c1f9228d90c8e9c9e0",
"classSgNcdfAttribute.html#afdca24fcfdfec4385b1a02a87a794160",
"classSgOceanLoad.html",
"classSgParametersDescriptor.html#a2857b34d635502dd61c917cc49f3e4e5",
"classSgPlot.html#a8133dff69005a25e61a0517e4965abea",
"classSgPlotArea.html#afc138398c4df350308ff989843220a34",
"classSgSolutionReporter.html#a131068574a4639e018dac91d1f8e9d3f",
"classSgStnLogReadings.html#a6dae814864df0005093a34f9245d4b36",
"classSgTaskConfig.html#a91e4686b0bec45f090c814910009adf2",
"classSgTsysReading.html#a7e377b2f81dc7b98eae0a456f0e63490",
"classSgVexFile_1_1BbcSetup.html#ae94937d3d14079a448c19ce6314b1bb5",
"classSgVgosDb.html#a49ffdac9a99c11e00bd4eceada931224",
"classSgVgosDb.html#af4b5e5174d915c799a84ff501ab2f3da",
"classSgVlbiBand.html#a1f428022336a5c1a9eaf035e89bfb974",
"classSgVlbiNetworkId.html#a2d1ae29de3a0d9215eb96fce09493ff0",
"classSgVlbiObservable.html#acbce592c669358d71302e9eee1780de5",
"classSgVlbiObservation.html#a60f631251109256410e1b1c1ddb223a0",
"classSgVlbiObservation.html#ae175ba95c1b7f93812c9d5bebe831ab5",
"classSgVlbiSession.html#a98e2ebdc4b5cc71c1fa4946603cb9e14",
"classSgVlbiSourceInfo.html#a78d86604c1bd4b32c7a7a80cb1183279",
"classSgVlbiStationInfo.html#abbcfb26c3faa04c4564043f42841fcdf",
"classVcCalc2SessionIfc.html#a912d722f6383bea656a5aca6c39dacf6",
"classVmStartupWizard.html#aaed78227bead1c53f49be28569888644",
"functions_vars_u.html",
"structSgVgosDb_1_1BandData.html#a0005bfc05b03e8fff6a441ce6300f731",
"structqt__meta__stringdata__SgGuiVlbiStationList__t.html#a3d208b12cb52889e6ce3ac6e5d60953f",
"vgosDbProcLogs_8h_source.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';