var classSgModelEop__JMG__96__hf =
[
    [ "HfEopRec", "structSgModelEop__JMG__96__hf_1_1HfEopRec.html", "structSgModelEop__JMG__96__hf_1_1HfEopRec" ],
    [ "SgModelEop_JMG_96_hf", "classSgModelEop__JMG__96__hf.html#a5a4332fe94591892130bc383ba3983e4", null ],
    [ "~SgModelEop_JMG_96_hf", "classSgModelEop__JMG__96__hf.html#a349cffef2d68f27e25f5c63009b4febc", null ],
    [ "calcCorrections", "classSgModelEop__JMG__96__hf.html#addb64e00eef498d74af2b23c7090d49c", null ],
    [ "className", "classSgModelEop__JMG__96__hf.html#af18e5bcb975630829a1c98f7113a11c5", null ],
    [ "getFileName", "classSgModelEop__JMG__96__hf.html#abdd783f59ce93c19fcdb307f92e713a5", null ],
    [ "isOk", "classSgModelEop__JMG__96__hf.html#af63b2790a1283753bb604e42848b64b7", null ],
    [ "readFile", "classSgModelEop__JMG__96__hf.html#a276611200480945f9fc61acbd7590c81", null ],
    [ "setFileName", "classSgModelEop__JMG__96__hf.html#aa4c2acc5300b87412f609435b8181268", null ],
    [ "baseModel_", "classSgModelEop__JMG__96__hf.html#a624a09983697dacbdee5c4a82b10371c", null ],
    [ "fileName_", "classSgModelEop__JMG__96__hf.html#a1c756400e28b07087581bf6311133bcd", null ],
    [ "isOk_", "classSgModelEop__JMG__96__hf.html#a4da63dc82c958244ab6d99a2c5eb8993", null ],
    [ "numPm_", "classSgModelEop__JMG__96__hf.html#a98df2432b18fe04c162f4fbe627493e0", null ],
    [ "numUt_", "classSgModelEop__JMG__96__hf.html#ad7316ca6b1199fb41f2fc8bbe6571bbc", null ],
    [ "pmModel_", "classSgModelEop__JMG__96__hf.html#a580a8e315aa3ee383b22d0ce2d369c7e", null ],
    [ "utModel_", "classSgModelEop__JMG__96__hf.html#a9da3393e1c5fb7eb366d40f1358f22eb", null ]
];