var classNsScrPrx4VlbiAuxObservation =
[
    [ "NsScrPrx4VlbiAuxObservation", "classNsScrPrx4VlbiAuxObservation.html#a56a599f546aba97a36077a1d731fe745", null ],
    [ "~NsScrPrx4VlbiAuxObservation", "classNsScrPrx4VlbiAuxObservation.html#a37bbcbf3ee95a72341e93d65296c75a7", null ],
    [ "getAtmHumidity", "classNsScrPrx4VlbiAuxObservation.html#a5d9dd54e66a51f1a499ce6a074432e4c", null ],
    [ "getAtmPressure", "classNsScrPrx4VlbiAuxObservation.html#a1fdbbb5e0ec110b75448de7ae8998e03", null ],
    [ "getAtmTemperature", "classNsScrPrx4VlbiAuxObservation.html#ab76346558abe3fcb34226555208b5a32", null ],
    [ "getCableCalibration", "classNsScrPrx4VlbiAuxObservation.html#aec5f1debdab442413b254169f424fa3b", null ],
    [ "getEpoch", "classNsScrPrx4VlbiAuxObservation.html#ab9e6416a51689f7c5ada953046ce9a6d", null ],
    [ "getStation", "classNsScrPrx4VlbiAuxObservation.html#a980d40fa675d6a0816ff44c1ab8bb57d", null ],
    [ "isProcessed", "classNsScrPrx4VlbiAuxObservation.html#a8c75b0e99dc9fe9810d5ad9ab7405ed3", null ],
    [ "isValid", "classNsScrPrx4VlbiAuxObservation.html#a13fe3e0cbe8229a505c71c8d73b3f46a", null ],
    [ "aux_", "classNsScrPrx4VlbiAuxObservation.html#a8ee29fe670d57295f773d00b8bf4fd15", null ],
    [ "station_", "classNsScrPrx4VlbiAuxObservation.html#a2c1f3efaa7b983f4c2e58b10588b5974", null ],
    [ "atmHumidity", "classNsScrPrx4VlbiAuxObservation.html#a631ac906954b7a3a1c5f2020b217c075", null ],
    [ "atmPressure", "classNsScrPrx4VlbiAuxObservation.html#abed2f8a094c6595a61b70d3afdba2b24", null ],
    [ "atmTemperature", "classNsScrPrx4VlbiAuxObservation.html#a62b894a2e7e5706f8fa1c7accf05cba5", null ],
    [ "cableCalibration", "classNsScrPrx4VlbiAuxObservation.html#a62d09f31aa5c6a4a0901f0dcc23b624f", null ],
    [ "epoch", "classNsScrPrx4VlbiAuxObservation.html#a23fff75d0d466206d4231ed9dffd1d1e", null ],
    [ "isValid", "classNsScrPrx4VlbiAuxObservation.html#a6b6434748a31e0f0aa3b2761f9a6b700", null ],
    [ "station", "classNsScrPrx4VlbiAuxObservation.html#a2327ae9f28af5d7fbeaef80c132e2e2e", null ]
];