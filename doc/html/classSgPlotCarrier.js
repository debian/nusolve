var classSgPlotCarrier =
[
    [ "AxisType", "classSgPlotCarrier.html#ad0507ac29c6d8e52e2877e154e798f5a", [
      [ "AxisType_DATA", "classSgPlotCarrier.html#ad0507ac29c6d8e52e2877e154e798f5aa5eed0ed4209a56ed597445a8a582f304", null ],
      [ "AxisType_MJD", "classSgPlotCarrier.html#ad0507ac29c6d8e52e2877e154e798f5aa01a186464f6243c9002c994f77f684fa", null ]
    ] ],
    [ "DataAttr", "classSgPlotCarrier.html#a51f2770191aa35f092604657955e7c5a", [
      [ "DA_NONUSABLE", "classSgPlotCarrier.html#a51f2770191aa35f092604657955e7c5aadef4565812005f8ae47c210ff306ac36", null ],
      [ "DA_REJECTED", "classSgPlotCarrier.html#a51f2770191aa35f092604657955e7c5aa5feaf6b8705ba1a24cbc9d9a84dd24da", null ],
      [ "DA_BAR", "classSgPlotCarrier.html#a51f2770191aa35f092604657955e7c5aa8fd31b0c9be72ed7510bd97829eb6b9b", null ],
      [ "DA_SELECTED", "classSgPlotCarrier.html#a51f2770191aa35f092604657955e7c5aad6aa846fb07130e79d5ca508ee887d77", null ]
    ] ],
    [ "SgPlotCarrier", "classSgPlotCarrier.html#a3a36c3558b4ba6fa441a58ae3e00b832", null ],
    [ "~SgPlotCarrier", "classSgPlotCarrier.html#ad90ba135f4a166898c04e85625a139b4", null ],
    [ "className", "classSgPlotCarrier.html#ad76cdd47ae229afadaed6148d0768cec", null ],
    [ "columnNames", "classSgPlotCarrier.html#a056eebefeafd1c534cb16d0f51984a5b", null ],
    [ "createBranch", "classSgPlotCarrier.html#a06515e0db604bc2b3095475731c17ea5", null ],
    [ "getAxisType", "classSgPlotCarrier.html#a3e34aec886d16e409aa77616e9ac7282", null ],
    [ "getFile2SaveBaseName", "classSgPlotCarrier.html#ab17ea3b133f7a691cc5cfec227ec3496", null ],
    [ "getName", "classSgPlotCarrier.html#a8d91dc589aa4b50d5b64d56cbff3eaaa", null ],
    [ "getStdVarIdx", "classSgPlotCarrier.html#a0875cca36f58b8cc162561a834b3ade8", null ],
    [ "isOK", "classSgPlotCarrier.html#ae5751c8257436a6cdc1b9a907146889d", null ],
    [ "listOfBranches", "classSgPlotCarrier.html#a0be48368a238bf8888408576d50fe79e", null ],
    [ "numOfColumns", "classSgPlotCarrier.html#af7698631a4b4a2b2acd9ff5c8d13efb2", null ],
    [ "numOfSigmasColumns", "classSgPlotCarrier.html#ac7641273729601223cf4c497a173b1b3", null ],
    [ "numOfValuesColumns", "classSgPlotCarrier.html#a1dcafe3ddea59688aabc3d09e61beaf8", null ],
    [ "selfCheck", "classSgPlotCarrier.html#aee399da092efa3fd1fc337103f244ac6", null ],
    [ "setAxisType", "classSgPlotCarrier.html#a2b4539598a77a141607e749ce036a655", null ],
    [ "setFile2SaveBaseName", "classSgPlotCarrier.html#a16a3ba0185cdd02c830334afd5094809", null ],
    [ "setName", "classSgPlotCarrier.html#ae6760ae3e50cde23662a6d01f6cfcd1d", null ],
    [ "setNameOfColumn", "classSgPlotCarrier.html#a22627acd4ae3ffc614e2154651174233", null ],
    [ "setStdVarIdx", "classSgPlotCarrier.html#a47c6b85139b16c935cc27fdb1cd4c25f", null ],
    [ "columnNames_", "classSgPlotCarrier.html#a51ac04dcb22f629f9b99a72c9a91bb72", null ],
    [ "dataStdVarIdx_", "classSgPlotCarrier.html#a8dc52ab3b93edc8b2e9d79d67cdb686f", null ],
    [ "dataTypes_", "classSgPlotCarrier.html#ae784206fe5c6e2228db5cc74c6df81c5", null ],
    [ "file2SaveBaseName_", "classSgPlotCarrier.html#aa5c13e393d661ca1fd20e0cf82dfdba2", null ],
    [ "isOK_", "classSgPlotCarrier.html#a17f48a1e9f79a45aff77f89f8464b876", null ],
    [ "listOfBranches_", "classSgPlotCarrier.html#a1c3714aec379f81581336d71bce17bbd", null ],
    [ "name_", "classSgPlotCarrier.html#aa5d3579e9d09ceaae01e3d0087ae045d", null ],
    [ "numOfSigmasColumns_", "classSgPlotCarrier.html#a915c0ca95220cf6e1038cb8ed06d099b", null ],
    [ "numOfValuesColumns_", "classSgPlotCarrier.html#ac6ae783450b2f40dd8aa3c96155ba2d8", null ]
];