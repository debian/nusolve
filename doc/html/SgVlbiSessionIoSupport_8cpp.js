var SgVlbiSessionIoSupport_8cpp =
[
    [ "ObsCal_v10_index", "SgVlbiSessionIoSupport_8cpp.html#a7251c34c24a3669e1022564c9e6ca29f", [
      [ "O10_POLE_TIDE", "SgVlbiSessionIoSupport_8cpp.html#a7251c34c24a3669e1022564c9e6ca29faabe095ba1a2fc45beb181cd5645ab7f6", null ],
      [ "O10_WOBBLE_X", "SgVlbiSessionIoSupport_8cpp.html#a7251c34c24a3669e1022564c9e6ca29fa27e568eaa99e246fcd4c390a310ac382", null ],
      [ "O10_WOBBLE_Y", "SgVlbiSessionIoSupport_8cpp.html#a7251c34c24a3669e1022564c9e6ca29fab12d6951e340dbf6f567f657cfd4e153", null ],
      [ "O10_EARTH_TIDE", "SgVlbiSessionIoSupport_8cpp.html#a7251c34c24a3669e1022564c9e6ca29fa6e0a2f2315afad6cd9ae360e4412f6df", null ],
      [ "O10_OCEAN_TIDE", "SgVlbiSessionIoSupport_8cpp.html#a7251c34c24a3669e1022564c9e6ca29fa805dc7bb150d5d5fbe486058b6b398a3", null ],
      [ "O10_POLE_TIDE_OLD", "SgVlbiSessionIoSupport_8cpp.html#a7251c34c24a3669e1022564c9e6ca29fab9497128d51786160f3e3b240b708d18", null ],
      [ "O10_UT1_ORTHO", "SgVlbiSessionIoSupport_8cpp.html#a7251c34c24a3669e1022564c9e6ca29fa7d326112a2df6ebe623a2e3653e3e88b", null ],
      [ "O10_WOB_ORTHO", "SgVlbiSessionIoSupport_8cpp.html#a7251c34c24a3669e1022564c9e6ca29fa3ac1c04fe0153f2521834d66fb53cc9d", null ],
      [ "O10_WOB_NUTAT", "SgVlbiSessionIoSupport_8cpp.html#a7251c34c24a3669e1022564c9e6ca29fa699d8154a7c11400c9a84ae894dcc82b", null ],
      [ "O10_FEED_ROT", "SgVlbiSessionIoSupport_8cpp.html#a7251c34c24a3669e1022564c9e6ca29fad348e851080798104353917b14b813d7", null ],
      [ "O10_WAHR_NUTAT", "SgVlbiSessionIoSupport_8cpp.html#a7251c34c24a3669e1022564c9e6ca29fa105e55cf3ef38991847f8884e4ba7de3", null ],
      [ "O10_TILT_RMVR", "SgVlbiSessionIoSupport_8cpp.html#a7251c34c24a3669e1022564c9e6ca29fad9dce4a5675b5eca518935f82b80200f", null ]
    ] ],
    [ "ObsCal_v11_index", "SgVlbiSessionIoSupport_8cpp.html#af063ac6b1b2529e721bc7b3e23391110", [
      [ "O11_POLE_TIDE", "SgVlbiSessionIoSupport_8cpp.html#af063ac6b1b2529e721bc7b3e23391110acda311518677368fcd2cfbe3779c8d55", null ],
      [ "O11_WOBBLE_X", "SgVlbiSessionIoSupport_8cpp.html#af063ac6b1b2529e721bc7b3e23391110a3774e9f9c201b37bb9be6441da901a62", null ],
      [ "O11_WOBBLE_Y", "SgVlbiSessionIoSupport_8cpp.html#af063ac6b1b2529e721bc7b3e23391110a4e5a5c7d57c35e6a825ae49499501e2e", null ],
      [ "O11_EARTH_TIDE", "SgVlbiSessionIoSupport_8cpp.html#af063ac6b1b2529e721bc7b3e23391110abe6493298a7bd6435e453896db94a835", null ],
      [ "O11_OCEAN_TIDE", "SgVlbiSessionIoSupport_8cpp.html#af063ac6b1b2529e721bc7b3e23391110a65ff53338689ae03ee3fe34f2543dd24", null ],
      [ "O11_UT1_ORTHO", "SgVlbiSessionIoSupport_8cpp.html#af063ac6b1b2529e721bc7b3e23391110aa063141dce5f43109ac10edf6e1f90f2", null ],
      [ "O11_WOB_ORTHO", "SgVlbiSessionIoSupport_8cpp.html#af063ac6b1b2529e721bc7b3e23391110a6d1c6e993c28b1657761e7ece97b75f7", null ],
      [ "O11_WOB_LIBRA", "SgVlbiSessionIoSupport_8cpp.html#af063ac6b1b2529e721bc7b3e23391110a54e18858c4bfdb7e7471bd1d59a7a55b", null ],
      [ "O11_UT1_LIBRA", "SgVlbiSessionIoSupport_8cpp.html#af063ac6b1b2529e721bc7b3e23391110acc5a49a42f89fee068cfbb7a640a826d", null ],
      [ "O11_OCN_POLE_TIDE", "SgVlbiSessionIoSupport_8cpp.html#af063ac6b1b2529e721bc7b3e23391110a5b0005dc4203b6961aaef12b9c34efd3", null ],
      [ "O11_FEED_ROT", "SgVlbiSessionIoSupport_8cpp.html#af063ac6b1b2529e721bc7b3e23391110a525da665e850cde0fb3fe2219feec35c", null ],
      [ "O11_OCEAN_TIDE_OLD", "SgVlbiSessionIoSupport_8cpp.html#af063ac6b1b2529e721bc7b3e23391110ad5b950384f37ab072d0b0c4f6feeb04e", null ],
      [ "O11_TILT_RMVR", "SgVlbiSessionIoSupport_8cpp.html#af063ac6b1b2529e721bc7b3e23391110a59af46b30b1809cb34f3e158036980dd", null ],
      [ "O11_POLE_TIDE_OLD", "SgVlbiSessionIoSupport_8cpp.html#af063ac6b1b2529e721bc7b3e23391110af39895929cafcd8b78ba93417cb77531", null ]
    ] ],
    [ "sAntennaMountTypes", "SgVlbiSessionIoSupport_8cpp.html#ad71bc733e3ed698a2b629cea207af8df", null ],
    [ "sFlybyCalibrationList", "SgVlbiSessionIoSupport_8cpp.html#ae13ae6677703ab5c8b99e9be116aa156", null ],
    [ "sObsCalibrationList_v10", "SgVlbiSessionIoSupport_8cpp.html#aa6c1278ef31a35e8a708a479f05e6e79", null ],
    [ "sObsCalibrationList_v11", "SgVlbiSessionIoSupport_8cpp.html#a46ecfd34e54f331ad48b9a10008ea65e", null ],
    [ "sStationCalibrationList", "SgVlbiSessionIoSupport_8cpp.html#af34dca8855e0e6a70aff114f8dd96af3", null ]
];