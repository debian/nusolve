var dir_8315963740f7082d06b3c9585293bd90 =
[
    [ "Sg3dMatrix.cpp", "Sg3dMatrix_8cpp.html", "Sg3dMatrix_8cpp" ],
    [ "Sg3dMatrix.h", "Sg3dMatrix_8h.html", "Sg3dMatrix_8h" ],
    [ "Sg3dMatrixR.cpp", "Sg3dMatrixR_8cpp.html", null ],
    [ "Sg3dMatrixR.h", "Sg3dMatrixR_8h.html", [
      [ "Sg3dMatrixR", "classSg3dMatrixR.html", "classSg3dMatrixR" ]
    ] ],
    [ "Sg3dMatrixRdots.cpp", "Sg3dMatrixRdots_8cpp.html", null ],
    [ "Sg3dMatrixRdots.h", "Sg3dMatrixRdots_8h.html", [
      [ "Sg3dMatrixRdot", "classSg3dMatrixRdot.html", "classSg3dMatrixRdot" ],
      [ "Sg3dMatrixR2dot", "classSg3dMatrixR2dot.html", "classSg3dMatrixR2dot" ]
    ] ],
    [ "Sg3dVector.cpp", "Sg3dVector_8cpp.html", "Sg3dVector_8cpp" ],
    [ "Sg3dVector.h", "Sg3dVector_8h.html", "Sg3dVector_8h" ],
    [ "SgAPrioriData.cpp", "SgAPrioriData_8cpp.html", null ],
    [ "SgAPrioriData.h", "SgAPrioriData_8h.html", [
      [ "SgAPrioriRecComponent", "classSgAPrioriRecComponent.html", "classSgAPrioriRecComponent" ],
      [ "SgAPrioriRec", "classSgAPrioriRec.html", "classSgAPrioriRec" ],
      [ "SgAPriories", "classSgAPriories.html", "classSgAPriories" ]
    ] ],
    [ "SgArcStorage.cpp", "SgArcStorage_8cpp.html", null ],
    [ "SgArcStorage.h", "SgArcStorage_8h.html", [
      [ "SgArcStorage", "classSgArcStorage.html", "classSgArcStorage" ]
    ] ],
    [ "SgAttribute.cpp", "SgAttribute_8cpp.html", null ],
    [ "SgAttribute.h", "SgAttribute_8h.html", [
      [ "SgAttribute", "classSgAttribute.html", "classSgAttribute" ]
    ] ],
    [ "SgBreakModel.cpp", "SgBreakModel_8cpp.html", "SgBreakModel_8cpp" ],
    [ "SgBreakModel.h", "SgBreakModel_8h.html", [
      [ "SgParameterBreak", "classSgParameterBreak.html", "classSgParameterBreak" ],
      [ "SgBreakModel", "classSgBreakModel.html", "classSgBreakModel" ]
    ] ],
    [ "SgConstants.cpp", "SgConstants_8cpp.html", "SgConstants_8cpp" ],
    [ "SgConstants.h", "SgConstants_8h.html", "SgConstants_8h" ],
    [ "SgCubicSpline.cpp", "SgCubicSpline_8cpp.html", null ],
    [ "SgCubicSpline.h", "SgCubicSpline_8h.html", [
      [ "SgCubicSpline", "classSgCubicSpline.html", "classSgCubicSpline" ]
    ] ],
    [ "SgDbhFormat.cpp", "SgDbhFormat_8cpp.html", "SgDbhFormat_8cpp" ],
    [ "SgDbhFormat.h", "SgDbhFormat_8h.html", [
      [ "SgDbhPhysicalRecord", "classSgDbhPhysicalRecord.html", "classSgDbhPhysicalRecord" ],
      [ "SgDbhDataRecordString", "classSgDbhDataRecordString.html", "classSgDbhDataRecordString" ],
      [ "SgDbhDataRecord", "classSgDbhDataRecord.html", "classSgDbhDataRecord" ],
      [ "SgDbhServiceRecord", "classSgDbhServiceRecord.html", "classSgDbhServiceRecord" ],
      [ "SgDbhServiceRecordHS1", "classSgDbhServiceRecordHS1.html", "classSgDbhServiceRecordHS1" ],
      [ "SgDbhServiceRecordHS2", "classSgDbhServiceRecordHS2.html", "classSgDbhServiceRecordHS2" ],
      [ "SgDbhServiceRecordTc", "classSgDbhServiceRecordTc.html", "classSgDbhServiceRecordTc" ],
      [ "SgDbhServiceRecordTe", "classSgDbhServiceRecordTe.html", "classSgDbhServiceRecordTe" ],
      [ "SgDbhServiceRecordDr", "classSgDbhServiceRecordDr.html", "classSgDbhServiceRecordDr" ],
      [ "SgDbhServiceRecordDe", "classSgDbhServiceRecordDe.html", "classSgDbhServiceRecordDe" ],
      [ "SgDbhStartBlock", "classSgDbhStartBlock.html", "classSgDbhStartBlock" ],
      [ "SgDbhHistoryEntry", "classSgDbhHistoryEntry.html", "classSgDbhHistoryEntry" ],
      [ "SgDbhListOfDescriptors", "classSgDbhListOfDescriptors.html", "classSgDbhListOfDescriptors" ],
      [ "SgDbhDataBlock", "classSgDbhDataBlock.html", "classSgDbhDataBlock" ],
      [ "SgDbhTeBlock", "classSgDbhTeBlock.html", "classSgDbhTeBlock" ],
      [ "SgDbhServiceRecordP3", "classSgDbhTeBlock_1_1SgDbhServiceRecordP3.html", "classSgDbhTeBlock_1_1SgDbhServiceRecordP3" ],
      [ "SgDbhServiceRecordP4", "classSgDbhTeBlock_1_1SgDbhServiceRecordP4.html", "classSgDbhTeBlock_1_1SgDbhServiceRecordP4" ],
      [ "SgDbhTcBlock", "classSgDbhTcBlock.html", "classSgDbhTcBlock" ],
      [ "SgDbhObservationEntry", "classSgDbhObservationEntry.html", "classSgDbhObservationEntry" ],
      [ "SgDbhFormat", "classSgDbhFormat.html", "classSgDbhFormat" ]
    ] ],
    [ "SgDbhImage.cpp", "SgDbhImage_8cpp.html", "SgDbhImage_8cpp" ],
    [ "SgDbhImage.h", "SgDbhImage_8h.html", [
      [ "SgDbhStream", "classSgDbhStream.html", null ],
      [ "SgDbhDatumDescriptor", "classSgDbhDatumDescriptor.html", "classSgDbhDatumDescriptor" ],
      [ "SgDbhImage", "classSgDbhImage.html", "classSgDbhImage" ]
    ] ],
    [ "SgEccDat.cpp", "SgEccDat_8cpp.html", null ],
    [ "SgEccDat.h", "SgEccDat_8h.html", [
      [ "SgEccDat", "classSgEccDat.html", "classSgEccDat" ]
    ] ],
    [ "SgEccRec.cpp", "SgEccRec_8cpp.html", null ],
    [ "SgEccRec.h", "SgEccRec_8h.html", [
      [ "SgEccRec", "classSgEccRec.html", "classSgEccRec" ]
    ] ],
    [ "SgEccSite.cpp", "SgEccSite_8cpp.html", "SgEccSite_8cpp" ],
    [ "SgEccSite.h", "SgEccSite_8h.html", [
      [ "SgEccSite", "classSgEccSite.html", "classSgEccSite" ]
    ] ],
    [ "SgEstimator.cpp", "SgEstimator_8cpp.html", "SgEstimator_8cpp" ],
    [ "SgEstimator.h", "SgEstimator_8h.html", [
      [ "SgEstimator", "classSgEstimator.html", "classSgEstimator" ],
      [ "StochasticSolutionCarrier", "structSgEstimator_1_1StochasticSolutionCarrier.html", "structSgEstimator_1_1StochasticSolutionCarrier" ],
      [ "SmoothCarrier", "structSgEstimator_1_1SmoothCarrier.html", "structSgEstimator_1_1SmoothCarrier" ],
      [ "RPCarrier", "structSgEstimator_1_1RPCarrier.html", "structSgEstimator_1_1RPCarrier" ],
      [ "RegularSolutionCarrier", "structSgEstimator_1_1RegularSolutionCarrier.html", "structSgEstimator_1_1RegularSolutionCarrier" ]
    ] ],
    [ "SgExternalErpFile.cpp", "SgExternalErpFile_8cpp.html", null ],
    [ "SgExternalErpFile.h", "SgExternalErpFile_8h.html", [
      [ "SgExternalEopFile", "classSgExternalEopFile.html", "classSgExternalEopFile" ]
    ] ],
    [ "SgExternalWeights.cpp", "SgExternalWeights_8cpp.html", null ],
    [ "SgExternalWeights.h", "SgExternalWeights_8h.html", [
      [ "SgBaselineExternalWeight", "classSgBaselineExternalWeight.html", "classSgBaselineExternalWeight" ],
      [ "SgExternalWeights", "classSgExternalWeights.html", "classSgExternalWeights" ]
    ] ],
    [ "SgGuiLogger.cpp", "SgGuiLogger_8cpp.html", null ],
    [ "SgGuiLogger.h", "SgGuiLogger_8h.html", [
      [ "SgGuiLogger", "classSgGuiLogger.html", "classSgGuiLogger" ]
    ] ],
    [ "SgGuiLogger.moc.cpp", "SgGuiLogger_8moc_8cpp.html", "SgGuiLogger_8moc_8cpp" ],
    [ "SgGuiLoggerConfig.cpp", "SgGuiLoggerConfig_8cpp.html", "SgGuiLoggerConfig_8cpp" ],
    [ "SgGuiLoggerConfig.h", "SgGuiLoggerConfig_8h.html", [
      [ "SgGuiLoggerConfig", "classSgGuiLoggerConfig.html", "classSgGuiLoggerConfig" ]
    ] ],
    [ "SgGuiLoggerConfig.moc.cpp", "SgGuiLoggerConfig_8moc_8cpp.html", "SgGuiLoggerConfig_8moc_8cpp" ],
    [ "SgGuiParameterCfg.cpp", "SgGuiParameterCfg_8cpp.html", null ],
    [ "SgGuiParameterCfg.h", "SgGuiParameterCfg_8h.html", [
      [ "SgGuiParameterCfg", "classSgGuiParameterCfg.html", "classSgGuiParameterCfg" ]
    ] ],
    [ "SgGuiParameterCfg.moc.cpp", "SgGuiParameterCfg_8moc_8cpp.html", "SgGuiParameterCfg_8moc_8cpp" ],
    [ "SgGuiPiaReport.cpp", "SgGuiPiaReport_8cpp.html", null ],
    [ "SgGuiPiaReport.h", "SgGuiPiaReport_8h.html", [
      [ "SgGuiPiaReport", "classSgGuiPiaReport.html", "classSgGuiPiaReport" ]
    ] ],
    [ "SgGuiPiaReport.moc.cpp", "SgGuiPiaReport_8moc_8cpp.html", "SgGuiPiaReport_8moc_8cpp" ],
    [ "SgGuiPlotter.cpp", "SgGuiPlotter_8cpp.html", null ],
    [ "SgGuiPlotter.h", "SgGuiPlotter_8h.html", [
      [ "SgPlotBranch", "classSgPlotBranch.html", "classSgPlotBranch" ],
      [ "SgPlotCarrier", "classSgPlotCarrier.html", "classSgPlotCarrier" ],
      [ "SgPlotArea", "classSgPlotArea.html", "classSgPlotArea" ],
      [ "SgPlotScroller", "classSgPlotScroller.html", "classSgPlotScroller" ],
      [ "SgPlot", "classSgPlot.html", "classSgPlot" ]
    ] ],
    [ "SgGuiPlotter.moc.cpp", "SgGuiPlotter_8moc_8cpp.html", "SgGuiPlotter_8moc_8cpp" ],
    [ "SgGuiQTreeWidgetExt.cpp", "SgGuiQTreeWidgetExt_8cpp.html", null ],
    [ "SgGuiQTreeWidgetExt.h", "SgGuiQTreeWidgetExt_8h.html", [
      [ "SgGuiQTreeWidgetExt", "classSgGuiQTreeWidgetExt.html", "classSgGuiQTreeWidgetExt" ]
    ] ],
    [ "SgGuiQTreeWidgetExt.moc.cpp", "SgGuiQTreeWidgetExt_8moc_8cpp.html", "SgGuiQTreeWidgetExt_8moc_8cpp" ],
    [ "SgGuiTaskConfig.cpp", "SgGuiTaskConfig_8cpp.html", null ],
    [ "SgGuiTaskConfig.h", "SgGuiTaskConfig_8h.html", [
      [ "SgGuiTcUserCorrectionItem", "classSgGuiTcUserCorrectionItem.html", "classSgGuiTcUserCorrectionItem" ],
      [ "SgGuiTaskConfig", "classSgGuiTaskConfig.html", "classSgGuiTaskConfig" ],
      [ "SgGuiTaskConfigDialog", "classSgGuiTaskConfigDialog.html", "classSgGuiTaskConfigDialog" ]
    ] ],
    [ "SgGuiTaskConfig.moc.cpp", "SgGuiTaskConfig_8moc_8cpp.html", "SgGuiTaskConfig_8moc_8cpp" ],
    [ "SgGuiVlbiBaselineList.cpp", "SgGuiVlbiBaselineList_8cpp.html", "SgGuiVlbiBaselineList_8cpp" ],
    [ "SgGuiVlbiBaselineList.h", "SgGuiVlbiBaselineList_8h.html", [
      [ "SgGuiVlbiBaselineItem", "classSgGuiVlbiBaselineItem.html", "classSgGuiVlbiBaselineItem" ],
      [ "SgGuiVlbiBaselineList", "classSgGuiVlbiBaselineList.html", "classSgGuiVlbiBaselineList" ],
      [ "SgGuiVlbiBlnInfoEditor", "classSgGuiVlbiBlnInfoEditor.html", "classSgGuiVlbiBlnInfoEditor" ]
    ] ],
    [ "SgGuiVlbiBaselineList.moc.cpp", "SgGuiVlbiBaselineList_8moc_8cpp.html", "SgGuiVlbiBaselineList_8moc_8cpp" ],
    [ "SgGuiVlbiHistory.cpp", "SgGuiVlbiHistory_8cpp.html", null ],
    [ "SgGuiVlbiHistory.h", "SgGuiVlbiHistory_8h.html", [
      [ "SgGuiVlbiHistory", "classSgGuiVlbiHistory.html", "classSgGuiVlbiHistory" ]
    ] ],
    [ "SgGuiVlbiHistory.moc.cpp", "SgGuiVlbiHistory_8moc_8cpp.html", "SgGuiVlbiHistory_8moc_8cpp" ],
    [ "SgGuiVlbiSourceList.cpp", "SgGuiVlbiSourceList_8cpp.html", "SgGuiVlbiSourceList_8cpp" ],
    [ "SgGuiVlbiSourceList.h", "SgGuiVlbiSourceList_8h.html", [
      [ "SgGuiVlbiSourceItem", "classSgGuiVlbiSourceItem.html", "classSgGuiVlbiSourceItem" ],
      [ "SgGuiVlbiSourceList", "classSgGuiVlbiSourceList.html", "classSgGuiVlbiSourceList" ],
      [ "SgGuiVlbiSrcInfoEditor", "classSgGuiVlbiSrcInfoEditor.html", "classSgGuiVlbiSrcInfoEditor" ],
      [ "SgGuiVlbiSrcStrModelItem", "classSgGuiVlbiSrcStrModelItem.html", "classSgGuiVlbiSrcStrModelItem" ],
      [ "SgGuiVlbiSrcStrModelEditor", "classSgGuiVlbiSrcStrModelEditor.html", "classSgGuiVlbiSrcStrModelEditor" ]
    ] ],
    [ "SgGuiVlbiSourceList.moc.cpp", "SgGuiVlbiSourceList_8moc_8cpp.html", "SgGuiVlbiSourceList_8moc_8cpp" ],
    [ "SgGuiVlbiStationList.cpp", "SgGuiVlbiStationList_8cpp.html", "SgGuiVlbiStationList_8cpp" ],
    [ "SgGuiVlbiStationList.h", "SgGuiVlbiStationList_8h.html", [
      [ "SgGuiVlbiStationItem", "classSgGuiVlbiStationItem.html", "classSgGuiVlbiStationItem" ],
      [ "SgGuiVlbiStationList", "classSgGuiVlbiStationList.html", "classSgGuiVlbiStationList" ],
      [ "SgGuiVlbiClockBreakItem", "classSgGuiVlbiClockBreakItem.html", "classSgGuiVlbiClockBreakItem" ],
      [ "SgGuiVlbiStnInfoEditor", "classSgGuiVlbiStnInfoEditor.html", "classSgGuiVlbiStnInfoEditor" ],
      [ "SgGuiVlbiStnClockBreakEditor", "classSgGuiVlbiStnClockBreakEditor.html", "classSgGuiVlbiStnClockBreakEditor" ]
    ] ],
    [ "SgGuiVlbiStationList.moc.cpp", "SgGuiVlbiStationList_8moc_8cpp.html", "SgGuiVlbiStationList_8moc_8cpp" ],
    [ "SgIdentities.cpp", "SgIdentities_8cpp.html", null ],
    [ "SgIdentities.h", "SgIdentities_8h.html", [
      [ "SgIdentities", "classSgIdentities.html", "classSgIdentities" ]
    ] ],
    [ "SgIoAgv.cpp", "SgIoAgv_8cpp.html", "SgIoAgv_8cpp" ],
    [ "SgIoAgv.h", "SgIoAgv_8h.html", "SgIoAgv_8h" ],
    [ "SgIoAgvDriver.cpp", "SgIoAgvDriver_8cpp.html", "SgIoAgvDriver_8cpp" ],
    [ "SgIoAgvDriver.h", "SgIoAgvDriver_8h.html", "SgIoAgvDriver_8h" ],
    [ "SgIoAgvDriverDds.cpp", "SgIoAgvDriverDds_8cpp.html", "SgIoAgvDriverDds_8cpp" ],
    [ "SgIoDriver.cpp", "SgIoDriver_8cpp.html", null ],
    [ "SgIoDriver.h", "SgIoDriver_8h.html", "SgIoDriver_8h" ],
    [ "SgIoExternalFilter.cpp", "SgIoExternalFilter_8cpp.html", "SgIoExternalFilter_8cpp" ],
    [ "SgIoExternalFilter.h", "SgIoExternalFilter_8h.html", "SgIoExternalFilter_8h" ],
    [ "SgKombFormat.cpp", "SgKombFormat_8cpp.html", "SgKombFormat_8cpp" ],
    [ "SgKombFormat.h", "SgKombFormat_8h.html", [
      [ "SgKombStream", "classSgKombStream.html", null ],
      [ "SgKombHeaderRecord", "classSgKombHeaderRecord.html", "classSgKombHeaderRecord" ],
      [ "SgKombHeader", "classSgKombHeader.html", "classSgKombHeader" ],
      [ "SgKombOb01Record", "classSgKombOb01Record.html", "classSgKombOb01Record" ],
      [ "SgKombOb02Record", "classSgKombOb02Record.html", "classSgKombOb02Record" ],
      [ "SgKombOb03Record", "classSgKombOb03Record.html", "classSgKombOb03Record" ],
      [ "SgKombBd01Record", "classSgKombBd01Record.html", "classSgKombBd01Record" ],
      [ "SgKombBd02Record", "classSgKombBd02Record.html", "classSgKombBd02Record" ],
      [ "SgKombBd03Record", "classSgKombBd03Record.html", "classSgKombBd03Record" ],
      [ "SgKombBd04Record", "classSgKombBd04Record.html", "classSgKombBd04Record" ],
      [ "SgKombBd05Record", "classSgKombBd05Record.html", "classSgKombBd05Record" ],
      [ "SgKombReader", "classSgKombReader.html", "classSgKombReader" ]
    ] ],
    [ "SgLogger.cpp", "SgLogger_8cpp.html", "SgLogger_8cpp" ],
    [ "SgLogger.h", "SgLogger_8h.html", "SgLogger_8h" ],
    [ "SgMappingFunction.cpp", "SgMappingFunction_8cpp.html", null ],
    [ "SgMappingFunction.h", "SgMappingFunction_8h.html", [
      [ "SgMappingFunction", "classSgMappingFunction.html", "classSgMappingFunction" ],
      [ "SgDryMF_NMF", "classSgDryMF__NMF.html", "classSgDryMF__NMF" ],
      [ "SgWetMF_NMF", "classSgWetMF__NMF.html", "classSgWetMF__NMF" ],
      [ "SgGradMF_CH", "classSgGradMF__CH.html", "classSgGradMF__CH" ],
      [ "SgDryMF_MTT", "classSgDryMF__MTT.html", "classSgDryMF__MTT" ],
      [ "SgWetMF_MTT", "classSgWetMF__MTT.html", "classSgWetMF__MTT" ]
    ] ],
    [ "SgMasterRecord.cpp", "SgMasterRecord_8cpp.html", null ],
    [ "SgMasterRecord.h", "SgMasterRecord_8h.html", [
      [ "SgMasterRecord", "classSgMasterRecord.html", "classSgMasterRecord" ]
    ] ],
    [ "SgMathSupport.cpp", "SgMathSupport_8cpp.html", "SgMathSupport_8cpp" ],
    [ "SgMathSupport.h", "SgMathSupport_8h.html", "SgMathSupport_8h" ],
    [ "SgMatrix.cpp", "SgMatrix_8cpp.html", "SgMatrix_8cpp" ],
    [ "SgMatrix.h", "SgMatrix_8h.html", "SgMatrix_8h" ],
    [ "SgMeteoData.cpp", "SgMeteoData_8cpp.html", null ],
    [ "SgMeteoData.h", "SgMeteoData_8h.html", [
      [ "SgMeteoData", "classSgMeteoData.html", "classSgMeteoData" ]
    ] ],
    [ "SgMJD.cpp", "SgMJD_8cpp.html", "SgMJD_8cpp" ],
    [ "SgMJD.h", "SgMJD_8h.html", "SgMJD_8h" ],
    [ "SgModelEop_JMG_96_hf.cpp", "SgModelEop__JMG__96__hf_8cpp.html", "SgModelEop__JMG__96__hf_8cpp" ],
    [ "SgModelEop_JMG_96_hf.h", "SgModelEop__JMG__96__hf_8h.html", [
      [ "SgModelEop_JMG_96_hf", "classSgModelEop__JMG__96__hf.html", "classSgModelEop__JMG__96__hf" ],
      [ "HfEopRec", "structSgModelEop__JMG__96__hf_1_1HfEopRec.html", "structSgModelEop__JMG__96__hf_1_1HfEopRec" ]
    ] ],
    [ "SgModelsInfo.cpp", "SgModelsInfo_8cpp.html", null ],
    [ "SgModelsInfo.h", "SgModelsInfo_8h.html", [
      [ "SgModelsInfo", "classSgModelsInfo.html", "classSgModelsInfo" ],
      [ "DasModel", "classSgModelsInfo_1_1DasModel.html", "classSgModelsInfo_1_1DasModel" ]
    ] ],
    [ "SgNetCdf.cpp", "SgNetCdf_8cpp.html", "SgNetCdf_8cpp" ],
    [ "SgNetCdf.h", "SgNetCdf_8h.html", "SgNetCdf_8h" ],
    [ "SgNetworkStnRecord.cpp", "SgNetworkStnRecord_8cpp.html", null ],
    [ "SgNetworkStnRecord.h", "SgNetworkStnRecord_8h.html", [
      [ "SgNetworkStnRecord", "classSgNetworkStnRecord.html", "classSgNetworkStnRecord" ],
      [ "SgNetworkStations", "classSgNetworkStations.html", "classSgNetworkStations" ]
    ] ],
    [ "SgObjectInfo.cpp", "SgObjectInfo_8cpp.html", null ],
    [ "SgObjectInfo.h", "SgObjectInfo_8h.html", "SgObjectInfo_8h" ],
    [ "SgObservation.cpp", "SgObservation_8cpp.html", null ],
    [ "SgObservation.h", "SgObservation_8h.html", "SgObservation_8h" ],
    [ "SgOceanLoad.cpp", "SgOceanLoad_8cpp.html", null ],
    [ "SgOceanLoad.h", "SgOceanLoad_8h.html", [
      [ "SgOceanLoad", "classSgOceanLoad.html", "classSgOceanLoad" ]
    ] ],
    [ "SgParameter.cpp", "SgParameter_8cpp.html", "SgParameter_8cpp" ],
    [ "SgParameter.h", "SgParameter_8h.html", "SgParameter_8h" ],
    [ "SgParameterCfg.cpp", "SgParameterCfg_8cpp.html", "SgParameterCfg_8cpp" ],
    [ "SgParameterCfg.h", "SgParameterCfg_8h.html", [
      [ "SgParameterCfg", "classSgParameterCfg.html", "classSgParameterCfg" ]
    ] ],
    [ "SgParametersDescriptor.cpp", "SgParametersDescriptor_8cpp.html", null ],
    [ "SgParametersDescriptor.h", "SgParametersDescriptor_8h.html", "SgParametersDescriptor_8h" ],
    [ "SgPartial.cpp", "SgPartial_8cpp.html", null ],
    [ "SgPartial.h", "SgPartial_8h.html", [
      [ "SgPartial", "classSgPartial.html", "classSgPartial" ]
    ] ],
    [ "SgPwlStorage.cpp", "SgPwlStorage_8cpp.html", null ],
    [ "SgPwlStorage.h", "SgPwlStorage_8h.html", [
      [ "SgPwlStorage", "classSgPwlStorage.html", "classSgPwlStorage" ]
    ] ],
    [ "SgPwlStorageBSplineL.cpp", "SgPwlStorageBSplineL_8cpp.html", null ],
    [ "SgPwlStorageBSplineL.h", "SgPwlStorageBSplineL_8h.html", [
      [ "SgPwlStorageBSplineL", "classSgPwlStorageBSplineL.html", "classSgPwlStorageBSplineL" ]
    ] ],
    [ "SgPwlStorageBSplineQ.cpp", "SgPwlStorageBSplineQ_8cpp.html", null ],
    [ "SgPwlStorageBSplineQ.h", "SgPwlStorageBSplineQ_8h.html", [
      [ "SgPwlStorageBSplineQ", "classSgPwlStorageBSplineQ.html", "classSgPwlStorageBSplineQ" ]
    ] ],
    [ "SgPwlStorageIncRates.cpp", "SgPwlStorageIncRates_8cpp.html", null ],
    [ "SgPwlStorageIncRates.h", "SgPwlStorageIncRates_8h.html", [
      [ "SgPwlStorageIncRates", "classSgPwlStorageIncRates.html", "classSgPwlStorageIncRates" ]
    ] ],
    [ "SgRefraction.cpp", "SgRefraction_8cpp.html", null ],
    [ "SgRefraction.h", "SgRefraction_8h.html", [
      [ "SgRefraction", "classSgRefraction.html", "classSgRefraction" ]
    ] ],
    [ "SgSingleSessionTaskManager.cpp", "SgSingleSessionTaskManager_8cpp.html", null ],
    [ "SgSingleSessionTaskManager.h", "SgSingleSessionTaskManager_8h.html", [
      [ "SgSingleSessionTaskManager", "classSgSingleSessionTaskManager.html", "classSgSingleSessionTaskManager" ]
    ] ],
    [ "SgSolutionReporter.cpp", "SgSolutionReporter_8cpp.html", "SgSolutionReporter_8cpp" ],
    [ "SgSolutionReporter.h", "SgSolutionReporter_8h.html", [
      [ "SgSolutionReporter", "classSgSolutionReporter.html", "classSgSolutionReporter" ]
    ] ],
    [ "SgSourceKey.cpp", "SgSourceKey_8cpp.html", null ],
    [ "SgSourceKey.h", "SgSourceKey_8h.html", [
      [ "SgSourceKey", "classSgSourceKey.html", "classSgSourceKey" ]
    ] ],
    [ "SgStnLogCollector.cpp", "SgStnLogCollector_8cpp.html", "SgStnLogCollector_8cpp" ],
    [ "SgStnLogCollector.h", "SgStnLogCollector_8h.html", "SgStnLogCollector_8h" ],
    [ "SgSymMatrix.cpp", "SgSymMatrix_8cpp.html", "SgSymMatrix_8cpp" ],
    [ "SgSymMatrix.h", "SgSymMatrix_8h.html", "SgSymMatrix_8h" ],
    [ "SgTask.cpp", "SgTask_8cpp.html", null ],
    [ "SgTask.h", "SgTask_8h.html", [
      [ "SgTask", "classSgTask.html", "classSgTask" ]
    ] ],
    [ "SgTaskConfig.cpp", "SgTaskConfig_8cpp.html", null ],
    [ "SgTaskConfig.h", "SgTaskConfig_8h.html", [
      [ "SgTaskConfig", "classSgTaskConfig.html", "classSgTaskConfig" ],
      [ "AutomaticProcessing", "structSgTaskConfig_1_1AutomaticProcessing.html", "structSgTaskConfig_1_1AutomaticProcessing" ]
    ] ],
    [ "SgTaskManager.cpp", "SgTaskManager_8cpp.html", null ],
    [ "SgTaskManager.h", "SgTaskManager_8h.html", [
      [ "SgTaskManager", "classSgTaskManager.html", "classSgTaskManager" ]
    ] ],
    [ "SgTidalUt1.cpp", "SgTidalUt1_8cpp.html", "SgTidalUt1_8cpp" ],
    [ "SgTidalUt1.h", "SgTidalUt1_8h.html", "SgTidalUt1_8h" ],
    [ "SgTidalUt1_Tables.cpp", "SgTidalUt1__Tables_8cpp.html", "SgTidalUt1__Tables_8cpp" ],
    [ "SgUtMatrix.cpp", "SgUtMatrix_8cpp.html", "SgUtMatrix_8cpp" ],
    [ "SgUtMatrix.h", "SgUtMatrix_8h.html", "SgUtMatrix_8h" ],
    [ "SgVector.cpp", "SgVector_8cpp.html", "SgVector_8cpp" ],
    [ "SgVector.h", "SgVector_8h.html", "SgVector_8h" ],
    [ "SgVersion.cpp", "SgVersion_8cpp.html", "SgVersion_8cpp" ],
    [ "SgVersion.h", "SgVersion_8h.html", "SgVersion_8h" ],
    [ "SgVex.cpp", "SgVex_8cpp.html", "SgVex_8cpp" ],
    [ "SgVex.h", "SgVex_8h.html", "SgVex_8h" ],
    [ "SgVgosDb.cpp", "SgVgosDb_8cpp.html", "SgVgosDb_8cpp" ],
    [ "SgVgosDb.h", "SgVgosDb_8h.html", "SgVgosDb_8h" ],
    [ "SgVgosDbLoadObs.cpp", "SgVgosDbLoadObs_8cpp.html", "SgVgosDbLoadObs_8cpp" ],
    [ "SgVgosDbLoadScan.cpp", "SgVgosDbLoadScan_8cpp.html", "SgVgosDbLoadScan_8cpp" ],
    [ "SgVgosDbLoadSession.cpp", "SgVgosDbLoadSession_8cpp.html", "SgVgosDbLoadSession_8cpp" ],
    [ "SgVgosDbLoadStation.cpp", "SgVgosDbLoadStation_8cpp.html", "SgVgosDbLoadStation_8cpp" ],
    [ "SgVgosDbStoreObs.cpp", "SgVgosDbStoreObs_8cpp.html", null ],
    [ "SgVgosDbStoreScan.cpp", "SgVgosDbStoreScan_8cpp.html", null ],
    [ "SgVgosDbStoreSession.cpp", "SgVgosDbStoreSession_8cpp.html", null ],
    [ "SgVgosDbStoreStation.cpp", "SgVgosDbStoreStation_8cpp.html", null ],
    [ "SgVlbiAuxObservation.cpp", "SgVlbiAuxObservation_8cpp.html", null ],
    [ "SgVlbiAuxObservation.h", "SgVlbiAuxObservation_8h.html", [
      [ "SgVlbiAuxObservation", "classSgVlbiAuxObservation.html", "classSgVlbiAuxObservation" ]
    ] ],
    [ "SgVlbiBand.cpp", "SgVlbiBand_8cpp.html", null ],
    [ "SgVlbiBand.h", "SgVlbiBand_8h.html", [
      [ "SgVlbiBand", "classSgVlbiBand.html", "classSgVlbiBand" ]
    ] ],
    [ "SgVlbiBaselineInfo.cpp", "SgVlbiBaselineInfo_8cpp.html", null ],
    [ "SgVlbiBaselineInfo.h", "SgVlbiBaselineInfo_8h.html", "SgVlbiBaselineInfo_8h" ],
    [ "SgVlbiHistory.cpp", "SgVlbiHistory_8cpp.html", null ],
    [ "SgVlbiHistory.h", "SgVlbiHistory_8h.html", [
      [ "SgVlbiHistoryRecord", "classSgVlbiHistoryRecord.html", "classSgVlbiHistoryRecord" ],
      [ "SgVlbiHistory", "classSgVlbiHistory.html", "classSgVlbiHistory" ]
    ] ],
    [ "SgVlbiMeasurement.cpp", "SgVlbiMeasurement_8cpp.html", null ],
    [ "SgVlbiMeasurement.h", "SgVlbiMeasurement_8h.html", "SgVlbiMeasurement_8h" ],
    [ "SgVlbiNetworkId.cpp", "SgVlbiNetworkId_8cpp.html", "SgVlbiNetworkId_8cpp" ],
    [ "SgVlbiNetworkId.h", "SgVlbiNetworkId_8h.html", "SgVlbiNetworkId_8h" ],
    [ "SgVlbiObservable.cpp", "SgVlbiObservable_8cpp.html", "SgVlbiObservable_8cpp" ],
    [ "SgVlbiObservable.h", "SgVlbiObservable_8h.html", [
      [ "SgVlbiObservable", "classSgVlbiObservable.html", "classSgVlbiObservable" ]
    ] ],
    [ "SgVlbiObservation.cpp", "SgVlbiObservation_8cpp.html", "SgVlbiObservation_8cpp" ],
    [ "SgVlbiObservation.h", "SgVlbiObservation_8h.html", [
      [ "SgVlbiObservation", "classSgVlbiObservation.html", "classSgVlbiObservation" ]
    ] ],
    [ "SgVlbiSession.cpp", "SgVlbiSession_8cpp.html", "SgVlbiSession_8cpp" ],
    [ "SgVlbiSession.h", "SgVlbiSession_8h.html", "SgVlbiSession_8h" ],
    [ "SgVlbiSessionInfo.cpp", "SgVlbiSessionInfo_8cpp.html", "SgVlbiSessionInfo_8cpp" ],
    [ "SgVlbiSessionInfo.h", "SgVlbiSessionInfo_8h.html", "SgVlbiSessionInfo_8h" ],
    [ "SgVlbiSessionIoAgv.cpp", "SgVlbiSessionIoAgv_8cpp.html", null ],
    [ "SgVlbiSessionIoDbh.cpp", "SgVlbiSessionIoDbh_8cpp.html", "SgVlbiSessionIoDbh_8cpp" ],
    [ "SgVlbiSessionIoFringes.cpp", "SgVlbiSessionIoFringes_8cpp.html", null ],
    [ "SgVlbiSessionIoKomb.cpp", "SgVlbiSessionIoKomb_8cpp.html", "SgVlbiSessionIoKomb_8cpp" ],
    [ "SgVlbiSessionIoNgs.cpp", "SgVlbiSessionIoNgs_8cpp.html", null ],
    [ "SgVlbiSessionIoSupport.cpp", "SgVlbiSessionIoSupport_8cpp.html", "SgVlbiSessionIoSupport_8cpp" ],
    [ "SgVlbiSessionIoVdbInput.cpp", "SgVlbiSessionIoVdbInput_8cpp.html", null ],
    [ "SgVlbiSessionIoVdbOutput.cpp", "SgVlbiSessionIoVdbOutput_8cpp.html", "SgVlbiSessionIoVdbOutput_8cpp" ],
    [ "SgVlbiSessionUtilities.cpp", "SgVlbiSessionUtilities_8cpp.html", "SgVlbiSessionUtilities_8cpp" ],
    [ "SgVlbiSessionUtils4Automation.cpp", "SgVlbiSessionUtils4Automation_8cpp.html", "SgVlbiSessionUtils4Automation_8cpp" ],
    [ "SgVlbiSessionUtils4LogProc.cpp", "SgVlbiSessionUtils4LogProc_8cpp.html", null ],
    [ "SgVlbiSourceInfo.cpp", "SgVlbiSourceInfo_8cpp.html", null ],
    [ "SgVlbiSourceInfo.h", "SgVlbiSourceInfo_8h.html", "SgVlbiSourceInfo_8h" ],
    [ "SgVlbiStationInfo.cpp", "SgVlbiStationInfo_8cpp.html", null ],
    [ "SgVlbiStationInfo.h", "SgVlbiStationInfo_8h.html", "SgVlbiStationInfo_8h" ],
    [ "SgWrmsable.cpp", "SgWrmsable_8cpp.html", null ],
    [ "SgWrmsable.h", "SgWrmsable_8h.html", "SgWrmsable_8h" ],
    [ "SgZenithDelay.cpp", "SgZenithDelay_8cpp.html", null ],
    [ "SgZenithDelay.h", "SgZenithDelay_8h.html", [
      [ "SgZenithDelay", "classSgZenithDelay.html", "classSgZenithDelay" ],
      [ "SgDryZD_Saastamoinen", "classSgDryZD__Saastamoinen.html", "classSgDryZD__Saastamoinen" ],
      [ "SgWetZD_Saastamoinen", "classSgWetZD__Saastamoinen.html", "classSgWetZD__Saastamoinen" ]
    ] ]
];