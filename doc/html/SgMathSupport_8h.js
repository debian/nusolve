var SgMathSupport_8h =
[
    [ "DAY2SEC", "SgMathSupport_8h.html#a4e1704eb3db922624519ac502604d916", null ],
    [ "DEG2RAD", "SgMathSupport_8h.html#af7e8592d0a634bd3642e9fd508ea8022", null ],
    [ "HR2RAD", "SgMathSupport_8h.html#af722afcbbb082570cb55416b30d26144", null ],
    [ "RAD2DEG", "SgMathSupport_8h.html#ac5a945020d3528355cda82d383676736", null ],
    [ "RAD2HR", "SgMathSupport_8h.html#a23f072b1316bb6950b1bf1a96293e045", null ],
    [ "RAD2MAS", "SgMathSupport_8h.html#a10d3779582c4a6ad502945f76edff439", null ],
    [ "RAD2MS", "SgMathSupport_8h.html#adcb89fdd1f3f1d18477ddd96c5a6810e", null ],
    [ "RAD2SEC", "SgMathSupport_8h.html#ad571838c4e83c54efc21c8a1d3f9e8e1", null ],
    [ "SEC2RAD", "SgMathSupport_8h.html#a1bc7b251bd5f04fbd2b83af52143a1de", null ],
    [ "DIRECTION", "SgMathSupport_8h.html#aa268a41a13430b18e933ed40207178d0", [
      [ "X_AXIS", "SgMathSupport_8h.html#aa268a41a13430b18e933ed40207178d0a93ed1386222f87948b972889ad82cd33", null ],
      [ "VERTICAL", "SgMathSupport_8h.html#aa268a41a13430b18e933ed40207178d0a1a88641fcd39f2ed3e58a18526e97138", null ],
      [ "Y_AXIS", "SgMathSupport_8h.html#aa268a41a13430b18e933ed40207178d0aa1ec9649c0a631b27ad7280af3522dd0", null ],
      [ "EAST", "SgMathSupport_8h.html#aa268a41a13430b18e933ed40207178d0ab5b3793b961949c817c7c0d99c05dad7", null ],
      [ "Z_AXIS", "SgMathSupport_8h.html#aa268a41a13430b18e933ed40207178d0a4b8a53bafca6b43388660925049d734f", null ],
      [ "NORTH", "SgMathSupport_8h.html#aa268a41a13430b18e933ed40207178d0ad0611de6f28d4a9c9eac959f5344698e", null ]
    ] ],
    [ "cpsign", "SgMathSupport_8h.html#ada86803865aaeecf1905b26f919b6af9", null ],
    [ "fft", "SgMathSupport_8h.html#af66a05f3e2ac0af41dec95f3ffdf0db0", null ],
    [ "signum", "SgMathSupport_8h.html#a63d7a809cbb0cc66b7e7f6e05d29b5de", null ],
    [ "swap", "SgMathSupport_8h.html#a1dea510af39cd39c3954a7ab47d4e094", null ],
    [ "m3E", "SgMathSupport_8h.html#a081cb1d1fbc343b6913eccc895eefa14", null ],
    [ "m3Zero", "SgMathSupport_8h.html#a5b19a872f947016bc40e28fee26c5584", null ],
    [ "v3Unit", "SgMathSupport_8h.html#a77e7486689ab6325a062e9ead78faa67", null ],
    [ "v3Zero", "SgMathSupport_8h.html#a899fb6aec0763b51c987d2d77300e3c5", null ]
];