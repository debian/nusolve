var dir_04cf926ea6ea8446634a49a60927be45 =
[
    [ "vgosDbProcLogs.cpp", "vgosDbProcLogs_8cpp.html", "vgosDbProcLogs_8cpp" ],
    [ "vgosDbProcLogs.h", "vgosDbProcLogs_8h.html", "vgosDbProcLogs_8h" ],
    [ "vgosDbProcLogs_resources.cpp", "vgosDbProcLogs__resources_8cpp.html", "vgosDbProcLogs__resources_8cpp" ],
    [ "VpSetup.cpp", "VpSetup_8cpp.html", null ],
    [ "VpSetup.h", "VpSetup_8h.html", [
      [ "VpSetup", "classVpSetup.html", "classVpSetup" ]
    ] ],
    [ "VpStartupWizard.cpp", "VpStartupWizard_8cpp.html", null ],
    [ "VpStartupWizard.h", "VpStartupWizard_8h.html", [
      [ "VpWizardPage", "classVpWizardPage.html", "classVpWizardPage" ],
      [ "VpStartupWizard", "classVpStartupWizard.html", "classVpStartupWizard" ],
      [ "VpDefaultCableCalSignEditor", "classVpDefaultCableCalSignEditor.html", "classVpDefaultCableCalSignEditor" ],
      [ "VpRinexDataEditor", "classVpRinexDataEditor.html", "classVpRinexDataEditor" ]
    ] ],
    [ "VpStartupWizard.moc.cpp", "VpStartupWizard_8moc_8cpp.html", "VpStartupWizard_8moc_8cpp" ],
    [ "VpVersion.cpp", "VpVersion_8cpp.html", "VpVersion_8cpp" ]
];