var classSgIoExtFilterHandler =
[
    [ "SgIoExtFilterHandler", "classSgIoExtFilterHandler.html#a7202db65d9a0aef232152466944caa95", null ],
    [ "SgIoExtFilterHandler", "classSgIoExtFilterHandler.html#a07fc788d8517d01134687e75ba076983", null ],
    [ "~SgIoExtFilterHandler", "classSgIoExtFilterHandler.html#ab2c2b4c461a7416f2e3a6b2b9a06a469", null ],
    [ "addFilter", "classSgIoExtFilterHandler.html#ac3450b62743c532938cb4cdd0b018d3f", null ],
    [ "addFilter", "classSgIoExtFilterHandler.html#a0f4887c6504ea55818c2f357694bd2f6", null ],
    [ "className", "classSgIoExtFilterHandler.html#a3aaddfbc27f01bfaa16d52a6c0292a7c", null ],
    [ "closeFlt", "classSgIoExtFilterHandler.html#adfe63eee40e2640f9e09fdc4a8c54ac1", null ],
    [ "getFilterByExt", "classSgIoExtFilterHandler.html#a0fdf3693b8ba7298cc81c20c09981103", null ],
    [ "lookupFilterByFileName", "classSgIoExtFilterHandler.html#a6b808c3f4d02ddab12276cbb373396c3", null ],
    [ "openFlt", "classSgIoExtFilterHandler.html#af4959a97ce776c609ef838cf4544de23", null ],
    [ "operator=", "classSgIoExtFilterHandler.html#a629539deac20c2bba5b862faa6d4e052", null ],
    [ "removeFilter", "classSgIoExtFilterHandler.html#a6a754e835c9a112fa1eb2149c0f46797", null ],
    [ "removeFilter", "classSgIoExtFilterHandler.html#a78a106e11e33ea17c32776f0258c010c", null ],
    [ "setFilterByExt", "classSgIoExtFilterHandler.html#ab727d65caa56d2d03fcfed67655283c5", null ],
    [ "filterByExt_", "classSgIoExtFilterHandler.html#aad4b08049aa5c637a3fd5ee503a6f0fe", null ]
];