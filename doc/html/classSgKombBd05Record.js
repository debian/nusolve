var classSgKombBd05Record =
[
    [ "SgKombBd05Record", "classSgKombBd05Record.html#a2cc7cf48015a58f7deb24edd83bd38d3", null ],
    [ "~SgKombBd05Record", "classSgKombBd05Record.html#a2411142e41f31cc7edba498b4e89c2d4", null ],
    [ "bandId", "classSgKombBd05Record.html#ae7395a31d5d47498664f2cbcee0dd0be", null ],
    [ "bwsMode", "classSgKombBd05Record.html#ae94bfd7a11d92fd088656ab03c7ae3cf", null ],
    [ "className", "classSgKombBd05Record.html#ab01853c3eef8ef2579d2a5a88f2acca6", null ],
    [ "coarseSearchFringeAmplitude", "classSgKombBd05Record.html#ad66d1581a576983a74b0e6158457d7d9", null ],
    [ "debugReport", "classSgKombBd05Record.html#a79dc3e7ad77b97fe34c87471eeaccae9", null ],
    [ "falseDetectionProbability", "classSgKombBd05Record.html#a875cebb056c53d5f5b2f1f534857f9cf", null ],
    [ "fineSearchFringeAmplitude", "classSgKombBd05Record.html#ac4538e55c5ca9ba6dfe3e06aeb22a483", null ],
    [ "fringeAmpPhase", "classSgKombBd05Record.html#a28b2c295a70765987991669e57d87b24", null ],
    [ "incohFringeAmplitude", "classSgKombBd05Record.html#ad41f787a7cc0ced1c70dccd3f484bd58", null ],
    [ "obsCoarseDelayAtReferEpoch", "classSgKombBd05Record.html#ac48de1c2b730fbd1b293d6b51fa3e976", null ],
    [ "obsCoarseDelayRateResid", "classSgKombBd05Record.html#a2d61117fcf09b20642c59acdca8bcb8c", null ],
    [ "obsCoarseDelayResid", "classSgKombBd05Record.html#a276c479bb5c5db2944b2d68ad94b5806", null ],
    [ "obsCoarseDelaySigma", "classSgKombBd05Record.html#acc28cb902d3ef3ff6e335d74a8a1d905", null ],
    [ "obsDelayAmbiguity", "classSgKombBd05Record.html#a785631a312b704f1fe91bc773818083b", null ],
    [ "obsDelayAtReferEpoch", "classSgKombBd05Record.html#afddf408c34beb3d46855c731b475ddcf", null ],
    [ "obsDelayRateAtReferEpoch", "classSgKombBd05Record.html#aaaf3436d5351dbd759d652eb707936b8", null ],
    [ "obsDelayRateResid", "classSgKombBd05Record.html#a7468a0f3d474b58fb0240e3c7b4cf629", null ],
    [ "obsDelayRateSigma", "classSgKombBd05Record.html#aede6fd56db9067383bfc2d3efd982dbc", null ],
    [ "obsDelayResid", "classSgKombBd05Record.html#a2da53dba806ca5f1dffb1d8bef0ce3a1", null ],
    [ "obsDelaySigma", "classSgKombBd05Record.html#a26c74c309c091695662bfce78d279cdf", null ],
    [ "phaseDelayAtReferEpoch", "classSgKombBd05Record.html#a35f473465c16528c285ce05edb7327d2", null ],
    [ "phaseDelayAtReferEpochMinus_1sec", "classSgKombBd05Record.html#a5de34e15f6128783818188c965a86c77", null ],
    [ "phaseDelayAtReferEpochPlus_1sec", "classSgKombBd05Record.html#a3cbc0c9a7d681c745c6f31a84a3f5480", null ],
    [ "prefix", "classSgKombBd05Record.html#a323204ac3759cf59fda5afab1b242e32", null ],
    [ "snr", "classSgKombBd05Record.html#abfd26fbfc365611cbd1ff1f4642c44f1", null ],
    [ "operator>>", "classSgKombBd05Record.html#a53aea8b0cb6dc3d1e00e93473e944a20", null ],
    [ "bandId_", "classSgKombBd05Record.html#a255ee8402db07c5d38c1b6ca318868bd", null ],
    [ "bwsMode_", "classSgKombBd05Record.html#a04e1612e38c0beb861eff02f0e8a2d4c", null ],
    [ "coarseSearchFringeAmplitude_", "classSgKombBd05Record.html#a3d50b75f3881a3c9f00624b3b92cc2a2", null ],
    [ "falseDetectionProbability_", "classSgKombBd05Record.html#abf117bfce5afad7312c589da3e037a81", null ],
    [ "fineSearchFringeAmplitude_", "classSgKombBd05Record.html#a4b17aeb8ea89148f7ced061d67932498", null ],
    [ "fringeAmpPhase_", "classSgKombBd05Record.html#ad8bddc8567efa3a215d6b86e962fdc04", null ],
    [ "incohFringeAmplitude_", "classSgKombBd05Record.html#a64a88c8ee1c74dcb3d153889ca920f78", null ],
    [ "obsCoarseDelayAtReferEpoch_", "classSgKombBd05Record.html#a0ed7b35198bc4bc79e70b9f2d7c5197e", null ],
    [ "obsCoarseDelayRateResid_", "classSgKombBd05Record.html#adbadd48dbe3d519a0963f28a2c2ac329", null ],
    [ "obsCoarseDelayResid_", "classSgKombBd05Record.html#aba8e6ab97b9844bd2f19be0451b24a46", null ],
    [ "obsCoarseDelaySigma_", "classSgKombBd05Record.html#a54c47ca170535ba8d48a27f52eeee9f5", null ],
    [ "obsDelayAmbiguity_", "classSgKombBd05Record.html#a814bc81bd4dc0b98bbc8a0e94c5bf861", null ],
    [ "obsDelayAtReferEpoch_", "classSgKombBd05Record.html#a208c21732749f0c61e302711eb8946e8", null ],
    [ "obsDelayRateAtReferEpoch_", "classSgKombBd05Record.html#a9efd906e4dd71bfd44803b710688bf29", null ],
    [ "obsDelayRateResid_", "classSgKombBd05Record.html#ac5da63783ef0f60d0fc0fed2fe679cf5", null ],
    [ "obsDelayRateSigma_", "classSgKombBd05Record.html#a33d985a853e2237342dbf752216d4595", null ],
    [ "obsDelayResid_", "classSgKombBd05Record.html#a76ddbb40303db204e731ce23d5ced17e", null ],
    [ "obsDelaySigma_", "classSgKombBd05Record.html#af6678047cc105f7624889475880814a3", null ],
    [ "phaseDelayAtReferEpoch_", "classSgKombBd05Record.html#ade648cad68c72408f8dcddc9269ad5c2", null ],
    [ "phaseDelayAtReferEpochMinus_1sec_", "classSgKombBd05Record.html#a248c01868c7995d22040b63df7f9ba8d", null ],
    [ "phaseDelayAtReferEpochPlus_1sec_", "classSgKombBd05Record.html#a82b19f206fb1c31729b44980061bf39d", null ],
    [ "prefix_", "classSgKombBd05Record.html#a6688e930907bd2e536d5d5daea9d025e", null ],
    [ "snr_", "classSgKombBd05Record.html#a6ec4e4cddcb73c451c46ed5f201c59bb", null ]
];