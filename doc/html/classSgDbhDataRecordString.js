var classSgDbhDataRecordString =
[
    [ "SgDbhDataRecordString", "classSgDbhDataRecordString.html#a2c3934cbd6b7b69a61d2f160789976af", null ],
    [ "SgDbhDataRecordString", "classSgDbhDataRecordString.html#a85ceddc5425be4f638a337a933cae2fa", null ],
    [ "~SgDbhDataRecordString", "classSgDbhDataRecordString.html#ad745a3c8dd65d76bfbb31bcaa0f21628", null ],
    [ "className", "classSgDbhDataRecordString.html#abad2d3bf28636735f622088185aca67e", null ],
    [ "getText", "classSgDbhDataRecordString.html#a6b2ea0e093a058bf7315a67989e46f62", null ],
    [ "getValue", "classSgDbhDataRecordString.html#ac9589aa99c6da12c2774c82db94b9260", null ],
    [ "operator=", "classSgDbhDataRecordString.html#a3893845cfb7ed34cfedd53573704543c", null ],
    [ "readLR", "classSgDbhDataRecordString.html#a991c746b2eff2d5fbf004e7e6339a33e", null ],
    [ "setText", "classSgDbhDataRecordString.html#a543f1c3c8006f9330de4f2517e254f04", null ],
    [ "setValue", "classSgDbhDataRecordString.html#ae0192fe39b20975ff0c6061e057b4541", null ],
    [ "text_", "classSgDbhDataRecordString.html#ad8871a992a4ef7da859c67bad17a56c3", null ]
];