var SgStnLogCollector_8h =
[
    [ "SgCableCalReading", "classSgCableCalReading.html", "classSgCableCalReading" ],
    [ "SgMeteoReading", "classSgMeteoReading.html", "classSgMeteoReading" ],
    [ "SgTraklReading", "classSgTraklReading.html", "classSgTraklReading" ],
    [ "SgTsysReading", "classSgTsysReading.html", "classSgTsysReading" ],
    [ "SgDbbc3TpReading", "classSgDbbc3TpReading.html", "classSgDbbc3TpReading" ],
    [ "TpRecord", "classSgDbbc3TpReading_1_1TpRecord.html", "classSgDbbc3TpReading_1_1TpRecord" ],
    [ "SgPcalReading", "classSgPcalReading.html", "classSgPcalReading" ],
    [ "SgSefdReading", "classSgSefdReading.html", "classSgSefdReading" ],
    [ "SgDot2xpsReading", "classSgDot2xpsReading.html", "classSgDot2xpsReading" ],
    [ "SgOnSourceRecord", "classSgOnSourceRecord.html", "classSgOnSourceRecord" ],
    [ "SgChannelSetup", "classSgChannelSetup.html", "classSgChannelSetup" ],
    [ "SgChannelSkeded", "classSgChannelSkeded.html", "classSgChannelSkeded" ],
    [ "ChanCfg", "classSgChannelSkeded_1_1ChanCfg.html", "classSgChannelSkeded_1_1ChanCfg" ],
    [ "StnCfg", "classSgChannelSkeded_1_1StnCfg.html", "classSgChannelSkeded_1_1StnCfg" ],
    [ "SgStnLogReadings", "classSgStnLogReadings.html", "classSgStnLogReadings" ],
    [ "SgStnLogCollector", "classSgStnLogCollector.html", "classSgStnLogCollector" ],
    [ "Procedure", "classSgStnLogCollector_1_1Procedure.html", "classSgStnLogCollector_1_1Procedure" ],
    [ "SgVlbaLogCollector", "classSgVlbaLogCollector.html", "classSgVlbaLogCollector" ],
    [ "NUM_OF_VLBA_ENTRIES", "SgStnLogCollector_8h.html#a321b7a36041822ec9d42ec91636997f9", null ],
    [ "AntcalOutputData", "SgStnLogCollector_8h.html#ac7cc059582a886f570e15c58176f11e1", [
      [ "AOD_NONE", "SgStnLogCollector_8h.html#ac7cc059582a886f570e15c58176f11e1adcef36692c73783f4439a8cbc44685f0", null ],
      [ "AOD_DATA_ON", "SgStnLogCollector_8h.html#ac7cc059582a886f570e15c58176f11e1aab4c891ccb08af00d9de1b78f5d20ab9", null ],
      [ "AOD_CABLE_SIGN", "SgStnLogCollector_8h.html#ac7cc059582a886f570e15c58176f11e1a4a6d9571aa06213f229ed6571c75b5db", null ],
      [ "AOD_CABLE_CAL", "SgStnLogCollector_8h.html#ac7cc059582a886f570e15c58176f11e1a3756ea5c37e1ea7462fcaf0a85bc28ab", null ],
      [ "AOD_METEO", "SgStnLogCollector_8h.html#ac7cc059582a886f570e15c58176f11e1a811bab4dc3b870806e6803b7989de51d", null ],
      [ "AOD_TSYS", "SgStnLogCollector_8h.html#ac7cc059582a886f570e15c58176f11e1af8fa5f0cbf3d39782c7c5d019b01cd30", null ],
      [ "AOD_TPI", "SgStnLogCollector_8h.html#ac7cc059582a886f570e15c58176f11e1a80692783108bdbda374270477a2b960f", null ],
      [ "AOD_PCAL", "SgStnLogCollector_8h.html#ac7cc059582a886f570e15c58176f11e1a3aa3d8cbcd03bcdeb4c46f52a908b385", null ],
      [ "AOD_FMTGPS", "SgStnLogCollector_8h.html#ac7cc059582a886f570e15c58176f11e1a5376797017a17cdddea19647680a7992", null ],
      [ "AOD_DBBC3TP", "SgStnLogCollector_8h.html#ac7cc059582a886f570e15c58176f11e1a4e99548c9f6eddfaa6c5d86eebd268c0", null ],
      [ "AOD_SEFD", "SgStnLogCollector_8h.html#ac7cc059582a886f570e15c58176f11e1acc7dd58a99fd2408222795e089c40004", null ],
      [ "AOD_TRADITIONAL", "SgStnLogCollector_8h.html#ac7cc059582a886f570e15c58176f11e1a82fc6af61ce7223d84fe9844e705ea6e", null ],
      [ "AOD_NOVEL", "SgStnLogCollector_8h.html#ac7cc059582a886f570e15c58176f11e1a85788ff73ad6c7f17f6486c4aca9a373", null ],
      [ "AOD_ALL", "SgStnLogCollector_8h.html#ac7cc059582a886f570e15c58176f11e1a5e4da7f2c451676238f008471ede5acf", null ]
    ] ]
];