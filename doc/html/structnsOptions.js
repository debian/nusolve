var structnsOptions =
[
    [ "altSetupAppName", "structnsOptions.html#a48755fd0f031357ef3ecdefd4dfa872d", null ],
    [ "altSetupName", "structnsOptions.html#a57a46e0857cb436f8f9743b1868e2339", null ],
    [ "args", "structnsOptions.html#a43e5efc45b40ef0bf7365a370751ad4f", null ],
    [ "databaseName", "structnsOptions.html#ab1f8422716ed5fc344598cee8bfc89f3", null ],
    [ "have2ForceAutomaticProcessing", "structnsOptions.html#a31e4f3931f9a5a63abbed042744685bb", null ],
    [ "have2ForceWizard", "structnsOptions.html#aa16103781c54253f65f98d8ba8721f32", null ],
    [ "have2LoadImmatureSession", "structnsOptions.html#a039c74918f14f2e728f66bfb3fadaa42", null ],
    [ "have2SkipAutomaticProcessing", "structnsOptions.html#a225fe8e2226d87a6276b1b7df0ca657d", null ],
    [ "have2UseAltSetup", "structnsOptions.html#a83c48a5db5f8379a2e223f5ac29b663f", null ],
    [ "isForcedCatalogMode", "structnsOptions.html#a53ed392234a0abfccd401ad16910b5dd", null ],
    [ "isForcedStandaloneMode", "structnsOptions.html#a99fff2d4224ee8338ad5e1835da98a41", null ],
    [ "isNeedSignalHandler", "structnsOptions.html#a8bb82637c7826ba8da80c4128a4a3168", null ],
    [ "ofFmt", "structnsOptions.html#a64c1379a8694baa0456ae23aa05f409e", null ],
    [ "scriptFileName", "structnsOptions.html#aa2bd12ec4050b2ecb7e691d4d71be1e7", null ],
    [ "settings", "structnsOptions.html#a517cfd8a5b434b786d92b5d488f1bf8e", null ],
    [ "shouldInvokeSystemWideWizard", "structnsOptions.html#a3ea3c9b472bc24c029af0936a6931e74", null ],
    [ "useDefaultSetup", "structnsOptions.html#af662e0bcc571b030b326a1b9376dacdf", null ]
];