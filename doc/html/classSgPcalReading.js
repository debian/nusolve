var classSgPcalReading =
[
    [ "SgPcalReading", "classSgPcalReading.html#aa059af1429bb23ef3d79a0400f179bd9", null ],
    [ "SgPcalReading", "classSgPcalReading.html#a6db4e33b27f982402cafb02f9596abc9", null ],
    [ "~SgPcalReading", "classSgPcalReading.html#a8e8787204c3e7bca82b76ec4f9cdacb6", null ],
    [ "addPcals", "classSgPcalReading.html#aeff8cbd985eb846f0a0dc425856d0402", null ],
    [ "className", "classSgPcalReading.html#aa7ef7272cf05b4950c74109bc1d256ea", null ],
    [ "getIsOk", "classSgPcalReading.html#ae362fbbf23f42e8884187142b1665936", null ],
    [ "getOsRec", "classSgPcalReading.html#a5bf2fbfafec309a3b7fa4b5ad3be246a", null ],
    [ "getPcal", "classSgPcalReading.html#a9ffb31b09e0470e9bf03fe644bab4c8e", null ],
    [ "getT", "classSgPcalReading.html#ad3c34234fa4549f1697b15e14a55ac23", null ],
    [ "osRec", "classSgPcalReading.html#a5aea0609c356e1df69f5fdf820429339", null ],
    [ "pcal", "classSgPcalReading.html#aae6bcd5d406ffbdfe122760a2e209c98", null ],
    [ "setIsOk", "classSgPcalReading.html#ac0d67beddda1ee8e5ae4e85b8ca2b439", null ],
    [ "setOsRec", "classSgPcalReading.html#a990f3c83ef07599b3145e6b8a5b1a9b5", null ],
    [ "setT", "classSgPcalReading.html#a89414e8851f8eaac8518e46aa1c1d1fc", null ],
    [ "isOk_", "classSgPcalReading.html#afe6d1b6d08a13fdcac44586648ed12b8", null ],
    [ "osRec_", "classSgPcalReading.html#a9b1304de70e5b89c99a679d833dfc451", null ],
    [ "pcal_", "classSgPcalReading.html#afe646544db27863ef0173483d4b63db0", null ],
    [ "t_", "classSgPcalReading.html#aba4c70fa864fc1446681b34f6db89f81", null ]
];