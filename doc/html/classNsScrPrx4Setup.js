var classNsScrPrx4Setup =
[
    [ "NsScrPrx4Setup", "classNsScrPrx4Setup.html#a3d560923b17bda70d56c6f9d709a7d55", null ],
    [ "~NsScrPrx4Setup", "classNsScrPrx4Setup.html#a83d1fd89442fd8e95ce5264df5e2485c", null ],
    [ "getHave2AutoloadAllBands", "classNsScrPrx4Setup.html#a69fe086b7e7b3d9d6f33db9be040021e", null ],
    [ "getHave2KeepSpoolFileReports", "classNsScrPrx4Setup.html#a4fb2c0a9a5bdc0ebeebccba100471ada", null ],
    [ "getHave2LoadImmatureSession", "classNsScrPrx4Setup.html#a6c8c9b54f5bb25fcfc4374cef2e7f152", null ],
    [ "getHave2MaskSessionCode", "classNsScrPrx4Setup.html#a37864b5e3ea75fbc8242f8b7e7d0e240", null ],
    [ "getHave2SavePerSessionLog", "classNsScrPrx4Setup.html#a74415168eb3f4018daeb84e7645b25d0", null ],
    [ "getHave2UpdateCatalog", "classNsScrPrx4Setup.html#a4acd3ca15c772d4e17a4628281eb24a4", null ],
    [ "getPath2APrioriFiles", "classNsScrPrx4Setup.html#a9883c06856642bc8d8a37c54c87ae101", null ],
    [ "getPath2AuxLogs", "classNsScrPrx4Setup.html#a4dd1b2e3208f6bd111833fc576f10cc0", null ],
    [ "getPath2CatNuInterfaceExec", "classNsScrPrx4Setup.html#a6fb36c20374d0082e18977bddd9e1b04", null ],
    [ "getPath2DbhFiles", "classNsScrPrx4Setup.html#af25c6f99bc370624b3a9feaf36ab504a", null ],
    [ "getPath2Home", "classNsScrPrx4Setup.html#a10430d2fb670542c7cc40fd9be8f665a", null ],
    [ "getPath2MasterFiles", "classNsScrPrx4Setup.html#aa18684edfd5838021dc1f7d1eee46fcb", null ],
    [ "getPath2NgsOutput", "classNsScrPrx4Setup.html#a2188ef32a4a2eceac6d8691df292d4d3", null ],
    [ "getPath2NotUsedObsFileOutput", "classNsScrPrx4Setup.html#a9ab50a9ecbab5e40a06f897530a27b5d", null ],
    [ "getPath2PlotterOutput", "classNsScrPrx4Setup.html#a284ee18d39acb715bf7c0c5389a2f999", null ],
    [ "getPath2ReportOutput", "classNsScrPrx4Setup.html#a435153edb7890c6cea1b9474406937bc", null ],
    [ "getPath2SpoolFileOutput", "classNsScrPrx4Setup.html#a183d06424d8198c120032462c9cf399c", null ],
    [ "getPath2VgosDaFiles", "classNsScrPrx4Setup.html#a071098bae7f30b8d4fbc8bb8182a25a5", null ],
    [ "getPath2VgosDbFiles", "classNsScrPrx4Setup.html#a9eaf5b3ead5776b23305a9567c3d0151", null ],
    [ "getPwd", "classNsScrPrx4Setup.html#acd9c8717c0227569f0fbefbdafdb7dc8", null ],
    [ "setHave2AutoloadAllBands", "classNsScrPrx4Setup.html#a31ff6e7426dab7ef5ab9ab532133b16a", null ],
    [ "setHave2KeepSpoolFileReports", "classNsScrPrx4Setup.html#a6fb49a9e8c92fbe01c7d4599cd942708", null ],
    [ "setHave2LoadImmatureSession", "classNsScrPrx4Setup.html#a9166875d393648d1c6533ba8038e6cd7", null ],
    [ "setHave2MaskSessionCode", "classNsScrPrx4Setup.html#aac7024d2c44814e6460f1d1c2c443b54", null ],
    [ "setHave2SavePerSessionLog", "classNsScrPrx4Setup.html#a17cedac481025d5575dd0f8db8f2e614", null ],
    [ "setHave2UpdateCatalog", "classNsScrPrx4Setup.html#aa241963b7faf823228284acf917c8612", null ],
    [ "setPath2APrioriFiles", "classNsScrPrx4Setup.html#ab7cf42dd78ebf0c92c30d8d131df8748", null ],
    [ "setPath2AuxLogs", "classNsScrPrx4Setup.html#abdfa84af22eba0d4a9652b0fda24af4a", null ],
    [ "setPath2CatNuInterfaceExec", "classNsScrPrx4Setup.html#a511c2170189571d78af4c039dc540797", null ],
    [ "setPath2DbhFiles", "classNsScrPrx4Setup.html#aaa94fe1625404adeeae2cf0d54e5605e", null ],
    [ "setPath2Home", "classNsScrPrx4Setup.html#a375440257d23317277cf60376eb38223", null ],
    [ "setPath2MasterFiles", "classNsScrPrx4Setup.html#ac0ad172eb34b722ee95b7aa461e5951d", null ],
    [ "setPath2NgsOutput", "classNsScrPrx4Setup.html#a9f18d33ff7a0b65feb024c1ac80268ec", null ],
    [ "setPath2NotUsedObsFileOutput", "classNsScrPrx4Setup.html#a4580ea30bf4ad31b30a7c713aa82634a", null ],
    [ "setPath2PlotterOutput", "classNsScrPrx4Setup.html#adf7b2d0f89a4f859dd7e7df4c1b3b504", null ],
    [ "setPath2ReportOutput", "classNsScrPrx4Setup.html#a9ae13cd143ed631d38fa106044a31489", null ],
    [ "setPath2SpoolFileOutput", "classNsScrPrx4Setup.html#a37075db4706104e0e156ccaaad7eaa40", null ],
    [ "setPath2VgosDaFiles", "classNsScrPrx4Setup.html#a97f1b4150f136d6bcc627c7609cbfdaf", null ],
    [ "setPath2VgosDbFiles", "classNsScrPrx4Setup.html#a1c8e14bed16baa4570848ab78d4d1d84", null ],
    [ "setup_", "classNsScrPrx4Setup.html#abc26e370582f55592bed6c21402ab151", null ],
    [ "have2AutoloadAllBands", "classNsScrPrx4Setup.html#ad359ae62afb92744e7bcc4e14f277b82", null ],
    [ "have2KeepSpoolFileReports", "classNsScrPrx4Setup.html#a0b51f0691f63be782030021090a7202d", null ],
    [ "have2LoadImmatureSession", "classNsScrPrx4Setup.html#a7565407411a04929b51e753806df926a", null ],
    [ "have2MaskSessionCode", "classNsScrPrx4Setup.html#a0469dda37030a3dc5e562e55178636eb", null ],
    [ "have2SavePerSessionLog", "classNsScrPrx4Setup.html#a68d13fb5211cb25764cb9195c5806be8", null ],
    [ "have2UpdateCatalog", "classNsScrPrx4Setup.html#ac3f638d628441b00faf5abcc6331e1a8", null ],
    [ "path2APrioriFiles", "classNsScrPrx4Setup.html#a3b2400447fef2b89643d636212deff6a", null ],
    [ "path2AuxLogs", "classNsScrPrx4Setup.html#a06b9466090917c97653cb22978494bca", null ],
    [ "path2CatNuInterfaceExec", "classNsScrPrx4Setup.html#a20079aee3bf6c88fbb9be3ffd37d2fe2", null ],
    [ "path2DbhFiles", "classNsScrPrx4Setup.html#a7c33dc7c918f56571fe217b7cf05ca1f", null ],
    [ "path2Home", "classNsScrPrx4Setup.html#a006f1c32be8778dfbe2e030709e799f0", null ],
    [ "path2MasterFiles", "classNsScrPrx4Setup.html#aea425dab903d423d68909e63ea843e86", null ],
    [ "path2NgsOutput", "classNsScrPrx4Setup.html#a14277e64b2df0c8966d0196ad6bfd531", null ],
    [ "path2NotUsedObsFileOutput", "classNsScrPrx4Setup.html#a56f1ac8a5d52ed0cdd0c56d14633109c", null ],
    [ "path2PlotterOutput", "classNsScrPrx4Setup.html#a3cd1ef5397af0e8bff4e89704a9e9247", null ],
    [ "path2ReportOutput", "classNsScrPrx4Setup.html#afb303e0f6bfd196948c7a073f53fa5d9", null ],
    [ "path2SpoolFileOutput", "classNsScrPrx4Setup.html#a7f88a98a681ffa3f0b725b9d576b09a4", null ],
    [ "path2VgosDaFiles", "classNsScrPrx4Setup.html#a9643d8d80953ddeb85cc3adae07b763a", null ],
    [ "path2VgosDbFiles", "classNsScrPrx4Setup.html#a6095745e4c7d55af927eaeae0aed3bfd", null ],
    [ "pwd", "classNsScrPrx4Setup.html#ac6ec5abee20e38b1a23482e99c96f21a", null ]
];