var structSgVgosDb_1_1ProgramSolveDescriptor =
[
    [ "ProgramSolveDescriptor", "structSgVgosDb_1_1ProgramSolveDescriptor.html#a21d184a06b7c668e64154fb05123d7e6", null ],
    [ "hasSomething4output", "structSgVgosDb_1_1ProgramSolveDescriptor.html#ab16283ff64f788d393ce0763d5f68575", null ],
    [ "programName_", "structSgVgosDb_1_1ProgramSolveDescriptor.html#aa268b10d004ccd0311ede97840e7ddd1", null ],
    [ "vAtmSetup_", "structSgVgosDb_1_1ProgramSolveDescriptor.html#aff208612d188e8fb8aa8bf2ef2bc68b7", null ],
    [ "vBaselineClockSetup_", "structSgVgosDb_1_1ProgramSolveDescriptor.html#ad33e8eed46418bd48045699e88f3b527", null ],
    [ "vCalcErp_", "structSgVgosDb_1_1ProgramSolveDescriptor.html#a5b4e007079a01da1ed981b506ea50886", null ],
    [ "vCalcInfo_", "structSgVgosDb_1_1ProgramSolveDescriptor.html#a5e4b2f4c23eb10ce89ae3d584a245b2b", null ],
    [ "vCalibrationSetup_", "structSgVgosDb_1_1ProgramSolveDescriptor.html#a37a05ab68133c3bc90b504fed5c3e922", null ],
    [ "vClockSetup_", "structSgVgosDb_1_1ProgramSolveDescriptor.html#a741b3e4d2c3e046407e7aab56c296655", null ],
    [ "vdbVars_", "structSgVgosDb_1_1ProgramSolveDescriptor.html#a33283ffdd348be86a3ee663b56233216", null ],
    [ "vErpSetup_", "structSgVgosDb_1_1ProgramSolveDescriptor.html#aee348123d8f4b4ead4ae60101edaec46", null ],
    [ "vFractC_", "structSgVgosDb_1_1ProgramSolveDescriptor.html#a246504c083a14f75c13fa412f9dbbf9c", null ],
    [ "vIonoBits_", "structSgVgosDb_1_1ProgramSolveDescriptor.html#a8aca16060b5af47ea40704f80c05fd9c", null ],
    [ "vIonoSetup_", "structSgVgosDb_1_1ProgramSolveDescriptor.html#a36519a6f0a32f689a27f97ab0238384a", null ],
    [ "vScanTimeMJD_", "structSgVgosDb_1_1ProgramSolveDescriptor.html#a6ba090f35ec0b09a69829191c757395a", null ],
    [ "vSelectionStatus_", "structSgVgosDb_1_1ProgramSolveDescriptor.html#a9af361a7f933881be01803e87c1b1664", null ],
    [ "vUnPhaseCalFlag_", "structSgVgosDb_1_1ProgramSolveDescriptor.html#aa6fdee719e7d76c85b071483c1e482ff", null ],
    [ "vUserSup_", "structSgVgosDb_1_1ProgramSolveDescriptor.html#ad6323ec3f41cddc3c494c5f83959ceec", null ]
];