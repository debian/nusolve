var classSgVexFile_1_1BbcSetup =
[
    [ "BbcSetup", "classSgVexFile_1_1BbcSetup.html#ace0cdea3d29aa1bc2757750c1bdf6d9f", null ],
    [ "BbcSetup", "classSgVexFile_1_1BbcSetup.html#a6b70dbf317302e1a827c024b28b30bb6", null ],
    [ "BbcSetup", "classSgVexFile_1_1BbcSetup.html#a9eeb1ecbbb79581f1ca841d0f4039e60", null ],
    [ "~BbcSetup", "classSgVexFile_1_1BbcSetup.html#a1fe5fbd3c593ba7b6eb63fd2427f8078", null ],
    [ "getBbcId", "classSgVexFile_1_1BbcSetup.html#a14ee5ff30068b2f601732d8241d80ea7", null ],
    [ "getIfId", "classSgVexFile_1_1BbcSetup.html#ac2a076a21b99bc0a59a318bd27f2bffb", null ],
    [ "getPhysNumber", "classSgVexFile_1_1BbcSetup.html#a526bc3cb5c4be438b24c9ee89700ed89", null ],
    [ "setBbcId", "classSgVexFile_1_1BbcSetup.html#aab498569de31412bc3d37e3b85711b5b", null ],
    [ "setIfId", "classSgVexFile_1_1BbcSetup.html#ae63b8524a1774f040bbaf25ba52542d7", null ],
    [ "setPhysNumber", "classSgVexFile_1_1BbcSetup.html#af7bc749b9e922f66fad0399b997bd68e", null ],
    [ "bbcId_", "classSgVexFile_1_1BbcSetup.html#a6f8369d8707137a4627e930c3e5358a1", null ],
    [ "ifId_", "classSgVexFile_1_1BbcSetup.html#ae94937d3d14079a448c19ce6314b1bb5", null ],
    [ "physNumber_", "classSgVexFile_1_1BbcSetup.html#aeb860c76af1bf3bb97a4c5583217def0", null ]
];