var classSgDbhImage =
[
    [ "ContentState", "classSgDbhImage.html#ac0f0fa84027ae4da11d88a9f5d0388e2", [
      [ "CS_Bare", "classSgDbhImage.html#ac0f0fa84027ae4da11d88a9f5d0388e2aac10bce2c394e858f2279e73a83d2454", null ],
      [ "CS_DataFromFile", "classSgDbhImage.html#ac0f0fa84027ae4da11d88a9f5d0388e2a2757949c0ec2eee7851626640a44b7fe", null ],
      [ "CS_DataAltered", "classSgDbhImage.html#ac0f0fa84027ae4da11d88a9f5d0388e2a11becc1fbe71ad348e5f29b41ce25196", null ]
    ] ],
    [ "FormatState", "classSgDbhImage.html#abea82f84081284845392f85b2166cb5b", [
      [ "FS_Bare", "classSgDbhImage.html#abea82f84081284845392f85b2166cb5ba9fabaa63e3fd4958274171b613429d09", null ],
      [ "FS_FormatFromFile", "classSgDbhImage.html#abea82f84081284845392f85b2166cb5ba3ec035a3a9b1698b48b5745d0fd0a065", null ],
      [ "FS_FormatAltering", "classSgDbhImage.html#abea82f84081284845392f85b2166cb5badf3ee25a653f06ee366410cc0006f94d", null ],
      [ "FS_FormatAltered", "classSgDbhImage.html#abea82f84081284845392f85b2166cb5ba09543517930e3b9e00ea3c54e13539fc", null ]
    ] ],
    [ "SgDbhImage", "classSgDbhImage.html#ad7cf66e2a15497fcbc965914dca52b1d", null ],
    [ "~SgDbhImage", "classSgDbhImage.html#a5a1e2ec21145c7d792a0ab8d3ec58a4f", null ],
    [ "addDescriptor", "classSgDbhImage.html#ad0c88e8a8c38b6ae7df48ca707f5e367", null ],
    [ "addHistoryEntry", "classSgDbhImage.html#a44b2b926eb085dd3c182ef4a03e68fcf", null ],
    [ "alterCode", "classSgDbhImage.html#a5beef677c8c11b72f7b4826907b42a43", null ],
    [ "className", "classSgDbhImage.html#affdb0226e2e5a08cf3d9386d116cea22", null ],
    [ "clearHistoryList", "classSgDbhImage.html#abb7af3db130b3d9bf2668f271ec7cf03", null ],
    [ "clearListOfObservations", "classSgDbhImage.html#aabf55c50807103e70699549e6a4c6b6b", null ],
    [ "contentState", "classSgDbhImage.html#a1f2d9319af4922b6cfd3342a298cbaf9", null ],
    [ "copyToc0Content", "classSgDbhImage.html#a9538734ea103b3d8b191e033d60ba055", null ],
    [ "currentVersion", "classSgDbhImage.html#a1fe6de7879710cb5bae111ef42fb42ca", null ],
    [ "delDescriptor", "classSgDbhImage.html#ab3005357180b69a6fd38d411bcafbccf", null ],
    [ "dumpFormat", "classSgDbhImage.html#a7db89446f67e9dc060e7417cd743e962", null ],
    [ "fileCreationEpoch", "classSgDbhImage.html#a77757aded5e3c011b82d10487e7a5b48", null ],
    [ "fileName", "classSgDbhImage.html#a05a77bf8233a995a653422db1916fc25", null ],
    [ "fileVersion", "classSgDbhImage.html#a09e66a4fa8e513043d6b7f68bf871c2d", null ],
    [ "finisFormatModifying", "classSgDbhImage.html#a9ad895a44947742a1b0a437e73c1c1a9", null ],
    [ "formatState", "classSgDbhImage.html#a4cfa1442a9ed522aa0cefe55255d4b0c", null ],
    [ "getD8", "classSgDbhImage.html#a4d7bf15c5a3d7a78ee2d5475a6fe3cfe", null ],
    [ "getData", "classSgDbhImage.html#a39c1220b608db2f0a0fd5f51956f01e6", null ],
    [ "getDumpStream", "classSgDbhImage.html#a79afc6e9d16dafe893f7dd73dd0b8337", null ],
    [ "getI2", "classSgDbhImage.html#a35137e6e36bd86040e1b0ebdffa889f8", null ],
    [ "getJ4", "classSgDbhImage.html#af23cbc6c14bfcf0e43d3935cefec6577", null ],
    [ "getR8", "classSgDbhImage.html#a350a798dfc7fe5bd18b376f5ce785e85", null ],
    [ "getStr", "classSgDbhImage.html#af09f6ade04281f618e6b503ea87ddbeb", null ],
    [ "historyList", "classSgDbhImage.html#a9fe5802cbc65fb6e733d2cddb0230156", null ],
    [ "lookupDescriptor", "classSgDbhImage.html#ac5ad77c8b935a3b915d4710ceebc28f8", null ],
    [ "numberOfObservations", "classSgDbhImage.html#ac2fea9ce42d3b170be44c83ec3e7021a", null ],
    [ "prepare2save", "classSgDbhImage.html#af606ce7cba15763185157cbe0dfd1987", null ],
    [ "previousFileDescription", "classSgDbhImage.html#aeed174c00760cb526d9860430b3b983e", null ],
    [ "previousFileName", "classSgDbhImage.html#aacfb39fe76458c300ff3c31be9996f9d", null ],
    [ "properRecord", "classSgDbhImage.html#acac1e3af8aa36e796b9f49a046031e6e", null ],
    [ "sessionDescription", "classSgDbhImage.html#a9adf980f65d9f9ac8d513a9663ffac1f", null ],
    [ "sessionID", "classSgDbhImage.html#a12e4118e74d867e5b60f52b059ab0030", null ],
    [ "setD8", "classSgDbhImage.html#a84a5f3a7954b637d5c83077675daaa5d", null ],
    [ "setData", "classSgDbhImage.html#a65710a691b4910c3e9c5e5f7ec32e73d", null ],
    [ "setDumpStream", "classSgDbhImage.html#accdb5c2b92f9638ad4bfbc3bbefd95f3", null ],
    [ "setI2", "classSgDbhImage.html#a461889f0557c4c2ef87f273b0bc2b2ff", null ],
    [ "setJ4", "classSgDbhImage.html#a5e564cdf803c431e16c53072ea522560", null ],
    [ "setR8", "classSgDbhImage.html#ac2f30cde826a74b88eef1a14af84a078", null ],
    [ "setStr", "classSgDbhImage.html#a9455ed8a7828231e0dac1862c9e8358e", null ],
    [ "startFormatModifying", "classSgDbhImage.html#a2e72b5327c040e688488d898cef41205", null ],
    [ "updateDataRecords", "classSgDbhImage.html#acf2a5a20bf02e543e0a890cca8bdb56c", null ],
    [ "updateDescriptorsParameters", "classSgDbhImage.html#a099eb51181ee6ac0148da12316f8a430", null ],
    [ "writeDataRecords", "classSgDbhImage.html#a0117509125c7ceb9f440a70bfd2ed26d", null ],
    [ "operator<<", "classSgDbhImage.html#a3c6baa38d4450939d7a97c57b6a6615f", null ],
    [ "operator>>", "classSgDbhImage.html#a029af82591f41b43c4ac179c4e02c1ca", null ],
    [ "alterCode_", "classSgDbhImage.html#a76d1acda4d11c1c5d46318a0720ba471", null ],
    [ "canonicalName_", "classSgDbhImage.html#a1c6a02548211936ccfb9fef072ef1e00", null ],
    [ "contentState_", "classSgDbhImage.html#a68acbaa6098cb6bc9442e32bb1cfb1c6", null ],
    [ "currentVersion_", "classSgDbhImage.html#adcdaaab74588bd5a6c0b8ef69935d494", null ],
    [ "descriptorByLCode_", "classSgDbhImage.html#a9dcd15527c477b604c05455f7f74dcc0", null ],
    [ "dumpStream_", "classSgDbhImage.html#aa2f29ec85e67360b1c34d473a6f673e6", null ],
    [ "format_", "classSgDbhImage.html#ae182845803df1491b265059dee463c61", null ],
    [ "formatState_", "classSgDbhImage.html#a0631ea58743d30673965a642766f6a17", null ],
    [ "history_", "classSgDbhImage.html#a0d57e3d44a5f157f5425d778599ba885", null ],
    [ "isSessionCodeAltered_", "classSgDbhImage.html#aee5d10b95dfd6180db7cc287c38054ba", null ],
    [ "listOfDataBlocksToc0_", "classSgDbhImage.html#a36de4b59a456c62f7e8c5ebc269d67e2", null ],
    [ "listOfDeletedDescriptors_", "classSgDbhImage.html#a9aaf96923e5df044973f25e7bf144e58", null ],
    [ "listOfNewDescriptors_", "classSgDbhImage.html#a77ab77708a3b81d7f57b3293bcba0792", null ],
    [ "listOfObservations_", "classSgDbhImage.html#ac1a8a0a82e4e396eebb318770a89952a", null ],
    [ "startBlock_", "classSgDbhImage.html#a79fc5d492fab6e9f6ba405cc4fb2e881", null ]
];