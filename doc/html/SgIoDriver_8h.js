var SgIoDriver_8h =
[
    [ "SgIoDriver", "classSgIoDriver.html", "classSgIoDriver" ],
    [ "SpecDims", "SgIoDriver_8h.html#ad46d6b2cfde4d87fb93df6486db4f2f3", [
      [ "SD_NumObs", "SgIoDriver_8h.html#ad46d6b2cfde4d87fb93df6486db4f2f3a4b924fecd0a6e8577ee3645bffe7a144", null ],
      [ "SD_NumScans", "SgIoDriver_8h.html#ad46d6b2cfde4d87fb93df6486db4f2f3a7656fe4deb411995294dc24f7cadd77d", null ],
      [ "SD_NumChans", "SgIoDriver_8h.html#ad46d6b2cfde4d87fb93df6486db4f2f3a3366c5c6cad4afacc49d1ce0b6e97bcd", null ],
      [ "SD_NumStnPts", "SgIoDriver_8h.html#ad46d6b2cfde4d87fb93df6486db4f2f3aa576fb6ee10474d70a27232f875d7fd0", null ],
      [ "SD_NumSrc", "SgIoDriver_8h.html#ad46d6b2cfde4d87fb93df6486db4f2f3af732ec943fd1ae2a469b52e2922a362d", null ],
      [ "SD_NumStn", "SgIoDriver_8h.html#ad46d6b2cfde4d87fb93df6486db4f2f3a4649d32058fac715b5bd995cbea4b939", null ],
      [ "SD_2NumChans", "SgIoDriver_8h.html#ad46d6b2cfde4d87fb93df6486db4f2f3a4658b16d46c7d7b00df9d6f0e2b9c17e", null ],
      [ "SD_NumBands", "SgIoDriver_8h.html#ad46d6b2cfde4d87fb93df6486db4f2f3afa6e59d2dc39b2c0270ad54252116c7a", null ],
      [ "SD_NumBln", "SgIoDriver_8h.html#ad46d6b2cfde4d87fb93df6486db4f2f3a14e992ce38b196c422673b7d544ec62a", null ],
      [ "SD_Any", "SgIoDriver_8h.html#ad46d6b2cfde4d87fb93df6486db4f2f3aef2b96dd374c819fa13d9d81696999c5", null ]
    ] ]
];