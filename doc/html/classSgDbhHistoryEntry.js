var classSgDbhHistoryEntry =
[
    [ "SgDbhHistoryEntry", "classSgDbhHistoryEntry.html#af1899225151a462cd0dfb5aabbaa3814", null ],
    [ "~SgDbhHistoryEntry", "classSgDbhHistoryEntry.html#aa6d2912fc1bb4d6b1eb690fa6c07385b", null ],
    [ "className", "classSgDbhHistoryEntry.html#a4a74584e0fe7491d22e0d76ca329f236", null ],
    [ "dump", "classSgDbhHistoryEntry.html#aa63bd27c2fb7f04c6091ca34dc5f7721", null ],
    [ "getEpoch", "classSgDbhHistoryEntry.html#ad0a4483170e94378c603a977fc25f6e3", null ],
    [ "getText", "classSgDbhHistoryEntry.html#af75bcd441c90d7ea24fb5feb3b0d1c53", null ],
    [ "getVersion", "classSgDbhHistoryEntry.html#a98a39595139861b5b9aba620100470ba", null ],
    [ "isHistoryLine", "classSgDbhHistoryEntry.html#ab591ac964119ae0c9b66881a4aab5304", null ],
    [ "isLast", "classSgDbhHistoryEntry.html#ace0711a074b9fea5f7def59b87c70c72", null ],
    [ "isOk", "classSgDbhHistoryEntry.html#a79c147eb6442fa2745c8e62f156710c6", null ],
    [ "setEpoch", "classSgDbhHistoryEntry.html#a7b7b60ca5dfc18867f99182719fa0e9c", null ],
    [ "setEvent", "classSgDbhHistoryEntry.html#a2f4e72ebdcb9ac82fce7c853671f7006", null ],
    [ "setText", "classSgDbhHistoryEntry.html#ab7240055170988a39c3dc31db7abc0c7", null ],
    [ "setVersion", "classSgDbhHistoryEntry.html#ad49a1f928f7816f914411218e997398e", null ],
    [ "unsetOkFlag", "classSgDbhHistoryEntry.html#a04460849076396e20927145a00bfec04", null ],
    [ "operator<<", "classSgDbhHistoryEntry.html#a7621dfcbcfa5851b84c4066780a92b30", null ],
    [ "operator>>", "classSgDbhHistoryEntry.html#a68869aaade4b30b4a2f1a18c253cd419", null ],
    [ "isOK_", "classSgDbhHistoryEntry.html#ac58a66aa3325e872eb9c3a85039d96ab", null ],
    [ "record1_", "classSgDbhHistoryEntry.html#af5f43d61d5a7d549d3b21ab89a48eb19", null ],
    [ "record2_", "classSgDbhHistoryEntry.html#a89b9b5fff5fe5f75be442d678ed65695", null ],
    [ "record3_", "classSgDbhHistoryEntry.html#a6ba155242d9bfd6c16ae078cf5a5c484", null ]
];