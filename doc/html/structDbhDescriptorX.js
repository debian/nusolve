var structDbhDescriptorX =
[
    [ "d_", "structDbhDescriptorX.html#aa1e95030c2661ea9523b45248eddfdc7", null ],
    [ "description_", "structDbhDescriptorX.html#a9ffe4912e2262434c24c9b1fa77f7d2e", null ],
    [ "dimensions_", "structDbhDescriptorX.html#a5580d895d60e1a63e13e06e46c703b33", null ],
    [ "expectedVersion_", "structDbhDescriptorX.html#a81b444489a6f14a4c9307de615481a2b", null ],
    [ "isDimensionsFixed_", "structDbhDescriptorX.html#a3f0aabd034c63b40475d7ecec0942581", null ],
    [ "isMandatory_", "structDbhDescriptorX.html#a672444d85c3e2d5dd2305f44e9e89b7b", null ],
    [ "isPresent_", "structDbhDescriptorX.html#a5f613dfe4528e67afc2b5898a70deb7d", null ],
    [ "lCode_", "structDbhDescriptorX.html#a0d389329c7435d73a911bd57bd774bca", null ],
    [ "numOfTc_", "structDbhDescriptorX.html#afd0d1f28d873ca671e871cf9b20d3958", null ],
    [ "type_", "structDbhDescriptorX.html#ad987163ebdf6d4bc396b160b8edbe263", null ]
];