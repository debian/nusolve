var classSgDbhServiceRecord =
[
    [ "SgDbhServiceRecord", "classSgDbhServiceRecord.html#a639f7ef5c9753d083175f1ec8fa994c2", null ],
    [ "SgDbhServiceRecord", "classSgDbhServiceRecord.html#ab843cff9b2fd3da7a30aab961d797952", null ],
    [ "~SgDbhServiceRecord", "classSgDbhServiceRecord.html#ad062173ed41a7a1c062d83ffae4ea189", null ],
    [ "className", "classSgDbhServiceRecord.html#aef0d6a8d69b79e2d38874d1faac5f2aa", null ],
    [ "isAltered", "classSgDbhServiceRecord.html#ad598648962ce6ef3ecef651398ae4134", null ],
    [ "isCorrectPrefix", "classSgDbhServiceRecord.html#a558eacaaa90e8ee6899ea31f7476d139", null ],
    [ "isPrefixParsed", "classSgDbhServiceRecord.html#adba0bf1d93920bf631666b7af87420a4", null ],
    [ "operator=", "classSgDbhServiceRecord.html#a15776824aec067230cb79adbfabf3a57", null ],
    [ "readLR", "classSgDbhServiceRecord.html#af85afd232003a676baeb560620f38d66", null ],
    [ "setAltered", "classSgDbhServiceRecord.html#a8ed7c96635051450de64a8c80ba57b38", null ],
    [ "setPrefix", "classSgDbhServiceRecord.html#a9f35c556a2506ff28d576168430848c7", null ],
    [ "writeLR", "classSgDbhServiceRecord.html#a0c0c2a830fa2758d9710f78b285e8365", null ],
    [ "expectedPrefix_", "classSgDbhServiceRecord.html#a1fc82ddfad83740662284ada96b9b3e2", null ],
    [ "prefix_", "classSgDbhServiceRecord.html#ad5eb27e5219ea6410c688c5cc3c90447", null ]
];