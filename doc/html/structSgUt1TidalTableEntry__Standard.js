var structSgUt1TidalTableEntry__Standard =
[
    [ "lod_cos_", "structSgUt1TidalTableEntry__Standard.html#aba5bf75456e62f652975674b6fde6f9c", null ],
    [ "lod_sin_", "structSgUt1TidalTableEntry__Standard.html#a8f1e94a8da3afc6dbe984a4af70d28d8", null ],
    [ "n_", "structSgUt1TidalTableEntry__Standard.html#a064a3d85d787daced6e647d241dfe5a9", null ],
    [ "omg_cos_", "structSgUt1TidalTableEntry__Standard.html#a9df4be9bf52a06e24007a168eed199d4", null ],
    [ "omg_sin_", "structSgUt1TidalTableEntry__Standard.html#ae88b7d9d412840209867bad3d583d416", null ],
    [ "ut1_cos_", "structSgUt1TidalTableEntry__Standard.html#a7a7ddd62f995ae6635e7e8cd45e2f143", null ],
    [ "ut1_sin_", "structSgUt1TidalTableEntry__Standard.html#af17069973ab6a09d004944ecdef912aa", null ]
];