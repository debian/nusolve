var structvdbcOptions =
[
    [ "altSetupAppName", "structvdbcOptions.html#a558ab6b8b2bd2e38f438bcd35ec0ff53", null ],
    [ "altSetupName", "structvdbcOptions.html#aed5a1cb5341319271fb0b75b668882c5", null ],
    [ "have2ForceWizard", "structvdbcOptions.html#a76675cdd8fc4d0f57eb6606f947a0eea", null ],
    [ "have2UseAltSetup", "structvdbcOptions.html#adb1c97439a09581b9407835f0498b787", null ],
    [ "inputArg", "structvdbcOptions.html#a9fe7bfee76e5a7ce755efe42951a0335", null ],
    [ "isDryRun", "structvdbcOptions.html#a2d48157c3af4c22336c56c0ed08d926c", null ],
    [ "settings", "structvdbcOptions.html#a6b174b11033b9bb1d8e9e41f80a3fea4", null ],
    [ "shouldInvokeSystemWideWizard", "structvdbcOptions.html#af3d6a9022bfa641d71403a364f92ede4", null ],
    [ "useStdLocale", "structvdbcOptions.html#ab2a148e32c5fae53d24805901e0815d7", null ]
];