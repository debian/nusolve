var classSgDbbc3TpReading_1_1TpRecord =
[
    [ "TpRecord", "classSgDbbc3TpReading_1_1TpRecord.html#a8bac38886b48a5d7818ab391a0cbd014", null ],
    [ "TpRecord", "classSgDbbc3TpReading_1_1TpRecord.html#a36b80a0a28e8af0b4b4557eef7758b41", null ],
    [ "operator=", "classSgDbbc3TpReading_1_1TpRecord.html#a946e6c174fdbf0ac909a79b5f907a5df", null ],
    [ "agc_", "classSgDbbc3TpReading_1_1TpRecord.html#aba4074b61cabea9a626745072d46ada9", null ],
    [ "boardId_", "classSgDbbc3TpReading_1_1TpRecord.html#ad12c6de5de22b3f1f89171640d8fb145", null ],
    [ "bw_", "classSgDbbc3TpReading_1_1TpRecord.html#a26074032ed32a6cd319c26f313d56491", null ],
    [ "gainL_", "classSgDbbc3TpReading_1_1TpRecord.html#a8d38b34fd10c3eb11b39b5cb145ece34", null ],
    [ "gainU_", "classSgDbbc3TpReading_1_1TpRecord.html#afd4f64c2fa2e3a86a7517fbc71bc4aae", null ],
    [ "sefdL_", "classSgDbbc3TpReading_1_1TpRecord.html#a6ffd815e67fa053042a67f1df0af46bb", null ],
    [ "sefdU_", "classSgDbbc3TpReading_1_1TpRecord.html#a88595fc562bbec43a0c88e0603362fb0", null ],
    [ "sensorId_", "classSgDbbc3TpReading_1_1TpRecord.html#a8256fc6ded95e62d253ec1b8afe376c2", null ],
    [ "tpOffL_", "classSgDbbc3TpReading_1_1TpRecord.html#a7ac9b6ebb1ada334581ba99b2211819f", null ],
    [ "tpOffU_", "classSgDbbc3TpReading_1_1TpRecord.html#a91084e807548011339f818d17420abd4", null ],
    [ "tpOnL_", "classSgDbbc3TpReading_1_1TpRecord.html#a786bf1da699bb00c0347728f79f8cc1c", null ],
    [ "tpOnU_", "classSgDbbc3TpReading_1_1TpRecord.html#a5f66ed253ff7ad7544bd5aaa42de6c08", null ]
];