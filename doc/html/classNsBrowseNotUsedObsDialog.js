var classNsBrowseNotUsedObsDialog =
[
    [ "NsBrowseNotUsedObsDialog", "classNsBrowseNotUsedObsDialog.html#af3f14f1a7437d3f5e76747b6736340b8", null ],
    [ "~NsBrowseNotUsedObsDialog", "classNsBrowseNotUsedObsDialog.html#a72f416865fbd0993599638fd8fc67024", null ],
    [ "accept", "classNsBrowseNotUsedObsDialog.html#a690683648d3fe4ced501bc7c62e369a4", null ],
    [ "className", "classNsBrowseNotUsedObsDialog.html#a56c158d0b4ef2e094377a046bb8b6971", null ],
    [ "closeEvent", "classNsBrowseNotUsedObsDialog.html#aafe0e675d3495fa5205a5d6ca9be1327", null ],
    [ "export2file", "classNsBrowseNotUsedObsDialog.html#a7e069c4794fc5c89c7c1c1445f61dbcd", null ],
    [ "reject", "classNsBrowseNotUsedObsDialog.html#a7731950301dde30a572912b85532d363", null ],
    [ "deselectedObservations_", "classNsBrowseNotUsedObsDialog.html#a4d7afda0a7e6f3b39aaa2d1796ea4631", null ],
    [ "nonUsableObservations_", "classNsBrowseNotUsedObsDialog.html#a61d3d98fca316d4a1eb07d6611fc9dff", null ],
    [ "reporter_", "classNsBrowseNotUsedObsDialog.html#a5be400505cd73e773d372b6b214ac5af", null ],
    [ "twDeselected_", "classNsBrowseNotUsedObsDialog.html#a1c2be4049647bc09c36d7757b5ebf7bb", null ],
    [ "twUnusable_", "classNsBrowseNotUsedObsDialog.html#ae45e3398f178bfca49a36c411ae90222", null ]
];