var classSgDbhServiceRecordHS1 =
[
    [ "SgDbhServiceRecordHS1", "classSgDbhServiceRecordHS1.html#a2b6a3106fe74f8a081434dcb52e956e9", null ],
    [ "~SgDbhServiceRecordHS1", "classSgDbhServiceRecordHS1.html#ac8318840e207f1433cd5c010e6c5256f", null ],
    [ "className", "classSgDbhServiceRecordHS1.html#a8ab2566cb72c9f09105420166db8c923", null ],
    [ "getHistoryEpoch", "classSgDbhServiceRecordHS1.html#a734e9d316af476e23b7e4ae7921912ad", null ],
    [ "getLengthOfHistoryString", "classSgDbhServiceRecordHS1.html#a71e7e0986fbb39b961e037175268d519", null ],
    [ "getVersionNumber", "classSgDbhServiceRecordHS1.html#a970b248145460b5be7e07c420c66d988", null ],
    [ "isAltered", "classSgDbhServiceRecordHS1.html#a976f33301ca134656aca0bcc968c4437", null ],
    [ "isZ1", "classSgDbhServiceRecordHS1.html#a360ba49adf831b6b570a8f854c6f4ba3", null ],
    [ "readLR", "classSgDbhServiceRecordHS1.html#ae7e02ac07526dd2ea03c55ae55dd20ce", null ],
    [ "setHistoryEpoch", "classSgDbhServiceRecordHS1.html#adcc5db049ee3abd9ff7e445765892749", null ],
    [ "setLengthOfHistoryString", "classSgDbhServiceRecordHS1.html#a03ba4eb12f11427d40df8f8f9d25f4a2", null ],
    [ "setVersionNumber", "classSgDbhServiceRecordHS1.html#a9c2d2fd4a92dafdd5a871678f3c4420e", null ],
    [ "writeLR", "classSgDbhServiceRecordHS1.html#aa10573d698f9d4213f019923edaf9f66", null ],
    [ "epoch_", "classSgDbhServiceRecordHS1.html#a5c2e18b159caea816f0849a1ae86afcf", null ],
    [ "lengthOfHistoryString_", "classSgDbhServiceRecordHS1.html#a76ae33806addf2edaf8b58161f604a5a", null ],
    [ "versionNumber_", "classSgDbhServiceRecordHS1.html#add2463a4d535014361ba54c0d7de4c6f", null ]
];