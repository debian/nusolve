var structl2aOptions =
[
    [ "antcalOutputData", "structl2aOptions.html#a5cff52bcd87b9b867bbfe4fb45b7828a", null ],
    [ "compressExt", "structl2aOptions.html#a722a5382f37b81e11b5226e4036776aa", null ],
    [ "dbbc3InputFileName", "structl2aOptions.html#aa1085967b052f20480b5e1f530565b50", null ],
    [ "inputFileName", "structl2aOptions.html#a04a19cefcf4e15d6d518e8f04d7c5747", null ],
    [ "knownWishedData", "structl2aOptions.html#aef112520d06241fe9c4b726d3ba0df12", null ],
    [ "logFileName", "structl2aOptions.html#af93c32631de1e8742c67a1f346df09be", null ],
    [ "logLevel", "structl2aOptions.html#a3d870f4890d542025933bf3cc612bedd", null ],
    [ "outputFileName", "structl2aOptions.html#a68abde063c50ee3b863958c02c95e1f3", null ],
    [ "reportAllTsysData", "structl2aOptions.html#a07ea54020b5fe20b84d9ae343707a94d", null ],
    [ "stationKey", "structl2aOptions.html#a55a222f160152a76dbef8f4851cc21b1", null ],
    [ "supressNonUsedSensors", "structl2aOptions.html#a1ce340e4d658e5f2b27028576aa02291", null ],
    [ "tBegin", "structl2aOptions.html#a90300b31b2e15abcfa98cdc009149d74", null ],
    [ "tEnd", "structl2aOptions.html#a651b4577be5c85d889b89cc872213d7a", null ]
];