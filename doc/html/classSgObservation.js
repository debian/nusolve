var classSgObservation =
[
    [ "Attributes", "classSgObservation.html#ae20cba63c6b8e2b974fc5359c147bae7", [
      [ "Attr_NOT_VALID", "classSgObservation.html#ae20cba63c6b8e2b974fc5359c147bae7a933b7668b6dfac2483ed8e21c32df1e4", null ],
      [ "Attr_PROCESSED", "classSgObservation.html#ae20cba63c6b8e2b974fc5359c147bae7adc42bb92efeef14da1f3ae79d70db663", null ],
      [ "Attr_FORCE_2_PROCESS", "classSgObservation.html#ae20cba63c6b8e2b974fc5359c147bae7a116881798ff4526e327c070158b918f4", null ]
    ] ],
    [ "SgObservation", "classSgObservation.html#aa70eb2cbb4e889840da7a8f2f8b714cc", null ],
    [ "SgObservation", "classSgObservation.html#af900e452f5cf2e919941c7ea2d82877a", null ],
    [ "~SgObservation", "classSgObservation.html#a12f907cab7f61355911b832d95a07d71", null ],
    [ "className", "classSgObservation.html#a5d50b3919a6bd11ea107fc38d9ef8f98", null ],
    [ "evaluateResiduals", "classSgObservation.html#a29e64e3e87c6723a437691f688413451", null ],
    [ "evaluateTheoreticalValues", "classSgObservation.html#ad598e5f92ab23aeec2924874e9593926", null ],
    [ "getMediaIdx", "classSgObservation.html#a28870a3c51fa1407b7a1cf69b0c60d44", null ],
    [ "getMJD", "classSgObservation.html#af070b74c61d7a8f660277e30271dad5c", null ],
    [ "getTechniqueID", "classSgObservation.html#a3dd81ebc7fa01e52cc4eae3d2191dea1", null ],
    [ "isEligible", "classSgObservation.html#a8bfa3e2a9f82726e11daddc371570ecf", null ],
    [ "o_c", "classSgObservation.html#a2d0298fbd2de0cee00088af6c7857f09", null ],
    [ "operator!=", "classSgObservation.html#a0043ae73f9382e59ffcfac49e4dce1f7", null ],
    [ "operator<", "classSgObservation.html#a7ff7bb717d33a608585a1d8547a1170d", null ],
    [ "operator=", "classSgObservation.html#ab474d807a5df615e5596e0497221af74", null ],
    [ "operator==", "classSgObservation.html#a2d19592bf23e49e04b266c29e90493af", null ],
    [ "prepare4Analysis", "classSgObservation.html#a1cec07b03e77586de7682526f65079dc", null ],
    [ "selfCheck", "classSgObservation.html#acf33eb905900a65ddb758a35474a70dc", null ],
    [ "setMediaIdx", "classSgObservation.html#abfea2e32fea96dbe9a17e5f482c05726", null ],
    [ "setMJD", "classSgObservation.html#acdb34d94bea355a3179bdd299efbba1e", null ],
    [ "setTechniqueID", "classSgObservation.html#a368a1ecba9be0e3e658e925aa77a7a62", null ],
    [ "sigma", "classSgObservation.html#a5fba59c3a06285050de8f00814c67926", null ],
    [ "mediaIdx_", "classSgObservation.html#ab241a4f96f9a0233ff0aeb58facea318", null ],
    [ "techniqueID_", "classSgObservation.html#ac390b9dfdbd7fd8a2ce8abbb3e9ed649", null ]
];