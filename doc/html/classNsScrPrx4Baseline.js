var classNsScrPrx4Baseline =
[
    [ "NsScrPrx4Baseline", "classNsScrPrx4Baseline.html#ab5df630a6885fa518a730006981f6707", null ],
    [ "~NsScrPrx4Baseline", "classNsScrPrx4Baseline.html#a53bc65f41d0052b95fd3aebd43c59e20", null ],
    [ "bln", "classNsScrPrx4Baseline.html#a1634be0b7d0498be5e40ad93f5a94c94", null ],
    [ "bln", "classNsScrPrx4Baseline.html#a30012a1fa65fbb7e692dc77bcc3a9589", null ],
    [ "getEstimateClocks", "classNsScrPrx4Baseline.html#a3d431cee34441938ba9f1efd7c2e2ef7", null ],
    [ "getLength", "classNsScrPrx4Baseline.html#a4655f825b9c881bce7eb406bd86e3c8a", null ],
    [ "isValid", "classNsScrPrx4Baseline.html#a07dbb8fbde799bc3c97f023aeae258a5", null ],
    [ "setEstimateClocks", "classNsScrPrx4Baseline.html#a47cf06872ab408ca79abed5d66df2243", null ],
    [ "setIsValid", "classNsScrPrx4Baseline.html#a9b1d151045092e6d1ca7301ef24bf56e", null ],
    [ "setUpName", "classNsScrPrx4Baseline.html#a4d126311bd08bfad40a50bb56a08de52", null ],
    [ "estimateClocks", "classNsScrPrx4Baseline.html#ad8ef0ce41b99075878467f30bf86097c", null ],
    [ "isValid", "classNsScrPrx4Baseline.html#a2800edd64d0ac12c8c0a69a2523d305d", null ],
    [ "length", "classNsScrPrx4Baseline.html#a63c9968924815578caf9603be9b4d8f3", null ]
];