var classSgCableCalReading =
[
    [ "SgCableCalReading", "classSgCableCalReading.html#afb135423bec03eed9f48a3d83bc67019", null ],
    [ "SgCableCalReading", "classSgCableCalReading.html#a60a31c6e06274461912ff859b44a9282", null ],
    [ "SgCableCalReading", "classSgCableCalReading.html#ad7ccfe9932be1ef04993dc7d4d1c8b08", null ],
    [ "~SgCableCalReading", "classSgCableCalReading.html#aebfdcd377294a3dc14c6f63f60867560", null ],
    [ "className", "classSgCableCalReading.html#a1bbcee853a97bbf8f9db436d8ef67a29", null ],
    [ "getIsOk", "classSgCableCalReading.html#a084728902e8ec036723fea9f262560e1", null ],
    [ "getOsRec", "classSgCableCalReading.html#aff5e34716c573cf78d5ec90f15ace55d", null ],
    [ "getT", "classSgCableCalReading.html#ae28948c8f57b0db5510019ce8dcc0e89", null ],
    [ "getV", "classSgCableCalReading.html#a930af3788daa9da6e21446b9f0620d5d", null ],
    [ "operator==", "classSgCableCalReading.html#ab1f8774592cda407693a9002427365c8", null ],
    [ "setIsOk", "classSgCableCalReading.html#a3b293f2f2d9c7f9cfd4a17ee9d270371", null ],
    [ "setOsRec", "classSgCableCalReading.html#a4ef36eea33ba87b7985278f40d083ace", null ],
    [ "setT", "classSgCableCalReading.html#a3ff9f5056a06b0c94bbe5c36d819b3b6", null ],
    [ "setV", "classSgCableCalReading.html#a282fe4f54555e0ffced893a762922e82", null ],
    [ "isOk_", "classSgCableCalReading.html#aa3918fc0c9e56fbd4e4a816447015690", null ],
    [ "osRec_", "classSgCableCalReading.html#ae92961df6643fa68cd7d6d77ef65a799", null ],
    [ "t_", "classSgCableCalReading.html#a4bb352069138611123122992670f4f09", null ],
    [ "v_", "classSgCableCalReading.html#ac4b27f292690c61dcd9cce202cf6a02f", null ]
];