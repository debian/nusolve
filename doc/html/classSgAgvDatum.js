var classSgAgvDatum =
[
    [ "SgAgvDatum", "classSgAgvDatum.html#a00a97d85520ba501ed6d525b25edf5f5", null ],
    [ "SgAgvDatum", "classSgAgvDatum.html#aec56a20d7f242268d0e0ef444d6b3ef3", null ],
    [ "~SgAgvDatum", "classSgAgvDatum.html#a090abaada31af849936e26f4a00cb6f4", null ],
    [ "allocateSpace", "classSgAgvDatum.html#a2381d61878e473d6b15f7a5236634aab", null ],
    [ "className", "classSgAgvDatum.html#a2138303f8f27fc1af7b601e05a22d6c7", null ],
    [ "freeSpace", "classSgAgvDatum.html#afdb4e0076a5b89a2cca52465fc78463c", null ],
    [ "getValue", "classSgAgvDatum.html#abd85067b799d3f4417d835e1c4b8b54a", null ],
    [ "isAllocated", "classSgAgvDatum.html#a9c635864eba724816a9ae23fc28a84fd", null ],
    [ "isEmpty", "classSgAgvDatum.html#aaaa06062ee7243691021aac3ec0f6612", null ],
    [ "value", "classSgAgvDatum.html#ae261f7414df10140a8a5535f82e79f36", null ],
    [ "data_", "classSgAgvDatum.html#ace4ba684a3a42f971f518ded525598e0", null ],
    [ "descriptor_", "classSgAgvDatum.html#a48c10c7cf5f747989ac66eae69b6d991", null ]
];