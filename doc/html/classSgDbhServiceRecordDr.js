var classSgDbhServiceRecordDr =
[
    [ "SgDbhServiceRecordDr", "classSgDbhServiceRecordDr.html#a74be02931e098e31c7a499acfd4f614c", null ],
    [ "~SgDbhServiceRecordDr", "classSgDbhServiceRecordDr.html#a7e0b4c3f438fe138e4c402bd64bf07c8", null ],
    [ "className", "classSgDbhServiceRecordDr.html#a3ab0b7ea9ae5b764bee048bd06cf7b05", null ],
    [ "dump", "classSgDbhServiceRecordDr.html#aa8fa80d9987ff38f72ed6ca9c2787eeb", null ],
    [ "getNumOfTeBlocks", "classSgDbhServiceRecordDr.html#a7495ed6b40dac49be19c064b391b6a8e", null ],
    [ "getTcNo", "classSgDbhServiceRecordDr.html#a9f5ed3e35c435972e380c2dd9e69ea50", null ],
    [ "readLR", "classSgDbhServiceRecordDr.html#a396803d3d60c459d40df9f691496bcc0", null ],
    [ "setNumOfTeBlocks", "classSgDbhServiceRecordDr.html#af324c6ff472dab877420ed713b6a05d8", null ],
    [ "setTcNo", "classSgDbhServiceRecordDr.html#a837205b4f1fd09bebb45ac09824d7b11", null ],
    [ "writeLR", "classSgDbhServiceRecordDr.html#a71d8b0dc46306cfca7b7c198cb0f8073", null ],
    [ "numOfTeBlocks_", "classSgDbhServiceRecordDr.html#a0c13a2dacf3385e11a6ba86c9c710e9c", null ],
    [ "pRest_", "classSgDbhServiceRecordDr.html#a0d551642846e9a47702ce9b650a6848b", null ],
    [ "tcNo_", "classSgDbhServiceRecordDr.html#ae6c1947f69187d9028ec79c63a6ac59e", null ]
];