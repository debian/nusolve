var classSgNcdfAttribute =
[
    [ "SgNcdfAttribute", "classSgNcdfAttribute.html#ad1236511214399bc9c1af6948499ef8f", null ],
    [ "SgNcdfAttribute", "classSgNcdfAttribute.html#a3642ff261aaf085ef2c7a83dd412ce2c", null ],
    [ "SgNcdfAttribute", "classSgNcdfAttribute.html#ab0be6f722bceb6b4597a3fc59cb23b6e", null ],
    [ "~SgNcdfAttribute", "classSgNcdfAttribute.html#adc7782009b5d072955fd49acdb438251", null ],
    [ "className", "classSgNcdfAttribute.html#a0acc4e70825b29341f4b7025f899c350", null ],
    [ "debug_output", "classSgNcdfAttribute.html#adca50cc318c19b4c8cbb748f01a396c3", null ],
    [ "getData", "classSgNcdfAttribute.html#ada049c655f7f7dad55d89e360eacf97c", null ],
    [ "getName", "classSgNcdfAttribute.html#af564dded74a7c7564c61df249ccc064f", null ],
    [ "getNumOfElements", "classSgNcdfAttribute.html#a7837c73bd8b341ec0ff9c04ac31d6398", null ],
    [ "getTypeOfData", "classSgNcdfAttribute.html#a31aac3e9c0c5236c0468d0c3a5db5691", null ],
    [ "nc_get_attr", "classSgNcdfAttribute.html#ae0dc39e889e6c23e089ed5d4bf33a945", null ],
    [ "nc_put_attr", "classSgNcdfAttribute.html#a7bbcbba243b9a7146fb08cbcb68b08e6", null ],
    [ "setData", "classSgNcdfAttribute.html#a34b09fe7988e8994b3a5c85ba597af46", null ],
    [ "setName", "classSgNcdfAttribute.html#ab20a7107b08efe1fc6664cad3b619d4a", null ],
    [ "setNumOfElements", "classSgNcdfAttribute.html#a323cb0c2a362839dd6b1333ab2d17495", null ],
    [ "setTypeOfData", "classSgNcdfAttribute.html#a5e862b7dd7676ecbf7152936230c6827", null ],
    [ "data_", "classSgNcdfAttribute.html#aec8dcc454beba5add86452bdae549fb8", null ],
    [ "name_", "classSgNcdfAttribute.html#afdca24fcfdfec4385b1a02a87a794160", null ],
    [ "numOfElements_", "classSgNcdfAttribute.html#a521ce19758527c9330bfac55977d5855", null ],
    [ "typeOfData_", "classSgNcdfAttribute.html#ab798dba7263bf75e24e41d6db2b9c704", null ]
];