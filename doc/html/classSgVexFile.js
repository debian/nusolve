var classSgVexFile =
[
    [ "BbcSetup", "classSgVexFile_1_1BbcSetup.html", "classSgVexFile_1_1BbcSetup" ],
    [ "FreqSetup", "classSgVexFile_1_1FreqSetup.html", "classSgVexFile_1_1FreqSetup" ],
    [ "IfSetup", "classSgVexFile_1_1IfSetup.html", "classSgVexFile_1_1IfSetup" ],
    [ "StationSetup", "classSgVexFile_1_1StationSetup.html", "classSgVexFile_1_1StationSetup" ],
    [ "SgVexFile", "classSgVexFile.html#a6b1eb6aad355da895ff47e2c5ef76147", null ],
    [ "SgVexFile", "classSgVexFile.html#afc925b580d7af42ff334a21d408b4a18", null ],
    [ "~SgVexFile", "classSgVexFile.html#a39e52892bd3a10975d6fd4ae94d8bfd2", null ],
    [ "analyzeVex", "classSgVexFile.html#a80ac3c985ba33c909e826847b9682bc8", null ],
    [ "bbcSetup", "classSgVexFile.html#aced26643120f1bb1586dac9eef63a1a0", null ],
    [ "className", "classSgVexFile.html#a79abfbd28f437b37142c4b9f007e5e6d", null ],
    [ "freqSetup", "classSgVexFile.html#af38469b9053ff333115df34009fe452a", null ],
    [ "getcontactEmail", "classSgVexFile.html#a048b9464f375f4022df82cde4945a554", null ],
    [ "getContactName", "classSgVexFile.html#a9be00e24aec63855c3cbd5e9174c7655", null ],
    [ "getExperDescription", "classSgVexFile.html#a3d8f3fe0d4ee9646852b199423973035", null ],
    [ "getExperName", "classSgVexFile.html#ae0206267e7de2d41e83a3ad62a6b4657", null ],
    [ "getExperNominalStart", "classSgVexFile.html#a86659ad4932d116eaf44568c8c47b282", null ],
    [ "getExperNominalStop", "classSgVexFile.html#ab527e8b327a5449fd770d7bdba6197bd", null ],
    [ "getInputFileName", "classSgVexFile.html#a7df7f9adb39c430f818f8d8308c8ace4", null ],
    [ "getPiEmail", "classSgVexFile.html#a21c65fd197bfe92a41eb0d9e7039d008", null ],
    [ "getPiName", "classSgVexFile.html#a4eb730c50c8bb8aa7fd3f90aa1343350", null ],
    [ "getSchedulerEmail", "classSgVexFile.html#abf03db546e42ba73fef440177e144e10", null ],
    [ "getSchedulerName", "classSgVexFile.html#a1634abf7c5ecad8cac559e20e43e4971", null ],
    [ "getSections", "classSgVexFile.html#aa497a9578ecac43d87875c1caa36a7c0", null ],
    [ "getTargetCorrelator", "classSgVexFile.html#aaad5e0e9b539051fe61d2fa6ad54947a", null ],
    [ "ifSetup", "classSgVexFile.html#a6271b9a7075db6a4811a07f01d592a2e", null ],
    [ "lookupDef", "classSgVexFile.html#a196b57901239509bd780a8a535d201bd", null ],
    [ "lookupRef", "classSgVexFile.html#a4214d4eb38e44d0728a7b2de583ab92d", null ],
    [ "lookupRef", "classSgVexFile.html#a3a7fa0af57630bf4252416400c85418b", null ],
    [ "lookupRef", "classSgVexFile.html#ad8fafac6794aff74be02fef7befe574d", null ],
    [ "parseSection", "classSgVexFile.html#aac537fd4e100ce2dd8ea2f4e0c68e530", null ],
    [ "parseVexFile", "classSgVexFile.html#a6847f96d025a0da697298cad2d0d34da", null ],
    [ "setContactEmail", "classSgVexFile.html#a7717e21693e42674d41c3a333921721d", null ],
    [ "setContactName", "classSgVexFile.html#af0c0a423d6ab419017bcee247d52cca2", null ],
    [ "setExperDescription", "classSgVexFile.html#a11b82b25c34753b2e81eb4e1d15fc37c", null ],
    [ "setExperName", "classSgVexFile.html#a36d008c54d33367976f0bd31590a6547", null ],
    [ "setExperNominalStart", "classSgVexFile.html#a1d15fb7042a301eb563201dbd8595e97", null ],
    [ "setExperNominalStop", "classSgVexFile.html#ad4ccf5b016ef0398b5eb09c663c57f23", null ],
    [ "setInputFileName", "classSgVexFile.html#a3606233f1e6ef3988d0213abe4105861", null ],
    [ "setPiEmail", "classSgVexFile.html#ae02a38bab5b768fac54d3a61ff4ebbd6", null ],
    [ "setPiName", "classSgVexFile.html#abda10686ec0e755d4180756de038e1dc", null ],
    [ "setSchedulerEmail", "classSgVexFile.html#a8ff9cf8a255c078385a086d254fe0e3b", null ],
    [ "setSchedulerName", "classSgVexFile.html#ae5602338b07a9323f35b4701c7ffb6b5", null ],
    [ "setTargetCorrelator", "classSgVexFile.html#aefa7c0bec6f1224fe487410a59c25b7c", null ],
    [ "str2frq", "classSgVexFile.html#a7f8d9ddf341aea7833b25e742fdf4332", null ],
    [ "contactEmail_", "classSgVexFile.html#a0e210f1da030fa40bfded0b25c168074", null ],
    [ "contactName_", "classSgVexFile.html#a6487cf470f106beb8bd91c9803739c3e", null ],
    [ "exper_", "classSgVexFile.html#ae4cbc1ae3ebdb5ba5212970973641cb7", null ],
    [ "experDescription_", "classSgVexFile.html#ad94ccc724b59697bf21ea061c6239b15", null ],
    [ "experName_", "classSgVexFile.html#a1b8c634e3802a5dc02c1e826620e4f2f", null ],
    [ "experNominalStart_", "classSgVexFile.html#ab0662212365c28be51be31884f220113", null ],
    [ "experNominalStop_", "classSgVexFile.html#a21587ef5a4378bfa5e44bb548d3f3780", null ],
    [ "inputFileName_", "classSgVexFile.html#a5240d8dbf979ef0181842f17a3c6ae08", null ],
    [ "piEmail_", "classSgVexFile.html#a3425d24095fa9fef5a13939adce696d5", null ],
    [ "piName_", "classSgVexFile.html#a7fe5a62ab280dedda629338c2e629edc", null ],
    [ "schedulerEmail_", "classSgVexFile.html#a42b168f7e069b79bea2c2b167c6068cb", null ],
    [ "schedulerName_", "classSgVexFile.html#a6cd83eabe92494d085dfd7fbecd8daac", null ],
    [ "sectionByName_", "classSgVexFile.html#a83f83e25c6ca62afa94493f86b81e208", null ],
    [ "sections_", "classSgVexFile.html#a275b0a1522e00f9a0b54b9cf491b44ea", null ],
    [ "statements_", "classSgVexFile.html#a2ad905df08074742958a03104fb5d7f5", null ],
    [ "stnSetupById_", "classSgVexFile.html#a696ced497079c08539b803422ab822bc", null ],
    [ "targetCorrelator_", "classSgVexFile.html#afa6f30c8b5634215ac1d206a35d78fdb", null ]
];