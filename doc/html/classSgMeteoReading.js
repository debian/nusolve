var classSgMeteoReading =
[
    [ "SgMeteoReading", "classSgMeteoReading.html#ad9eac9980402d94e824932af73a4430a", null ],
    [ "SgMeteoReading", "classSgMeteoReading.html#a4ac6cbc362af650944ca9f8b9c779875", null ],
    [ "SgMeteoReading", "classSgMeteoReading.html#a7c5ae9b75a88f5402c6e567350ea53de", null ],
    [ "SgMeteoReading", "classSgMeteoReading.html#ae102f029473124800664d1ae43f103f8", null ],
    [ "~SgMeteoReading", "classSgMeteoReading.html#a0228e963f4b41816a94985cac988fa29", null ],
    [ "className", "classSgMeteoReading.html#aaa59b5903bf7c4ad3c7ab69ba697f96f", null ],
    [ "getIsOk", "classSgMeteoReading.html#afff4de7bccb32314ada6acadb4ae6847", null ],
    [ "getM", "classSgMeteoReading.html#ada7d29a22db82048bf15bf0d536092ae", null ],
    [ "getOsRec", "classSgMeteoReading.html#a2f83f20c2bbd511f1376f3f4ea546d42", null ],
    [ "getT", "classSgMeteoReading.html#a2cfb54e8b911ae8e0aebba71049a054a", null ],
    [ "operator==", "classSgMeteoReading.html#a4a25e558838c115f9e4f05717b11b97d", null ],
    [ "setIsOk", "classSgMeteoReading.html#a61f329dacae957e7f704b4cce4699fee", null ],
    [ "setM", "classSgMeteoReading.html#aaf122af52c1ea31d2d647babccf43804", null ],
    [ "setOsRec", "classSgMeteoReading.html#a4bb921457c56519cc01098d9c7aacb8e", null ],
    [ "setT", "classSgMeteoReading.html#a47a14ff70b0e72e28df4cbc86cf5b072", null ],
    [ "isOk_", "classSgMeteoReading.html#a03e691d9c6c015a5c7a8bd056347e8d2", null ],
    [ "m_", "classSgMeteoReading.html#ac28cafce6dee3393d77d55593b6f78a9", null ],
    [ "osRec_", "classSgMeteoReading.html#a46cefd7880a4d20d4ed28d1d83e69ec6", null ],
    [ "t_", "classSgMeteoReading.html#a6ede884cac5e4eb0bf3d65fa3c382450", null ]
];