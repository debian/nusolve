var classSgNcdfDimension =
[
    [ "SgNcdfDimension", "classSgNcdfDimension.html#ad59817ccc90d4bb0bf781ba7a723bf35", null ],
    [ "SgNcdfDimension", "classSgNcdfDimension.html#a2c43de376d75a38fbab757a42ba53c32", null ],
    [ "SgNcdfDimension", "classSgNcdfDimension.html#a93bbcc2a574ffed1eda7532227089e86", null ],
    [ "~SgNcdfDimension", "classSgNcdfDimension.html#aabae46eabd59c969e7a919ed795eebe4", null ],
    [ "className", "classSgNcdfDimension.html#aca99c17ada31af8cd5dd856d29805e7f", null ],
    [ "debug_output", "classSgNcdfDimension.html#ac2876636e54e298c6cb1d8364c6cb752", null ],
    [ "getId", "classSgNcdfDimension.html#a8e342e40a2d548582968c9782252e827", null ],
    [ "getN", "classSgNcdfDimension.html#a5553e2b4144b1cce597375904a4a0ee5", null ],
    [ "getName", "classSgNcdfDimension.html#a6b43943567fb3338427d38d986227c77", null ],
    [ "operator=", "classSgNcdfDimension.html#a208b70ef3b433db20fb28c0f543a0e06", null ],
    [ "setId", "classSgNcdfDimension.html#a283092ea2410103392ff5e2c3f7f8349", null ],
    [ "setN", "classSgNcdfDimension.html#a9acab84d87afd825b63fd0ec8b5e6679", null ],
    [ "setName", "classSgNcdfDimension.html#adaa16b1a6160b5b81af7ddfbfcdabdf5", null ],
    [ "id_", "classSgNcdfDimension.html#ade8196163d9180a8bc228573eb9a0781", null ],
    [ "n_", "classSgNcdfDimension.html#a0ceff74673b37cd306902d8366fd7818", null ],
    [ "name_", "classSgNcdfDimension.html#ae99fdb7e433667eaea5f6c6c40e935b6", null ]
];