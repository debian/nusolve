var structSgVgosDb_1_1StationDescriptor =
[
    [ "StationDescriptor", "structSgVgosDb_1_1StationDescriptor.html#a251041c47a01848e4389a360e83d1dbf", null ],
    [ "propagateStnKey", "structSgVgosDb_1_1StationDescriptor.html#a7721d505d58ec7fc68f16e11d6692a03", null ],
    [ "numOfPts_", "structSgVgosDb_1_1StationDescriptor.html#a436f11a7238c21af030cb9e1e62e616c", null ],
    [ "stationKey_", "structSgVgosDb_1_1StationDescriptor.html#acf4afba45b59e33f7b3cf9c0528c8472", null ],
    [ "stationName_", "structSgVgosDb_1_1StationDescriptor.html#a5f4dd99329925e1d06528e78659c38d8", null ],
    [ "vAzEl_", "structSgVgosDb_1_1StationDescriptor.html#a9a5727fd1c0b97fb047b0753040e7b62", null ],
    [ "vCal_AxisOffset_", "structSgVgosDb_1_1StationDescriptor.html#a9c4d3c8be55adb680159430bc7edfe55", null ],
    [ "vCal_Cable_", "structSgVgosDb_1_1StationDescriptor.html#a56f0df1c6b9d6e893c3d4ad1544c01b5", null ],
    [ "vCal_OceanLoad_", "structSgVgosDb_1_1StationDescriptor.html#ad48c45bf9960c7dceec0b95e4349b95c", null ],
    [ "vCal_SlantPathTropDry_", "structSgVgosDb_1_1StationDescriptor.html#a1700aec04c52e7cef3033ebe8123b7bd", null ],
    [ "vCal_SlantPathTropWet_", "structSgVgosDb_1_1StationDescriptor.html#a1586fc1709986957591d12a46081cb8b", null ],
    [ "vDis_OceanLoad_", "structSgVgosDb_1_1StationDescriptor.html#ace76400a41dec2c0008163d074c37a9b", null ],
    [ "vFeedRotation_", "structSgVgosDb_1_1StationDescriptor.html#a8f6d4198e8e68aca7b5b8ddbff217e64", null ],
    [ "vMet_", "structSgVgosDb_1_1StationDescriptor.html#aec46624879fb2b8d1608e5e9897bfe66", null ],
    [ "vPart_AxisOffset_", "structSgVgosDb_1_1StationDescriptor.html#ac1a0ecd1e20fba064330e62c63b54b75", null ],
    [ "vPart_HorizonGrad_", "structSgVgosDb_1_1StationDescriptor.html#a28f3038d3d655e1cbc676c1f9f445761", null ],
    [ "vPart_ZenithPathTropDry_", "structSgVgosDb_1_1StationDescriptor.html#a44d1b2b14df1f6f5d1fb4481efd991a5", null ],
    [ "vPart_ZenithPathTropWet_", "structSgVgosDb_1_1StationDescriptor.html#ae406b1d09e54f8ceb5a4196d833070f6", null ],
    [ "vRefClockOffset_", "structSgVgosDb_1_1StationDescriptor.html#a122dbfbde621118a4c881ce961a6f605", null ],
    [ "vTimeUTC_", "structSgVgosDb_1_1StationDescriptor.html#a082978871f8e23104bf3a80a1ce2ba64", null ],
    [ "vTsys_", "structSgVgosDb_1_1StationDescriptor.html#a2b14bf10222d7d03993271b4cf3e1b13", null ]
];