var structvdbpOptions =
[
    [ "altPath2InputFiles", "structvdbpOptions.html#a6668b4a5ebbff1b836baccf47e52fddc", null ],
    [ "altSetupAppName", "structvdbpOptions.html#a05891d2145cdbd20144e3b65d7239237", null ],
    [ "altSetupName", "structvdbpOptions.html#a5326475997670230bff7883744e4a15d", null ],
    [ "have2clearCableData", "structvdbpOptions.html#af9863f7a6c1474a23033f43181da485c", null ],
    [ "have2clearMeteoData", "structvdbpOptions.html#a858d6bab69455fa5b4be8383cb47df70", null ],
    [ "have2clearTsysData", "structvdbpOptions.html#aa43976c14e77a2c656ec8c34e53e95f9", null ],
    [ "have2ForceWizard", "structvdbpOptions.html#ae09f7ccb3d299f62172a17bed19d070c", null ],
    [ "have2UseAltSetup", "structvdbpOptions.html#ad398a3cdd2ffba63588f9107e9fc3503", null ],
    [ "inputArg", "structvdbpOptions.html#ad318416510082cf49d8b26e6007d6652", null ],
    [ "isDryRun", "structvdbpOptions.html#aed59327dd5a5af514829cf7185f30ce5", null ],
    [ "kinds", "structvdbpOptions.html#a3eea781da6439ed5bd8406af91e119b3", null ],
    [ "knownKinds", "structvdbpOptions.html#a921e6294ed63af036771fed45429c1d2", null ],
    [ "orderOfMeteo", "structvdbpOptions.html#ac6e9e706bfefa6df96cda45973f0ed35", null ],
    [ "settings", "structvdbpOptions.html#a47d4b9e8d6a578bbfe85de9583b70fca", null ],
    [ "shouldInvokeSystemWideWizard", "structvdbpOptions.html#abe74c329f4f690e299a20bf91079a09d", null ],
    [ "stations", "structvdbpOptions.html#ad5091fd2cb1b1c51459bd7fe4973544f", null ],
    [ "useStdLocale", "structvdbpOptions.html#a8328a4a7ecd3e2e2b1515c2b1e880284", null ]
];