var classSgVexFile_1_1IfSetup =
[
    [ "IfSetup", "classSgVexFile_1_1IfSetup.html#a1727162ab5bf68d41cef1c307f0a8bfb", null ],
    [ "IfSetup", "classSgVexFile_1_1IfSetup.html#ae563ea40db996f7ebf416e3fb0807e2a", null ],
    [ "IfSetup", "classSgVexFile_1_1IfSetup.html#a1146e7b72d180cdefc0a507d64a43538", null ],
    [ "~IfSetup", "classSgVexFile_1_1IfSetup.html#a1bf9a5015b9f9ef38c7b668d9693611b", null ],
    [ "getIfId", "classSgVexFile_1_1IfSetup.html#a04b2deeabe141f5cd4b2c6b7fdb8e531", null ],
    [ "getIfName", "classSgVexFile_1_1IfSetup.html#aad8965c02c52054c5d978e36e34f70d1", null ],
    [ "getNetSideBand", "classSgVexFile_1_1IfSetup.html#a4892e3ecdc98d09915e458eec1f9c0b8", null ],
    [ "getPolarization", "classSgVexFile_1_1IfSetup.html#a2ed5f21a533394a613211beca7441baf", null ],
    [ "getTotalLo", "classSgVexFile_1_1IfSetup.html#a0fe4c17922fc030435ac9334cc6f8dd3", null ],
    [ "setIfId", "classSgVexFile_1_1IfSetup.html#a06d1f6b02d7a41227a03be013312fbe5", null ],
    [ "setIfName", "classSgVexFile_1_1IfSetup.html#a3e74817834802c9abe3eb1cb0a79fac8", null ],
    [ "setNetSideBand", "classSgVexFile_1_1IfSetup.html#a9d79da70bfe4cc41c5b6b33cd0ee6d54", null ],
    [ "setPolarization", "classSgVexFile_1_1IfSetup.html#ac04a3ef549e89323d9ad7ecc23343053", null ],
    [ "setTotalLo", "classSgVexFile_1_1IfSetup.html#abbfdcf8b1eaebad204876d9763b29203", null ],
    [ "ifId_", "classSgVexFile_1_1IfSetup.html#aaad032859091b3ca0158b2b6ce5ac081", null ],
    [ "ifName_", "classSgVexFile_1_1IfSetup.html#aa4777e380d7d0e886b42fb5608622a1d", null ],
    [ "netSideBand_", "classSgVexFile_1_1IfSetup.html#ae708a67901d14653c9b8ecab675b5dfe", null ],
    [ "polarization_", "classSgVexFile_1_1IfSetup.html#a145a67c2d4a21a66ec4a602083ba8c37", null ],
    [ "totalLo_", "classSgVexFile_1_1IfSetup.html#a8196bb54446ea6853fa3579835c6a55a", null ]
];