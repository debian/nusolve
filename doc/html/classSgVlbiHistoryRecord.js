var classSgVlbiHistoryRecord =
[
    [ "SgVlbiHistoryRecord", "classSgVlbiHistoryRecord.html#a9ddbd3f64c249b0ed1e81dc164adba57", null ],
    [ "SgVlbiHistoryRecord", "classSgVlbiHistoryRecord.html#a4b4e9362e5907fc2cb6b3b9140b1fde3", null ],
    [ "SgVlbiHistoryRecord", "classSgVlbiHistoryRecord.html#aa7bb2d572dabb1379262ac9043feeec3", null ],
    [ "~SgVlbiHistoryRecord", "classSgVlbiHistoryRecord.html#a5cfd5f5828ec20ef61709c6d4742907d", null ],
    [ "className", "classSgVlbiHistoryRecord.html#a8971c026fdc63cb9d3f7f955dffef0c6", null ],
    [ "getEpoch", "classSgVlbiHistoryRecord.html#ad368e4b8399c3be49c937f0621e9b31f", null ],
    [ "getIsEditable", "classSgVlbiHistoryRecord.html#a585c3389d179fee520440d04308c51eb", null ],
    [ "getText", "classSgVlbiHistoryRecord.html#af144c03cf8afc4b913353c3958cc19c3", null ],
    [ "getVersion", "classSgVlbiHistoryRecord.html#a191292027d058535211b031ed93ab644", null ],
    [ "setEpoch", "classSgVlbiHistoryRecord.html#ae5b4b5a2086b8d27890910b09a3ecf62", null ],
    [ "setIsEditable", "classSgVlbiHistoryRecord.html#a2dbd28f5675c4e76b72d5c68da12ee04", null ],
    [ "setText", "classSgVlbiHistoryRecord.html#ae320f34ca48bec74c1ab31fff6fef214", null ],
    [ "setVersion", "classSgVlbiHistoryRecord.html#a70ef70d05be564432f1a3c47fd709f6c", null ],
    [ "epoch_", "classSgVlbiHistoryRecord.html#a5182b2b38f700c64e09ccdc14f5123ad", null ],
    [ "isEditable_", "classSgVlbiHistoryRecord.html#aafe39c5603154ba09316f3c657651b9a", null ],
    [ "text_", "classSgVlbiHistoryRecord.html#a675d13531c3fff0ec86d88fe305bf3bb", null ],
    [ "version_", "classSgVlbiHistoryRecord.html#a00d6f8e398a764bf42aaab2303580c8a", null ]
];