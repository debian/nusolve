var classSgDryMF__NMF =
[
    [ "SgDryMF_NMF", "classSgDryMF__NMF.html#a7856c7f4755c7d7bf3bda275866759c4", null ],
    [ "~SgDryMF_NMF", "classSgDryMF__NMF.html#a163cdcc1bbd4fa085e7ee3779f0e3cdf", null ],
    [ "calc", "classSgDryMF__NMF.html#ad51e24de9fc824cf5971cc89762464f3", null ],
    [ "className", "classSgDryMF__NMF.html#a4b6ee6cc771d6958820b4148231432fd", null ],
    [ "linterpolate", "classSgDryMF__NMF.html#ac02286053155714efab0beeb7b3599f2", null ],
    [ "modelA_amp_", "classSgDryMF__NMF.html#a53a806b8ef8a1c023a057d6fecb05356", null ],
    [ "modelA_avg_", "classSgDryMF__NMF.html#a0943af98efc156c27e342a1f6d0c6e78", null ],
    [ "modelArg_", "classSgDryMF__NMF.html#a010176287a34832d7a804139c7419df8", null ],
    [ "modelB_amp_", "classSgDryMF__NMF.html#a7ca9199ab2a35bb686dc12e00d4d901e", null ],
    [ "modelB_avg_", "classSgDryMF__NMF.html#ad10108fb354808d69c0b9edc1060c262", null ],
    [ "modelC_amp_", "classSgDryMF__NMF.html#a45719fefa5828ab9f95a30abf711403d", null ],
    [ "modelC_avg_", "classSgDryMF__NMF.html#a83792244eb77637e2067ceac87ce4406", null ]
];