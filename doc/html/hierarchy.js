var hierarchy =
[
    [ "arg4Prc_01", "structarg4Prc__01.html", null ],
    [ "arg4Prc_02", "structarg4Prc__02.html", null ],
    [ "SgTaskConfig::AutomaticProcessing", "structSgTaskConfig_1_1AutomaticProcessing.html", null ],
    [ "SgVgosDb::BandData", "structSgVgosDb_1_1BandData.html", null ],
    [ "SgVexFile::BbcSetup", "classSgVexFile_1_1BbcSetup.html", null ],
    [ "SgChannelSkeded::ChanCfg", "classSgChannelSkeded_1_1ChanCfg.html", null ],
    [ "SgModelsInfo::DasModel", "classSgModelsInfo_1_1DasModel.html", null ],
    [ "DbhDescriptorX", "structDbhDescriptorX.html", null ],
    [ "SgVgosDb::FmtChkVar", "classSgVgosDb_1_1FmtChkVar.html", null ],
    [ "SgVexFile::FreqSetup", "classSgVexFile_1_1FreqSetup.html", null ],
    [ "SgModelEop_JMG_96_hf::HfEopRec", "structSgModelEop__JMG__96__hf_1_1HfEopRec.html", null ],
    [ "SgVgosDb::HistoryDescriptor", "structSgVgosDb_1_1HistoryDescriptor.html", null ],
    [ "SgVexFile::IfSetup", "classSgVexFile_1_1IfSetup.html", null ],
    [ "anonymous_namespace{vgosDbProcLogs_resources.cpp}::initializer", "structanonymous__namespace_02vgosDbProcLogs__resources_8cpp_03_1_1initializer.html", null ],
    [ "l2aOptions", "structl2aOptions.html", null ],
    [ "NsDeselectedObsInfo", "classNsDeselectedObsInfo.html", null ],
    [ "NsNotUsedObsInfo", "classNsNotUsedObsInfo.html", null ],
    [ "nsOptions", "structnsOptions.html", null ],
    [ "NsSessionHandler", "classNsSessionHandler.html", [
      [ "NsSessionEditDialog", "classNsSessionEditDialog.html", null ]
    ] ],
    [ "NsSetup", "classNsSetup.html", null ],
    [ "SgStnLogCollector::Procedure", "classSgStnLogCollector_1_1Procedure.html", null ],
    [ "SgVgosDb::ProgramGenericDescriptor", "structSgVgosDb_1_1ProgramGenericDescriptor.html", null ],
    [ "SgVgosDb::ProgramSolveDescriptor", "structSgVgosDb_1_1ProgramSolveDescriptor.html", null ],
    [ "QDataStream", "classQDataStream.html", [
      [ "SgDbhStream", "classSgDbhStream.html", null ],
      [ "SgKombStream", "classSgKombStream.html", null ]
    ] ],
    [ "QDialog", "classQDialog.html", [
      [ "NsBrowseNotUsedObsDialog", "classNsBrowseNotUsedObsDialog.html", null ],
      [ "NsSessionEditDialog", "classNsSessionEditDialog.html", null ],
      [ "NsSessionNameDialog", "classNsSessionNameDialog.html", null ],
      [ "NsSetupDialog", "classNsSetupDialog.html", null ],
      [ "NsTestDialog", "classNsTestDialog.html", null ],
      [ "NsTestFour1Dialog", "classNsTestFour1Dialog.html", null ],
      [ "SgGuiParameterCfg", "classSgGuiParameterCfg.html", null ],
      [ "SgGuiPiaReport", "classSgGuiPiaReport.html", null ],
      [ "SgGuiTaskConfigDialog", "classSgGuiTaskConfigDialog.html", null ],
      [ "SgGuiVlbiBlnInfoEditor", "classSgGuiVlbiBlnInfoEditor.html", null ],
      [ "SgGuiVlbiSrcInfoEditor", "classSgGuiVlbiSrcInfoEditor.html", null ],
      [ "SgGuiVlbiSrcStrModelEditor", "classSgGuiVlbiSrcStrModelEditor.html", null ],
      [ "SgGuiVlbiStnClockBreakEditor", "classSgGuiVlbiStnClockBreakEditor.html", null ],
      [ "SgGuiVlbiStnInfoEditor", "classSgGuiVlbiStnInfoEditor.html", null ],
      [ "VpDefaultCableCalSignEditor", "classVpDefaultCableCalSignEditor.html", null ],
      [ "VpRinexDataEditor", "classVpRinexDataEditor.html", null ]
    ] ],
    [ "QList", "classQList.html", [
      [ "SgAPrioriRec", "classSgAPrioriRec.html", null ],
      [ "SgAgvDriver", "classSgAgvDriver.html", null ],
      [ "SgAgvSection", "classSgAgvSection.html", [
        [ "SgAgvDataSection", "classSgAgvDataSection.html", null ],
        [ "SgAgvFileSection", "classSgAgvFileSection.html", null ],
        [ "SgAgvPreaSection", "classSgAgvPreaSection.html", null ],
        [ "SgAgvTextSection", "classSgAgvTextSection.html", null ],
        [ "SgAgvTocsSection", "classSgAgvTocsSection.html", null ]
      ] ],
      [ "SgBreakModel", "classSgBreakModel.html", null ],
      [ "SgVlbiHistory", "classSgVlbiHistory.html", null ],
      [ "SgVlbiNetworkId", "classSgVlbiNetworkId.html", null ]
    ] ],
    [ "QMainWindow", "classQMainWindow.html", [
      [ "NsMainWindow", "classNsMainWindow.html", null ]
    ] ],
    [ "QMultiMap", "classQMultiMap.html", [
      [ "SgAPriories", "classSgAPriories.html", null ]
    ] ],
    [ "QObject", "classQObject.html", [
      [ "NsScrPrx4Logger", "classNsScrPrx4Logger.html", null ],
      [ "NsScrPrx4Object", "classNsScrPrx4Object.html", [
        [ "NsScrPrx4Band", "classNsScrPrx4Band.html", null ],
        [ "NsScrPrx4Baseline", "classNsScrPrx4Baseline.html", null ],
        [ "NsScrPrx4Source", "classNsScrPrx4Source.html", null ],
        [ "NsScrPrx4Station", "classNsScrPrx4Station.html", null ]
      ] ],
      [ "NsScrPrx4ParametersDescriptor", "classNsScrPrx4ParametersDescriptor.html", null ],
      [ "NsScrPrx4Session", "classNsScrPrx4Session.html", null ],
      [ "NsScrPrx4SessionHandler", "classNsScrPrx4SessionHandler.html", null ],
      [ "NsScrPrx4Setup", "classNsScrPrx4Setup.html", null ],
      [ "NsScrPrx4TaskConfig", "classNsScrPrx4TaskConfig.html", null ],
      [ "NsScrPrx4VlbiAuxObservation", "classNsScrPrx4VlbiAuxObservation.html", null ],
      [ "NsScrPrx4VlbiObservation", "classNsScrPrx4VlbiObservation.html", null ]
    ] ],
    [ "QPlainTextEdit", "classQPlainTextEdit.html", [
      [ "SgGuiLogger", "classSgGuiLogger.html", null ]
    ] ],
    [ "QScrollArea", "classQScrollArea.html", [
      [ "SgPlotScroller", "classSgPlotScroller.html", null ]
    ] ],
    [ "qt_meta_stringdata_NsBrowseNotUsedObsDialog_t", "structqt__meta__stringdata__NsBrowseNotUsedObsDialog__t.html", null ],
    [ "qt_meta_stringdata_NsMainWindow_t", "structqt__meta__stringdata__NsMainWindow__t.html", null ],
    [ "qt_meta_stringdata_NsScrPrx4Band_t", "structqt__meta__stringdata__NsScrPrx4Band__t.html", null ],
    [ "qt_meta_stringdata_NsScrPrx4Baseline_t", "structqt__meta__stringdata__NsScrPrx4Baseline__t.html", null ],
    [ "qt_meta_stringdata_NsScrPrx4Logger_t", "structqt__meta__stringdata__NsScrPrx4Logger__t.html", null ],
    [ "qt_meta_stringdata_NsScrPrx4Object_t", "structqt__meta__stringdata__NsScrPrx4Object__t.html", null ],
    [ "qt_meta_stringdata_NsScrPrx4ParametersDescriptor_t", "structqt__meta__stringdata__NsScrPrx4ParametersDescriptor__t.html", null ],
    [ "qt_meta_stringdata_NsScrPrx4Session_t", "structqt__meta__stringdata__NsScrPrx4Session__t.html", null ],
    [ "qt_meta_stringdata_NsScrPrx4SessionHandler_t", "structqt__meta__stringdata__NsScrPrx4SessionHandler__t.html", null ],
    [ "qt_meta_stringdata_NsScrPrx4Setup_t", "structqt__meta__stringdata__NsScrPrx4Setup__t.html", null ],
    [ "qt_meta_stringdata_NsScrPrx4Source_t", "structqt__meta__stringdata__NsScrPrx4Source__t.html", null ],
    [ "qt_meta_stringdata_NsScrPrx4Station_t", "structqt__meta__stringdata__NsScrPrx4Station__t.html", null ],
    [ "qt_meta_stringdata_NsScrPrx4TaskConfig_t", "structqt__meta__stringdata__NsScrPrx4TaskConfig__t.html", null ],
    [ "qt_meta_stringdata_NsScrPrx4VlbiAuxObservation_t", "structqt__meta__stringdata__NsScrPrx4VlbiAuxObservation__t.html", null ],
    [ "qt_meta_stringdata_NsScrPrx4VlbiObservation_t", "structqt__meta__stringdata__NsScrPrx4VlbiObservation__t.html", null ],
    [ "qt_meta_stringdata_NsSessionEditDialog_t", "structqt__meta__stringdata__NsSessionEditDialog__t.html", null ],
    [ "qt_meta_stringdata_NsSessionNameDialog_t", "structqt__meta__stringdata__NsSessionNameDialog__t.html", null ],
    [ "qt_meta_stringdata_NsSetupDialog_t", "structqt__meta__stringdata__NsSetupDialog__t.html", null ],
    [ "qt_meta_stringdata_NsStartupWizard_t", "structqt__meta__stringdata__NsStartupWizard__t.html", null ],
    [ "qt_meta_stringdata_NsTestDialog_t", "structqt__meta__stringdata__NsTestDialog__t.html", null ],
    [ "qt_meta_stringdata_NsTestFour1Dialog_t", "structqt__meta__stringdata__NsTestFour1Dialog__t.html", null ],
    [ "qt_meta_stringdata_SgGuiLogger_t", "structqt__meta__stringdata__SgGuiLogger__t.html", null ],
    [ "qt_meta_stringdata_SgGuiLoggerConfig_t", "structqt__meta__stringdata__SgGuiLoggerConfig__t.html", null ],
    [ "qt_meta_stringdata_SgGuiParameterCfg_t", "structqt__meta__stringdata__SgGuiParameterCfg__t.html", null ],
    [ "qt_meta_stringdata_SgGuiPiaReport_t", "structqt__meta__stringdata__SgGuiPiaReport__t.html", null ],
    [ "qt_meta_stringdata_SgGuiQTreeWidgetExt_t", "structqt__meta__stringdata__SgGuiQTreeWidgetExt__t.html", null ],
    [ "qt_meta_stringdata_SgGuiTaskConfig_t", "structqt__meta__stringdata__SgGuiTaskConfig__t.html", null ],
    [ "qt_meta_stringdata_SgGuiTaskConfigDialog_t", "structqt__meta__stringdata__SgGuiTaskConfigDialog__t.html", null ],
    [ "qt_meta_stringdata_SgGuiVlbiBaselineList_t", "structqt__meta__stringdata__SgGuiVlbiBaselineList__t.html", null ],
    [ "qt_meta_stringdata_SgGuiVlbiBlnInfoEditor_t", "structqt__meta__stringdata__SgGuiVlbiBlnInfoEditor__t.html", null ],
    [ "qt_meta_stringdata_SgGuiVlbiHistory_t", "structqt__meta__stringdata__SgGuiVlbiHistory__t.html", null ],
    [ "qt_meta_stringdata_SgGuiVlbiSourceList_t", "structqt__meta__stringdata__SgGuiVlbiSourceList__t.html", null ],
    [ "qt_meta_stringdata_SgGuiVlbiSrcInfoEditor_t", "structqt__meta__stringdata__SgGuiVlbiSrcInfoEditor__t.html", null ],
    [ "qt_meta_stringdata_SgGuiVlbiSrcStrModelEditor_t", "structqt__meta__stringdata__SgGuiVlbiSrcStrModelEditor__t.html", null ],
    [ "qt_meta_stringdata_SgGuiVlbiStationList_t", "structqt__meta__stringdata__SgGuiVlbiStationList__t.html", null ],
    [ "qt_meta_stringdata_SgGuiVlbiStnClockBreakEditor_t", "structqt__meta__stringdata__SgGuiVlbiStnClockBreakEditor__t.html", null ],
    [ "qt_meta_stringdata_SgGuiVlbiStnInfoEditor_t", "structqt__meta__stringdata__SgGuiVlbiStnInfoEditor__t.html", null ],
    [ "qt_meta_stringdata_SgPlot_t", "structqt__meta__stringdata__SgPlot__t.html", null ],
    [ "qt_meta_stringdata_SgPlotArea_t", "structqt__meta__stringdata__SgPlotArea__t.html", null ],
    [ "qt_meta_stringdata_SgPlotScroller_t", "structqt__meta__stringdata__SgPlotScroller__t.html", null ],
    [ "qt_meta_stringdata_VcStartupWizard_t", "structqt__meta__stringdata__VcStartupWizard__t.html", null ],
    [ "qt_meta_stringdata_VmStartupWizard_t", "structqt__meta__stringdata__VmStartupWizard__t.html", null ],
    [ "qt_meta_stringdata_VpDefaultCableCalSignEditor_t", "structqt__meta__stringdata__VpDefaultCableCalSignEditor__t.html", null ],
    [ "qt_meta_stringdata_VpRinexDataEditor_t", "structqt__meta__stringdata__VpRinexDataEditor__t.html", null ],
    [ "qt_meta_stringdata_VpStartupWizard_t", "structqt__meta__stringdata__VpStartupWizard__t.html", null ],
    [ "QTreeWidget", "classQTreeWidget.html", [
      [ "SgGuiQTreeWidgetExt", "classSgGuiQTreeWidgetExt.html", null ]
    ] ],
    [ "QTreeWidgetItem", "classQTreeWidgetItem.html", [
      [ "NsQTreeWidgetItem", "classNsQTreeWidgetItem.html", null ],
      [ "SgGuiTcUserCorrectionItem", "classSgGuiTcUserCorrectionItem.html", null ],
      [ "SgGuiVlbiBaselineItem", "classSgGuiVlbiBaselineItem.html", null ],
      [ "SgGuiVlbiClockBreakItem", "classSgGuiVlbiClockBreakItem.html", null ],
      [ "SgGuiVlbiSourceItem", "classSgGuiVlbiSourceItem.html", null ],
      [ "SgGuiVlbiSrcStrModelItem", "classSgGuiVlbiSrcStrModelItem.html", null ],
      [ "SgGuiVlbiStationItem", "classSgGuiVlbiStationItem.html", null ]
    ] ],
    [ "QWidget", "classQWidget.html", [
      [ "SgGuiLoggerConfig", "classSgGuiLoggerConfig.html", null ],
      [ "SgGuiTaskConfig", "classSgGuiTaskConfig.html", null ],
      [ "SgGuiVlbiBaselineList", "classSgGuiVlbiBaselineList.html", null ],
      [ "SgGuiVlbiHistory", "classSgGuiVlbiHistory.html", null ],
      [ "SgGuiVlbiSourceList", "classSgGuiVlbiSourceList.html", null ],
      [ "SgGuiVlbiStationList", "classSgGuiVlbiStationList.html", null ],
      [ "SgPlot", "classSgPlot.html", null ],
      [ "SgPlotArea", "classSgPlotArea.html", null ]
    ] ],
    [ "QWizard", "classQWizard.html", [
      [ "NsStartupWizard", "classNsStartupWizard.html", null ],
      [ "VcStartupWizard", "classVcStartupWizard.html", null ],
      [ "VmStartupWizard", "classVmStartupWizard.html", null ],
      [ "VpStartupWizard", "classVpStartupWizard.html", null ]
    ] ],
    [ "QWizardPage", "classQWizardPage.html", [
      [ "NsWizardPage", "classNsWizardPage.html", null ],
      [ "VcWizardPage", "classVcWizardPage.html", null ],
      [ "VmWizardPage", "classVmWizardPage.html", null ],
      [ "VpWizardPage", "classVpWizardPage.html", null ]
    ] ],
    [ "SgEstimator::RegularSolutionCarrier", "structSgEstimator_1_1RegularSolutionCarrier.html", null ],
    [ "SgEstimator::RPCarrier", "structSgEstimator_1_1RPCarrier.html", null ],
    [ "Sg3dMatrix", "classSg3dMatrix.html", [
      [ "Sg3dMatrixR", "classSg3dMatrixR.html", [
        [ "Sg3dMatrixR2dot", "classSg3dMatrixR2dot.html", null ],
        [ "Sg3dMatrixRdot", "classSg3dMatrixRdot.html", null ]
      ] ]
    ] ],
    [ "Sg3dVector", "classSg3dVector.html", [
      [ "SgOceanLoad", "classSgOceanLoad.html", null ]
    ] ],
    [ "SgAgvChunk", "classSgAgvChunk.html", null ],
    [ "SgAgvDatum< C >", "classSgAgvDatum.html", null ],
    [ "SgAgvDatumDescriptor", "classSgAgvDatumDescriptor.html", null ],
    [ "SgAgvDatumString", "classSgAgvDatumString.html", null ],
    [ "SgAgvRecord", "classSgAgvRecord.html", null ],
    [ "SgAPrioriRecComponent", "classSgAPrioriRecComponent.html", null ],
    [ "SgArcStorage", "classSgArcStorage.html", null ],
    [ "SgAttribute", "classSgAttribute.html", [
      [ "SgMeteoData", "classSgMeteoData.html", null ],
      [ "SgObjectInfo", "classSgObjectInfo.html", [
        [ "SgVlbiBand", "classSgVlbiBand.html", null ],
        [ "SgVlbiBaselineInfo", "classSgVlbiBaselineInfo.html", null ],
        [ "SgVlbiSourceInfo", "classSgVlbiSourceInfo.html", null ],
        [ "SgVlbiStationInfo", "classSgVlbiStationInfo.html", null ]
      ] ],
      [ "SgObservation", "classSgObservation.html", [
        [ "SgVlbiAuxObservation", "classSgVlbiAuxObservation.html", null ],
        [ "SgVlbiObservation", "classSgVlbiObservation.html", null ]
      ] ],
      [ "SgParameterBreak", "classSgParameterBreak.html", null ],
      [ "SgParameterCfg", "classSgParameterCfg.html", null ],
      [ "SgPartial", "classSgPartial.html", [
        [ "SgParameter", "classSgParameter.html", null ]
      ] ],
      [ "SgVlbiSessionInfo", "classSgVlbiSessionInfo.html", [
        [ "SgVlbiSession", "classSgVlbiSession.html", null ]
      ] ]
    ] ],
    [ "SgBaselineExternalWeight", "classSgBaselineExternalWeight.html", null ],
    [ "SgCableCalReading", "classSgCableCalReading.html", null ],
    [ "SgChannelSetup", "classSgChannelSetup.html", null ],
    [ "SgChannelSkeded", "classSgChannelSkeded.html", null ],
    [ "SgClockBreakAgent", "classSgClockBreakAgent.html", null ],
    [ "SgClockBreakIndicator", "classSgClockBreakIndicator.html", null ],
    [ "SgCubicSpline", "classSgCubicSpline.html", null ],
    [ "SgDbbc3TpReading", "classSgDbbc3TpReading.html", null ],
    [ "SgDbhDataBlock", "classSgDbhDataBlock.html", [
      [ "SgDbhTeBlock", "classSgDbhTeBlock.html", null ]
    ] ],
    [ "SgDbhDatumDescriptor", "classSgDbhDatumDescriptor.html", null ],
    [ "SgDbhFormat", "classSgDbhFormat.html", null ],
    [ "SgDbhHistoryEntry", "classSgDbhHistoryEntry.html", null ],
    [ "SgDbhImage", "classSgDbhImage.html", null ],
    [ "SgDbhListOfDescriptors", "classSgDbhListOfDescriptors.html", [
      [ "SgDbhTeBlock", "classSgDbhTeBlock.html", null ]
    ] ],
    [ "SgDbhObservationEntry", "classSgDbhObservationEntry.html", null ],
    [ "SgDbhPhysicalRecord", "classSgDbhPhysicalRecord.html", [
      [ "SgDbhDataRecord< short >", "classSgDbhDataRecord.html", null ],
      [ "SgDbhDataRecord< C >", "classSgDbhDataRecord.html", null ],
      [ "SgDbhDataRecordString", "classSgDbhDataRecordString.html", null ],
      [ "SgDbhServiceRecord", "classSgDbhServiceRecord.html", [
        [ "SgDbhServiceRecordDe", "classSgDbhServiceRecordDe.html", null ],
        [ "SgDbhServiceRecordDr", "classSgDbhServiceRecordDr.html", null ],
        [ "SgDbhServiceRecordHS1", "classSgDbhServiceRecordHS1.html", null ],
        [ "SgDbhServiceRecordHS2", "classSgDbhServiceRecordHS2.html", null ],
        [ "SgDbhServiceRecordTc", "classSgDbhServiceRecordTc.html", null ],
        [ "SgDbhServiceRecordTe", "classSgDbhServiceRecordTe.html", null ]
      ] ],
      [ "SgDbhTeBlock::SgDbhServiceRecordP3", "classSgDbhTeBlock_1_1SgDbhServiceRecordP3.html", null ],
      [ "SgDbhTeBlock::SgDbhServiceRecordP4", "classSgDbhTeBlock_1_1SgDbhServiceRecordP4.html", null ]
    ] ],
    [ "SgDbhStartBlock", "classSgDbhStartBlock.html", null ],
    [ "SgDbhTcBlock", "classSgDbhTcBlock.html", null ],
    [ "SgDot2xpsReading", "classSgDot2xpsReading.html", null ],
    [ "SgEccDat", "classSgEccDat.html", null ],
    [ "SgEccRec", "classSgEccRec.html", null ],
    [ "SgEccSite", "classSgEccSite.html", null ],
    [ "SgEstimator", "classSgEstimator.html", null ],
    [ "SgExternalEopFile", "classSgExternalEopFile.html", null ],
    [ "SgExternalWeights", "classSgExternalWeights.html", null ],
    [ "SgIdentities", "classSgIdentities.html", null ],
    [ "SgIoDriver", "classSgIoDriver.html", [
      [ "SgAgvDriver", "classSgAgvDriver.html", null ],
      [ "SgVgosDb", "classSgVgosDb.html", null ]
    ] ],
    [ "SgIoExternalFilter", "classSgIoExternalFilter.html", null ],
    [ "SgIoExtFilterHandler", "classSgIoExtFilterHandler.html", null ],
    [ "SgKombBd01Record", "classSgKombBd01Record.html", null ],
    [ "SgKombBd02Record", "classSgKombBd02Record.html", null ],
    [ "SgKombBd03Record", "classSgKombBd03Record.html", null ],
    [ "SgKombBd04Record", "classSgKombBd04Record.html", null ],
    [ "SgKombBd05Record", "classSgKombBd05Record.html", null ],
    [ "SgKombHeader", "classSgKombHeader.html", null ],
    [ "SgKombHeaderRecord", "classSgKombHeaderRecord.html", null ],
    [ "SgKombOb01Record", "classSgKombOb01Record.html", null ],
    [ "SgKombOb02Record", "classSgKombOb02Record.html", null ],
    [ "SgKombOb03Record", "classSgKombOb03Record.html", null ],
    [ "SgKombReader", "classSgKombReader.html", null ],
    [ "SgLogger", "classSgLogger.html", [
      [ "SgGuiLogger", "classSgGuiLogger.html", null ]
    ] ],
    [ "SgMappingFunction", "classSgMappingFunction.html", [
      [ "SgDryMF_MTT", "classSgDryMF__MTT.html", null ],
      [ "SgDryMF_NMF", "classSgDryMF__NMF.html", null ],
      [ "SgGradMF_CH", "classSgGradMF__CH.html", null ],
      [ "SgWetMF_MTT", "classSgWetMF__MTT.html", null ],
      [ "SgWetMF_NMF", "classSgWetMF__NMF.html", null ]
    ] ],
    [ "SgMasterRecord", "classSgMasterRecord.html", null ],
    [ "SgMatrix", "classSgMatrix.html", [
      [ "SgUtMatrix", "classSgUtMatrix.html", [
        [ "SgSymMatrix", "classSgSymMatrix.html", null ]
      ] ]
    ] ],
    [ "SgMeteoReading", "classSgMeteoReading.html", null ],
    [ "SgMJD", "classSgMJD.html", [
      [ "SgObservation", "classSgObservation.html", null ],
      [ "SgParameterBreak", "classSgParameterBreak.html", null ]
    ] ],
    [ "SgModelEop_JMG_96_hf", "classSgModelEop__JMG__96__hf.html", null ],
    [ "SgModelsInfo", "classSgModelsInfo.html", null ],
    [ "SgNcdfAttribute", "classSgNcdfAttribute.html", null ],
    [ "SgNcdfDimension", "classSgNcdfDimension.html", null ],
    [ "SgNcdfVariable", "classSgNcdfVariable.html", null ],
    [ "SgNetCdf", "classSgNetCdf.html", null ],
    [ "SgNetworkStations", "classSgNetworkStations.html", null ],
    [ "SgNetworkStnRecord", "classSgNetworkStnRecord.html", null ],
    [ "SgOnSourceRecord", "classSgOnSourceRecord.html", null ],
    [ "SgParametersDescriptor", "classSgParametersDescriptor.html", null ],
    [ "SgPcalReading", "classSgPcalReading.html", null ],
    [ "SgPlotBranch", "classSgPlotBranch.html", null ],
    [ "SgPlotCarrier", "classSgPlotCarrier.html", null ],
    [ "SgPwlStorage", "classSgPwlStorage.html", [
      [ "SgPwlStorageBSplineL", "classSgPwlStorageBSplineL.html", null ],
      [ "SgPwlStorageBSplineQ", "classSgPwlStorageBSplineQ.html", null ],
      [ "SgPwlStorageIncRates", "classSgPwlStorageIncRates.html", null ]
    ] ],
    [ "SgRefraction", "classSgRefraction.html", null ],
    [ "SgSefdReading", "classSgSefdReading.html", null ],
    [ "SgSolutionReporter", "classSgSolutionReporter.html", null ],
    [ "SgSourceKey", "classSgSourceKey.html", null ],
    [ "SgStnLogCollector", "classSgStnLogCollector.html", null ],
    [ "SgStnLogReadings", "classSgStnLogReadings.html", null ],
    [ "SgTask", "classSgTask.html", null ],
    [ "SgTaskConfig", "classSgTaskConfig.html", null ],
    [ "SgTaskManager", "classSgTaskManager.html", [
      [ "SgSingleSessionTaskManager", "classSgSingleSessionTaskManager.html", null ]
    ] ],
    [ "SgTidalUt1", "classSgTidalUt1.html", null ],
    [ "SgTraklReading", "classSgTraklReading.html", null ],
    [ "SgTsysReading", "classSgTsysReading.html", null ],
    [ "SgUt1TidalTableEntry_Clipped", "structSgUt1TidalTableEntry__Clipped.html", null ],
    [ "SgUt1TidalTableEntry_Simplified", "structSgUt1TidalTableEntry__Simplified.html", null ],
    [ "SgUt1TidalTableEntry_Standard", "structSgUt1TidalTableEntry__Standard.html", null ],
    [ "SgVdbVariable", "classSgVdbVariable.html", null ],
    [ "SgVector", "classSgVector.html", null ],
    [ "SgVersion", "classSgVersion.html", null ],
    [ "SgVexDefBlock", "classSgVexDefBlock.html", null ],
    [ "SgVexFile", "classSgVexFile.html", null ],
    [ "SgVexLiteralBlock", "classSgVexLiteralBlock.html", null ],
    [ "SgVexParameter", "classSgVexParameter.html", null ],
    [ "SgVexRefSatement", "classSgVexRefSatement.html", null ],
    [ "SgVexScanBlock", "classSgVexScanBlock.html", null ],
    [ "SgVexSection", "classSgVexSection.html", null ],
    [ "SgVlbaLogCollector", "classSgVlbaLogCollector.html", null ],
    [ "SgVlbiHistoryRecord", "classSgVlbiHistoryRecord.html", null ],
    [ "SgVlbiMeasurement", "classSgVlbiMeasurement.html", null ],
    [ "SgVlbiObservable", "classSgVlbiObservable.html", null ],
    [ "SgWrmsable", "classSgWrmsable.html", null ],
    [ "SgZenithDelay", "classSgZenithDelay.html", [
      [ "SgDryZD_Saastamoinen", "classSgDryZD__Saastamoinen.html", null ],
      [ "SgWetZD_Saastamoinen", "classSgWetZD__Saastamoinen.html", null ]
    ] ],
    [ "SgEstimator::SmoothCarrier", "structSgEstimator_1_1SmoothCarrier.html", null ],
    [ "SgVgosDb::StationDescriptor", "structSgVgosDb_1_1StationDescriptor.html", null ],
    [ "SgVexFile::StationSetup", "classSgVexFile_1_1StationSetup.html", null ],
    [ "SgChannelSkeded::StnCfg", "classSgChannelSkeded_1_1StnCfg.html", null ],
    [ "SgEstimator::StochasticSolutionCarrier", "structSgEstimator_1_1StochasticSolutionCarrier.html", null ],
    [ "SgVlbiSourceInfo::StructModel", "classSgVlbiSourceInfo_1_1StructModel.html", [
      [ "SgVlbiSourceInfo::StructModelMp", "classSgVlbiSourceInfo_1_1StructModelMp.html", null ]
    ] ],
    [ "SgDbbc3TpReading::TpRecord", "classSgDbbc3TpReading_1_1TpRecord.html", null ],
    [ "VcCalc2SessionIfc", "classVcCalc2SessionIfc.html", null ],
    [ "VcSetup", "classVcSetup.html", null ],
    [ "vdbcOptions", "structvdbcOptions.html", null ],
    [ "vdbmOptions", "structvdbmOptions.html", null ],
    [ "vdbpOptions", "structvdbpOptions.html", null ],
    [ "VmSetup", "classVmSetup.html", null ],
    [ "VpSetup", "classVpSetup.html", null ]
];