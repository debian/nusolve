var classSgVdbVariable =
[
    [ "Scope", "classSgVdbVariable.html#a538b7579749be94fd1ba85b40e2d0023", [
      [ "S_Session", "classSgVdbVariable.html#a538b7579749be94fd1ba85b40e2d0023a79d1bcc2b8e9f3b16113753bfcdcfb63", null ],
      [ "S_Scan", "classSgVdbVariable.html#a538b7579749be94fd1ba85b40e2d0023ad517c43f24f2f2dcd6ce5083c3224981", null ],
      [ "S_Obs", "classSgVdbVariable.html#a538b7579749be94fd1ba85b40e2d0023ad4e4c21ca6cb274fbb797e5ca33993a6", null ]
    ] ],
    [ "SgVdbVariable", "classSgVdbVariable.html#af5ed854d8b066b9aa1263c143e283ecf", null ],
    [ "SgVdbVariable", "classSgVdbVariable.html#a217ea6871ac04183059d6fbcb6291082", null ],
    [ "~SgVdbVariable", "classSgVdbVariable.html#a961b12b9128fd88dae9fa92f387a7213", null ],
    [ "className", "classSgVdbVariable.html#a87173fed9f73ce1852ca8d00b8f70d40", null ],
    [ "compositeName", "classSgVdbVariable.html#a6f93df1fd1dd1d9dcef30202dee57097", null ],
    [ "empty", "classSgVdbVariable.html#a77c7ad12120da2bb53e478861e373673", null ],
    [ "getBand", "classSgVdbVariable.html#a415d2605eeeefe1e918774ed8c6e289a", null ],
    [ "getCurrentInstitution", "classSgVdbVariable.html#a5118a0ab9d26e17904318be445a99f09", null ],
    [ "getFileName", "classSgVdbVariable.html#ab88471bae44facb57f64cfd9c6b9cd8b", null ],
    [ "getFileName4Output", "classSgVdbVariable.html#ac5c567ff1eff60195923f01615a2cc32", null ],
    [ "getInstitution", "classSgVdbVariable.html#afa8b16ea8c90444c46198cf200042563", null ],
    [ "getKind", "classSgVdbVariable.html#a7d2dce31a1e765314fab75d438838bb9", null ],
    [ "getStub", "classSgVdbVariable.html#aaee06b1a05812db4e7161f2ddf80dbcd", null ],
    [ "getSubDir", "classSgVdbVariable.html#a7a8963d966cf4810990dfbb23301c8ff", null ],
    [ "getVersion", "classSgVdbVariable.html#a42b4a1f715a81584bf785708071d1a4c", null ],
    [ "isEmpty", "classSgVdbVariable.html#a3dc6175e551aa99e281a983dd56cc54b", null ],
    [ "name4export", "classSgVdbVariable.html#a7e17b374d1ad83dba9b1fcb371f6ae21", null ],
    [ "parseString", "classSgVdbVariable.html#a405f2d56cff64322850980841b3f71ca", null ],
    [ "setBand", "classSgVdbVariable.html#a4a8845640365acfc5418daee5e1543c7", null ],
    [ "setCurrentInstitution", "classSgVdbVariable.html#a78aaace85b081952d8bbced0be460c36", null ],
    [ "setFileName", "classSgVdbVariable.html#acb8be3e72784acee9d802f144402b2cf", null ],
    [ "setFileName4Output", "classSgVdbVariable.html#aa781ea6787e3062045089867830415bb", null ],
    [ "setInstitution", "classSgVdbVariable.html#ae7931bed450dccfac6ce3d9ef87400dc", null ],
    [ "setKind", "classSgVdbVariable.html#a9364cfdad6ea160ba519447f28fca458", null ],
    [ "setStub", "classSgVdbVariable.html#ab7d567cb889e66e2f94d0a80e217e44c", null ],
    [ "setSubDir", "classSgVdbVariable.html#aa685093980d2a7beca3c21d4019cacf9", null ],
    [ "setVersion", "classSgVdbVariable.html#a979a99251772c3d4d78c0849905b927b", null ],
    [ "band_", "classSgVdbVariable.html#a3ac1e80c35328a318d008a14f6efd5d8", null ],
    [ "currentInstitution_", "classSgVdbVariable.html#a4e7f305bfc220fa0dea902680b0378ee", null ],
    [ "fileName4Output_", "classSgVdbVariable.html#ae0693f04522ad933305859ac6ad8b9ad", null ],
    [ "fileName_", "classSgVdbVariable.html#ab3ea31ce371d43ab840c0b3be5d951b1", null ],
    [ "have2adjustPermissions_", "classSgVdbVariable.html#a485a567c76ca62726e079ae92c9f6f12", null ],
    [ "institution_", "classSgVdbVariable.html#ae92dab5fb5575587a83198a1beeb349b", null ],
    [ "kind_", "classSgVdbVariable.html#a272b00586be847f040882b80f0ac3911", null ],
    [ "stdStub_", "classSgVdbVariable.html#a68b73857a8585476c0f33a3dc41c6d07", null ],
    [ "stdSubDir_", "classSgVdbVariable.html#a23b93ab9503222c62388a570479b2ebb", null ],
    [ "stub_", "classSgVdbVariable.html#aee3fa564b599c608c02314afeb7ff97a", null ],
    [ "subDir_", "classSgVdbVariable.html#a139ec187a4957c23058f4ab47e1caccd", null ],
    [ "type_", "classSgVdbVariable.html#a499e0cd98497526333beaa0588a6f615", null ],
    [ "version_", "classSgVdbVariable.html#a68d67c0299899be9ad8111e0309d101a", null ]
];