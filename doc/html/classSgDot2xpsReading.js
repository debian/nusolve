var classSgDot2xpsReading =
[
    [ "SgDot2xpsReading", "classSgDot2xpsReading.html#a2cd4d247c5b749ba800e3ff9eb3895bd", null ],
    [ "SgDot2xpsReading", "classSgDot2xpsReading.html#a513ef6dd2c0f8305db01d629b9efb5e4", null ],
    [ "~SgDot2xpsReading", "classSgDot2xpsReading.html#aa0120f812b86520e5342d8f91970a27f", null ],
    [ "addDot2gpsByBrd", "classSgDot2xpsReading.html#a4d59e29562d598245502eaf9a14a8088", null ],
    [ "className", "classSgDot2xpsReading.html#abedae806e29ba491f690477b56b2d1fc", null ],
    [ "getDot2gpsByBrd", "classSgDot2xpsReading.html#a70964ae8d9072a5324a3f2b0d7a79982", null ],
    [ "getOsRec", "classSgDot2xpsReading.html#a27b89d9f87b43c9f0f45f58d095a0823", null ],
    [ "getT", "classSgDot2xpsReading.html#ac9cee7b750d1d7cde7ed006b9fa025c0", null ],
    [ "osRec", "classSgDot2xpsReading.html#ada6f3ddfda95bc05f67c68b065310404", null ],
    [ "setOsRec", "classSgDot2xpsReading.html#a93030190f1f8d29ba44cda29c361c668", null ],
    [ "setT", "classSgDot2xpsReading.html#a065f07a39bff65bdfd72e142d5765737", null ],
    [ "dot2gpsByBrd_", "classSgDot2xpsReading.html#a08fd78e097ab6f5b0f9059e288b90bc4", null ],
    [ "osRec_", "classSgDot2xpsReading.html#ac2c568e0077a20a2bab6de994980ee1c", null ],
    [ "t_", "classSgDot2xpsReading.html#abb33f85c87f3ce8cb909c7e996eb3f57", null ]
];