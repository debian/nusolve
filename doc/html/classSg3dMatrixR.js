var classSg3dMatrixR =
[
    [ "Sg3dMatrixR", "classSg3dMatrixR.html#a078862f6eb7da18196f94de0f5c99bf1", null ],
    [ "Sg3dMatrixR", "classSg3dMatrixR.html#ae75167e3fc568ef52db568922c3d03af", null ],
    [ "~Sg3dMatrixR", "classSg3dMatrixR.html#ae46aba9349f5d5031da69bb9649622b2", null ],
    [ "angle", "classSg3dMatrixR.html#ac76f0c0a26792cbad33372ed6cb48a1b", null ],
    [ "axis", "classSg3dMatrixR.html#a9af1b35ed2ba45a58a89530b106eed0a", null ],
    [ "operator()", "classSg3dMatrixR.html#a1b1e8314df6c6fbee8f6105825322717", null ],
    [ "angle_", "classSg3dMatrixR.html#a7b299fa95fa3510a2828f2b29fcf3a65", null ],
    [ "axis_", "classSg3dMatrixR.html#abea23d4caec8ef9c5065793e83cf0afb", null ],
    [ "cosA_", "classSg3dMatrixR.html#a9bc59fe426420d757b39d2c25e546e5e", null ],
    [ "sinA_", "classSg3dMatrixR.html#a1f648b7eaefe16295017627736ce95cf", null ]
];