# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/nusolve/issues
# Bug-Submit: https://github.com/<user>/nusolve/issues/new
# Changelog: https://github.com/<user>/nusolve/blob/master/CHANGES
# Documentation: https://github.com/<user>/nusolve/wiki
# Repository-Browse: https://github.com/<user>/nusolve
# Repository: https://github.com/<user>/nusolve.git
