/*
 *
 *    This file is a part of nuSolve. nuSolve is a part of CALC/SOLVE system
 *    and is designed to perform data analyis of a geodetic VLBI session.
 *    Copyright (C) 2017-2020 Sergei Bolotin.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
//#include "nuSolve.h"
//#include "NsScrSupport.h"

//#include <iostream>

//#include <QtCore/QFile>


//#include <SgVlbiSession.h>


#include "NsScrPrx4TaskConfig.h"


//#if 0 < HAVE_SCRIPTS

//#include <QtScript/QScriptEngine>

/*=======================================================================================================
*
*                           METHODS:
* 
*======================================================================================================*/


/*=======================================================================================================
*
*                           FRIENDS:
* 
*======================================================================================================*/
//


















/*=====================================================================================================*/
//
// constants:
//
const QString                   str_WRONG("WRONG");

const QString                   str_VD_NONE("CFG.VD_NONE");
const QString                   str_VD_SB_DELAY("CFG.VD_SB_DELAY");
const QString                   str_VD_GRP_DELAY("CFG.VD_GRP_DELAY");
const QString                   str_VD_PHS_DELAY("CFG.VD_PHS_DELAY");

const QString                   str_VR_NONE("CFG.VR_NONE");
const QString                   str_VR_PHS_RATE("CFG.VR_PHS_RATE");

const QString                   str_WCM_BAND("CFG.WCM_BAND");
const QString                   str_WCM_BASELINE("CFG.WCM_BASELINE");

const QString                   str_OPM_BAND("CFG.OPM_BAND");
const QString                   str_OPM_BASELINE("CFG.OPM_BASELINE");

const QString                   str_OPA_ELIMINATE("CFG.OPA_ELIMINATE");
const QString                   str_OPA_RESTORE("CFG.OPA_RESTORE");

const QString                   str_EPM_INCRATE("CFG.EPM_INCRATE");
const QString                   str_EPM_BSPLINE_LINEA("CFG.EPM_BSPLINE_LINEA");
const QString                   str_EPM_BSPLINE_QUADR("CFG.EPM_BSPLINE_QUADR");


const QString                   str_TZM_NONE("CFG.TZM_NONE");
const QString                   str_TZM_NMF ("CFG.TZM_NMF");
const QString                   str_TZM_MTT ("CFG.TZM_MTT");

const QString    								str_CCS_DEFAULT("CFG.CCS_DEFAULT");
const QString    								str_CCS_FSLG("CFG.CCS_FSLG");
const QString    								str_CCS_CDMS("CFG.CCS_CDMS");
const QString    								str_CCS_PCMT("CFG.CCS_PCMT");

/*=====================================================================================================*/





/*=====================================================================================================*/
//#endif // SCRIPT_SUPPORT

