/*
 *
 *    This file is a part of nuSolve. nuSolve is a part of CALC/SOLVE system
 *    and is designed to perform data analyis of a geodetic VLBI session.
 *    Copyright (C) 2017-2020 Sergei Bolotin.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef NS_SCR_PRX_4_SESSION_H
#define NS_SCR_PRX_4_SESSION_H


#ifdef HAVE_CONFIG_H
#   include <config.h>
#endif


#include <QtCore/QtGlobal>


#include <QtCore/QDateTime>
#include <QtCore/QMap>
#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QStringList>


#if 0 < HAVE_SCRIPTS
#    include <QtScript/QScriptEngine>
#    include <QtScript/QScriptValue>
#endif

#include <SgObjectInfo.h>
#include <SgVlbiBand.h>
#include <SgVlbiBaselineInfo.h>
#include <SgVlbiSession.h>
#include <SgVlbiSourceInfo.h>
#include <SgVlbiStationInfo.h>


#include "NsScrPrx4TaskConfig.h"


class NsScrPrx4VlbiObservation;
class NsScrPrx4VlbiAuxObservation;
class NsScrPrx4Session;
class NsScrPrx4Station;
class NsScrPrx4Baseline;
class NsScrPrx4Source;


/***===================================================================================================*/
/**
 *
 *
 */
/**====================================================================================================*/
class NsScrPrx4Object : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString key
    READ getKey)
  Q_PROPERTY(QString name
    READ getName)
  Q_PROPERTY(int numTotal
    READ getNumTotal)
  Q_PROPERTY(int numProcessed
    READ getNumProcessed)
  Q_PROPERTY(int numUsable
    READ getNumUsable)
  Q_PROPERTY(QDateTime tFirst
    READ getTfirst)
  Q_PROPERTY(QDateTime tLast
    READ getTlast)
  Q_PROPERTY(double sigma2add
    READ getSigma2add WRITE setSigma2add)
//  Q_PROPERTY(double normalizedResid
//    READ getNormedResid)
//  Q_PROPERTY(double dispersion
//    READ getDispersion)
  Q_PROPERTY(double wrms
    READ getWrms)
  Q_PROPERTY(double chi2
    READ getChi2)
  Q_PROPERTY(double reducedChi2
    READ getReducedChi2)
  Q_PROPERTY(double dof
    READ getDof)


public:
  inline NsScrPrx4Object(SgObjectInfo& obj, QObject *parent=0)
    : QObject(parent) {obj_=&obj; setUpName();};
  inline ~NsScrPrx4Object() {obj_=NULL;};

public slots:
  inline const QString& getKey() const {return obj_->getKey();};
  inline QString getName() const {return name_;};
  inline int getNumTotal() const {return obj_->numTotal(DT_DELAY);};
  inline int getNumProcessed() const {return obj_->numProcessed(DT_DELAY);};
  inline int getNumUsable() const {return obj_->numUsable(DT_DELAY);};
  inline QDateTime getTfirst() const {return obj_->tFirst(DT_DELAY).toQDateTime();};
  inline QDateTime getTlast() const {return obj_->tLast(DT_DELAY).toQDateTime();};
   inline double getSigma2add() const {return obj_->getSigma2add(DT_DELAY);};
  //inline double getNormedResid() const {return obj_->normedResid(DT_DELAY);};
  //inline double getDispersion() const {return obj_->dispersion(DT_DELAY, false);};
  inline double getWrms() const {return obj_->wrms(DT_DELAY);};
  inline double getChi2() const {return obj_->chi2(DT_DELAY);};
  inline double getReducedChi2() const {return obj_->reducedChi2(DT_DELAY);};
  inline double getDof() const {return obj_->dof(DT_DELAY);};

  inline void setSigma2add(double d) {obj_->setSigma2add(DT_DELAY, d);};


protected:
  SgObjectInfo                 *obj_;
  QString                       name_;
  inline void setUpName() {name_=obj_->getKey().simplified();};
};
/*=====================================================================================================*/





/***===================================================================================================*/
/**
 *
 *
 */
/**====================================================================================================*/
class NsScrPrx4Band : public NsScrPrx4Object
{
  Q_OBJECT
  Q_PROPERTY(double refFreq
    READ getRefFreq)
  Q_PROPERTY(int numOfChannels
    READ getNumOfChannels)
  Q_PROPERTY(QDateTime tCreation
    READ getTCreation)
  Q_PROPERTY(int inputFileVersion
    READ getInputFileVersion)
  Q_PROPERTY(QString correlatorType
    READ getCorrelatorType)
  Q_PROPERTY(double groupDelaysAmbigSpacing
    READ getGroupDelaysAmbigSpacing)
  Q_PROPERTY(const QList<NsScrPrx4Station*>& stations
    READ getStations)
  Q_PROPERTY(const QList<NsScrPrx4Baseline*>& baselines
    READ getBaselines)
  Q_PROPERTY(const QList<NsScrPrx4Source*>& sources
    READ getSources)

public:
  inline NsScrPrx4Band(SgVlbiBand& band, QObject *parent=0) : NsScrPrx4Object(band, parent),
		prxStations_(), prxBaselines_(), prxSources_() {};
  inline ~NsScrPrx4Band() {};
  void postLoad();

public slots:
  inline double getRefFreq() const {return bnd()->getFrequency();};
  inline int getNumOfChannels() const {return bnd()->getMaxNumOfChannels();};
  inline QDateTime getTCreation() const {return bnd()->getTCreation().toQDateTime();};
  inline int getInputFileVersion() const {return bnd()->getInputFileVersion();};
  inline const QString& getCorrelatorType() const {return bnd()->getCorrelatorType();};
  inline double getGroupDelaysAmbigSpacing() const {return bnd()->typicalGrdAmbigSpacing();};
  inline const QList<NsScrPrx4Station*>& getStations() {return prxStations_;};
  inline const QList<NsScrPrx4Baseline*>& getBaselines() {return prxBaselines_;};
  inline const QList<NsScrPrx4Source*>& getSources() {return prxSources_;};

protected:
  inline SgVlbiBand* bnd() {return (SgVlbiBand*)obj_;};
  inline const SgVlbiBand* bnd() const {return (SgVlbiBand*)obj_;};

private:
  QList<NsScrPrx4Station*>      prxStations_;
  QList<NsScrPrx4Baseline*>     prxBaselines_;
  QList<NsScrPrx4Source*>       prxSources_;
};
/*=====================================================================================================*/




/***===================================================================================================*/
/**
 *
 *
 */
/**====================================================================================================*/
class NsScrPrx4Station : public NsScrPrx4Object
{
  Q_OBJECT

  Q_PROPERTY(int numOfClockPolynoms
    READ getClocksModelOrder WRITE setClocksModelOrder)
  Q_PROPERTY(double cableCalSign
    READ getCableCalSign)
  Q_PROPERTY(double latitude
    READ getLatitude)
  Q_PROPERTY(double longitude
    READ getLongitude)
  Q_PROPERTY(QString sId
    READ getSid)

  // Attributes:
  Q_PROPERTY(bool isValid
    READ isValid WRITE setIsValid)
  Q_PROPERTY(bool estimateCoords
    READ getEstimateCoords WRITE setEstimateCoords)
  Q_PROPERTY(bool constrainCoords
    READ getConstrainCoords WRITE setConstrainCoords)
  Q_PROPERTY(bool referenceClocks
    READ getRreferenceClocks WRITE setRreferenceClocks)
  Q_PROPERTY(bool useCableCal
    READ getUseCableCal WRITE setUseCableCal)
  //
  Q_PROPERTY(bool badMeteo
    READ getBadMeteo WRITE setBadMeteo)
  //
  Q_PROPERTY(bool hasCccFslg
    READ getHasCccFslg)
  Q_PROPERTY(bool hasCccCdms
    READ getHasCccCdms)
  Q_PROPERTY(bool hasCccPcmt
    READ getHasCccPcmt)
  //
  Q_PROPERTY(bool estimateTroposphere
    READ getEstimateTroposphere WRITE setEstimateTroposphere)
  Q_PROPERTY(int numOfScans
    READ getNumOfScans)
  Q_PROPERTY(const QList<NsScrPrx4VlbiAuxObservation*>& auxObs
    READ getAuxObs)
  Q_PROPERTY(NsScrPrx4TaskConfig::CableCalSource flybyCableCalSource
    READ getFlybyCableCalSource)
//  WRITE setFlybyCableCalSource READ getFlybyCableCalSource)


public:
  inline NsScrPrx4Station(SgVlbiStationInfo& stn, QObject *parent=0) : 
    NsScrPrx4Object(stn, parent), prxAuxes_() {};
  inline ~NsScrPrx4Station() {prxAuxes_.clear();};

public slots:
  inline int getClocksModelOrder() const {return stn()->getClocksModelOrder();};
  inline double getCableCalSign() const {return stn()->getCableCalMultiplierDBCal();};
  inline bool isValid() const {return !stn()->isAttr(SgVlbiStationInfo::Attr_NOT_VALID);};
  inline bool getEstimateCoords() const {return stn()->isAttr(SgVlbiStationInfo::Attr_ESTIMATE_COO);};
  inline bool getConstrainCoords() const {return stn()->isAttr(SgVlbiStationInfo::Attr_CONSTRAIN_COO);};
  inline bool getRreferenceClocks() const 
    {return stn()->isAttr(SgVlbiStationInfo::Attr_REFERENCE_CLOCKS);};
  inline bool getUseCableCal() const {return !stn()->isAttr(SgVlbiStationInfo::Attr_IGNORE_CABLE_CAL);};
  inline bool getBadMeteo() const {return stn()->isAttr(SgVlbiStationInfo::Attr_BAD_METEO);};
  inline bool getHasCccFslg() const {return stn()->isAttr(SgVlbiStationInfo::Attr_HAS_CCC_FSLG);};
  inline bool getHasCccCdms() const {return stn()->isAttr(SgVlbiStationInfo::Attr_HAS_CCC_CDMS);};
  inline bool getHasCccPcmt() const {return stn()->isAttr(SgVlbiStationInfo::Attr_HAS_CCC_PCMT);};
  inline bool getEstimateTroposphere() const 
    {return !stn()->isAttr(SgVlbiStationInfo::Attr_DONT_ESTIMATE_TRPS);};
  inline NsScrPrx4TaskConfig::CableCalSource getFlybyCableCalSource() const 
		{return (NsScrPrx4TaskConfig::CableCalSource)stn()->getFlybyCableCalSource();};
 
  inline int getNumOfScans() const {return stn()->auxObservationByScanId()->size();};
  inline const QList<NsScrPrx4VlbiAuxObservation*>& getAuxObs() const {return prxAuxes_;};
  inline double getLatitude() const {return stn()->getLatitude();};
  inline double getLongitude() const {return stn()->getLongitude();};
  inline QString getSid() const {return stn()->getSid();};

  inline void setClocksModelOrder(int m) {stn()->setClocksModelOrder(m);};
  //  inline void setCableCalMultiplier(double d)  {stn()->setCableCalMultiplier(d);};
  inline void setIsValid(bool is) {stn()->assignAttr(SgVlbiStationInfo::Attr_NOT_VALID, !is);};

  inline void setEstimateCoords(bool is) {stn()->assignAttr(SgVlbiStationInfo::Attr_ESTIMATE_COO, is);};
  inline void setConstrainCoords(bool is)
    {stn()->assignAttr(SgVlbiStationInfo::Attr_CONSTRAIN_COO, is);};
  inline void setRreferenceClocks(bool is)
    {stn()->assignAttr(SgVlbiStationInfo::Attr_REFERENCE_CLOCKS, is);};
  inline void setUseCableCal(bool is)
    {stn()->assignAttr(SgVlbiStationInfo::Attr_IGNORE_CABLE_CAL, !is);};
  inline void setBadMeteo(bool is)
    {stn()->assignAttr(SgVlbiStationInfo::Attr_BAD_METEO, is);};
  inline void setEstimateTroposphere(bool is)
    {stn()->assignAttr(SgVlbiStationInfo::Attr_DONT_ESTIMATE_TRPS, !is);};
//inline void setFlybyCableCalSource(NsScrPrx4TaskConfig::CableCalSource s) 
//  {stn()->setFlybyCableCalSource((SgTaskConfig::CableCalSource) s);};
  bool setFlybyCableCalSource(NsScrPrx4TaskConfig::CableCalSource s);


private:
  QList<NsScrPrx4VlbiAuxObservation*>
                                prxAuxes_;

  inline SgVlbiStationInfo* stn() {return (SgVlbiStationInfo*)obj_;};
  inline const SgVlbiStationInfo* stn() const {return (SgVlbiStationInfo*)obj_;};
  friend class NsScrPrx4Session;
};
/*=====================================================================================================*/







/***===================================================================================================*/
/**
 *
 *
 */
/**====================================================================================================*/
class NsScrPrx4Baseline : public NsScrPrx4Object
{
  Q_OBJECT
  Q_PROPERTY(double length
    READ getLength)
  // Attributes:
  Q_PROPERTY(bool isValid
    READ isValid WRITE setIsValid)
  Q_PROPERTY(bool estimateClocks
    READ getEstimateClocks WRITE setEstimateClocks)

public:
  inline NsScrPrx4Baseline(SgVlbiBaselineInfo& bln, QObject *parent=0) : 
    NsScrPrx4Object(bln, parent) {setUpName();};
  inline ~NsScrPrx4Baseline() {};

public slots:
  inline double getLength() const {return bln()->getLength();};
  inline bool isValid() const {return !bln()->isAttr(SgVlbiBaselineInfo::Attr_NOT_VALID);};
  inline bool getEstimateClocks() const
    {return bln()->isAttr(SgVlbiBaselineInfo::Attr_ESTIMATE_CLOCKS);};

  inline void setIsValid(bool is) {bln()->assignAttr(SgVlbiBaselineInfo::Attr_NOT_VALID, !is);};
  inline void setEstimateClocks(bool is)
    {bln()->assignAttr(SgVlbiBaselineInfo::Attr_ESTIMATE_CLOCKS, is);};

private:
  inline SgVlbiBaselineInfo* bln() {return (SgVlbiBaselineInfo*)obj_;};
  inline const SgVlbiBaselineInfo* bln() const {return (SgVlbiBaselineInfo*)obj_;};
  inline void setUpName()
    {name_=obj_->getKey().left(8).simplified() + ":" + obj_->getKey().right(8).simplified();};
};
/*=====================================================================================================*/






/***===================================================================================================*/
/**
 *
 *
 */
/**====================================================================================================*/
class NsScrPrx4Source : public NsScrPrx4Object
{
  Q_OBJECT

//  Q_PROPERTY(int numOfTotalScans
//    READ getNumOfTotalScans)
//  Q_PROPERTY(int numOfProcdScans
//    READ getNumOfProcdScans)
  Q_PROPERTY(double rightAscension
    READ getRa WRITE setRa)
  Q_PROPERTY(double declination
    READ getDn WRITE setDn)
//
/*
  Q_PROPERTY(double K
    READ getK WRITE setK)
  Q_PROPERTY(double B
    READ getB WRITE setB)
  Q_PROPERTY(double X
    READ getX WRITE setX)
  Q_PROPERTY(double Y
    READ getY WRITE setY)
  Q_PROPERTY(double Ksig
    READ getKsig)
  Q_PROPERTY(double Bsig
    READ getBsig)
  Q_PROPERTY(double Xsig
    READ getXsig)
  Q_PROPERTY(double Ysig
    READ getYsig)


  Q_PROPERTY(double a0
    READ getA0 WRITE setA0)
  Q_PROPERTY(double b0
    READ getB0 WRITE setB0)
  Q_PROPERTY(double theta0
    READ getTheta0 WRITE setTheta0)
  Q_PROPERTY(double a1
    READ getA1 WRITE setA1)
  Q_PROPERTY(double b1
    READ getB1 WRITE setB1)
  Q_PROPERTY(double theta1
    READ getTheta1 WRITE setTheta1)

  Q_PROPERTY(double a0sig
    READ getA0sig)
  Q_PROPERTY(double b0sig
    READ getB0sig)
  Q_PROPERTY(double theta0sig
    READ getTheta0sig)
  Q_PROPERTY(double a1sig
    READ getA1sig)
  Q_PROPERTY(double b1sig
    READ getB1sig)
  Q_PROPERTY(double theta1sig
    READ getTheta1sig)

  Q_PROPERTY(bool estimateRatio
    READ getEstimateRatio WRITE setEstimateRatio)
  Q_PROPERTY(bool estimateSpIdx
    READ getEstimateSpIdx WRITE setEstimateSpIdx)
  Q_PROPERTY(bool estimatePosition
    READ getEstimatePosition WRITE setEstimatePosition)

  Q_PROPERTY(bool estimateA0
    READ getEstimateA0 WRITE setEstimateA0)
  Q_PROPERTY(bool estimateB0
    READ getEstimateB0 WRITE setEstimateB0)
  Q_PROPERTY(bool estimateTheta0
    READ getEstimateTheta0 WRITE setEstimateTheta0)
  Q_PROPERTY(bool estimateA1
    READ getEstimateA1 WRITE setEstimateA1)
  Q_PROPERTY(bool estimateB1
    READ getEstimateB1 WRITE setEstimateB1)
  Q_PROPERTY(bool estimateTheta1
    READ getEstimateTheta1 WRITE setEstimateTheta1)

  Q_PROPERTY(int smtModelType
    READ getSmtType WRITE setSmtType)
*/

  Q_PROPERTY(QString aprioriComments
    READ getAprioriComments)
  //
  // Attributes:
  Q_PROPERTY(bool isValid
    READ isValid WRITE setIsValid)
  Q_PROPERTY(bool estimateCoords
    READ getEstimateCoords WRITE setEstimateCoords)
  Q_PROPERTY(bool constrainCoords
    READ getConstrainCoords WRITE setConstrainCoords)
//
  Q_PROPERTY(bool applySsm
    READ getApplySsm WRITE setApplySsm)
  Q_PROPERTY(bool testAttr
    READ getTestAttr WRITE setTestAttr)
    
public:
  inline NsScrPrx4Source(SgVlbiSourceInfo& src, QObject *parent=0) : NsScrPrx4Object(src, parent) {};
  inline ~NsScrPrx4Source() {};

public slots:
  inline double getRa() const {return src()->getRA();};
  inline double getDn() const {return src()->getDN();};
//
/*
  inline double getK() const {return src()->getK();};
  inline double getB() const {return src()->getB();};
  inline double getX() const {return src()->getX();};
  inline double getY() const {return src()->getY();};

  inline double getA0() const {return src()->getA0();};
  inline double getB0() const {return src()->getB0();};
  inline double getTheta0() const {return src()->getTheta0();};
  inline double getA1() const {return src()->getA1();};
  inline double getB1() const {return src()->getB1();};
  inline double getTheta1() const {return src()->getTheta1();};

  inline double getKsig() const {return src()->getKsig();};
  inline double getBsig() const {return src()->getBsig();};
  inline double getXsig() const {return src()->getXsig();};
  inline double getYsig() const {return src()->getYsig();};
  inline double getA0sig() const {return src()->getA0Sig();};
  inline double getB0sig() const {return src()->getB0Sig();};
  inline double getTheta0sig() const {return src()->getTheta0Sig();};
  inline double getA1sig() const {return src()->getA1Sig();};
  inline double getB1sig() const {return src()->getB1Sig();};
  inline double getTheta1sig() const {return src()->getTheta1Sig();};

  inline bool getEstimateRatio() const {return src()->getEstimateRatio();};
  inline bool getEstimateSpIdx() const {return src()->getEstimateSpIdx();};
  inline bool getEstimatePosition() const {return src()->getEstimatePosition();};
  inline bool getEstimateA0() const {return src()->getEstimateA0();};
  inline bool getEstimateB0() const {return src()->getEstimateB0();};
  inline bool getEstimateTheta0() const {return src()->getEstimateT0();};
  inline bool getEstimateA1() const {return src()->getEstimateA1();};
  inline bool getEstimateB1() const {return src()->getEstimateB1();};
  inline bool getEstimateTheta1() const {return src()->getEstimateT1();};
  inline int  getSmtType() const {return src()->getSmtType();};
*/
//inline int getNumOfTotalScans() const {return src()->getTotalScanNum();};
//inline int getNumOfProcdScans() const {return src()->getProcdScanNum();};

  inline bool isValid() const {return !src()->isAttr(SgVlbiSourceInfo::Attr_NOT_VALID);};
  inline bool getEstimateCoords() const {return src()->isAttr(SgVlbiSourceInfo::Attr_ESTIMATE_COO);};
  inline bool getConstrainCoords() const {return src()->isAttr(SgVlbiSourceInfo::Attr_CONSTRAIN_COO);};
  inline bool getApplySsm() const {return src()->isAttr(SgVlbiSourceInfo::Attr_APPLY_SSM);};
  inline bool getTestAttr() const {return src()->isAttr(SgVlbiSourceInfo::Attr_TEST);};

  inline const QString& getAprioriComments() const {return src()->getAprioriComments();};

  inline void setRa(double d)  {src()->setRA(d);};
  inline void setDn(double d)  {src()->setDN(d);};

/*
  inline void setK(double d)  {src()->setK(d);};
  inline void setB(double d)  {src()->setB(d);};
  inline void setX(double d)  {src()->setX(d);};
  inline void setY(double d)  {src()->setY(d);};
  inline void setA0(double d)  {src()->setA0(d);};
  inline void setB0(double d)  {src()->setB0(d);};
  inline void setTheta0(double d)  {src()->setTheta0(d);};
  inline void setA1(double d)  {src()->setA1(d);};
  inline void setB1(double d)  {src()->setB1(d);};
  inline void setTheta1(double d)  {src()->setTheta1(d);};

  inline void setEstimateRatio(bool e) {src()->setEstimateRatio(e);};
  inline void setEstimateSpIdx(bool e) {src()->setEstimateSpIdx(e);};
  inline void setEstimatePosition(bool e) {src()->setEstimatePosition(e);};
  inline void setEstimateA0(bool e) {src()->setEstimateA0(e);};
  inline void setEstimateB0(bool e) {src()->setEstimateB0(e);};
  inline void setEstimateTheta0(bool e) {src()->setEstimateT0(e);};
  inline void setEstimateA1(bool e) {src()->setEstimateA1(e);};
  inline void setEstimateB1(bool e) {src()->setEstimateB1(e);};
  inline void setEstimateTheta1(bool e) {src()->setEstimateT1(e);};

  inline void setSmtType(int t) {src()->setSmtType((SgVlbiSourceInfo::StructureModelType) t);};
*/
  //
  //
  inline void setIsValid(bool is) {src()->assignAttr(SgVlbiSourceInfo::Attr_NOT_VALID, !is);};
  inline void setEstimateCoords(bool is) {src()->assignAttr(SgVlbiSourceInfo::Attr_ESTIMATE_COO, is);};
  inline void setConstrainCoords(bool is) {src()->assignAttr(SgVlbiSourceInfo::Attr_CONSTRAIN_COO, is);};
  inline void setTestAttr(bool is) {src()->assignAttr(SgVlbiSourceInfo::Attr_TEST, is);};
  inline void setApplySsm(bool is) {src()->assignAttr(SgVlbiSourceInfo::Attr_APPLY_SSM, is);};

  //
  inline int numOfSrcStructPoints() {return src()->sModel().size();};
  //
  inline void addSrcStructPoint(double k, double b, double x, double y, 
    bool estK=false, bool estB=false, bool estR=false)
    {src()->addSrcStructPoint(k, b, x, y, estK, estB, estR);};
  //
  inline void clearSrcStructPoints() {src()->clearSrcStructPoints();};

  //
  inline void setK_i(int i, double d)  {if (0<=i && i<src()->sModel().size()) src()->sModel()[i].setK(d);};
  inline void setB_i(int i, double d)  {if (0<=i && i<src()->sModel().size()) src()->sModel()[i].setB(d);};
  inline void setX_i(int i, double d)  {if (0<=i && i<src()->sModel().size()) src()->sModel()[i].setX(d);};
  inline void setY_i(int i, double d)  {if (0<=i && i<src()->sModel().size()) src()->sModel()[i].setY(d);};

  inline double getK_i(int i) const 
    {return 0<=i && i<src()->sModel().size()?src()->sModel().at(i).getK():0.0;};
  inline double getB_i(int i) const 
    {return 0<=i && i<src()->sModel().size()?src()->sModel().at(i).getB():0.0;};
  inline double getX_i(int i) const 
    {return 0<=i && i<src()->sModel().size()?src()->sModel().at(i).getX():0.0;};
  inline double getY_i(int i) const 
    {return 0<=i && i<src()->sModel().size()?src()->sModel().at(i).getY():0.0;};
  inline double getK_iSig(int i) const 
    {return 0<=i && i<src()->sModel().size()?src()->sModel().at(i).getKsig():0.0;};
  inline double getB_iSig(int i) const 
    {return 0<=i && i<src()->sModel().size()?src()->sModel().at(i).getBsig():0.0;};
  inline double getX_iSig(int i) const 
    {return 0<=i && i<src()->sModel().size()?src()->sModel().at(i).getXsig():0.0;};
  inline double getY_iSig(int i) const 
    {return 0<=i && i<src()->sModel().size()?src()->sModel().at(i).getYsig():0.0;};


private:
  inline SgVlbiSourceInfo* src() {return (SgVlbiSourceInfo*)obj_;};
  inline const SgVlbiSourceInfo* src() const {return (SgVlbiSourceInfo*)obj_;};
};
/*=====================================================================================================*/







/***===================================================================================================*/
/**
 *
 *
 */
/**====================================================================================================*/
class NsScrPrx4Session : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString name 
    READ getName)
  Q_PROPERTY(bool isOk
    READ getIsOk)
  Q_PROPERTY(QString networkSuffix
    READ getNetworkSuffix)
  Q_PROPERTY(QString sessionCode
    READ getSessionCode)
  Q_PROPERTY(QString networkID
    READ getNetworkID)
  Q_PROPERTY(QString description
    READ getDescription)
  Q_PROPERTY(QString userFlag
    READ getUserFlag)
  Q_PROPERTY(QString officialName
    READ getOfficialName)
  Q_PROPERTY(QString correlatorName
    READ getCorrelatorName)
  Q_PROPERTY(QString correlatorType
    READ getCorrelatorType)
  Q_PROPERTY(QString submitterName
    READ getSubmitterName)
  Q_PROPERTY(QString schedulerName
    READ getSchedulerName)
  Q_PROPERTY(QString piAgencyName
    READ getPiAgencyName)
  Q_PROPERTY(int numOfBands
    READ getNumOfBands)
  Q_PROPERTY(int numOfStations
    READ getNumOfStations)
  Q_PROPERTY(int numOfBaselines
    READ getNumOfBaselines)
  Q_PROPERTY(int numOfSources
    READ getNumOfSources)
  Q_PROPERTY(int numOfObservations
    READ getNumOfObservations)
  Q_PROPERTY(QDateTime tCreation
    READ getTCreation)
  Q_PROPERTY(QDateTime tStart
    READ getTStart)
  Q_PROPERTY(QDateTime tFinis
    READ getTFinis)
  Q_PROPERTY(QDateTime tMean
    READ getTMean)

  Q_PROPERTY(int primaryBandIdx
    READ getPrimaryBandIdx)
  Q_PROPERTY(const QList<NsScrPrx4Band*>& bands
    READ getBands)
  Q_PROPERTY(const QList<NsScrPrx4Station*>& stations
    READ getStations)
  Q_PROPERTY(const QList<NsScrPrx4Baseline*>& baselines
    READ getBaselines)
  Q_PROPERTY(const QList<NsScrPrx4Source*>& sources
    READ getSources)
  Q_PROPERTY(const QList<NsScrPrx4VlbiObservation*>& observations
    READ getObservations)

  Q_PROPERTY(bool hasReferenceClocksStation
    READ hasReferenceClocksStation)
  Q_PROPERTY(bool hasReferenceCoordinatesStation
    READ hasReferenceCoordinatesStation)

  Q_PROPERTY(double dUt1Value
    READ dUt1Value)
  Q_PROPERTY(double dUt1Correction
    READ dUt1Correction)
  Q_PROPERTY(double dUt1StdDev
    READ dUt1StdDev)



public:
  inline NsScrPrx4Session(SgVlbiSession& session, QObject *parent=0)
    : QObject(parent), prxBands_(), prxStations_(), prxBaselines_(), prxSources_(),
      prxObservations_(), stationsByKey_(), baselinesByKey_(), sourcesByKey_()
      {session_=&session; primaryBandIdx_=-1;};

  inline ~NsScrPrx4Session() 
    {session_=NULL;
      prxBands_.clear(); prxStations_.clear(); prxBaselines_.clear(), prxSources_.clear();
      prxObservations_.clear(); stationsByKey_.clear(); baselinesByKey_.clear(); sourcesByKey_.clear();};

  inline void setIsOk(bool is) {isOk_=is;};
  void postLoad();
  


public slots:
  inline const QString& getName() const {return session_->getName();};
  inline const QString& getNetworkSuffix() const {return session_->getNetworkSuffix();};
  inline const QString& getSessionCode() const {return session_->getSessionCode();};
  inline const QString& getNetworkID() const {return session_->getNetworkID();};
  inline const QString& getDescription() const {return session_->getDescription();};
  inline const QString& getUserFlag() const {return session_->getUserFlag();};
  inline const QString& getOfficialName() const {return session_->getOfficialName();};
  inline const QString& getCorrelatorName() const {return session_->getCorrelatorName();};
  inline const QString& getCorrelatorType() const {return session_->getCorrelatorType();};
  inline const QString& getSubmitterName() const {return session_->getSubmitterName();};
  inline const QString& getSchedulerName() const {return session_->getSchedulerName();};
  inline const QString& getPiAgencyName() const {return session_->getPiAgencyName();};
  inline int getNumOfBands() const {return session_->bandByKey().size();};
  inline int getNumOfStations() const {return session_->stationsByName().size();};
  inline int getNumOfBaselines() const {return session_->baselinesByName().size();};
  inline int getNumOfSources() const {return session_->sourcesByName().size();};
  inline int getNumOfObservations() const {return session_->observations().size();};
  inline QDateTime getTCreation() const {return session_->getTCreation().toQDateTime();};
  inline QDateTime getTStart() const {return session_->getTStart().toQDateTime();};
  inline QDateTime getTFinis() const {return session_->getTFinis().toQDateTime();};
  inline QDateTime getTMean() const {return session_->getTMean().toQDateTime();};
  inline bool getIsOk() const {return isOk_;};
  //
  inline int getPrimaryBandIdx() const {return primaryBandIdx_;};
  inline const QList<NsScrPrx4Band*>& getBands() {return prxBands_;};
  inline const QList<NsScrPrx4Station*>& getStations() {return prxStations_;};
  inline const QList<NsScrPrx4Baseline*>& getBaselines() {return prxBaselines_;};
  inline const QList<NsScrPrx4Source*>& getSources() {return prxSources_;};
  inline const QList<NsScrPrx4VlbiObservation*>& getObservations() {return prxObservations_;};
  

  inline double dUt1Value() const {return session_->dUt1Value();};
  inline double dUt1Correction() const {return session_->dUt1Correction();};
  inline double dUt1StdDev() const {return session_->dUt1StdDev();};


  inline NsScrPrx4Station* lookUpStation(const QString& key)
    {return stationsByKey_.contains(key)?stationsByKey_.value(key):NULL;};
  inline NsScrPrx4Baseline* lookUpBaseline(const QString& key)
    {return baselinesByKey_.contains(key)?baselinesByKey_.value(key):NULL;};
  inline NsScrPrx4Source* lookUpSource(const QString& key)
    {return sourcesByKey_.contains(key)?sourcesByKey_.value(key):NULL;};


  void dispatchChangeOfClocksParameterModel(SgParameterCfg::PMode);
  void dispatchChangeOfZenithParameterModel(SgParameterCfg::PMode);

  //
  // functionality:
  //
  //
  inline bool hasReferenceClocksStation() {return isOk_ && session_->hasReferenceClocksStation();};
  //
  inline bool hasReferenceCoordinatesStation()
    {return isOk_ && session_->hasReferenceCoordinatesStation();};
  //
  //
  inline void resetAllEditings() {if (isOk_) session_->resetAllEditings(false);};
  //
  inline void clearAuxSigmas() {if (isOk_) session_->zerofySigma2add();};
  //
  inline void suppressNotSoGoodObs() {if (isOk_) session_->suppressNotSoGoodObs();};
  //
  //
  inline void pickupReferenceClocksStation() {if (isOk_) session_->pickupReferenceClocksStation();};
  //
  inline void pickupReferenceCoordinatesStation()
    {if (isOk_) session_->pickupReferenceCoordinatesStation();};
  //
  inline void setNumOfClockPolynoms4Stations(int n) {if (isOk_) session_->setClockModelOrder4Stations(n);};
  //
  inline void setReferenceClocksStation(const QString& stnKey)
    {if (isOk_) session_->setReferenceClocksStation(stnKey);};
  //
  inline void checkUseOfManualPhaseCals() {if (isOk_) session_->checkUseOfManualPhaseCals();};


  //
  inline void process() {if (isOk_) session_->process(true, false);};
  //
  inline void calcIono(bool sbdOnly=false) 
    {if (isOk_) session_->calculateIonoCorrections(sbdOnly?NULL:session_->getConfig());};
  //
  inline void zeroIono() {if (isOk_) session_->zerofyIonoCorrections(session_->getConfig());};

  //
  inline void checkClockBreaks(int bandIdx)
    {if (isOk_) session_->checkBandForClockBreaks(bandIdx, true);};
  //
  inline int eliminateOutliers(int bandIdx) {return isOk_?session_->eliminateOutliers(bandIdx):0;};
  //
  inline int restoreOutliers(int bandIdx) {return isOk_?session_->restoreOutliers(bandIdx):0;};
  //
  inline int doReWeighting() {return isOk_?session_->doReWeighting():0;};

  //
  inline void scanAmbiguityMultipliers(int bandIdx)
    {if (isOk_) session_->scanBaselines4GrDelayAmbiguities(bandIdx);};

  inline void eliminateOutliersSimpleMode(int bandIdx, int maxNumOfPasses, 
    double threshold, double upperLimit)
    {if (isOk_) session_->eliminateOutliersSimpleMode(bandIdx, maxNumOfPasses, threshold, upperLimit);};
  
	inline void eliminateLargeOutliers(int bandIdx, int maxNumOfPasses, double wrmsRatio)
		{if (isOk_) session_->eliminateLargeOutliers(bandIdx, maxNumOfPasses, wrmsRatio);};

	inline bool writeUserData2File(const QString& fileName)
		{if (isOk_) return session_->writeUserData2File(fileName); else return false;};
  
  // just a shortcut:
  inline void doStdSetup()
    {suppressNotSoGoodObs(); pickupReferenceClocksStation();
      pickupReferenceCoordinatesStation(); checkUseOfManualPhaseCals();};


private:
  bool                          isOk_;
  SgVlbiSession                *session_;
  int                           primaryBandIdx_;
  QList<NsScrPrx4Band*>         prxBands_;
  QList<NsScrPrx4Station*>      prxStations_;
  QList<NsScrPrx4Baseline*>     prxBaselines_;
  QList<NsScrPrx4Source*>       prxSources_;
  QList<NsScrPrx4VlbiObservation*>
                                prxObservations_;
  QMap<QString, NsScrPrx4Station*>
                                stationsByKey_;
  QMap<QString, NsScrPrx4Baseline*>
                                baselinesByKey_;
  QMap<QString, NsScrPrx4Source*>
                                sourcesByKey_;
  
};
/*=====================================================================================================*/





#if 0 < HAVE_SCRIPTS

Q_DECLARE_METATYPE(NsScrPrx4Band*);
Q_DECLARE_METATYPE(QList<NsScrPrx4Band*>);

Q_DECLARE_METATYPE(NsScrPrx4Station*);
Q_DECLARE_METATYPE(QList<NsScrPrx4Station*>);

Q_DECLARE_METATYPE(NsScrPrx4Baseline*);
Q_DECLARE_METATYPE(QList<NsScrPrx4Baseline*>);

Q_DECLARE_METATYPE(NsScrPrx4Source*);
Q_DECLARE_METATYPE(QList<NsScrPrx4Source*>);

#endif















/*=====================================================================================================*/
//
// aux functions:
//

#if 0 < HAVE_SCRIPTS


// band:
inline QScriptValue toScriptValue4Band(QScriptEngine *eng, NsScrPrx4Band* const &in)
{
  return eng->newQObject(in);
};
//
inline void fromScriptValue4Band(const QScriptValue &obj, NsScrPrx4Band* &out)
{
  out = qobject_cast<NsScrPrx4Band*>(obj.toQObject());
};



// station:
inline QScriptValue toScriptValue4Stn(QScriptEngine *eng, NsScrPrx4Station* const &in)
{
  return eng->newQObject(in);
};
//
inline void fromScriptValue4Stn(const QScriptValue &obj, NsScrPrx4Station* &out)
{
  out = qobject_cast<NsScrPrx4Station*>(obj.toQObject());
};



// baseline:
inline QScriptValue toScriptValue4Bln(QScriptEngine *eng, NsScrPrx4Baseline* const &in)
{
  return eng->newQObject(in);
};
//
inline void fromScriptValue4Bln(const QScriptValue &obj, NsScrPrx4Baseline* &out)
{
  out = qobject_cast<NsScrPrx4Baseline*>(obj.toQObject());
};




// source:
inline QScriptValue toScriptValue4Src(QScriptEngine *eng, NsScrPrx4Source* const &in)
{
  return eng->newQObject(in);
};
//
inline void fromScriptValue4Src(const QScriptValue &obj, NsScrPrx4Source* &out)
{
  out = qobject_cast<NsScrPrx4Source*>(obj.toQObject());
};
#endif  // SCRIPT_SUPPORT



/*=====================================================================================================*/
#endif // NS_SCR_PRX_4_SESSION_H
