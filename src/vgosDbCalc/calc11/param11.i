!     Include file param11.i 
!
!     ******************************************************************
!     *      CALC 11 Version                                           *
!     ******************************************************************
!
!     JPL_DE421 is the path name for the JPL ephemeris file.
!
      Integer*4   N_RECL_JPL
      PARAMETER ( N_RECL_JPL =    4 )
      Integer*4   K_SIZE_JPL
      PARAMETER ( K_SIZE_JPL = 2036 )
      Integer*4   I_RECL_JPL
! For Little Endians
      CHARACTER  JPL_DE421*80
      PARAMETER (JPL_DE421 = '/sgpvlbi/apriori/calc/DE421_little_Endian ') ! Local customization
!     PARAMETER (JPL_DE421 = 'DE421_little_Endian             ') ! Local customization
! For Big Endians
!     CHARACTER  JPL_DE421*80
!     PARAMETER (JPL_DE421 = '/sgpvlbi/apriori/calc/DE421_big_Endian    ') ! Local customization
!     PARAMETER (JPL_DE421 = 'DE421_big_Endian                ') ! Local customization
!  Sorry, no Medium Endians
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
      CHARACTER  A_TILTS*80
      PARAMETER (A_TILTS   = '/sgpvlbi/apriori/calc/tilt.dat            ') ! Local customization
!
      CHARACTER  OC_file*80
      PARAMETER (OC_file   = '/sgpvlbi/apriori/calc/blokq.c11.dat       ') ! Local customization
!
      CHARACTER  OPTL_file*80
      PARAMETER (OPTL_file = '/sgpvlbi/apriori/calc/blokq.c11.dat       ') ! Local customization
!
!  Leap seconds file (Not needed by difx correlator)
      CHARACTER  DFLEAP*80
      PARAMETER (DFLEAP    = '/sgpvlbi/apriori/calc/ut1ls.dat           ') ! Local customization
!     PARAMETER (DFLEAP    = 'ut1ls.dat                       ') ! Local customization
!
