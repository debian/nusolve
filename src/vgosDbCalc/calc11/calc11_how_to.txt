How to run calc10 and calc11
2014-FEB-07
David Gordon (David.Gordon-1@nasa.gov)


The Feb. 2014 Calc/Solve release contains both calc10 and calc11. This is to
allow a smooth transition from calc10 to calc11. Users should switch to calc11
as soon as possible. GSFC plans to make the switch to calc11 for operational
analysis in March 2014. At that time we will begin submitting calc11 analyzed 
databases to IVS. If necessary, other analysis centers could run calc10 on 
them.

Running calc10:
Calc10 has not changed, except that it is now called 'calc10' instead of 
'calc'.  To execute calc10:

calc10  0  0  <calcon-file>  <ext-file>
  or
calc10  0 -1  <calcon-file>  <ext-file>

 <calcon-file> is the name of the calc10 control file, which has 3
 lines for each database, and looks like:

Calc 10 with External Inputs  -DG-
$06APR03XA               $06APR03XA
00000000000000000000000000000000000000000000000000000000000000

The first line being a history entry of your choice.

 <ext-file> is the name of an external input control file. It should have
 5 lines giving the names of the files that are to be used for site positions,
 source positions, ocean loading coefficients, EOP's and antenna tilts. A
 typical file looks like:

Sites    /500/oper/solve_apriori_files/blokq.dat
Sources  /500/oper/solve_apriori_files/blokq.dat
Ocean    /500/oper/solve_apriori_files/blokq.dat
EOPs     /500/oper/solve_apriori_files/usno_finals.erp
Tilts    /500/oper/solve_apriori_files/tilt.dat

The blokq.dat file is normally used for sites, sources, and ocean loading
coefficients. Sources could alternately be a Solve source mod file.

The Fortran program 'up10' will still work to run calc10, as in earlier
Calc/Solve releases. It will require minor editing for other analysis
centers. The input to up10 is a file listing of the databases starting in
column 1.

However there is a new version of this program, called 'up10new', which
is a Python script. It is located in the $MK5_ROOT/scripts directory. It will
need some minor editing for other analysis centers. Edit the definition of 
variable s1 (line 76) for your analysis center. It expects to find the 
<ext-file> at $MK5_APRIORI/calc10.ext. This can be changed by modifying the
definitions of cfileroot (line 42) and/or runcalc (line 84). up10new should
be executable. Copy it to the $MK5_ROOT/bin directory. To use up10new:

up10new  <exp-list>  <initials/name>

 <exp-list> is the same as for program up10, a file listing the databases 
 starting in column 1. 
 <initials/name> are your initials or name, no blanks allowed.


Running calc11:
Running calc11 is similar to calc10. The following will work:

calc11  0  0  <calcon-file>  <ext-file>
  or
calc11  0 -1  <calcon-file>  <ext-file>

The only difference from calc10 is that the <ext-file> file now has 6 lines,
the sixth line giving the file of ocean pole tide loading coefficients. It
should look something like:

Sites    /500/oper/solve_apriori_files/blokq.c11.dat
Sources  /500/oper/solve_apriori_files/blokq.c11.dat
Ocean    /500/oper/solve_apriori_files/blokq.c11.dat
EOPs     /500/oper/solve_apriori_files/usno_finals.erp
Tilts    /500/oper/solve_apriori_files/tilt.dat
OPTL     /500/oper/solve_apriori_files/blokq.c11.dat

Please note that there is a new version of the blokq file designed for 
calc11. It has updated site and source positions from recent solutions.
It has a new set of ocean loading coefficients using the TPX07.2 model.
And it has a table of ocean pole tide loading coefficients, for the 
new model in the IERS2010 Conventions. This new calc11 blokq file is 
located in $MK5_APRIORI/blokq.c11.dat. 

There are two Python scripts that can be used to run calc11. Both are
located in the $MK5_ROOT/scripts directory. Script 'up11' is very similar 
to 'up10new'. It will also need editing for the history entry (line 76) 
and possibly the <ext-file> (lines 42 and 84). Without editing it expects
the <ext-file> to be  $MK5_APRIORI/calc11.ext. It should also be copied to
the $MK5_ROOT/bin directory. It is executed in the same way as up10new:

up11  <exp-list>  <initials/name>

The other calc11 script, 'upcalc11', is more complicated. It was designed 
for mass updating of all old databases to calc11. Typing 'upcalc11' will give
a list of the available options. The <exp-list> can be a file of databases
starting in either column 1 (as for up10, up10new, and up11) or in column
3 (do not mix these in the same file!). For the later case, the <exp-list>
file can be a Solve arc list of databases, which can have comments designated
by an asterisk (*) in column 1. The options for upcalc11 include a) run
calc11 or do not run calc11, b) create a superfile, c) delete the calc11
database version after running calc11 and creating a superfile, and 
d) supply an alternate <ext-file> (default is $MK5_APRIORI/calc11.ext). 
It has a future option to create a new-DB data structure, that is not yet
ready to use. The option to delete the database entry is to allow testing
without having to delete entries later by hand, or to save disk space. 

If you create superfiles, be sure to have the environment variables 
$SUPCAT_FILE and $SUP_DIR_LIST properly defined. You should not mix calc10
and calc11 superfiles in the same directory.  Also, if the <exp-list> is
a Solve arc list of databases starting in column 3, AND you create superfiles,
then an output file will be written that is a copy of the input file 
(including comments) except that the version numbers will be updated. So this
output file can then be used as a Solve arc list.  

New CORFxx file for calc11:
A new Solve CORFxx file is needed for calc11 to use the new contributions.
It should look like:

1000 Section 20
    1  cable             CABL DEL         CBL STAT        1  -1
    2  pcal rmv          UNPHASCL                        -1  -1
    3  Nieltdry          NDRYCONT                         1   1
    4  Nieltwet          NWETCONT                         1   1
    5  user cal
    6  WVR               WVRDELAY         WVR STAT       -1   1
1000 Section 21
    1  cable
1000 Section 30
    1  WVR @ zenith      WVR DELY
1000  Section 40
    1  Pol Tide          PTD CONT
    2  WobXCont          WOBXCONT
    3  WobYCont          WOBYCONT
    4  EarthTid          ETD CONT
    5  Ocean             OCE CONT
    6  UT1Ortho          UT1ORTHO
    7  XpYpOrth          WOBORTHO
    8  XpYpLib           WOBLIBRA
    9  UT1Libra          UT1LIBRA
   10  OPTLCont          OPTLCONT
   11  Feed Rot          FEED.COR
   12  OldOcean          OCE_OLD
   13  TiltRmvr          TILTRMVR
   14  PTideOld          PTOLDCON
1000  Section 41
    1  Pol_Tide
    2  WobXCont
    3  WobYCont
    4  EarthTid
    5  Ocean
    6  Feed Rot
1000  Section 42
    1  Pol Tide
    2  WobXCont
    3  WobYCont
    4  EarthTid
1000  Section 50
    1  SourStru          SOURSTRU
    2  SpurPcal          SPURPCAL
    3  UserMcal          USERMCAL
1000  Section 51
    1  UserMcal


The high frequency EOP corrections are 'UT1Ortho', 'XpYpOrth', 'XpYpLib', and
'UT1Libra'. The new ocean pole tide contribution is 'OPTLCont'. The 'Ocean' 
contribution is the new IERS2010 Conventions model using 342 constituent tides.
'OldOcean' is the older compution. Typically you should apply the first 11
contributions from section 40 above, to comply with the IERS2010 conventions.



Installing Calc11:

Before compiling and linking calc11, make sure you have the
necessary auxilliary files defined and that the source and station
limits are set appropriately.

Auxilliary files:
 Edit file param11.i. First, define parameter JPL_DE421, the
 path and file name of the JPL DE421 ephemeris. Both little
 Endian and big Endian versions are included in the $MK5_APRIORI 
 directory. Linux systems will generally be little Endian.
 The normal place to put these files would be in the
 $MK5_APRIORI directory. There are similar definitions for the
 antenna tilt file, the ocean loading coefficients file, and
 the ocean pole tide loading coefficients file; however these
 are normally overridden on the calc11 command line. There is also
 the leap second file, which must be specified.

Maximum number of sources in a database:
 This is initially set to 300 sources. To change this limit, edit
 file cmxsr11.i, and change line 20 from 'Parameter(MAX_ARC_SRC=300)'
 to whatever limit you want. Note that you may also need to make
 corresponding changes in dbedit, Solve, and perhaps other programs.

Maximum number of stations in a database:
 This is initially set to 36 stations. To increase or decrease this
 limit, edit file cmxst11.i, and change line 7 from
 'Parameter(Max_Stat = 36)' to whatever limit you want. Note that you
 may also need to make correspionding changes in dbedit, Solve, and
 perhaps other programs.



Other stuff in the calc11 directory:
The calc11 directory has a number of other files not necessary for database
processing. These are part of a program called 'dcalc' which is being
designed for use with the difx correlator. This program is still under 
development. It uses many of the same subroutines as calc11, so it makes
sense for it to be in the same directory. 
