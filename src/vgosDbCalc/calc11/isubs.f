! 
!  isubs.f 
!
!   All initialization subroutines have been moved into this module
!    in order to make the other modules more generic, with no Mark3
!    database calls.
!    D. Gordon Jan. 2013
!
!***********************************************************************
      SUBROUTINE ATIMI()
      IMPLICIT None
!
! 2.    ATIMI
! 2.1   ATIMI PROGRAM SPECIFICATION
! 2.1.1 ATIMI IS THE ATIME UTILITY ROUTINE INPUT AND INITIALIZATION SECTION.
! 2.2   ATIMI PROGRAM INTERFACE
! 2.2.2 COMMON BLOCKS USED -
!
      Real*8  ATMUTC(3), ROTEPH(2,20), A1UTC(3), A1DIFF(3)
      COMMON / EOPCM / ATMUTC, ROTEPH, A1UTC, A1DIFF
!           VARIABLES 'TO/FROM':
!            1. ATMUTC(3)   - THE 'TAI MINUS UTC' INFORMATION ARRAY. THIS ARRAY
!                             CONTAINS RESPECTIVELY THE EPOCH, VALUE, AND TIME
!                             RATE OF CHANGE OF 'TAI MINUS UTC'.
!                             (DAYS, SEC, SEC/SEC)
!
      INCLUDE 'ccon.i'
!            VARIABLES 'FROM':
!              1. KATIC - THE ATIME UTILITY ROUTINE FLOW CONTROL FLAG.
!              2. KATID - THE ATIME UTILITY ROUTINE DEBUG OUTPUT FLAG.
!
      INCLUDE 'input11.i'
!            Variables from:
!              1. Input_EOP - T/F logical flag telling whether to use external
!                             EOP file input
!
! 2.2.3 PROGRAM SPECIFICATIONS -
!
      INTEGER*2 NDO(3), Kerr
      INTEGER*2      LATIU(40),      LON(40),    LOFF(40)
      CHARACTER*40 C_LATIU(2),     C_LON(2),   C_LOFF(2)
      EQUIVALENCE (C_LATIU,LATIU),(C_LON,LON),(C_LOFF,LOFF)
!
      DATA C_LATIU /                                                    &
     &'ATIME Utility routine - VERSION # 3, Las',                       &
     &'t modification - 89:12:12 Jim Ryan.     '/
!
      DATA C_LON /                                                      &
     &'ATIME Utility routine is turned on.     ',                       &
     &'                                        '/
!
      DATA C_LOFF /                                                     &
     &'ATIME Utility routine is turned off.    ',                       &
     &'                                        '/
!
! 2.2.4 DATA BASE ACCESS -
!            'GET' VARIABLES:
!              1. ATMUTC(3) - THE 'TAI MINUS UTC' INFORMATION ARRAY. THIS ARRAY
!                             CONTAINS RESPECTIVELY THE EPOCH, VALUE, AND TIME
!                             RATE OF CHANGE OF 'TAI MINUS UTC'.
!                             (DAYS, SEC, SEC/SEC)
!            'PUT' VARIABLES:
!              1. LATIU(40) - THE ATIME UTILITY ROUTINE TEXT MESSAGE.
!              2. LON(40)   - THE ATIME UTILITY 'TURNED ON' MESSAGE.
!              3. LOFF(40)  - THE ATIME UTILITY 'TURNED OFF' MESSAGE.
!            ACCESS CODES:
!              1. 'ATI MESS'  -  THE DATA BASE ACCESS CODE FOR THE
!                                 ATIME UTILITY ROUTINE TEXT MESSAGE.
!              2. 'TAI- UTC'  -  THE DATA BASE ACCESS CODE FOR THE
!                                 'TAI MINUS UTC' INFORMATION ARRAY.
!              3. 'ATI CFLG'  -  THE DATA BASE ACCESS CODE FOR THE
!                                 ATIME UTILITY FLOW CONTROL MESSAGE.
!
! 2.2.6 SUBROUTINE INTERFACE -
!             CALLER SUBROUTINES: INITL
!             CALLED SUBROUTINES: GET4, TERMINATE_CALC, PUTA
!
! 2.2.8 PROGRAM VARIABLES -
!           1. KERR   - THE DATA BASE ERROR RETURN FLAG.
!           2. NDO(3) - THE DATA BASE RETURN ARRAY INDICES.
!
! 2.2.9 PROGRAMMER - DALE MARKHAM  02/04/77
!                    PETER DENATALE 07/18/77
!                    BRUCE SCHUPLER 02/07/78
!                    Jim Ryan 89.07.25 Documentaton simplified.
!                    Jim Ryan 89.12.12 UNIX-like database interface
!                             implimented.
!                    D. Gordon 96.02.27 Double dimensioning of ATMUTC(3)
!                             removed, found by Warwick Wilson, ATNF.
!                    D. Gordon 98.05.01 Expanded and renamed common block
!                             ATICM to EOPCM. Pass in ATMUTC if using external
!                             EOP input.
!                    Jim Ryan 02Sept Integer*4 mods.
!                    Jim Ryan 03.03.10 Kill replaced with terminate_calc.
!                    D. Gordon 2012.04.18 Add computation of TT, Terrestrial 
!                             Time. Will be used for precession/nutation.
!
!     ATIMI Program Structure
!
!     PUT the ATIME utility text message.
      CALL PUTA ('ATI MESS      ', LATIU, int2(40), int2(1), int2(1))
!
!     PUT the flow control message based on the value of KATIC.
      IF (KATIC .NE. 1) CALL PUTA ('ATI CFLG      ', LON, int2(40),     &
     & int2(1), int2(1))
      IF (KATIC .EQ. 1) CALL PUTA ('ATI CFLG      ', LOFF, int2(40),    &
     & int2(1), int2(1))
!
      IF (.not. Input_EOP) THEN         ! Already have ATMUTC?
!      GET the 'TAI MINUS UTC' array from the database and check for errors.
        CALL GET4 ('TAI- UTC      ',ATMUTC,int2(3),int2(1),int2(1),NDO, &
     &   KERR)
        IF ( KERR .EQ. 0 )  GO TO 300
         CALL TERMINATE_CALC ( 'ATIMI ', int2(1), KERR)
      ENDIF                             ! Already have ATMUTC?
!
!     Check KATID for debug output.
  300 IF ( KATID .EQ. 0 )  GO TO 500
      WRITE ( 6, 9)
    9 FORMAT (1X, "Debug output for subroutine ATIMI." )
!
      WRITE(6,8)' ATMUTC  ',ATMUTC
    8 FORMAT(A,4D25.16/(7X,5D25.16))
!
!     Normal conclusion.
  500 RETURN
      END
!
!***********************************************************************
      SUBROUTINE CTIMI()
      IMPLICIT None
!
! 1.    CTIMI
!
! 1.1   CTIMI PROGRAM SPECIFICATION
!
! 1.1.1 CTIMI IS THE SUBROUTINE FOR THE CTIMG UTILITY WHICH GETS NECESSARY
!       PARAMETERS FROM THE DATA BASE HEADER AND ENTERS MESSAGES INTO THE
!       HEADER.
!
! 1.2   CTIMI PROGRAM INTERFACE
!
! 1.2.1 CALLING SEQUENCE - CALL CTIMI
!
! 1.2.2 COMMON BLOCKS USED:
!
      Real*8           A1TAI, ATCTEP, D1950, ECCEN, XL(2), XM(2),       &
     &       D(2), XLLJ(2), XLLSA(2), XMJ(2), XMSA(2), ACT(15)
      COMMON / CTICM / A1TAI, ATCTEP, D1950, ECCEN, XL,    XM,          &
     &       D,    XLLJ,    XLLSA,    XMJ,    XMSA,    ACT
!        VARIABLES 'TO'-
!          1. A1TAI - THE OFFSET BETWEEN A1 AND TAI (SECONDS)
!
      INCLUDE 'ccon.i'
!        VARIABLES 'FROM' -
!          1. KCTIC - THE CTIMG UTILITY FLOW CONTROL FLAG
!          2. KCTID - THE CTIMG UTILITY DEBUG CONTROL FLAG
!
      Real*8  ATMUTC(3), ROTEPH(2,20), A1UTC(3), A1DIFF(3)
!     COMMON / ATICM / ATMUTC, ROTEPH, A1UTC, A1DIFF
      COMMON / EOPCM / ATMUTC, ROTEPH, A1UTC, A1DIFF
!           VARIABLES 'TO/FROM':
!            1. A1DIFF(3) - THE ARRAY CONTAINING THE EPOCH, OFFSET AND RATE
!                           OF CHANGE OF THE OFFSET BETWEEN A1 AND TAI
!                           (DAYS, SECONDS, SECONDS/DAY)
!
      INCLUDE 'input11.i'
!            Variables from:
!              1. Input_EOP - T/F logical flag telling whether to use external
!                             EOP file input
!
! 1.2.3 PROGRAM SPECIFICATIONS -
!
!     Real*8    A1DIFF(3)
      Integer*2 NDO(3), KERR(1)
      INTEGER*2      LCTIU(40),   LON(40),   LOFFA(40),  LOFFP(40)
      CHARACTER*40 C_LCTIU(2),  C_LON(2),  C_LOFFA(2), C_LOFFP(2)
      EQUIVALENCE (C_LCTIU, LCTIU),(C_LON, LON),(C_LOFFA, LOFFA),       &
     &            (C_LOFFP, LOFFP)
!
      DATA C_LCTIU /                                                    &
     &'CTIMG Utility routine - Version # 2, las',                       &
     &'t modification - 08/06/81 Chopo Ma      '/
!
      DATA C_LON /                                                      &
     &'CTIMG Utility routine is turned on.     ',                       &
     &'                                        '/
!
      DATA C_LOFFA /                                                    &
     &'CTIMG utility routine is turned off.    ',                       &
     &'                                        '/
!
      DATA C_LOFFP  /                                                   &
     &'CTIMG utility routine periodic terms tur',                       &
     &'ned off.                                '/
!
! 1.2.4 DATA BASE ACCESS -
!
!      'GET' VARIABLES -
!         1. A1DIFF(3) - THE ARRAY CONTAINING THE EPOCH, OFFSET AND RATE
!                        OF CHANGE OF THE OFFSET BETWEEN A1 AND TAI
!                        (DAYS, SECONDS, SECONDS/DAY)
!
!      'PUT' VARIABLES -
!         1. LCTIU(40) - THE CTIMG UTILITY ROUTINE TEXT MESSAGE.
!         2. LON(40)   - THE CTIMG UTILITY 'TURNED ON' MESSAGE.
!         3. LOFFA(40) - THE CTIMG UTILITY 'TURNED OFF' MESSAGE.
!         4. LOFFP(40) - THE CTIMG UTILITY 'TURNED OFF PERIODIC TERMS' MESSAGE.
!
!      ACCESS CODES:
!         1. 'CTI MESS' - THE DATA BASE ACCESS CODE FOR THE CTIMG UTILITY
!                         ROUTINE TEXT MESSAGE.
!         2. 'CTI CFLG' - THE DATA BASE ACCESS CODE FOR THE CTIMG UTILITY FLOW
!                         CONTROL MESSAGE.
!         3. 'A1 - TAI' - THE DATA BASE ACCESS CODE FOR THE A1DIFF ARRAY.
!
! 1.2.5 EXTERNAL INPUT/OUTPUT - POSSIBLE DEBUG OUTPUT
!
! 1.2.6 SUBROUTINE INTERFACE:
!          CALLER SUBROUTINES - INITL
!          CALLED SUBROUTINES - PUTA, GET4
!
! 1.2.7 CONSTANTS USED - NONE
!
! 1.2.8 PROGRAM VARIABLES -
!       1. NDO(3)  - AN ARRAY USED TO HOLD THE DIMENSIONS FROM GET4_S
!       2. KERR(1) - THE ERROR RETURN CODE FROM GET4
!
! 1.2.9 PROGRAMMER - BRUCE SCHUPLER 02/21/78
!                    CHOPO MA       08/06/81
!                    Jim Ryan  89.07.26 Documentation simplified.
!                    D. Gordon 98.05.01 Added Include file 'inputs.i' and
!                              common block /EOPCM/. Added code to pass in
!                              A1DIFF(3) if using external EOP input.
!                    Jim Ryan Sept02 Integer*2/4 mods.
!
!    CTIMI Program Structure
!
!    PUT the CTIMG utility text message into the database.
      CALL PUTA ('CTI MESS      ',LCTIU,int2(40),int2(1),int2(1))
!
!   PUT the CTIMG utility flow control message into the database depending
!    on KCTIC. See strings above for meanings.
      IF (KCTIC .EQ. 0) CALL PUTA('CTI CFLG      ', LON, int2(40),      &
     &                  int2(1), int2(1))
      IF (KCTIC .EQ. 1) CALL PUTA('CTI CFLG      ', LOFFA, int2(40),    &
     &                  int2(1), int2(1))
      IF (KCTIC .EQ. 2) CALL PUTA('CTI CFLG      ', LOFFP, int2(40),    &
     &                  int2(1), int2(1))
!
      IF (.not. Input_EOP) THEN           !Already have A1DIFF?
!        Get the A1DIFF array and check for database error.
        CALL GET4('A1 - TAI      ',A1DIFF,int2(3),int2(1),int2(1),NDO,  &
     &   KERR(1))
        IF (KERR(1).NE.0) CALL TERMINATE_CALC('CTIMI ',int2(1),KERR(1))
      ENDIF                               !Already have A1DIFF?
!
!    Move the offset into A1TAI.
      A1TAI = A1DIFF(2)
!
!    See if debug output is needed.
      IF (KCTID .NE. 1) GO TO 600
      WRITE(6,9)
    9 FORMAT(1X,"Degug output from utility CTIMI")
      WRITE(6,8)' A1DIFF  ',A1DIFF
    8 FORMAT(A,4D25.16/(7X,5D25.16))
      WRITE(6,8)' A1TAI   ',A1TAI
600   CONTINUE
      RETURN
      END
!
!*************************************************************************
      SUBROUTINE PEPI()
      IMPLICIT None
!
! 3.    PEPI
!
! 3.1   PEPI PROGRAM SPECIFICATION
!
! 3.1.1 PEPI is the PEP utility routine input and initialization section.
!       A message is written into the header giving the PEP module version
!       number. The planetary ephemeris information is now designed to be
!       taken from the JPL DE405/LE405 ephemeris, in compliance with the
!       IERS 2003 Conventions. [The code should also work for other recent
!       ephemeris's such as DE200 or DE403.] Ehemeris information from the
!       input
!       database is no longer supported, and those Lcodes are not needed in
!       the data base. The epoch is now J2000 in all cases, so no 1950 =>
!       2000 rotation matrix is needed.
!
! 3.1.2 RESTRICTIONS - None.
!
! 3.2   PEPI PROGRAM INTERFACE
!
! 3.2.1 CALLING SEQUENCE - NONE
!
! 3.2.2 COMMON BLOCKS USED -
!
      INCLUDE 'ccon.i'
!            VARIABLES 'FROM':
!              1. KPEPD - THE PEP UTILITY ROUTINE DEBUG OUTPUT FLAG.
!              2. KPEPC - The PEP module flow control flag.
!                          = 0 ==> Solar system geometry from JPL DE/LE405
!                             ephemeris file (default). (Sun, Moon, Earth, and
!                             all other planets except Pluto)
!
! 3.2.3 PROGRAM SPECIFICATIONS -
!
      INTEGER*4 N
      INTEGER*2      LPEPU(40),  LPEPT(40)
      CHARACTER*40 C_LPEPU(2), C_LPEPT(2)
      EQUIVALENCE (C_LPEPU,LPEPU), (C_LPEPT,LPEPT)
!
      DATA C_LPEPU / &
     & 'PEP Routine - 2013.04.21 D. Gordon/GSFC,', &
     & ' J2000 Ephemeris from JPL DE421.        '/
!
      DATA C_LPEPT / &
     & 'EARTH, SUN, MOON Coordinates Replaced by', &
     & ' Calc 11 Using DE421.                   '/
!
! 3.2.4 DATA BASE ACCESS -
!            'PUT' VARIABLES:
!              1. LPEPU(40)  -  THE PEP UTILITY ROUTINE TEXT MESSAGE.
!            ACCESS CODES:
!              1. 'PEP MESS' -  THE DATA BASE ACCESS CODE FOR THE PEP UTILITY
!                               ROUTINE TEXT MESSAGE.
!
! 3.2.5 EXTERNAL INPUT/OUTPUT - POSSIBLE DEBUG OUTPUT
!
! 3.2.6 SUBROUTINE INTERFACE -
!           CALLER SUBROUTINES: INITL
!           CALLED SUBROUTINES: PUTA, GETI
!
! 3.2.7 CONSTANTS USED - NONE
!
! 3.2.8 PROGRAM VARIABLES -
!
! 3.2.9 PROGRAMMER - KATHY WATTS    05/25/77
!                    PETER DENATALE 07/19/77
!                    BRUCE SCHUPLER 03/09/78
!                    BRUCE SCHUPLER 09/14/78
!                    BRUCE SCHUPLER 09/20/78
!                    BRUCE SCHUPLER 01/08/80
!                    CHOPO MA       08/04/81
!                    SAVITA GOEL 06/04/87 (CDS FOR A900)
!                    Jim Ryan 89.07.25 Documentation simplied and strings.
!                    Jim Ryan 89.12.12 UNIX-like database interface
!                             implimented.
!                    D. Gordon 93.11.08 Code for use of JPL DE/LE200 ephemeris.
!                    D. Gordon 94.04.18 Converted to Implicit None.
!                    D. Gordon 94.05.23 Fixed bug, changed C_LPEPJPL to
!                              C_LPEPJPL(2).
!                    David Gordon 94.10.05 Minor fixup of 'PEP MESS' text.
!                    David Gordon 98.07.13 Removed option for getting the
!                              ephemeris values from the data base. Removed
!                              'PEP 2000' Lcode. Modified 'PEP MESS'.
!                    David Gordon 99.01.14 Addition to re-PUT 'PEP TAPE'
!                              access codes to clarify that Sun, Moon, and
!                              Earth coordinates are being inserted by Calc,
!                              replacing any previous values.
!                    Jim Ryan 2002 Sept. Interger*4 updates.
!                    D. Gordon 2004.04.21 Updated for DE405.
!                    D. Gordon 2013.??.?? Updated for DE421.
!
!   PEPI Program Structure
!
!     PUT the PEP utility text messages.
      CALL PUTA ('PEP MESS      ',LPEPU,int2(40),int2(1),int2(1))
!     CALL PUTA ('PEP TAPE      ',LPEPT,int2(64),int2(1),int2(1))
!
!   See if debug is requested.
      IF(KPEPD .ne. 0) Then
       WRITE(6,9)
       write (6,'("  LPEPU = ",40A2,/)') LPEPU
   9   FORMAT(10X, 'Debug output for subroutine PEPI.')
      Endif
!
      RETURN
      END
!
!*************************************************************************
      SUBROUTINE NUTI()
      IMPLICIT None
!
! 3.    NUTI
!
! 3.1   NUTI PROGRAM SPECIFICATION
!
! 3.1.1 NUTI IS THE NUTATION MODULE INPUT AND INITIALIZATION SECTION.
!       MESSAGES ARE PUT IN THE HEADER TO INDICATE PROGRAM FLOW.
!       KNUTC = 0 - IERS 2003 NUTATION (IAU200A Precession/Nutation)
!                   TO BE USED (DEFAULT)
!               1 - NO NUTATION TO BE USED
!
! 3.2   NUTI PROGRAM INTERFACE
!
! 3.2.1 CALLING SEQUENCE - NONE
!
! 3.2.2 COMMON BLOCKS USED -
      INCLUDE 'ccon.i'
!      VARIABLES 'FROM':
!       1. KNUTC - THE NUTATION MODULE FLOW CONTROL FLAG
!       2. KNUTD - THE NUTATION MODULE DEBUG CONTROL FLAG
!
      Real*8           PI, TWOPI, HALFPI, CONVD, CONVDS, CONVHS, SECDAY
      COMMON / CMATH / PI, TWOPI, HALFPI, CONVD, CONVDS, CONVHS, SECDAY
!     VARIABLES 'FROM':
!       1.  CONVDS  -  THE CONVERSION FACTOR OF RADIANS PER
!                      ARCSECOND.  (RAD/ARCSEC)
!
! 3.2.3 PROGRAM SPECIFICATIONS -
      Real*8  APAMPS(2,2)
      Integer*4 N, I
      INTEGER*2      LNUTM(40),  LON(40),  LOFF(40)
      CHARACTER*40  C_LNUTM(2), C_LON(2), C_LOFF(2)
      EQUIVALENCE (LNUTM,C_LNUTM), (LON,C_LON), (LOFF,C_LOFF)
!
!
      DATA C_LNUTM / &
     & 'IAU2000/2006 Nutation/Precession Module:', &
     & ', 2012.08.13, D. Gordon/GSFC.           '/
!
      DATA C_LON  / &
     & 'Nutation/Precession module is turned ON.', & 
     & ' IAU2000/2006 model used.               '/
!
      DATA C_LOFF / &
     & 'Nutation/Precession module is turned OFF', &
     & '.                                       '/
!
!
! 3.2.4 DATA BASE ACCESS -
!           'PUT' VARIABLES:
!              1.  LNUTM(40)   - THE NUTATION MODULE TEXT MESSAGE.
!              2.  LON(40)     - THE NUTATION MODULE TURNED ON MESSAGE.
!              3.  LOFF(40)    - THE NUTATION MODULE TURNED OFF MESSAGE.
!            ACCESS CODES:
!              1.  'NUT MESS'  - THE DATA BASE ACCESS CODE FOR THE
!                                NUTATION MODULE TEXT MESSAGE.
!              2.  'NUT CFLG'  - THE DATA BASE ACCESS CODE FOR THE
!                                NUTATION MODULE FLOW CONTROL MESSAGE.
!
! 3.2.6 SUBROUTINE INTERFACE -
!             CALLER SUBROUTINES: INITL
!             CALLED SUBROUTINES: PUTA
!
! 3.2.9 PROGRAMMER -
!          DALE MARKHAM   01/13/77
!          PETER DENATALE 07/12/77
!          BRUCE SCHUPLER 03/15/78
!          BRUCE SCHUPLER 02/01/79
!          BRUCE SCHUPLER 06/06/79
!          CHOPO MA       08/04/81
!          GEORGE KAPLAN  04/24/84
!          DAVID GORDON   11/13/84 REDIMENSIONED NT# AMPS
!          Jim Ryan       89.06.30 Documetation clean up.
!          Jim Ryan       89.12.12 UNIX-like database interface implemented.
!          David Gordon   94.04.15 Converted to Implicit None.
!          David Gordon   95.09.27 Conversion of X(9,120) nutation table
!                         coefficients to double precision removed - no
!                         longer needed.
!          David Gordon   98.02.03 Changed KNUTC definitions. Default
!                         is now IERS 1996 Nutation. Removed
!                         computation and PUT's of 'NT1 AMPS', etc.
!          Jim Ryan       2002.09  Integer*4 conversion.
!          David Gordon   2003/2004 Mods for IERS Conventions 2003.
!          David Gordon   Aug. 2012  Mods for IERS Conventions 2010.
!
!     NUTI PROGRAM STRUCTURE
!
!     PUT the nutation module text message.
      CALL PUTA ('NUT MESS      ', LNUTM, int2(40), int2(1), int2(1))
!
!     PUT the nutation module flow control message depending on KNUTC.
      IF (KNUTC .EQ. 0) CALL PUTA ('NUT CFLG      ',LON,int2(40), &
     &                  int2(1), int2(1))
      IF (KNUTC .EQ. 1) CALL PUTA ('NUT CFLG      ',LOFF,int2(40), &
     &                  int2(1), int2(1))
!
!     Normal conclusion.
      RETURN
      END
!
!**************************************************************************
      SUBROUTINE UT1I()
      IMPLICIT None
!
!     UT1I is the UT1 module input and initialization section.
!     Beginning with Calc 10, the input UT1 table MUST be a 1-day
!     series of 'UT1-TAI' or 'UT1-TAI'. Five-day series will not
!     be accepted. Series with tidal terms removed ('UT1S-TAI')
!     will not be accepted. 
!
!     Common blocks used -
!
      INCLUDE 'cmxut11.i'
!            Variables 'from':
!              1. IEPOCH   - The number of epochs at which TAI - UT1 is desired.
!              2. ASKKER   - The database error return code from ASK.
!              3. Leap_fix - Used in external input mode. .True. means
!                            correct the input EOP series for accumluated
!                            leap seconds. .False. means do not correct.
!              4. UT1type  - UT1 data type: 'UT1-TAI ' or 'UT1-UTC '.
!                            For 'UT1-UTC ', leap second corrections
!                            must be made.
!              5. EOP_time_scale - EOP table time scale, allowed values:
!                            'TAI     ', 'TCG     ', 'TDB     ',
!                            'TDT     ', 'UTC     ', 'UNDEF   '.
!
!            Variables 'to':
!              1. UT1IF(4)  - The final UT1 information array. This array
!                             contains respectively: 1) The Julian date of the
!                             first tabular point, 2) The increment in days of
!                             the tabular points, 3) The number of tabular
!                             points, 4) The units of the UT1 tabular array per
!                             second. (days, days, unitless, sec/table unit)
!              2. UT1PT(20) - The tabular values of 'TAI minus UT1'.
!                             (table units)
!              3. UT1RS(20) - The table of either 'TAI-UT1' (default 
!                             beginning with Calc 10) or 'TAI-UT1S'
!                             depending on the value of KUT1C. For
!                             'TAI-UT1S', the tidal terms are removed 
!                             using the model of Defrainge and Smits, 1999,
!                             from the IERS Conventions (2003)]. (seconds)
!              4. ISHRTFL   - The short period tidal terms flag, (unitless).
!                             = 1 --> UT1 table coming from input database is
!                             true UT1, (that is, fortnightly tidal terms have
!                             not been removed, as in the IERS series).
!                             [This is the normal case.]
!                             = -2 --> UT1 table coming from input database
!                             is UT1S (tidal terms removed).
!                             This mode no longer supported and should not
!                             be used. It will cause Calc 10 to abort!!!!
!                             = -1 --> UT1R (old Yoder fortnightly terms
!                             removed). This mode no longer supported and
!                             will cause Calc to abort!!!!!
!             5. Usecubic   - Set to true if cubic interpolation to be used.
!             6. Uselinear  - Set to true if linear interpolation to be used.
!             7. Usespline  - Set to true if spline interpolation to be used.
!
      INCLUDE 'ccon.i'
!            VARIABLES 'FROM':
!             1. KUT1C - UT1 module flow control flag, controls the
!                        temporary removal of periodic terms (UT1S)
!                        and the type of interpolation (spline or cubic)
!                        in the UT1 tables. Revised Sept. 2005.
!                        = 0. Leave table as TAI-UT1. Do spline 
!                           interpolation for a 1-day series. 
!                           5-day series no longer allowed!
!                        = 1. Module completely off, that is, UT1 set equal
!                           to AT.
!                        = 2. Use TAI-UT1; use cubic interpolation for a
!                           1-day series. 5-day series no longer allowed!
!                        = 3. Use TAI-UT1; use linear interpolation for a
!                           1-day series. 5-day series no longer allowed!
!                        = 4. Convert table to TAI-UT1S. Do spline 
!                           interpolation for a 1-day series, then 
!                           restore to true UT1 using the UT1S model of
!                           Defrainge and Smits, 1999. 
!                           5-day series no longer allowed!
!             2. KUT1D - The UT1 module debug output flag.
!
      INCLUDE 'input11.i'
!            Variables from:
!              1. Input_EOP - T/F logical flag telling whether to use external
!                             EOP file input
!              2. Ex_EOP    - File name for EOP external file input. If 'NONE'
!                             or blank, external EOP input will not be done.
!
      Real*8  ATMUTC(3), ROTEPH(2,20), A1UTC(3), A1DIFF(3)
      COMMON / EOPCM / ATMUTC, ROTEPH, A1UTC, A1DIFF
!           VARIABLES 'TO':
!            1. ATMUTC(3)   - THE 'TAI MINUS UTC' INFORMATION ARRAY. THIS ARRAY
!                             CONTAINS RESPECTIVELY THE EPOCH, VALUE, AND TIME
!                             RATE OF CHANGE OF 'TAI MINUS UTC'. Used in the
!                             atomic time module. (DAYS, SEC, SEC/SEC)
!            2. ROTEPH(2,20)- The array which contains the epochs at which
!                             TAI - UT1 is desired. The entries are:
!                             1) JD at 0:00 hours UTC,
!                             2) The fraction of a UTC day from 0:00 hours
!                                to the desired epoch.
!            3. A1UTC(3)    - A1 - UTC array ???? 
!            4. A1DIFF(3)   - THE ARRAY CONTAINING THE EPOCH, OFFSET AND RATE
!                             OF CHANGE OF THE OFFSET BETWEEN A1 AND TAI
!                             (DAYS, SECONDS, SECONDS/DAY)
!
!     Program Specifications -
!
      REAL*8     UT1TAB(2,20)
      REAL*8     XJD, CT, TC2000, FA2K(14), FAD2K(14), TT0
      Real*8     yp1, ypn, xleap(5), tol, xntv
      Real*8     Dut, Dlod, Domega, Dutr, Dlodr, Domegar, Duts, Dlods, &
     &           Domegas, Atmut1, Shortp, Divutc, tab_time
      INTEGER*4  get_leapsec, ierr, ierr4
      INTEGER*4  Increment, max_ut1_pts
      Integer*4  N, Itab, II, I
      INTEGER*2  KERR(8), NDO(3), Tab_len
      INTEGER*2  LUT1M(40), LOFF(40), LUT1S(40), LUT1(40)
      CHARACTER*40 C_LOFF(2), C_LUT1S(2), C_LUT1M(2), C_LUT1(2)
      Equivalence (LUT1S,C_LUT1S), (C_LUT1M,LUT1M), (LOFF,C_LOFF ), &
     &            (LUT1,C_LUT1)
!
      Character*20 C_LFI, C_LPR, C_LEX
      Integer*2    LFI(10), LPR(10), LEX(10)
      Equivalence (C_LFI,LFI), (C_LPR,LPR), (C_LEX,LEX)
!
      Integer*2    Lutext(40)
      Character*80 Utext
      Equivalence (Utext,Lutext)
!
      Integer*2    mess_linear(30), mess_cubic(30), mess_spline(3)
      Character*60 mess_linear_c , mess_cubic_c, mess_spline_c
      Equivalence (mess_linear,mess_linear_c),(mess_cubic,mess_cubic_c), &
     &            (mess_spline,mess_spline_c)
      Character*1 type_found, cdum1(3)
!
      Integer*2    L_EOPSCALE(4)
      Character*8  C_EOPSCALE
      Equivalence (L_EOPSCALE, C_EOPSCALE)
!
      Integer*2 Mxepch, MEPOCH
      DATA MXEPCH /20/
      DATA C_LUT1M / &
     & 'UT1 Module - Last Modified 2005.09.23, D', &
     & '. Gordon/GSFC:                          '/
!
      DATA C_LUT1   / &
     & 'UT1 Module ON. Interpolation in TAI-UT1.', &
     & '                                        '/
!
      DATA C_LUT1S  / &
     & 'UT1 Module ON. Tidal terms restored usin', &
     & 'g Defraigne and Smits 1999 (IERS 2003). '/
!
      DATA C_LOFF / &
     & 'UT1 Module is turned OFF.               ', &
     & '                                        '/
!
      DATA  C_LFI /' Final Values       '/
      DATA  C_LPR /' Preliminary Values '/
      DATA  C_LEX /' Extrapolated Values'/
!
      Data mess_cubic_c  / &
     & 'UT1 table interpolated with 4 point, 3rd order polynomial.  '/
!
      Data mess_linear_c / &
     & 'UT1 table interpolated with 2 point, linear interpolation.  '/
!
      Data mess_spline_c / &
     & 'UT1 table interpolated with 15 point cubic spline.          '/
!
      Data max_ut1_pts /20/
!
! 2.2.4 DATA BASE ACCESS -
!           'GET' VARIABLES:
!              1. UT1IF(4)   -   The UT1 information array. (See above.)
!              2. UT1PT(20)  -   The tabular values of 'TAI minus UT1'.
!                                (table units)
!              3. ROTEPH(2,20) - The array which contains the epochs at which
!                                TAI - UT1 is desired. The entries are:
!                                1) JD at 0:00 hours UTC,
!                                2) The fraction of a UTC day from 0:00 hours
!                                to the desired epoch.
!              4. ISHRTFL    -   The short period tidal terms flag. (See above.)
!           'PUT' Variables:
!              1. LUT1M(40)    - The UT1 module text message.
!              2. LON(40)      - The UT1 module 'turned on' message.
!              3. LOFF(40)     - The UT1 module 'turned off' message.
!              4. UT1TAB(2,20) - The array which contains the TAI - UT1 
!                                values (complete) and the fortnightly 
!                                corrections to TAI - UT1 (s,s).
!           Access codes:
!              1. 'UT1 MESS'  -  UT1 module text message code.
!              2. 'FUT1 INF'  -  The access code for the Final UT1 
!                                information array.
!              3. 'FUT1 PTS'  -  Final UT1 points access code.
!              4. 'PUT1 INF'  -  Like above but preliminary.
!              5. 'PUT1 PTS'  -  Like above but preliminary.
!              6. 'UT1 CFLG'  -  Module control flag access code.
!              7. 'EUT1 INF'  -  Like above but for extropolated data.
!              8. 'EUT1 PTS'  -  Like above but for extropolated data.
!              9. 'UT1EPOCH'  -  Access code for the UT1 epochs array.
!             10. 'ROTEPOCH'  -  Access code for the array of epochs at which
!                                TAI - UT1 is desired.
!             11. 'TIDALUT1'  -  The access code for the flag indicating
!                                whether the fortightly terms in UT1 are in
!                                the input UT1 series. (See below.)
!
!     Subroutine Interface -
!             Caller subroutines: INITL
!             Called subroutines: GETI, GET4, TERMINATE_CALC, PUTA, PUT4,
!                                 NUTFA, UT1RZT, UT1SZT, spline
!
!     Program variables:
!           1. Tab_len     - The number of tabular points in the UT1
!                            information array in the database
!           2. KERR(8)     - The database error return flags.
!           3. LUT1F(4)    - The message inserted into the UT1 module text
!                            message if the final UT1 information are used.
!           4. LUT1P(4)    - The message if preliminary data is used.
!           5. NDO(3)      - Database return array indices.
!           6. LUT1E(3)    - The message if extropolated data is used.
!           7. UT1TAB(2,20)- THe array which contains the values of TAI - UT1
!                            and the short period corrections. (s,s).
!           8. MEPOCH      - This is IEPOCH checked to be certain that it is no
!                            greater than MXEPCH.
!           9. MXEPCH      - The maximum number of TAI - UT1 EPOCHS allowed.
!          10. XJD         - The Julian date at 0 hours UTC of the epoch in
!                            question (days).
!          11. UTC         - The UTC fraction of day of the observation. (days)
!          12. ATMUT1      - The complete computed value of TAI - UT1 (s)
!          13. SHORTP      - 'UT1S - UT1' depending on KUT1C.
!                            (s). Notice the sense!
!          14. Increment   - The increment of the UT1 table in the database
!                            (days).
!          15. max_ut1_pts - The maximum size of the UT1 table from the
!                            database.
!          16. type_found  - Tracks the type of UT1 table in the database.
!
!     Programmer - Dale Markham   02/14/77
!      77.07.14  Peter Denatale
!      78.03.28  Bruch Schupler
!      78.07.03  Bruce Schupler
!      84.06.05  David Gordon
!      87.06.03  Savita Goel   CDS FOR A900
!      88.12.21  Gregg Cooke   CALC 7.0 mods.
!      89.06.08  Jim Ryan      Short period logic modified.
!      89.07.11  Jim Ryan      Documentation simplified.
!      89.12.12  Jim Ryan      UNIX-like database interface implimented.
!      90.11.30  Jim Ryan      Bug in short period UT1 control logic fixed
!                              again.
!      91.05.30  Jim Ryan      Documentation furthur simplilfied.
!      91.06.05  Jim Ryan      Mods to support linear interpolation.
!      91.06.19  Jim Ryan      Code added to construct UT1R table from UT1
!                              table. Needed for linear interpolation. For CALC
!                              7.4+ the scaling law for the UT1 table MUST be
!                              1, because SOLVE doesn't know about that law.
!      93.03.17  David Gordon  Code added to use UT1S, new control flag scheme.
!      93.09.07  Norbert Zacharias  Moved calculation of fundamental argumuments
!                              to subroutine NUTFA.
!      93 Dec.   David Gordon  Cubic spline interpolation added, modified
!                              control flag scheme for type of interpolation.
!      93.12.30  David Gordon  Cleaned up 'UT1 MESS'.
!      94.04.06  David Gordon  Changed to 'Implicit None'.
!      94.09.26  David Gordon  Added some debug printout, changed some I*2's to
!                              I*4's, documentation corrections, cosmetic mods.
!      94.09.27  David Gordon  Removed unused 'XLOVEK' Love number.
!      95.06.08  David Gordon  Corrected debug printout, wrong format statement
!                              used.
!      98.01.27  David Gordon  Removed Yoder (UT1R) model interpolation option.
!                              KUT1C values redefined. TERMINATE_CALC if no
!                              TIDALUT1
!                              Lcode (old database, UT1R assumed) or if ISHRTFL
!                              equals -1.
!      98.04.l3  David Gordon  Common /UT1CM/ moved to 'cmxut.i' include file.
!      98.05.01  David Gordon  Mods for external file input of EOP info.
!     2001.01.02 David Gordon  CT replaced with tab_time in UT1MU argument.
!                              Code added to PUT 'EOPSCALE'.
!      2002.09    Jim Ryan     Integer*2/4 mods.
!     2003-2004  David Gordon  Updated for IERS Conventions (2003).
!      2005.10   David Gordon  Change default to interpolate in 'TAI-UT1',
!                              i.e. Do NOT remove fortnightly, etc terms.
!                              Only for KUTC1=4 are the fortnightly terms
!                              removed/restored.
!     2015.09.09 D. Gordon     Fixed 'Leap_fix' usage.
!
!     UT1I Program Structure
!
!    Verify that the UT1 module flow control flag is valid
      If (KUT1C.lt.0 .or. KUT1C.gt.4) Then
        Write(6,'( "In UT1I, UT1 module control flag is invalid. ",/, &
     &  "  KUT1C =",I5,/)') KUT1C
        CALL TERMINATE_CALC('UT1I  ', int2(0), int2(0))
      Endif
!
!----------------------------------------------------------------------------
!  If using database input, get the UT1 values from the database now.
!   If using external EOP input, then we already have them.
!
      IF (.not. Input_EOP) THEN      !Database or External Input?
!
!     Call 'GETI' to get the short period tidal terms existance flag from the
!     header and place it in ISHRTFL for other programs to use. If a database
!     interface error is detected, call TERMINATE_CALC to terminate the program.
!
      CALL GETI('TIDALUT1      ',ISHRTFL,int2(1),int2(1),int2(1),NDO, &
     & KERR(1))
!
      IF (KERR(1).NE.0) Then
        IF (KERR(1).EQ.1) WRITE(6,'("No UT1 type Lcode (TIDALUT1)!", &
     &   " Update with true UT1 and try again. ")')
        CALL TERMINATE_CALC('UT1I  ', int2(1), KERR(1))
      Endif
!
!  Support for ISHRTFL=-2 (UT1S input) removed, 2005.10.06 -DG-
      If(ISHRTFL.ne.1) Then
        Write(6,'( &
     &  "In subroutine UT1I: The flag ISHRTFL from the database",/, &
     &  "must have a value of 1. Its value is",I5)') ISHRTFL
        CALL TERMINATE_CALC('UT1I   ', int2(0), int2(0))
      Endif
!
!     Call 'GET4' to obtain the final UT1 information from the database. If
!     final information is unavailable, call 'GET4' to obtain preliminary UT1
!     information. If preliminary information is unavailable, call 'GET4' to
!     obtain predicted UT1 information. If database interface error, terminate.
!
      table_found = .false.
      If(.not.table_found) Then
        Call GET4 ('FUT1 INF      ',UT1IF,int2(4),int2(1),int2(1),NDO, &
     &   KERR(1))
        If(KERR(1).eq.0)  Then
          type_found = 'F'
!         C_LUT1M(61:80) = ' Final Values       '
          do n = 1,10
           LUT1M(30+n) = LFI(n)
          enddo
          table_found = .true.
        Endif
      Endif
!
      If(.not.table_found) Then
        CALL GET4 ('PUT1 INF      ',UT1IF,int2(4),int2(1),int2(1),NDO, &
     &   KERR(1))
        If(KERR(1).eq.0) &
     &  then
          type_found = 'P'
!         C_LUT1M(61:80) = ' Preliminary Values '
          do n = 1,10
           LUT1M(30+n) = LPR(n)
          enddo
          table_found = .true.
        Endif
      Endif
!
      If(.not.table_found) Then
        CALL GET4 ('EUT1 INF      ',UT1IF,int2(4),int2(1),int2(1),NDO, &
     &             KERR(1))
        If(KERR(1).eq.0) &
     &  then
          type_found = 'X'
!         C_LUT1M(61:80) = ' Extrapolated Values'
          do n = 1,10
           LUT1M(30+n) = LEX(n)
          enddo
          table_found = .true.
        Endif
      Endif
!
      If(KERR(1).eq.2) Then
       Write(6,'("In UT1I: UT1 information array has wrong size!")')
       Call TERMINATE_CALC('UT1I  ', KERR(1), int2(1))
      Endif
!
      IF (.not.table_found)  Then
        Write(6,'( &
     &  "In UT1I. This database contains NO UT1 information.",/, &
     &  " Quitting!")')
        CALL TERMINATE_CALC('UT1I  ', int2(0), int2(0))
      Endif
!
      If(DABS(UT1IF(4)-1.D0) .gt. .00001D0) Then
        Write(6,'( &
     &  "In UT1I: The scaling law for UT1 table must be 1.0! ",/, &
     &  "It is not.  Quitting!")')
        Call TERMINATE_CALC('UT1I  ', int2(0), int2(0))
      Endif
!
      Increment = UT1IF(2) + .01
      Tab_len   = UT1IF(3) + .01
!
      If(Tab_len .gt. max_ut1_pts) Then
        Write(6,'( &
     &  "The maximum allowable UT1 table is ",i5," points.",/, &
     &  "The table in the database contains ",i5," points.", &
     &  "Quitting.")') max_ut1_pts, Tab_len
        Call TERMINATE_CALC( 'UT1I  ', int2(0), int2(0))
      Endif
!
!  Get the EOP timescale definition. If not present, we call it undefined,
!   but assume the EOP table epochs to be in TDB (= CT).
      CALL GETA('EOPSCALE      ',L_EOPSCALE,int2(4),int2(1),int2(1),NDO, &
     &           KERR(1))
        If(KERR(1).ne.0)  C_EOPSCALE = 'UNDEF   '
       EOP_time_scale = C_EOPSCALE
!
!
      ELSE                           !Database or External Input?
!
!       WRITE ( 6, * )  '!! External inputs, doing PUTs !!'
!
      CALL PUTI ('TIDALUT1      ',ISHRTFL,int2(1),int2(1),int2(1))
      CALL PUT4 ('ROTEPOCH      ',ROTEPH,int2(2),ndays,int2(1))
        C_EOPSCALE = EOP_time_scale
      CALL PUTA ('EOPSCALE      ',L_EOPSCALE,int2(4),int2(1),int2(1))
!
      Increment = UT1IF(2) + .01
      Tab_len   = UT1IF(3) + .01
!
          xntv = xintv(1)
!       WRITE ( 6, * )  ' UT1I/xintv: ', xintv
!   Get leap seconds, then load TAI - UTC array.
       ierr = get_leapsec(xintv(1),xleap)
!      If (ierr .ne. 0) go to ????
       ATMUTC(1) = xleap(1)
       ATMUTC(2) = xleap(2)
       ATMUTC(3) = xleap(4)
       tol = 1.D-8
       If (Dabs(ATMUTC(3)).gt.tol) ATMUTC(1) = xleap(3) + 2400000.5D0
       ATMUTC(3) = ATMUTC(3) / 8.64D4
      CALL PUT4 ('TAI- UTC      ', ATMUTC, int2(3), int2(1), int2(1))
!
!  Remove leap seconds if input table was UT1-UTC
       If (Leap_fix) Then
!
         WRITE ( 6, * )  '!!! Removing leap seconds !!!'
!
        Do I = 1,Tab_len
         UT1PT(I) = UT1PT(I) + ATMUTC(2)
        Enddo
       Endif
!
       Do I = 1,3
        A1utc(i) = ATMUTC(i)
       Enddo
        A1utc(2) = a1utc(2) + 0.03439D0
      CALL PUT4 ('A1 - UTC      ', a1utc, int2(3), int2(1), int2(1))
!
       A1DIFF(1) = ATMUTC(1)
       A1DIFF(2) =  0.03439D0
       A1DIFF(3) =  0.0D0
      CALL PUT4 ('A1 - TAI      ', A1DIFF, int2(3), int2(1), int2(1))
!
      ENDIF                          !Database or External Input?
!
!----------------------------------------------------------------------------
!  Mod 2005.09.23. Allow use of 1-day series only. There is no longer
!   any reason to use a 5-day series.
       IF (Increment .NE. 1) THEN
        Write(6,'("UT1I: The UT1 table increment must be 1 day! ",/, &
     &  "Instead it is ",I3," days. Calc is quitting!")') Increment
        Call TERMINATE_CALC('UT1I  ', int2(0), int2(0))
       ENDIF
!
!  Determine interpolation method. Options are cubic spline or 
!    cubic polynomial for a 1-day series.
!
            Usespline = .false.
            Usecubic  = .false.
            Uselinear = .false.
!
      If (KUT1C.eq.0 .or. KUT1C.eq.4) Then
            Usespline = .true.
            Call PUTA('UT1INTRP      ',mess_spline,int2(30),int2(1), &
     &                 int2(1))
      Endif
!
      If (KUT1C.eq.3) Then
            Uselinear = .true.
            Call PUTA ('UT1INTRP      ',mess_linear,int2(30),int2(1), &
     &                int2(1))
      Endif
!
      If (KUT1C.eq.2) Then
            Usecubic = .true.
            Call PUTA ('UT1INTRP      ',mess_cubic,int2(30),int2(1), &
     &                 int2(1))
      Endif
!
!----------------------------------------------------------------------------
      IF (.not. Input_EOP) THEN      !Database or External Input?
!
       If(type_found.eq.'F') CALL GET4 ('FUT1 PTS      ',UT1PT,Tab_len, &
     &     int2(1),int2(1),NDO,KERR(7))
       If(type_found.eq.'P') CALL GET4 ('PUT1 PTS      ',UT1PT,Tab_len, &
     &     int2(1),int2(1),NDO,KERR(7))
       If(type_found.eq.'X') CALL GET4 ('EUT1 PTS      ',UT1PT,Tab_len, &
     &     int2(1),int2(1),NDO,KERR(7))
       IF (KERR(7).ne.0) Call TERMINATE_CALC ('WOBI  ',int2(7),KERR(7))
!
      ELSE                           !Database or External Input?
!
       CALL PUT4 ('FUT1 INF      ',UT1IF,int2(4),int2(1),int2(1))
       CALL PUT4 ('FUT1 PTS      ',UT1PT,Tab_len,int2(1),int2(1))
!
         Utext(1:10) = 'UT1  from '
         Utext(11:80) = Ex_EOP(1:70)
       CALL PUTA ('FUT1TEXT      ',Lutext, int2(40),int2(1),int2(1))
!
      ENDIF                          !Data Base or External Input?
!----------------------------------------------------------------------------
!
!     PUT the UT1 type message into the database.
      CALL PUTA ('UT1 MESS      ',LUT1M,int2(40),int2(1),int2(1))
!
!  Construct the table of TAI-UT1 (or TAI-UT1S) (note the sense), 
!    depending on the value of KUT1C selected by the user.
!
!    Default: leave tables as true TAI-UT1                
       Do Itab = 1, Tab_len
          UT1RS(Itab) = UT1PT(Itab) 
       Enddo
!
!    Convert to TAI-UT1S (old method, should not be used except
!     for testing)
          If (KUT1C.eq.4) Then
           Do Itab = 1, Tab_len
            CT  = 0.0D0     ! Fraction of day
            TT0 = 0.0D0     ! Fraction of day
            XJD = UT1IF(1) + (Itab-1)*UT1IF(2)
            CALL NUTFA (XJD, TT0, CT, TC2000, FA2K, FAD2K)
!            New Code, UT1S2K replaces UT1SZT
            CALL UT1S2K (FA2K, FAD2K, DUT, DLOD, DOMEGA)
            UT1RS(Itab) = UT1PT(Itab) + DUT
           Enddo
          Endif
!
!     Place the UT1 module flow control message into the database.
!     UT1 module ON, Using true UT1.
         If (KUT1C.eq.0 .or. KUT1C.eq.2 .or. KUT1C.eq.3) Then
          Apply_tidal_correction = .false.
          Call PUTA ('UT1 CFLG      ',LUT1,int2(40),int2(1),int2(1))
         Endif
!     UT1 module ON, Tidal terms from UT1S model.
         If (KUT1C.eq.4) Then
          Apply_tidal_correction = .true.
          Call PUTA ('UT1 CFLG      ',LUT1S,int2(40),int2(1),int2(1))
         Endif
!
!???  Endif
!
!---------------------------------------------------------------------
!   Code for spline interpolation initialization, 93DEC08  -DG-
      If (Usespline) Then       ! Initialize spline routine
       Nspline = tab_len
!
       Do ii=1,Nspline
        Ya(ii) = UT1RS(ii)
       Enddo
!
       Do ii = 1, Nspline
        XT(ii) =  UT1IF(1) + (ii-1)*UT1IF(2)
       Enddo
!
!! !   If interval (UT1IF(2)) not 1.0 days, then divide by interval ?????
!!        If (Abs(UT1IF(2) - 1.D0) .gt. 1.D-10) Then
!!          Do ii=1,Nspline
!!           XT(ii) = XT(ii) / UT1IF(2)
!!          Enddo
!!        Endif
!
!   Take first derivatives at endpoints
       yp1 = (ya(2)-ya(1)) / UT1IF(2)
       ypn = (ya(Nspline)-ya(Nspline-1))/ UT1IF(2)
!
!  Call spline initialization subroutine
       CALL SPLINE(XT,ya,Nspline,yp1,ypn,y2s,ierr4)
!
      Endif                      ! Initialize spline routine
! ***************
!
!   If UT1 module off:
      IF (KUT1C .EQ. 1) then
        CALL PUTA ('UT1 CFLG      ',LOFF,int2(40),int2(1),int2(1))
        apply_tidal_correction = .false.
      Endif
!
!     If there is a ROTEPH array already in the database, get it.
!     Otherwise bypass this step. Also, protect yourself in case
!     there are more than MXEPCH (currently 20) entries in ROTEPH.
!
!    ?????
        MEPOCH = ndays
!    ?????
      IF (.not. Input_EOP) THEN   ! Already have ROTEPH?
        IF (ASKKER .NE. 0) GO TO 400
        MEPOCH = IEPOCH
        IF (MEPOCH .GT. MXEPCH) MEPOCH = MXEPCH
        CALL GET4 ('ROTEPOCH      ',ROTEPH,int2(2),MEPOCH,int2(1),NDO, &
     &   KERR(8))
        IF (KERR(8).NE.0) CALL TERMINATE_CALC('UT1I  ',int2(8),KERR(8))
      ENDIF                       ! Already have ROTEPH?
!
!     Compute TAI - UT1 and the short period tidal correction for the
!     desired epochs and place them in UT1TAB.
      DO  N=1,MEPOCH
        XJD = ROTEPH(1,N)
        CT  = ROTEPH(2,N)
        TT0 = ROTEPH(2,N)
        tab_time  = ROTEPH(2,N)
!
        If(KUT1C.ne.1) Then
          CALL NUTFA (XJD, TT0, CT, TC2000, FA2K, FAD2K)
          CALL UT1MU (XJD,tab_time,FA2K,FAD2K,TC2000,ATMUT1, &
     &              SHORTP,DIVUTC)
        Else
          ATMUT1 = 0.D0
          SHORTP = 0.D0
        Endif
!
        UT1TAB(1,N) = ATMUT1
        UT1TAB(2,N) = SHORTP
      ENDDO
!
!     PUT the UT1TAB array into the database.
      CALL PUT4 ('UT1EPOCH      ', UT1TAB, int2(2), MEPOCH, int2(1))
!
!     Go here if we are bypassing the updating of UT1TAB.
  400 CONTINUE
!
!     Check KUT1D for debug output.
    9 FORMAT (1X, "Debug output for subroutine UT1I.")
    7 FORMAT(A,15I8,/,(9X,15I8))
    8 FORMAT(A,4D25.16,/,(9X,4D25.16))
      IF ( KUT1D .ne. 0 ) Then
      WRITE ( 6, 9)
      WRITE(6,8)' UT1IF   ', UT1IF
      WRITE(6,8)' UT1PT   ', UT1PT
      WRITE(6,8)' UT1RS   ', UT1RS
      If (Usespline) Write(6,8)' XT      ', XT
      If (Usespline) Write(6,8)' ya      ', ya
      If (Usespline) Write(6,8)' y2s     ', y2s
      If (Usespline) Write(6,8)' yp1, ypn', yp1, ypn
      If (Usespline) Write(6,7)' Nspline, ierr4 ', Nspline, ierr4
      WRITE(6,8)' UT1TAB  ', UT1TAB
      WRITE(6,*)' tab_len ', tab_len
      WRITE(6,7)' IEPOCH  ', IEPOCH
      WRITE(6,7)' MEPOCH  ', MEPOCH
      WRITE(6,7)' MXEPCH  ', MXEPCH
      WRITE(6,7)' ASKKER  ', ASKKER
!
      ENDIF
!
!     Normal conclusion.
      RETURN
      END
!
!***********************************************************************
      SUBROUTINE WOBI()
      Implicit None
!
!     WOBI is the wobble module input and initialization section.
!
!     Common blocks used -
!
      INCLUDE 'cmwob11.i'
!            Variables 'from':
!              1) KERASK - The database return code from the 'ASK' for
!                          the rotation epochs array.
!              2) NEPOCH - The number of epochs in the rotation epochs array.
!            Variables 'to':
!              1. WOBIF(3)  -  The wobble information array. Contains
!                              respectively: 1) The Julian date of the first
!                              tabular point, 2) The increment in days of the
!                              tabular points, 3) The number of tabular points.
!                              (days, days, unitless)
!              2. XYWOB(2,20)- The wobble tabular points for the polar motion
!                              (wobble) X & Y offsets. (milliarcsec)
!                              (Note: Based on old BIH conventions, offsets
!                              are assumed to be left-handed.)
!
      INCLUDE 'cmxut11.i'
!            Variables 'from':
!              1. Ndays - number of days in the ROTEPH array.
!
      INCLUDE 'ccon.i'
!            Variables 'from':
!              1.  KWOBC  -  The Wobble Module flow control flag.
!                            0 --> Default, module on; spline interpolation
!                                  for 1-day series, cubic for 5-day series.
!                            1 --> Module off. No polar motion applied.
!                            2 --> Module on; linear interpolation for any
!                                  series.
!                            3 --> Module on; cubic interpolation for 1-day
!                                  series, spline for 5-day series.
!              2.  KWOBD  -  The Wobble Module debug output flag.
!
      INCLUDE 'input11.i'
!            Variables from:
!              1. Input_EOP - T/F logical flag telling whether to use external
!                             EOP file input
!              2. Ex_EOP    - File name for EOP external file input. If 'NONE'
!                             or blank, external EOP input will not be done.
!
      Real*8  ATMUTC(3), ROTEPH(2,20), A1UTC(3), A1DIFF(3)
      COMMON / EOPCM / ATMUTC, ROTEPH, A1UTC, A1DIFF
!           VARIABLES 'TO':
!            2. ROTEPH(2,20)- The array which contains the epochs at which
!                             TAI - UT1 is desired. The entries are:
!                             1) JD at 0:00 hours UTC,
!                             2) The fraction of a UTC day from 0:00 hours
!                                to the desired epoch.
!
!   Program specifications -
!
      Real*8 yp1x,ypnx,yp1y,ypny
      Integer*4 ierr4
      INTEGER*2  KERR(7), NDO(3), Increment, Tab_len
      INTEGER*2      LWOBM(40)
      CHARACTER*40 C_LWOBM(2)
      EQUIVALENCE (C_LWOBM,LWOBM)
      INTEGER*2      LON(40) ,   LOFF(40)
      CHARACTER*40 C_LON(2)  , C_LOFF(2)
      EQUIVALENCE(C_LON,LON),(C_LOFF,LOFF)
      REAL*8 WOBTAB(2,20), XJD, UTC, WOBXL, WOBYL, tab_time, &
     &       DWOBXL, DWOBYL
      Integer*2    mess_linear(30), mess_cubic(30), mess_spline(30)
      Character*60 mess_linear_c , mess_cubic_c, mess_spline_c
      Equivalence(mess_linear,mess_linear_c),(mess_cubic,mess_cubic_c), &
     &           (mess_spline,mess_spline_c)
!     logical*4 table_found
      Character*1 type_found, cdumm(3)
!
      Character*20 C_LFI, C_LPR, C_LEX
      Integer*2 LFI(10), LPR(10), LEX(10)
      Equivalence (C_LFI,LFI), (C_LPR,LPR), (C_LEX,LEX)
      Integer*2 MEPOCH, mdum1
      Integer*4 MXEPCH, N, II
!
      Integer*2 lwtext(40)
      Character wtext*80
      Equivalence  (wtext,lwtext)
!
      DATA LEN_WOB_TABLE /20/
      data roteph /40*0.0d0/
      data wobtab /40*0.0d0/
!
      DATA C_LWOBM / &
     &'Wobble Module - Last Modified 2004.04.21', &
     &', D. Gordon/GSFC:                       '/
!
      DATA  C_LFI /' Final Values       '/
      DATA  C_LPR /' Preliminary Values '/
      DATA  C_LEX /' Extrapolated Values'/
!
      DATA C_LON   / &
     &'Wobble Module is turned on.             ', &
     &'                                        '/
!
      DATA C_LOFF  / &
     &'Wobble Module is turned off.            ', &
     &'                                        '/
!
      Data mess_cubic_c  / &
     &'Polar motion table interp. w. 4 pt, 3rd order polynomial.   '/
!
      Data mess_linear_c/ &
     &'Polar motion table interp. w. 2 pt, linear interpolator.    '/
!
      Data mess_spline_c / &
     &'Polar motion table interpolated with 5 point cubic spline.  '/
!
      DATA MXEPCH /20/
!
!    Database access  -
!           'GET' Variables:
!             1. WOBIF(3)   -  The wobble information array. Contains
!                              respectively: 1) The Julian date of the first
!                              tabular point, 2) The increment in days of the
!                              tabular points, 3) The number of tabular points.
!                              (days, days, unitless)
!             2. XYWOB(2,20) - The tabular points for the long period
!                              wobble X & Y-offsets. (milli-arc-sec)
!                              (NOTE: in old BIH convention UT1, X-pole, Y-pole
!                              system is left handed.)
!             3. ROTEPH(2,20)- The epochs at which the interpolated WOBBLE are
!                              desired. The first entry is the Julian date at
!                              0:00 hours UTC of the day desired. The second
!                              entry is the UTC fraction of the UTC day of the
!                              desired epoch (days,days)
!           'PUT' Variables:
!             1. LWOBM(40)    - The Wobble Module text message.
!             2. LON(40)      - The Wobble Module 'TURNED ON' message.
!             3. LOFF(40)     - The Wobble Module 'TURNED OFF' message.
!             4. WOBTAB(2,20) - The array which contains the interpolated
!                               wobble in a left-handed system. The first entry
!                               is the X wobble. The second entry is the Y
!                               wobble. (milliarcsec, milliarcsec)
!           ACCESS CODES:
!             1. 'WOB MESS'  -  The database access code for the Wobble Module
!                               text message.
!             2. 'FWOB INF'  -  The database access code for the final wobble
!                               information array.
!             3. 'FWOBX&YT'  -  The database access code for the final wobble
!                               X & Y offsets.
!             4. 'PWOB INF'  -  The database access code for the preliminary
!                               wobble information array.
!             6. 'PWOBX&YT'  -  The database access code for the preliminary
!                               wobble X & Y offsets.
!             7. 'WOB CFLG'  -  The database access code for the Wobble Module
!                               flow control message.
!             8. 'ROTEPOCH'  -  The database access code for the epochs at
!                               which the interpolated values are computed.
!             9. 'WOBEPOCH'  -  The database access code for the interpolated
!                               wobble array.
!            10. 'EWOB INF'  -  The database access code for the extrapolated
!                               wobble information array.
!            11. 'EWOBX&YT'  -  The database access code for the extrapolated
!                               wobble X & Y offsets.
!            12. 'WOBINTRP'  -  The database access code for the polar motion
!                               interpolation message.
!
!     Subroutine interface -
!             Caller subroutines: INITL
!             Called subroutines: GET4, TERMINATE_CALC, PUTA, PUT4, WOBMU
!
!     Program Variables -
!           1. Increment -  The increment of the wobble table. (days)
!           2. KERR(7)   -  Database error return flags.
!           3. NDO(3)    -  Database return array indices.
!           4. MXEPCH    -  The maximum number of interpolated wobble epochs.
!           5. WOBXL     -  The interpolated X-wobble (left-handed).
!                           (milliarcsec)
!           6. WOBYL     -  The interpolated Y-wobble (left-handed).
!                           (milliarcsec)
!           7. DWOBXL    -  The time derivative of the interpolated X-wobble.
!                           (left-handed) (milliarcsec/sec)
!           8. DWOBYL    -  The time derivative of the interpolated Y-wobble.
!                           (left-handed) (milliarcsec/sec)
!           9. MEPOCH    -  The number of epochs at which the wobble
!                           is interpolated.
!          10. XJD       -  The Julian date at 0:00 hours UTC of the
!                           interpolation epoch. (days)
!          11. UTC       -  The UTC fraction of the UTC day of the
!                           interpolation epoch. (days)
!          12. Tab_len   -  The length of the Wobble table in the database.
!          13. type_found-  Tracks the type of wobble table in database.
!          14. tab_time  -  The fraction of the day of the interpolation epoch
!                           in WOBMU. (days)
!
! 3.2.9 PROGRAMMER - DALE MARKHAM  02/17/77
!     77.07.14 Peter Denatale
!     78.01.11 Bruce Schupler
!     89.07.26 Jim Ryan  Documentation simplifed and strings.
!     89.08.16 Jim Ryan  Use of LEN_WOB_TALBE implimented.
!     89.12.12 Jim Ryan  UNIX-like database interface implimented.
!     90.06.05 Jim Ryan  Mods for linear interpolation and more clean up.
!     92.07.17 Jim Ryan  Roteph and Wobtab initialized to avoid debug problem.
!     93.12.17 D. Gordon Spline interpolation added, new flow control logic.
!     93.12.30 D. Gordon Cleaned up 'WOB MESS'.
!     94.04.15 D. Gordon Converted to Implicit None.
!     94.05.23 D. Gordon Fixed bug - use_cubic, use_linear, use_spline were
!                        being dimensioned both Logical*2 and Real*8.
!     95.10.05 D. Gordon Skip interpolation if module OFF (KWOBC=1); set
!                        WOBXL and WOBYL equal to zero (previously undefined).
!     95.12.04 D. Gordon Interpolation epoch variable changed from UTC to
!                        tab_time.
!     95.12.11 D. Gordon Adding DWOBXL and DWOBYL, derivatives of X-wobble and
!                        Y-wobble, to WOBMU argument list. Not used here.
!     98.05.01 D.Gordon  Put Common /WOBCM/ into include file 'cmwob.i'.
!                        Added include files 'inputs.i' and 'cmxut.i', and
!                        common block EOPCM. Extensive mods for external
!                        EOP input.
!     99.10.27 D.Gordon  Corrected error in ADD/PUT of 'WOBEPOCH' when
!                        external input being used.
!     2002 Sept Jim Ryan Integer*2/4 mods.
!
!     WOBI Program Structure
!
!   Verify that the wobble control flag is okay.
      If(KWOBC.lt.0 .or. KWOBC.gt.3) Then
        Write(6,'( &
     &  "In WOBI, Wobble module control flag has invalid value.",/, &
     &  "KWOBC =",i5)') KWOBC
        Call TERMINATE_CALC( 'WOBI  ', int2(0), int2(0))
      Endif
! ---------------------------------------------------------------------------
!  If using data base EOP input, get the X and Y wobble values from the data
!   base now. If using external EOP input instead, then we already have them.
! ---------------------------------------------------------------------------
      IF (.not. Input_EOP) THEN      !Data Base or External Input?
!
!  'GET' the final wobble information from the database. If final information
!  is not available, then 'GET' the preliminary or extrapolated information.
!  If a database access error is detected, then TERMINATE_CALC CALC.
!
      table_found = .false.
      If(.not.table_found) Then     ! Get final values
        CALL GET4 ('FWOB INF      ',WOBIF,int2(3),int2(1),int2(1),NDO, &
     &   KERR(1))
        IF ( KERR(1) .eq. 0 ) Then
          table_found = .true.
          type_found  = 'F'
           do n = 1,10
             LWOBM(30+n) = LFI(n)
           enddo
        Endif
      Endif
!
      If(.not.table_found) Then     ! Get preliminary values
        CALL GET4 ('PWOB INF      ',WOBIF,int2(3),int2(1),int2(1),NDO, &
     &   KERR(1))
        IF (KERR(1) .eq. 0) Then
          table_found = .true.
          type_found  = 'P'
           do n = 1,10
             LWOBM(30+n) = LPR(n)
           enddo
        Endif
      Endif
!
      If(.not.table_found) Then     ! Get extrapolated values
        CALL GET4 ('EWOB INF      ',WOBIF,int2(3),int2(1),int2(1),NDO, &
     &   KERR(1))
        If(KERR(1).eq.0) Then
          table_found = .true.
          type_found  = 'X'
           do n = 1,10
             LWOBM(30+n) = LEX(n)
           enddo
        Endif
      Endif
!
      If(KERR(1).eq.2) Then
        Write(6,'( &
     &  "In WOBI: The wobble table in database has wrong size.")')
        Call TERMINATE_CALC('WOBI  ', KERR(1), int2(1))
      Endif
!
      If (.not.table_found) Then
        Write(6,'( &
     &  "This database contains NO polar motion data. Quitting!")')
        CALL TERMINATE_CALC('WOBI  ', int2(5), KERR(5))
      Endif
!
      Increment = WOBIF(2) + 0.01
      Tab_len   = WOBIF(3) + 0.01
!
      If (Tab_len .gt. LEN_WOB_TABLE) Then
        WRITE(6,'( &
     &  "The length of the wobble table from the database is",I5,/, &
     &  "CALC Wobble module currently supports only",I5)') Tab_len, &
     &  LEN_WOB_TABLE
        CALL TERMINATE_CALC('WOBI  ', int2(0), int2(0))
      Endif
!
      ELSE                           !Data Base or External Input?
!
!      Do PUT's for the case of EOP external file input
!
         Increment = WOBIF(2) + 0.01
         Tab_len   = WOBIF(3) + 0.01
        Call Put4 ('FWOB INF      ', WOBIF, int2(3), int2(1), int2(1))
        call Put4 ('FWOBX&YT      ', XYWOB, int2(2), Tab_len, int2(1))
! 
        wtext(1:16) = 'X/Y wobble from '
        wtext(17:80) =  Ex_EOP(1:64)
        call PUTA ('FWOBTEXT      ',lwtext,int2(40), int2(1), int2(1))
!
      Increment = WOBIF(2) + 0.01
      Tab_len   = WOBIF(3) + 0.01
!
      ENDIF                          !Data Base or External Input?
! ---------------------------------------------------------------------------
!
!  Determine interpolation method. Defaults are cubic spline for a 1-day series
!    and cubic polynomial for a 5-day (or any other) series.
!
        use_spline = .false.
        use_linear = .false.
        use_cubic  = .false.
!
      If(KWOBC.eq.0) Then       !Default interpolation
        If(Increment.eq.1) Then
          use_spline = .true.
          Call PUTA ('WOBINTRP      ',mess_spline,int2(30),int2(1), &
     &         int2(1))
        Else
          use_cubic = .true.
          Call PUTA ('WOBINTRP      ',mess_cubic,int2(30),int2(1), &
     &         int2(1))
        Endif
      Endif
!
      If(KWOBC.eq.2) Then       !Force linear interpolation
          use_linear = .true.
          Call PUTA ('WOBINTRP      ',mess_linear,int2(30),int2(1), &
     &         int2(1))
      Endif
!
      If(KWOBC.eq.3) Then       !Reverse default interpolation
        If(Increment.eq.1) Then
          use_cubic = .true.
          Call PUTA ('WOBINTRP      ',mess_cubic,int2(30),int2(1), &
     &         int2(1))
        Else
          use_spline = .true.
          Call PUTA ('WOBINTRP      ',mess_spline,int2(30),int2(1), &
     &         int2(1))
        Endif
      Endif
!
      IF (.not. Input_EOP) THEN
!        'GET' the tabular values, if not external input.
        If (type_found.eq.'F') CALL GET4 ('FWOBX&YT      ',XYWOB, &
     &      int2(2),Tab_len,int2(1),NDO,KERR(6))
        If (type_found.eq.'P') CALL GET4 ('PWOBX&YT      ',XYWOB, &
     &      int2(2),Tab_len,int2(1),NDO,KERR(6))
        If (type_found.eq.'X') CALL GET4 ('EWOBX&YT      ',XYWOB, &
     &      int2(2),Tab_len,int2(1),NDO,KERR(6))
        If (KERR(6).ne.0) Call TERMINATE_CALC('WOBI  ',int2(6), &
     &      kerr(6))
      ENDIF
!
!  'PUT' the module text message.
      CALL PUTA ('WOB MESS      ',LWOBM,int2(40),int2(1),int2(1))
!
!   'PUT' the Wobble module flow control message depending on KWOBC.
!    See above string definitions for meanings.
!
      If (KWOBC .ne. 1) Then
        CALL PUTA ('WOB CFLG      ',LON,int2(40),int2(1),int2(1))
      Else
        CALL PUTA ('WOB CFLG      ',LOFF,int2(40),int2(1),int2(1))
      Endif
!
!   Code for spline interpolation initialization, 93DEC17  -DG-
!
      If (use_spline) Then       ! Initialize spline routine
!
      n_spline = tab_len
      do ii=1,n_spline
       yax(ii) = XYWOB(1,ii)
       yay(ii) = XYWOB(2,ii)
      enddo
!
      xa(1) = WOBIF(1)
      do ii=2,n_spline
       xa(ii) = xa(ii-1) + WOBIF(2)
      enddo
!
!   If interval (WOBIF(2)) not 1.0 days, then divide by interval
      if ( Abs(WOBIF(2) - 1.D0) .gt. 1.D-10) then
        do ii=1,n_spline
         xa(ii) = xa(ii) / WOBIF(2)
        enddo
      endif
!
!   Take first derivatives at endpoints for X-wobble
      yp1x = (yax(2)-yax(1)) / (xa(2)-xa(1))
      ypnx = (yax(n_spline)-yax(n_spline-1))/ &
     &       (xa(n_spline)-xa(n_spline-1))
!  call spline initialization subroutine for X-wobble
      call spline(xa,yax,n_spline,yp1x,ypnx,y2sx,ierr4)
!
!   Take first derivatives at endpoints for Y-wobble
      yp1y = (yay(2)-yay(1)) / (xa(2)-xa(1))
      ypny = (yay(n_spline)-yay(n_spline-1))/ &
     &       (xa(n_spline)-xa(n_spline-1))
!  call spline initialization subroutine for Y-wobble
      call spline(xa,yay,n_spline,yp1y,ypny,y2sy,ierr4)
!
      Endif                      ! Initialize spline routine
!
!    If there is a ROTEPH array in the database, GET it. Otherwise,
!    bypass this step. Also, check in case there are more than MXEPCH
!    entries in ROTEPH.
!
!**   IF (.not. Input_EOP) THEN
!!??    IF (KERASK .NE. 0) GO TO 400
        MEPOCH = NEPOCH
        IF (MEPOCH .GT. MXEPCH) MEPOCH = MXEPCH
!@        mepoch = 2
        IF (KERASK .NE. 0) GO TO 401
        CALL GET4 ('ROTEPOCH      ', ROTEPH, int2(2), MEPOCH, int2(1), &
     &       NDO, KERR(7))
        IF (KERR(7) .NE. 0) write ( 6, * ) 'Failure to retreive lcode '// &
     &      ' ROTEPOCH  MEPOCH =', MEPOCH,' NEPOCH =',NEPOCH, ' MXEPCH = ', &
     &        MXEPCH, ' kerr(7) = ', kerr(7) ! %%%%%%%%%
        IF (KERR(7) .NE. 0) CALL TERMINATE_CALC('WOBI  ', int2(7), &
     &       KERR(7))
!**   ELSE
!**     MEPOCH = ndays
!**   ENDIF
!
      Go to 402
 401  Continue
       MEPOCH = ndays
 402  Continue  
! 
!      write(6,*) 'ISUBS/MEPOCH,NEPOCH= ', MEPOCH,NEPOCH 
!      write(6,*) 'ndays = ', ndays 
!      write(6,*) 'ROTEPH(2,1),ROTEPH(2,2) ',  ROTEPH(2,1),ROTEPH(2,2)
!   Compute the interpolated values of the wobble with a call to WOBMU.
      DO N=1,MEPOCH
        XJD = ROTEPH(1,N)
        tab_time = ROTEPH(2,N)
         If (KWOBC.ne.1) then             ! Module ON
          CALL WOBMU (XJD,tab_time,WOBXL,WOBYL,DWOBXL,DWOBYL)
         Else                             ! Module OFF
          WOBXL = 0.D0
          WOBYL = 0.D0
         Endif
        WOBTAB(1,N) = WOBXL
        WOBTAB(2,N) = WOBYL
!      write(6,*) 'WOBTAB ', WOBTAB(1,N),WOBTAB(2,N) 
      Enddo
!
!    'PUT' the WOBTAB array into the database.
      CALL PUT4 ('WOBEPOCH      ', WOBTAB, int2(2), MEPOCH, int2(1))
!
!    Go here if bypassing the interpolation step.
  400 CONTINUE
!
!    Check KWOBD for debug output.
      IF ( KWOBD .EQ. 0 ) GO TO 500
      WRITE ( 6, 9)
    9 FORMAT (1X, 'Debug output for subroutine WOBI.' )
    7 FORMAT(A,15I8/(9X,15I8))
      WRITE(6,8)' WOBIF ',WOBIF
    8 FORMAT(A,4D25.16/(8X,5D25.16))
      WRITE(6,8)' XYWOB   ',XYWOB
      WRITE(6,8)' ROTEPH  ',ROTEPH
      WRITE(6,8)' WOBTAB  ',WOBTAB
!
!     Normal conclusion.
  500 RETURN
      END
!
!***********************************************************************
      SUBROUTINE SITI()
      IMPLICIT None
!
! 3.    SITI
!
! 3.1   SITI PROGRAM SPECIFICATION
!
! 3.1.1 SITI is the Site Module input and initialization section.
!
! 3.1.2 RESTRICTIONS - NONE
!
! 3.1.3 REFERENCES - SMART, W.M., 'TEXTBOOK ON SPHERICAL ASTRONOMY',
!                    1965, P. 195-198
!                    MARKHAM'S X-DOCUMENT
!
! 3.2   SITI PROGRAM INTERFACE
!
! 3.2.1 CALLING SEQUENCE - NONE
!
! 3.2.2 COMMON BLOCKS USED -
!
      INCLUDE 'cphys11.i'
!            VARIABLES 'FROM':
!              1. EFLAT  - THE FLATTENNING OF THE ELLIPSOID APPROXIMATING
!                          THE SHAPE OF THE EARTH.  (UNITLESS)
!              2. REARTH - THE EQUATORIAL RADIUS OF THE EARTH. (M)
!
      Real*8           PI, TWOPI, HALFPI, CONVD, CONVDS, CONVHS, SECDAY
      COMMON / CMATH / PI, TWOPI, HALFPI, CONVD, CONVDS, CONVHS, SECDAY
!
      INCLUDE 'cmxst11.i'
!            Variables to:
!       1. CFRAD(Max_Stat)      -  THE SITE SPHERICAL EARTH RADII.  (M)
!       2. PLAT(3,Max_Stat)     -  THE PARTIAL DERIVATIVES OF THE SITE CRUST
!                                  FIXED VECTOR COMPONENTS WITH RESPECT TO THE
!                                  GEODETIC LATITUDES. (M/RAD)
!       3. PLON(3,Max_Stat)     -  THE PARTIAL DERIVATIVES OF THE SITE CRUST
!                                  FIXED VECTOR COMPONENTS WITH RESPECT TO THE
!                                  EAST LONGITUDES. (M/RAD)
!       4. SITAXO(Max_Stat)     -  THE SITE ANTENNA AXIS OFFSETS. (M)
!       5. SITOAM(11,Max_Stat)  -  THE SITE VERTICAL OCEAN LOADING AMPLITUDES.
!                                  (M)
!       6. SITOPH(11,Max_Stat)  -  THE SITE VERTICAL OCEAN LOADING PHASES.
!                                  (RAD)
!       7. SITXYZ(3,Max_Stat)   -  THE SITE CRUST FIXED X, Y, & Z
!                                  COORDINATES. (M, M, M )
!       8. SNRM(3,Max_Stat)     -  THE X, Y, AND Z COMPONENTS OF THE SITE
!                                  NORMAL UNIT VECTORS. (UNITLESS)
!       9. SITZEN(Max_Stat)     -  THE ZENITH ELECTRICAL PATH LENGTH AT EACH
!                                  OBSERVATION SITE. (SEC)
!      10. TCROT(3,3,Max_Stat)  -  THE ROTATION MATRICES WHICH ROTATE THE
!                                  TOPOCENTRIC REFERENCE SYSTEM TO THE CRUST
!                                  FIXED REFERENCE SYSTEM FOR EACH SITE.
!                                  (UNITLESS)
!      11. XLAT(Max_Stat)       -  THE SITE GEODETIC LATITUDES. (RAD)
!      12. XLON(Max_Stat)       -  THE SITE EAST LONGITUDES. (RAD)
!      13. KTYPE(Max_Stat)      -  THE SITE ANTENNA AXIS TYPES. (UNITLESS)
!      14. NLAST(2)             -  THE INTEGER VARIABLE WHICH DETERMINES IF
!                                  THE BASELINE ID HAS CHANGED FROM ONE
!                                  OBSERVATION TO THE NEXT.
!                                  (NOTE: THE SITE GEOMETRY NEED NOT BE
!                                  RELOADED FOR EACH OBSERVATION IF THE
!                                  BASELINE ID DOES NOT CHANGE. NLAST MUST BE
!                                  INITIALIZED TO ZERO IN THE INITIALIZATION
!                                  SECTION AND PASSED TO THE GEOMETRY SECTION
!                                  SO THAT IT WILL HAVE ZERO VALUES UNTIL
!                                  AFTER THE FIRST OBSERVATION IS PROCESSED.)
!      15. NUMSIT               -  THE NUMBER OF SITES IN THE SITE CATALOG.
!      16. LNSITE(4,Max_Stat)   -  THE EIGHT CHARACTER SITE NAMES OF THE
!                                  SITES IN THE SITE CATALOG. (ALPHAMERIC)
!      17. SITHOA(11,2,Max_Stat) - THE SITE HORIZONTAL OCEAN LOADING
!                                  AMPLITUDES. (M)
!      18. SITHOP(11,2,Max_Stat) - THE SITE HORIZONTAL OCEAN LOADING PHASES.
!                                  (RAD)
!      19. HEIGHT(Max_Stat)     -  Height above the geoid. (meters)
!      20. RTROT(3,3,Max_Stat)  -  The rotation matrices which rotate the
!                                  'radial-transverse' reference system to the
!                                  crust fixed reference system for each site.
!                                  (Unitless). The 'radial-transverse' ref.
!                                  system is nearly identical to the
!                                  topocentric system. 'Up' is in the radial
!                                  direction from the center of the Earth;
!                                  'East' is East; and 'North' is perpendicular
!                                  to the radial in the north direction.
!      21. GLAT(Max_Stat)       -  The geocentric latitude at each site. (rad)
!      22. Zero_site            -  The site number of the site at the
!                                  geocenter, if there is one in this data
!                                  set. For correlator usage.
!      23. Dbtilt(Max_Stat,2)   -  Antenna fixed axis tilts, in arc-minutes.
!                                  For alt-az mounts, 1 => East tilt,
!                                  2 => North tilt.
!      24. Rotilt(3,3,Max_Stat) -  Rotation matrices representing the antenna
!                                  fixed axis tilts, in the local topocentric
!                                  station frame. X = Up, Y = East, Z = North.
!      25. OPTL6(6,Max_stat)    -  The site ocean pole tide loading 
!                                  coefficients, interpolated from the Desai
!                                  lat/lon table.
!
      INCLUDE 'ccon.i'
!            VARIABLES 'FROM':
!              1. KSITC - THE SITE MODULE FLOW CONTROL FLAG.
!              2. KSITD - THE SITE MODULE DEBUG OUTPUT FLAG.
!
      INCLUDE 'input11.i'
!            Variables from:
!              1. Input_sites - T/F logical flag telling whether to use
!                               external site a priori input
!              2. Input_ocean - T/F logical flag telling whether to
!                               use external ocean loading a priori input
!              3. Input_OPTL  - T/F logical flag telling whether to
!                               use external ocean pole tide loading 
!                               apriori input
!              4. Ex_sites    - File name for sites external file input.
!                               If 'NONE' or blank, external site input
!                               will not be done.
!              5. Ex_ocean    - File name for ocean loading external file.
!                               If 'NONE' or blank, external ocean loading
!                               input will not be done.
!              6. Ex_OPTL     - File name for ocean pole tide loading
!                               external file. If 'None' or blank, external
!                               ocean pole tide loading input will not be done.
!
      INCLUDE 'cuser11.i'
!       Variables from:
!         1. Calc_user  - Calc user type. 'A' for Calc/SOLVE analysis.
!                         'C' for VLBI correlator.
!
      INCLUDE 'cmxut11.i'
!        Variables 'to':
!           1. Intrvl(5,2) - First and last time tag of data in the current
!                            data base. (First index: year, month, day,
!                            hour, minute. Second index: first, last.)
      INCLUDE 'param11.i'
!       Variables from:
!           1. A_tilts   - Antenna tilts file name (default file).
!           2. OPTL_file - Ocean pole tide loading file (default file).
!
! 3.2.3 PROGRAM SPECIFICATIONS -
!
      Integer*2 KERR(12), LSITM(40), NDO(3), NDI(3), NN, numsit_local, &
     &                       KERX(2)
      Integer*4 N, I, J, K, L, ik, kj, Imdoy(12), Intmov, Krr
      REAL*8 XLAT_DUMMY,XLON_DUMMY, RY(3,3), RZ(3,3), TCROT_DUMMY(3,3), &
     &       RGY(3,3), GLAT_DUMMY, RTROT_DUMMY(3,3), Sitxyzv(7,Max_stat), &
     &       T1(3,3), T2(3,3)
      Real*8 xlatlonht(3),Pie,A,Fl,Xyz(3), Xdoy1, Xepoch, X_frac
      CHARACTER*40 C_LSITM(2)
      EQUIVALENCE (LSITM,C_LSITM)
      CHARACTER*1 ITEST, idum(3)
      Character*80 xtlt
!     Logical*4 Input_sites, Input_ocean
!
      DATA C_LSITM / &
     &'Site Module - Last modified 2012.11.30, ', &
     &'D. Gordon, GSFC                         '/
!
      Data Imdoy /0,31,59,90,120,151,181,212,243,273,304,334/
!
! 3.2.4 DATA BASE ACCESS -
!
!    'GET' VARIABLES:
!      1. KTYPE(Max_Stat)       -  THE ANTENNA AXIS TYPES.  (UNITLESS)
!      2. LNSITE(4,Max_Stat)    -  THE EIGHT ALPHAMERIC CHARACTER SITE NAMES
!                                  OF THE SITES IN THE SITE CATALOG.
!      3. NUMSIT                -  THE NUMBER OF SITES IN THE SITE CATALOG.
!      4. SITAXO(Max_Stat)      -  THE SITE ANTENNA AXIS OFFSETS. (M)
!      5. SITOAM(11,Max_Stat)   -  THE SITE VERTICAL OCEAN LOADING
!                                  AMPLITUDES. (M)
!      6. SITOPH(11,Max_Stat)   -  THE SITE VERTICAL OCEAN LOADING PHASES.
!                                  (RAD)
!      7. SITHOA(11,2,Max_Stat) -  THE SITE HORIZONTAL OCEAN LOADING
!                                  AMPLITUDES. (M)
!      8. SITHOP(11,2,Max_Stat) -  THE SITE HORIZONTAL OCEAN LOADING PHASES.
!                                  (RAD)
!      9. SITXYZ(3,Max_Stat)    -  THE SITE CRUST FIXED X, Y, & Z
!                                  COORDINATES. (M, M, M )
!     10. SITZEN(Max_Stat)      -  THE ZENITH ELECTRICAL PATH LENGTH
!                                  AT EACH OBSERVATION SITE. (SEC)
!     11. Sitxyzv(7,Max_Stat)   -  Optional correlator input via access code
!                                  'SITERECV' in place of SITXYZ (via
!                                  'SITERECS'). First index runs over site crust
!                                  fixed coordinates (X, Y, Z), epoch for
!                                  those coordinates (4-digit and fractional
!                                  year, such as 1998.5), and site velocities
!                                  (X-velocity, Y-velocity, Z-velocity).
!                                  Second index runs over the sites. For
!                                  geocenter station make sure X-velocity =
!                                  Y-velocity = Z-velocity = 0.D0.
!                                  (m, m, m, yrs, m/sec, m/sec, m/sec)
!
!    'PUT' VARIABLES:
!      1. LSITM(40)  -  THE SITE MODULE TEXT MESSAGE.
!      2. SITXYZ(3,Max_Stat) - Correlator output of site crust fixed
!                       coordinates, modified if access code 'SITERECV'
!                       supplied.
!
!    ACCESS CODES:
!      1. 'SIT MESS'  -  THE DATA BASE ACCESS CODE FOR THE SITE MODULE TEXT
!                        MESSAGE.
!      2. 'AXISTYPS'  -  THE DATA BASE ACCESS CODE FOR THE ARRAY OF SITE
!                        ANTENNA TYPES.
!      3. 'SITNAMES'  -  THE DATA BASE ACCESS CODE FOR THE ARRAY OF SITE NAMES.
!      4. '# SITES '  -  THE DATA BASE ACCESS CODE FOR THE NUMBER OF
!                        OBSERVATION SITES.
!      5. 'AXISOFFS'  -  THE DATA BASE ACCESS CODE FOR THE ARRAY OF SITE
!                        ANTENNA AXIS OFFSETS.
!      6. 'SITERECS'  -  THE DATA BASE ACCESS CODE FOR THE  ARRAY OF SITE
!                        X,Y,Z COORDINATES.
!      7. 'SITEZENS'  -  THE DATA BASE ACCESS CODE FOR THE SITE ZENITH
!                        ELECTRICAL PATH LENGTH.
!      8. 'SITOCAMP'  -  THE DATA BASE ACCESS CODE FOR THE VERTICAL OCEAN
!                        LOADING AMPLITUDES.
!      9. 'SITOCPHS'  -  THE DATA BASE ACCESS CODE FOR THE VERTICAL OCEAN
!                        LOADING PHASES.
!     10. 'SITHOCAM'  -  THE DATA BASE ACCESS CODE FOR THE HORIZONTAL OCEAN
!                        LOADING AMPLITUDES.
!     11. 'SITHOCPH'  -  THE DATA BASE ACCESS CODE FOR THE HORIZONTAL OCEAN
!                        LOADING PHASES.
!
! 3.2.6 SUBROUTINE INTERFACE -
!           CALLER SUBROUTINES: INITL
!           CALLED SUBROUTINES: DCOS, DSIN, DSQRT, GETA, GETI,
!                    GET4, TERMINATE_CALC, MMUL2, PUTA, ROTATE, bkplh
!
! 3.2.7 CONSTANTS USED - EFLAT, REARTH
!
! 3.2.8 PROGRAM VARIABLES -
!           1. KERR(12) - THE DATA BASE ERROR RETURN FLAGS.
!           2. NDO(3)   - THE DATA BASE RETURN ARRAY INDICES.
!           3. RY(3,3)  - THE ROTATION MATRIX WHICH PERFORMS A COORDINATE
!                         SYSTEM ROTATION ABOUT THE TOPOCENTRIC Y-AXIS (EAST)
!                         THROUGH AN ANGLE EQUAL TO THE GEODETIC LATITUDE OF
!                         THE CURRENT SITE. (UNITLESS)
!           4. RZ(3,3)  - THE ROTATION MATRIX WHICH PERFORMS A COORDINATE
!                         SYSTEM ROTATION ABOUT THE TOPOCENTRIC Z-AXIS (NORTH)
!                         THROUGH AN ANGLE EQUAL TO THE NEGATIVE EAST LONGITUDE
!                         OF THE CURRENT SITE. (UNITLESS)
!
! 3.2.9 PROGRAMMER - DALE MARKHAM    01/13/77
!                    PETER DENATALE  07/13/77
!                    BRUCE SCHUPLER  03/08/78
!                    CHOPO MA        08/06/81
!                    HAROLD M. SCHUH 10/08/83
!                    SAVITA GOEL 06/03/87 (CDS FOR A900)
!                    LOTHAR MOHLMANN 03/23/89
!                    89.07.20 Jim Ryan Documentation simplified.
!                    Jim Ryan 89:10:05 CPHYS common made an include file.
!                    Jim Ryan 89.12.12 UNIX-like database interface
!                                      implimented.
!                    MSW 93.03.30 SITCM common and maximum number of station
!                                 variable put into newly created include file
!                                 "cmxst.i".
!                    David Gordon 93.10.12 Call to subroutine bkplh added for
!                                 geodetic latitude, longitude, and height;
!                                 Fixed debug printout errors.
!                    David Gordon 94.02.04 HEIGHT(Max_Stat) addded and put into
!                                 cmxst.i; heights above geoid (meters).
!                    David Gordon 94.04.16 Converted to Implicit None.
!                    David Gordon 98.01.22 Modified for station at or near the
!                         geocenter, set topocentric stuff to -999 or so.
!                    David Gordon 98.03.13 Mods for external site input via
!                         an Ascii file.
!                    David Gordon 98.03.17 Mods for source a priori's and ocean
!                         loading coefficients in the case of external file
!                         inputs.
!                    David Gordon 98.06.26 Add code to compute the 'radial-
!                         transverse rotation matrices at each site, for use
!                         in the Earth tide module.
!                    David Gordon 98.10.26 Add code for optional correlator
!                         usage of site velocities. Need input Lcodes
!                         'INTERVAL' and 'SITERECV'. New output Lcode is
!                         'SITERECS' - modified or not.
!                    David Gordon 98.11.05 Put in code to use proposed new
!                         Lcode 'INTRVAL4', the start/stop interval
!                         (yr/month/day/hr/min) using a 4-digit year. If
!                         not there will use 'INTERVAL' (2-digit year).
!                         Correlator output site position Lcode changed to
!                         'SITEXYZS', dimensions (3,Max_stat). 10 year limit
!                         set on velocity interpolation.
!                    Jim Ryan 02.Sept Integer*2/4 Updates.
!                    D. Gordon 2004.05.18 Axis tilt code added.
!                    D. Gordon 2006.01.26 Bug fix for Richmond tilt matrix.
!                    D. Gordon 2012 Nov.  Added code for ocean pole tide 
!                         loading coefficients. 
!
! 3.3   SITI PROGRAM STRUCTURE
!
!   Initialize site positions for correlator usage
       If (calc_user .eq. 'C') Then
         do ik=1,Max_Stat
          do kj=1,3
           SITXYZ(kj,ik) = 0.D0
          enddo
         enddo
       Endif
!
!     PUT the module text message.
      CALL PUTA ( 'SIT MESS      ', LSITM, int2(40), int2(1), int2(1))
!
!     Use GETA, GETI, & GET4 to obtain the site information from the
!       database. All accessed site info goes into COMMON/SITCM/ in cmxst.i.
!
      CALL GETI ('# SITES       ',NUMSIT,int2(1),int2(1),int2(1),NDO, &
     & KERR(1))
      NUMSIT_LOCAL = NUMSIT
      CALL GETA ('SITNAMES      ',LNSITE,int2(4),NUMSIT_LOCAL,int2(1), &
     & NDO, KERR(2))
!
!-----------------------------------------------------------------------------
!  Data base input or external file input for sites?
      IF (Input_sites) THEN                    ! Get site info
!
       CALL SITBLK(Kerr)
!
      ELSE                                     ! Get site info
!
       CALL GETI ('AXISTYPS      ',KTYPE,NUMSIT_LOCAL,int2(1),int2(1), &
     &  NDO, KERR(3))
       CALL GET4 ('AXISOFFS      ',SITAXO,NUMSIT_LOCAL,int2(1),int2(1), &
     &  NDO, KERR(4))
       CALL GET4 ('SITEZENS      ',SITZEN,NUMSIT_LOCAL,int2(1),int2(1), &
     &  NDO, KERR(6))
!
!           Look for 4-digit year interval
          CALL GETI ('INTRVAL4      ',Intrvl,int2(5),int2(2),int2(1), &
     &     NDO, KERX(2))
!           If no 4-digit year interval, get 2-digit year interval
         If (KERX(2).ne.0) then
          CALL GETI ('INTERVAL      ',Intrvl,int2(5),int2(2),int2(1), &
     &     NDO, KERX(2))
!            Convert 2-digit year to 4-digit year
          If (Intrvl(1,1) .ge. 70 .and. Intrvl(1,1) .le. 99) &
     &        Intrvl(1,1) = Intrvl(1,1)+1900
          If (Intrvl(1,1) .ge.  0 .and. Intrvl(1,1) .le. 69) &
     &        Intrvl(1,1) = Intrvl(1,1)+2000
         Endif
!
!***********************************************
!  Expanded for optional correlator usage of site velocities
       If (calc_user .eq. 'C') Then
!         'SITERECV' should contain X, Y, Z, epoch, X-dot, Y-dot, Z-dot
!           for each station, all Real*8. Epoch must be a 4-digit year and
!           fraction  thereof (1998.0, 2001.5, etc.) If Epoch is zero for a
!           site, then velocity corrections will be turned off for that site.
!           If X=Y=Z=0.D0 (geocenter), then velocity corrections will
!           automatically be turned off.
        CALL GET4 ('SITERECV      ',Sitxyzv,int2(7),NUMSIT_LOCAL, &
     &             int2(1), NDO, KERX(1))
!
! !  Apply site velocities from new L-code 'SITERECV'
        If (KERX(1) .eq. 0)  Then
!           Look for 4-digit year interval
!         CALL GETI ('INTRVAL4      ',Intrvl,int2(5),int2(2),int2(1),
!    #     NDO, KERX(2))
!           If no 4-digit year interval, get 2-digit year interval
!        If (KERX(2).ne.0) then
!         CALL GETI ('INTERVAL      ',Intrvl,int2(5),int2(2),int2(1),
!    #     NDO, KERX(2))
!            Convert 2-digit year to 4-digit year
!         If (Intrvl(1,1) .ge. 70 .and. Intrvl(1,1) .le. 99)
!    *        Intrvl(1,1) = Intrvl(1,1)+1900
!         If (Intrvl(1,1) .ge.  0 .and. Intrvl(1,1) .le. 69)
!    *        Intrvl(1,1) = Intrvl(1,1)+2000
!        Endif
!
!  Convert start time to year and fraction, not worrying about leap years:
           xdoy1 = imdoy(Intrvl(2,1)) + Intrvl(3,1) + Intrvl(4,1)/24.d0 &
     &             + Intrvl(5,1)/1440.d0
           Xepoch = Intrvl(1,1) + xdoy1/365.D0
             Do ik = 1, Numsit_local
              X_frac = Xepoch - Sitxyzv(4,ik)
!              Turn off corrections if no position epoch or at the geocenter
                If (Sitxyzv(4,ik) .eq. 0.D0) X_frac = 0.D0
                If ( (DABS(Sitxyzv(1,ik)) .le. 1.D-6) .and. &
     &               (DABS(Sitxyzv(2,ik)) .le. 1.D-6) .and. &
     &               (DABS(Sitxyzv(3,ik)) .le. 1.D-6) )  X_frac = 0.D0
!               Make sure interpolation is over no more than 10 years!!
                 If (DABS(X_frac) .gt. 10.D0) Then
                  Write(6,147) ik, Xepoch, Sitxyzv(4,ik)
 147              Format(' Problem in SITI for Site #',I2, &
     &             ', Data epoch = ',F10.2, ', Site epoch = ',F10.2,/, &
     &             ' Maximum difference allowed is 10 years!! Check', &
     &             ' input codes SITERECV and INTRVAL4/INTERVAL. ')
                  CALL TERMINATE_CALC ( 'SITI  ', int2(0), int2(0))
                  STOP
                 Endif
!
               Do kj = 1, 3
!                   Add motion rounded to nearest 0.1 mm
                 Intmov = Sitxyzv(kj+4,ik)*X_frac*1.D4 + .49D0
                SITXYZ(kj,ik) = Sitxyzv(kj,ik) + Intmov/1.D4
               Enddo
             Enddo
! !  No site velocities, use old L-code
        Else
          CALL GET4 ('SITERECS      ',SITXYZ,int2(3),NUMSIT_LOCAL, &
     &         int2(1), NDO, KERR(5))
        Endif
!
!   Write out new/old site coordinates for Correlator capture
        CALL PUTR('SITEXYZS      ',SITXYZ,int2(3),Max_Stat,int2(1))
!
       Else
        CALL GET4 ('SITERECS      ',SITXYZ,int2(3),NUMSIT_LOCAL,int2(1), &
     &   NDO, KERR(5))
       Endif
!***********************************************
!
      ENDIF                                    ! Get site info
!
!  Check for a geocenter station and set flag if so
        Zero_site = 0
      Do I = 1, Numsit
        If ( (DABS(SITXYZ(1,I)) .le. 1.D-6) .and. &
     &       (DABS(SITXYZ(2,I)) .le. 1.D-6) .and. &
     &       (DABS(SITXYZ(3,I)) .le. 1.D-6) )  Then
          If (Zero_site .eq. 0) Then
           Zero_site = I
           Write(6,'("SITI: Goecenter site = site # ",I3)') Zero_site
          Else      ! More than one geocenter site! Not allowed!
           Write(6,'("SITBLK: More than 1 geocenter site! Quitting!")')
           CALL TERMINATE_CALC ( 'SITI  ', int2(0), int2(0))
          Endif
        Endif
      Enddo
!
!-----------------------------------------------------------------------------
!
!  Data base input or external file input for ocean loading?
      IF (Input_OCEAN) THEN        ! Database/external file ocean loading?
!
       CALL OCNIN(Kerr)
!
      ELSE                         ! Database/external file ocean loading?
!
       CALL GET4 ('SITOCAMP      ',SITOAM,int2(11),NUMSIT_LOCAL,int2(1), &
     &  NDI, KERR(7))
       CALL GET4 ('SITOCPHS      ',SITOPH,int2(11),NUMSIT_LOCAL,int2(1), &
     &  NDI, KERR(8))
       CALL GET4 ('SITHOCAM      ',SITHOA,int2(11),int2(2),NUMSIT_LOCAL, &
     &  NDI, KERR(9))
       CALL GET4 ('SITHOCPH      ',SITHOP,int2(11),int2(2),NUMSIT_LOCAL, &
     &  NDI, KERR(10))
       IF(KERR(9).NE.0  .OR. KERR(10).NE.0) THEN
         DO I = 1,11
           DO J = 1,2
             DO K = 1,NUMSIT_LOCAL
               SITHOA(I,J,K) = 0.D0
               SITHOP(I,J,K) = 0.D0
             ENDDO
           ENDDO
         ENDDO
        KERR(9)  = 0
        KERR(10) = 0
!
        If(KOCEC.ne.3) Then       !See if the user wants to go on.
          WRITE(6,'(///, &
     &    "WARNING: This database does not contain horizontal ocean loa" &
     &    ,"ding amplitudes and",/, &
     &    "phases.  Arrays for all sites zeroed out.",//, &
     &    "Continue (y/(n)) ?",$)')
          ITEST = 'N'
          READ(5,'(A)') ITEST
          IF(ITEST.ne.'Y' .and. ITEST.ne.'y') THEN
            KERR(9)  = 1
            KERR(10) = 1
          ENDIF
!
        Else         !If the ocean loading module control flag is 3, proceed!
          Write(6,'( &
     &    " Proceeding with null horizontal ocean loading catalog!")')
        Endif
       ENDIF
!
      ENDIF                        ! Database/external file ocean loading?
!
!-----------------------------------------------------------------------------
!
!  Data base input or external file input for antenna tilts?
      IF (Input_tilts) THEN                 ! Get tilt info from file
       CALL ANTILT(Krr)
         Kerr(11) = Krr
      ELSE                                  ! Get tilt info from database?
       CALL GET4 ('AXISTILT      ',Dbtilt,int2(2),NUMSIT_LOCAL,int2(1), &
     &  NDO, KERR(11))
       If (KERR(11) .ne. 0) Then
        Do I = 1, Max_stat
         Dbtilt(1,I) = 0.0
         Dbtilt(2,I) = 0.0
        Enddo
       Endif
!
      ENDIF
!
!  Compute topocentric rotation matrices for each antenna axis tilt.
       Do I = 1, NUMSIT_LOCAL
!
!           Alt-Az case:
        If (KTYPE(I) .eq. 3) Then
         Call ROTAT(-Dbtilt(1,I)*CONVD/60.D0, int2(3), T1)
         Call ROTAT( Dbtilt(2,I)*CONVD/60.D0, int2(2), T2)
         Call MMUL2(T1, T2, Rotilt(1,1,I))
        Endif
!
!           Equatorial, X/Y N-S, or Richmond case:
        If (KTYPE(I) .eq. 1 .or. KTYPE(I) .eq. 2 .or.     &
     &      KTYPE(I) .eq. 5) Then
         Call ROTAT( Dbtilt(1,I)*CONVD/60.D0, int2(1), T1)
         Call ROTAT(-Dbtilt(2,I)*CONVD/60.D0, int2(2), T2)
         Call MMUL2(T1, T2, Rotilt(1,1,I))
        Endif
!
!           X/Y E-W case:
        If (KTYPE(I) .eq. 4) Then
         Call ROTAT( Dbtilt(1,I)*CONVD/60.D0, int2(1), T1)
         Call ROTAT( Dbtilt(2,I)*CONVD/60.D0, int2(3), T2)
         Call MMUL2(T1, T2, Rotilt(1,1,I))
        Endif
!
       Enddo
!
!-----------------------------------------------------------------------------
!
!  Data base input or external file input for ocean pole tide loading?
      IF (Input_OPTL ) THEN        ! Database/external file ocean pole tide?
!
!*     Write(6,'(" Calling OPTLIN, external Ocean pole tide loading.")')
       CALL OPTLIN(Kerr)
!
      ELSE                         ! Database/external ocean pole tide?
!
       Write(6,'(" Getting Ocean pole tide loading from database.")')
       CALL GET4 ('OPTLCOEF      ',OPTL6,int2(6),NUMSIT_LOCAL,int2(1),  &
     &  NDI, KERR(7))
       IF(KERR(7).NE.0) THEN
! If no OPTLCOEF Lcode, try to get coefficients from the default file
        Write(6,'(" OPTLCOEF Lcode not found, trying default file.")')
         If (OPTL_file(1:4).ne.'None' .and. OPTL_file(1:4).ne.'NONE'    &
     &    .and. OPTL_file(1:4).ne.'none' .and.OPTL_file(1:4).ne.'    ') &
     &      Then
            Write(6,'(" Using default ocean pole tide file.")')
           Ex_OPTL = OPTL_file
           CALL OPTLIN(Kerr)
         Else 
            Write(6,'(" No default ocean pole tide file, using zeroes.")')
           DO I = 1,6
             DO K = 1,NUMSIT_LOCAL
               OPTL6(I,K) = 0.D0
             ENDDO
           ENDDO
          KERR(12) = 0
         Endif
!
!?      If(KOCEC.ne.3) Then       !See if the user wants to go on.
!?        WRITE(6,'(///, &
!?   &    "WARNING: This database does not contain horizontal ocean loa" &
!?   &    ,"ding amplitudes and",/, &
!?   &    "phases.  Arrays for all sites zeroed out.",//, &
!?   &    "Continue (y/(n)) ?",$)')
!?        ITEST = 'N'
!?        READ(5,'(A)') ITEST
!?        IF(ITEST.ne.'Y' .and. ITEST.ne.'y') THEN
!?          KERR(9)  = 1
!?          KERR(10) = 1
!?        ENDIF
!
       ENDIF
!
      ENDIF                        ! Database/external file ocean loading?
!
!-----------------------------------------------------------------------------
!
      IF (.Not. Input_sites) THEN
!
!     If only one site zenith path delay, copy for all stations.
       IF( NDO(1) .NE. NUMSIT ) THEN
         DO 210 N = 2,NUMSIT
 210       SITZEN(N) = SITZEN(1)
       ENDIF
!
!    Check for database interface errors. If an error is found, TERMINATE_CALC.
       DO N = 1,5
          NN = N
          IF (KERR(NN) .NE. 0) CALL TERMINATE_CALC('SITI  ',NN,KERR(NN))
       ENDDO
!
!     There may be only one site zenith atmosphere delay.
       IF( KERR(6) .NE. 0 .AND. KERR(6) .NE. 2 ) &
     &     CALL TERMINATE_CALC ('SITI  ',int2(6),KERR(6))
!
      ENDIF
!
!     Check for database interface errors _ ocean loading.
!
      DO 302 N = 7,10
        NN = N
        IF( KERR(N) .EQ. 0) GO TO 302
          CALL TERMINATE_CALC ('SITI  ',NN, KERR(NN))
 302  CONTINUE
!
!     Calculate the neccesary site geometry.
!      Mod added 98JAN22: Dummy out topocentric type variables for station at
!      or near the geocenter, for correlator usage.
!
!     Loop once for each station in the site catalog.
      DO 490  N = 1,NUMSIT
!
!       Compute the site spherical radii.
        CFRAD(N) = DSQRT ( SITXYZ(1,N)**2  +  SITXYZ(2,N)**2  + &
     &                     SITXYZ(3,N)**2  )
!
!   Check for geocenter
         If (Zero_site .eq. N) Go to 491
!
!   Compute geocentric latitudes
         GLAT(N) = DASIN( SITXYZ(3,N) / CFRAD(N) )
!
!   93OCT12. Call subroutine bkplh to compute
!    geodetic latitude, geodetic longitude, and height. DG
         pie = pi
         fl = eflat
         a = rearth
         do i=1,3
          xyz(i)=SITXYZ(i,N)
         enddo
        call bkplh(xyz,xlatlonht,pie,a,fl)
! keep longitudes between -PI and +PI
         if (xlatlonht(2) .gt. pi) &
     &       xlatlonht(2) = xlatlonht(2) - 2.D0*pi
         XLAT(N)  =  xlatlonht(1)
         XLON(N)  =  xlatlonht(2)
         Height(N) = xlatlonht(3)     ! height in meters
!
!       Compute the site normal unit vectors.
        SNRM(1,N) = DCOS ( XLAT(N) ) * DCOS ( XLON(N) )
        SNRM(2,N) = DCOS ( XLAT(N) ) * DSIN ( XLON(N) )
        SNRM(3,N) = DSIN ( XLAT(N) )
!
!       Compute the partial derivatives of the crust fixed site
!       coordinates with respect to the East longitudes.
        PLON(1,N) = - SITXYZ(2,N)
        PLON(2,N) =   SITXYZ(1,N)
        PLON(3,N) =   0.D0
!
!       Compute the partial derivatives of the crust fixed site
!       coordinates with respect to the geodetic latitudes.
!       (NOTE: The following equations are actually for the geocentric partial
!       derivatives, however, these partials are sufficiently close to the
!       geodetic partials for the purposes of CALC use.)
        PLAT(1,N) = - SITXYZ(3,N) * DCOS (XLON(N) )
        PLAT(2,N) = - SITXYZ(3,N) * DSIN (XLON(N) )
        PLAT(3,N) = + CFRAD(N) * DCOS (XLAT(N) )
!
!     Compute the topocentric-to-crust-fixed rotation matrices by rotating
!     about the geodetic latitude and the longitude. Also now compute a
!     "radial-transverse" rotation matrix by rotating about the geocentric
!     latitude and the longitude.
!
        XLAT_DUMMY = XLAT(N)
        CALL ROTAT ( XLAT_DUMMY, int2(2), RY)
!      write(6,8) ' XLAT? ',  xlat(n)*57.29578
!
        XLON_DUMMY = XLON(N)
        CALL ROTAT ( -XLON_DUMMY, int2(3), RZ)
!      write(6,8) ' XLON? ',  xlon(n)*57.29578
!
        GLAT_DUMMY = GLAT(N)
        CALL ROTAT ( GLAT_DUMMY, int2(2), RGY)
!      write(6,8) ' GLAT? ',  glat(n)*57.29578
!
!       DO I=1,3
!         DO J=1,3
!           TCROT_DUMMY(I,J) = TCROT(I,J,N)
!           RTROT_DUMMY(I,J) = RTROT(I,J,N)
!         ENDDO
!       ENDDO
        CALL MMUL2 ( RZ, RY, TCROT_DUMMY(1,1) )
        CALL MMUL2 ( RZ, RGY,RTROT_DUMMY(1,1) )
        DO I=1,3
          DO J=1,3
            TCROT(I,J,N) = TCROT_DUMMY(I,J)
            RTROT(I,J,N) = RTROT_DUMMY(I,J)
          ENDDO
        ENDDO
!      write(6,8) ' TCROT ',  TCROT_DUMMY
!      write(6,8) ' RTROT ',  RTROT_DUMMY
!
      IF (KSITD .ne. 0) Then  !Station debug printout
       if (N.eq.1) Then
        WRITE ( 6, 1)
        WRITE(6,8)' EFLAT   ',EFLAT
        WRITE(6,8)' REARTH  ',REARTH
        WRITE(6,7)' NUMSIT  ',NUMSIT
       endif
    1  FORMAT (1X, 'Debug output for subroutine SITI.' )
       write(6,'(" For site #",i2)') N
       WRITE(6,4)' RY   ',((RY(J,K),J=1,3),K=1,3)
       WRITE(6,4)' RZ   ',((RZ(J,K),J=1,3),K=1,3)
       WRITE(6,4)' RGY  ',((RGY(J,K),J=1,3),K=1,3)
       WRITE (6,8)' Geoid Height  ',  xlatlonht(3)
      Endif          !Station debug printout
      GO TO 490
!
  491 CONTINUE
!    Dummy out the above topocentric quantities, they have no meaning at the
!     geocenter
        XLAT(N)   = -999.D0
        XLON(N)   = -999.D0
        Height(N) = -999.D0
        SNRM(1,N) = 0.D0
        SNRM(2,N) = 0.D0
        SNRM(3,N) = 0.D0
        GLAT(N)   = -999.D0
        PLON(1,N) = 0.D0
        PLON(2,N) = 0.D0
        PLON(3,N) = 0.D0
        PLAT(1,N) = 0.D0
        PLAT(2,N) = 0.D0
        PLAT(3,N) = 0.D0
        SITAXO(N) = 0.D0
        KTYPE(N)  = 0
        SITZEN(N) = 0.D0
        DO I=1,3
         DO J=1,3
          TCROT(I,J,N) = 0.D0
          RTROT(I,J,N) = 0.D0
         ENDDO
        ENDDO
        DO I=1,11
          SITOAM(I,N) = 0.D0
          SITHOA(I,1,N) = 0.D0
          SITHOA(I,2,N) = 0.D0
          SITOPH(I,N) = 0.D0
          SITHOP(I,1,N) = 0.D0
          SITHOP(I,2,N) = 0.D0
        ENDDO
!
!     Close the loop which runs over the sites in the catalog.
  490 CONTINUE
!
!     Initialize the integer variable NLAST to zero.
      NLAST(1) = 0
      NLAST(2) = 0
!
!     Check KSITD for debug output.
      IF ( KSITD .ne. 0 ) Then  !Debug printout
!     WRITE ( 6, 11)
   11 FORMAT (1X, 'Station Debug for subroutine SITI.' )
      WRITE(6,8)' CFRAD   ',(CFRAD(J),J=1,NUMSIT)
    8 FORMAT(A,4D25.16/(7X,5D25.16))
      WRITE(6,7)' KTYPE   ',(KTYPE(J),J=1,NUMSIT)
    7 FORMAT(/,A,15I8/(7X,15I8))
      WRITE(6,7)' NLAST   ',NLAST
      WRITE(6,4)' PLAT    ',(( PLAT(J,K),J=1,3),K=1,NUMSIT)
    4 FORMAT(/,A,3D25.16/(9X,3D25.16))
      WRITE(6,4)' PLON    ',(( PLON(J,K),J=1,3),K=1,NUMSIT)
      WRITE(6,8)' SITAXO  ',( SITAXO(J),J=1,NUMSIT)
      WRITE(6,9)' SITOAM, ',((SITOAM(J,K),J=1,11),K=1,NUMSIT)
      WRITE(6,9)' SITOPH, ',((SITOPH(J,K),J=1,11),K=1,NUMSIT)
      WRITE(6,9)' SITHOA, ',(((SITHOA(J,L,K),J=1,11),L=1,2),K=1, &
     &NUMSIT)
      WRITE(6,9)' SITHOP, ',(((SITHOP(J,L,K),J=1,11),L=1,2),K=1, &
     &NUMSIT)
    9 FORMAT(/,A,11F9.4,/,(9X,11F9.4))
      WRITE(6,6)' SITXYZ  ',((SITXYZ(J,K),J=1,3),K=1,NUMSIT)
    6 FORMAT(/,A,3F20.4,/,(9X,3F20.4))
      WRITE(6,8)' SITZEN  ',(SITZEN(K),K=1,NUMSIT)
      WRITE(6,4)' SNRM    ',((SNRM(I,J),I=1,3),J=1,NUMSIT)
      WRITE(6,4)' TCROT   ',(((TCROT(I,J,K),I=1,3),J=1,3),K=1, &
     &NUMSIT)
    5 FORMAT(/,A,/,3(3F20.4,/)/)
      WRITE(6,8)' XLAT    ',(XLAT(J),J=1,NUMSIT)
      WRITE(6,8)' XLON    ',(XLON(J),J=1,NUMSIT)
      WRITE(6,8)' HEIGHT    ',(HEIGHT(J),J=1,NUMSIT)
      WRITE(6,3)' LNSITE  ',((LNSITE(J,K),J=1,4),K=1,NUMSIT)
    3 FORMAT (/,A,4A2,/, 9X,4A2/)
       Do I = 1, NUMSIT_LOCAL
         Write(6,1012) I, Dbtilt(1,I), Dbtilt(2,I),          & 
     &     ((Rotilt(kj,ik,I), ik=1,3), kj=1,3)
 1012    Format('Station #',I2,2x,2F10.5,/,'Rotilt: ',3F20.10,   &
     &          /,8X,3F20.10,/,8X,3F20.10)
       Enddo
!
!
      Endif          !Debug printout
!
!     Normal conclusion.
      RETURN
      END
!
!*************************************************************************
      SUBROUTINE PLXI()
      IMPLICIT None
!
! 3.1.1 PLXI IS THE PARALLAX MODULE INPUT AND INITIALIZATION SECTION.
!
! 3.2   PLXI PROGRAM INTERFACE
!
! 3.2.2 COMMON BLOCKS USED -
      INCLUDE 'ccon.i'
!           VARIABLES 'FROM':
!             1.  KPLXC  -  THE PARALLAX MODULE FLOW CONTROL FLAG.
!                           0 => Do not compute parallax contributions
!                           1 => DO compute parallax contributionsns
!             2.  KPLXD  -  THE PARALLAX MODULE DEBUG OUTPUT FLAG.
!
! 3.2.3 PROGRAM SPECIFICATIONS -
      INTEGER*2  NDO(3), idm7
      INTEGER*2      LON(40),    LOFF(40),     LPLXM(40)
      CHARACTER*40 C_LON(2) ,  C_LOFF(2) ,   C_LPLXM(2)
      EQUIVALENCE( C_LON,LON),(C_LOFF,LOFF),(C_LPLXM,LPLXM)
!
      DATA C_LPLXM  / &
     &'Parallax Module, Last modified 98SEP11, ', &
     &'D. Gordon/GSFC                          '/
!
      DATA C_LON  / &
     &'Parallax Contributions OFF.             ', &
     &'                                        '/
!
      DATA C_LOFF / &
     &'Parallax Contributions ON.              ', &
     &'                                        '/
!
! 3.2.4 DATA BASE ACCESS -
!           'GET' VARIABLES: none
!           'PUT' VARIABLES:
!             1. LPLXM(40)  -  THE PARALLAX MODULE TEXT MESSAGE.
!             2. LON(40)    -  THE PARALLAX MODULE 'TURNED ON' MESSAGE.
!             3. LOFF(40)   -  THE PARALLAX MODULE 'TURNED OFF' MESSAGE.
!           ACCESS CODES:
!             1. 'PLX MESS' -  THE DATA BASE ACCESS CODE FOR THE PARALLAX
!                              MODULE TEXT MESSAGE.
!             2. 'PLX CFLG' -  THE DATA BASE ACCES CODE FOR THE PARALLAX
!                              MODULE FLOW CONTROL MESSAGE.
!
! 3.2.6 SUBROUTINE INTERFACE -
!             CALLER SUBROUTINES: INITL
!             CALLED SUBROUTINES: PUTA
!
! 3.2.9 PROGRAMMER - C. A. KNIGHT  02/26/80
!                    89.08.15 Jim Ryan Documentation simplified.
!                    Jim Ryan 89.12.12 UNIX-like database interface
!                    implimented.
!                    David Gordon 94.04.14 Converted to Implicit None.
!                    David Gordon 98.09.11 Updated data base text messages.
!                    Jim Ryan Sept 2002 Integer*2/4 mods.
!
!    PLXI PROGRAM STRUCTURE
!
!   PUT the module text message.
      CALL PUTA ('PLX MESS      ',LPLXM,int2(40),int2(1),int2(1))
!
!   PUT the module flow control message.
      IF (KPLXC .EQ. 0) CALL PUTA('PLX CFLG      ', LON, int2(40), &
     &    int2(1), int2(1))
      IF (KPLXC .NE. 0) CALL PUTA('PLX CFLG      ', LOFF, int2(40), &
     &    int2(1), int2(1))
!
  500 RETURN
      END
!
!************************************************************************
      SUBROUTINE ETDI()
      IMPLICIT None
!
!     ETDI is the Earth Tide Module input and initialization section.
!
!     ETDI Program Interface:
!
!     Common blocks used:
!
      INCLUDE 'ccon.i'
!         Variables 'from':
!           1. KETDC  -  The module flow control flag.
!                        = 0 => Default, IERS 1996 Earth Tide Model
!                        = 1 => Earth tide model OFF, no corrections computed
!             2. KETDD - Module debug output control flag.
!
!   Program specifications:
!     INTEGER*2 NDO(3), KERR
      INTEGER*2      LETDM(40),  New_ON(40),  LOFF(40)
      CHARACTER*40 C_LETDM(2), C_New_ON(2), C_LOFF(2)
      EQUIVALENCE (C_LETDM,LETDM), (C_New_ON,New_ON), (C_LOFF,LOFF)
!
      DATA C_LETDM &
     &   /'Earth Tide Module - IERS 2003 Model, Las', &
     &    't Modified 2004.03.26, D. Gordon/GSFC.  '/
      DATA C_New_ON &                                     ! KETDC=0
     &   /'IERS 2003 Earth Tide Model              ', &
     &    '                                        '/
      DATA C_LOFF &                                       ! KETDC=1
     &   /'Earth tide module is turned off.        ', &
     &    '                                        '/
!
!     Database access:
!          'PUT' variables:
!             1. LETDM(40)   - The Earth Tide Module text message.
!             2. New_ON(40)  - The 2003 IERS Earth tide model.
!             3. LOFF(40)    - The Earth tides completely turned OFF message.
!          Access codes:
!             1. 'ETD MESS' - The database access code for the Earth Tide
!                             Module text message.
!             2. 'ETD DATA' - The database access code for the Earth Tide data.
!             3. 'ETD CFLG' - The database access code for the Earth Tide
!                             Module flow control message.
!
!     Subroutine Interface:
!          Caller subroutines: INITL
!          Called subroutines: GET4, TERMINATE_CALC, PUTA
!
!     Program variables:
!           1. KERR   - The database error return flag.
!           2. NDO(3) - The database return error indices.
!
!     Programmer   - DALE MARKHAM  01/13/77
!                    PETER DENATALE 07/12/77
!                    BRUCE SCHUPLER 03/24/78
!                    BRUCE SCHUPLER 11/02/78
!                    DAVID GORDON   07/31/84
!                    Jim Ryan 89.06.29 Character strings and clean up comments.
!                    Jim Ryan 89.12.12 UNIX-like database interface
!                                 implimented.
!                    David Gordon 93.11.19 New database access messages for
!                                 IERS Earth tide computation.
!                    David Gordon 94.01.24 Database access codes modified for
!                                 third order Earth tide terms.
!                    David Gordon 94.04.06 Changed to 'Implicit None'
!                    David Gordon 94.04.27 New common block 'tide_stuff';
!                                 XLOVEH_IERS and XLOVEL_IERS defined as h2 and
!                                 l2 from database (which will be the IERS
!                                 Standards in Calc 8.0).
!                    David Gordon 94.07.12 Database header messsages revised,
!                                 message variable names changed.
!                    David Gordon 94.08.08 Love numbers changed, according to
!                                 July 28, 1994 letter from Sonny Mathews to
!                                 John Gipson.
!                    David Gordon 94.08.09 Love numbers changed again per Sonny
!                                 Mathews communication to John Gipson.
!                    David Gordon 94.08.24 Changing back to IERS model as the
!                                 default. Messages updated. XLOVEH and XLOVEL
!                                 reset to IERS values. XLOVEH_alt and
!                                 XLOVEL_alt for Mathews Love numbers.
!                    David Gordon 96.02.07 Changing XLOVEH_alt and XLOVEL_alt
!                                 to 0.6081 and .0845 per Feb. 6, 1996 John
!                                 Gipson memo.
!                    David Gordon 98.06.24 Extensive mods in Calc 9.0 for
!                                 IERS 1996 Earth tide model. Redefined KETDC
!                                 options.
!                    David Gordon 98.10.01 Remove common block ETDCM. Remove
!                                 GET of 'ETD DATA' - no longer needed.
!                    Jim Ryan     2002.09 Integer*4 conversion
!                    David Gordon 2004 IERS (2003) updates.
!
!  ETDI Program Structure.
!
!   PUT Earth tide module text message into database.
      CALL PUTA ('ETD MESS      ',LETDM,int2(40),int2(1),int2(1))
!
!   PUT the flow control text message depending on KETDC.
      IF (KETDC .EQ. 0) CALL PUTA ('ETD CFLG      ',New_ON,int2(40), &
     &  int2(1), int2(1))
      IF (KETDC .EQ. 1) CALL PUTA ('ETD CFLG      ',LOFF,int2(40), &
     &  int2(1), int2(1))
!
!   Normal conclusion.
      RETURN
      END
!
!************************************************************************
      SUBROUTINE PTDI()
      IMPLICIT None
!
! 3.    PTDI
!
! 3.1   PTDI PROGRAM SPECIFICATION
!
! 3.1.1 PTDI IS THE POLE TIDE MODULE INPUT AND INITIALIZATION SECTION.
!
! 3.2.2 COMMON BLOCKS USED -
      INCLUDE 'ccon.i'
!            VARIABLES 'FROM':
!              1.  KPTDC  -  THE POLE TIDE MODULE FLOW CONTROL FLAG.
!              2.  KPTDD  -  THE POLE TIDE MODULE DEBUG OUTPUT FLAG.
!
! 3.2.3 PROGRAM SPECIFICATIONS -
      INTEGER*2  NDO(3), KERR
      SAVE KERR
      INTEGER*2      LPTDM(40),      LON(40),    LOFF(40),     LOXX(40)
      CHARACTER*40 C_LPTDM(2)     ,C_LON(2)   ,C_LOFF(2)   ,C_LOXX(2)
      EQUIVALENCE (C_LPTDM,LPTDM),(C_LON,LON),(C_LOFF,LOFF), &
     &            (C_LOXX ,LOXX)
!
      DATA C_LPTDM / &
     &'Pole Tide Module - Last Modified 2012.11',                       &
     &'.30, D. Gordon/GSFC.                    '/
!
      DATA C_LON / &
     &'Pole Tide Module is turned on - contribu',                       &
     &'tions applied to the theoreticals.      '/
!
      DATA C_LOFF / &
     &'Pole Tide Module is turned off.         ',                       &
     &'                                        '/
!
      DATA C_LOXX / &
     &'Pole Tide Module is turned on - contribu',                       &
     &'tions NOT applied to theoreticals.      '/
!
! 3.2.4 DATA BASE ACCESS -
!            'PUT' VARIABLES:
!              1.  LPTDM(40)  -  THE POLE TIDE MODULE TEXT MESSAGE.
!              2.  LON(40)    -  THE POLE TIDE MODULE TURNED ON MESSAGE.
!              3.  LOFF(40)   -  THE POLE TIDE MODULE TURNED OFF MESSAGE.
!            ACCESS CODES:
!              1.  'PTD MESS'  -  THE DATA BASE ACCESS CODE FOR THE
!                                 POLE TIDE MODULE TEXT MESSAGE.
!              2.  'PTD DATA'  -  THE DATA BASE ACCESS CODE FOR THE
!                                 POLE TIDE DATA.
!              3.  'PTD CFLG'  -  THE DATA BASE ACCESS CODE FOR THE POLE
!                                 TIDE MODULE FLOW CONTROL MESSAGE.
!
! 3.2.6 SUBROUTINE INTERFACE -
!             CALLER SUBROUTINES: INITL
!             CALLED SUBROUTINES: TERMINATE_CALC, PUTA
!
! 3.2.8 PROGRAM VARIABLES -
!           1.  KERR   -  THE DATA BASE ERROR RETURN FLAG.
!           2.  NDO(3) -  THE DATA BASE RETURN ARRAY INDICES.
!
! 3.2.9 PROGRAMMER - TOM HERRING   07/01/84
!                    DAVID GORDON  01/03/85 (ADDED FLAG 2)
!                    Jim Ryan      89.07.08 Documentation simplified.
!                    Jim Ryan      89.12.12 UNIX-like database interface
!                                  implimented.
!                    David Gordon  94.04.15 Changed to Implicit None
!                    David Gordon  98.08.04 Changed data base message for
!                                  Calc 9.
!                    Jim Ryan 2002.09 Interger*4 mods.
!                    Jim Ryan 03.03.10 Kill replaced with terminate_calc.
!
!     PTDI PROGRAM STRUCTURE
!
!     PUT the Pole Tide Module text message.
      CALL PUTA ('PTD MESS      ',LPTDM, int2(40),int2(1),int2(1))
!
!     PUT the Pole Tide Module flow control message. See message above
!     for meanings.
      IF (KPTDC .EQ. 0) CALL PUTA('PTD CFLG      ',LON,int2(40),        &
     &    int2(1), int2(1))
      IF (KPTDC .EQ. 1) CALL PUTA('PTD CFLG      ',LOFF,int2(40),       &
     &    int2(1), int2(1))
      IF (KPTDC .EQ. 2) CALL PUTA('PTD CFLG      ',LOXX,int2(40),       &
     &    int2(1), int2(1))
      IF ( KERR .EQ. 0 ) GO TO 500
        CALL TERMINATE_CALC ('PTDI  ', int2(1), KERR)
!
!     Normal conclusion.
  500 CONTINUE
      RETURN
      END
!
!*********************************************************************
      SUBROUTINE OCEI()
      IMPLICIT None
!
!     OCEI is the ocean loading module input and initialization section.
!
!     OCEI Program Interface
!
!       Common Blocks used -
!
      INCLUDE 'ccon.i'
!       VARIABLES 'FROM':
!         1. KOCEC - THE OCEAN LOADING MODULE FLOW CONTROL FLAG.
!
      INCLUDE 'cuser11.i'
!       Variables from:
!         1. Calc_user   - Calc user type. 'A' for Mark III/SOLVE analysis.
!                          'C' for VLBI correlator.
!         2. Apply_ocean - Switch to apply ocean loading to theoreticals
!                          for correlator usage. 'Y' to apply (recommended),
!                          'N' for do not apply.
!
! 3.2.3 PROGRAM SPECIFICATIONS -
!
      INTEGER*2      LOCEM(40),  LON(40),  LOFF(40),  LOX(40),  LHOR(40)
      CHARACTER*40 C_LOCEM(2), C_LON(2), C_LOFF(2), C_LOX(2), C_LHOR(2)
      EQUIVALENCE (LOCEM,C_LOCEM),(LON,C_LON),(LOFF,C_LOFF), &
     &            (LOX,C_LOX), (LHOR,C_LHOR)
!
! 3.2.4 DATA BASE ACCESS -
!
!            'PUT' VARIABLES:
!              1. LOCEM(40)  -  THE OCEAN LOADING MODULE TEXT MESSAGE.
!              2. LON(40)    -  THE OCEAN LOADING MODULE TURNED ON MESSAGE.
!              3. LOFF(40)   -  THE OCEAN LOADING MODULE TURNED OFF MESSAGE.
!              4. LOX(40)    -  THE OCEAN LOADING SPECIAL SETUP MESSAGE.
!              5. LHOR(40)   -  "PROCEED EVEN WITHOUT HORIZONTAL CAT" MESSAGE.
!
!            ACCESS CODES:
!              1. 'OCE MESS' -  THE DATA BASE ACCESS CODE FOR THE OCEAN LOADING
!                               MODULE TEXT MESSAGE.
!              2. 'OCE CFLG' -  THE DATA BASE ACCESS CODE FOR THE OCEAN LOADING
!                               MODULE FLOW CONTROL MESSAGE.
!
! 3.2.6 SUBROUTINE INTERFACE -
!              CALLER SUBROUTINE: INITL
!              CALLED SUBROUTINE: PUTA
!
! 3.2.9 PROGRAMMER - HAROLD SCHUH 10/08/83
!                    JIM RYAN      6/20/84 (MODIFIED TEXT MESSAGE)
!                    Greg Cook     6/29/89 Inserted horizontal code
!                    JIM RYAN      6/29/89 (MODIFIED TEXT MESSAGE)
!                    Jim Ryan 89.12.12 UNIX-like database interface
!                                  implimented.
!                    David Gordon 94.04.18 Converted to Implicit None.
!                    David Gordon 98.10.16 Added 'cuser.i' include file
!                                 and code to use ocean loading included
!                                 message for correlator users.
!                    Jim Ryan Sept 2002 Integer*2/4 mods.
!
      DATA C_LOCEM  / &
     &'Ocean loading module - Version 3. Last m',                       &
     &'odified 2012.11.30 by D. Gordon, GSFC   '/
!
      DATA C_LON    / &
     &'Ocean loading module is turned on - cont',                       &
     &'ributions not applied to theoretical.   '/
!
      DATA C_LOFF   / &
     &'Ocean loading module is turned off.     ',                       &
     &'                                        '/
      DATA C_LOX    / &
     &'Ocean loading special setup - contributi',                       &
     &'ons are applied to the theoreticals.    '/
!
      DATA C_LHOR   / &
     &'Ocean loading proceeding even if horizon',                       &
     &'tal catalog missing. Contributions only.'/
!
!   Call 'PUTA' to place the ocean loading module text message into db.
      CALL PUTA ('OCE MESS      ',LOCEM,int2(40),int2(1),int2(1))
!
!   Check that the flow control variable has a legitimate value.
      IF(KOCEC.LT.0 .OR. KOCEC.GT.3) CALL TERMINATE_CALC('OCEI  ',      &
     &       int2(1), int2(1))
!
      IF (KOCEC.EQ.2 .or. (Calc_user.eq.'C' .and. Apply_ocean.eq.'Y'))  &
     &   Then
          CALL PUTA ('OCE CFLG      ',LOX,int2(40),int2(1),int2(1))
          Go to 112
      ENDIF
      IF (KOCEC .EQ. 0) CALL PUTA ('OCE CFLG      ',LON,int2(40),       &
     &    int2(1), int2(1))
      IF (KOCEC .EQ. 1) CALL PUTA ('OCE CFLG      ',LOFF,int2(40),      &
     &    int2(1), int2(1))
      IF (KOCEC .EQ. 3) CALL PUTA ('OCE CFLG      ',LHOR,int2(40),      &
     &    int2(1), int2(1))
 112   Continue
!
      RETURN
      END
!
!*********************************************************************
      SUBROUTINE STRI()
      IMPLICIT None
!
! 3.1.1 STRI is the STAR Module input and initialization section.
!
! 3.2.2 COMMON BLOCKS USED -
!
      INCLUDE 'cmxsr11.i'
!           VARIABLES 'TO':
!             1. LNSTAR(4,MAX_ARC_SRC) - THE EIGHT ALPHANUMERIC CHARACTER NAMES
!                                        OF THE STARS IN THE STAR CATALOG.
!             2. NUMSTR                - THE NUMBER OF STARS IN THE STAR
!                                        CATALOG.
!             3. RADEC(2,MAX_ARC_SRC)  - THE RIGHT ASCENSIONS AND DECLINATIONS
!                                        OF THE STARS IN THE STAR CATALOG.
!                                        (RAD, RAD)
!             4. P_motion(3,Max_arc_src)-The RA and Dec proper motions and
!                                        appropriate epoch for stars in the
!                                        star catalog. (arc-sec/yr, arc-sec/yr,
!                                        year (i.e. 1995.5, etc.))
!             5. D_psec(Max_arc_src)   - Distances, if known, for stars in the
!                                        star catalog. Zero => unknown.
!                                        (parsecs)
!             6. PRcorr(2,Max_arc_src) - Proper motion corrections in RA and
!                                        Declination for each star. (Radians)
!
      INCLUDE 'ccon.i'
!            VARIABLES 'FROM':
!             1. KSTRC  -  THE STAR MODULE FLOW CONTROL FLAG.
!                          0 => Do not add proper motion constants to data
!                               base when in external file mode (Lcode
!                               'PRMOTION'). Do not compute any proper
!                               motion contributions. Remove any previous
!                               proper motion contribution Lcodes.
!                          1 => Add proper motion constants ('PRMOTION') if
!                               in external file input mode. Compute proper
!                               motion contributions if proper motion
!                               constants are available and insert in data
!                               base under Lcode 'PMOTNCON'. Do NOT add
!                               these contributions to the theoretical.
!                               [They can be ADDED in SOLVE to get proper
!                               motion corrected delays and rates.]
!                               The proper motion RA and Delination offsets,
!                               in radians, will be stored for each source in
!                               the type 1 Lcode 'RADECADD'.
!                          2 => Add proper motion constants ('PRMOTION') if
!                               in external file input mode. Also, ADD the
!                               proper motions to source vector and use the
!                               proper motion corrected source vector
!                               throughout all computations. Compute a
!                               contribution (ADD to theoretical) that will
!                               (approximately) return the delays and rates
!                               to their non-proper motion corrected values,
!                               and put in Lcode 'PMOT2CON'. For cases where
!                               there is a large accumulated proper motion
!                               (greater than ~1 arcsec). Intended for
!                               correlator useage only. USE WITH CAUTION!!
!                               The RA's and Dec's for each star, AFTER
!                               correcting for proper motion offsets, are
!                               output in the type 1 Lcode 'STARPRMO'. These
!                               should be captured and used in any post
!                               processing adjustments, etc.
!             2. KSTRD  -  THE STAR MODULE DEBUG OUTPUT FLAG.
!             3. KPLXC  -  THE PARALLAX MODULE FLOW CONTROL FLAG.
!                          0 => Do not insert source distances into data
!                               base when in external file input mode, even
!                               if distances given in external source file.
!                          1 => Insert source distances into data base in
!                               external file input mode, even if all zeros,
!                               using Lcode 'DISTPSEC'.
!
      Real*8         PI, TWOPI, HALFPI, CONVD, CONVDS, CONVHS, SECDAY
      COMMON /CMATH/ PI, TWOPI, HALFPI, CONVD, CONVDS, CONVHS, SECDAY
!         CONVDS - THE CONVERSION FACTOR FROM ARCSECONDS TO RADIANS
!                 (RAD/ARCSECOND)
!
      INCLUDE 'input11.i'
!            Variables from:
!              1. Input_stars - T/F logical flag telling whether to use star
!                               a priori external input
!              2. Ex_stars    - File name for stars external file input.
!                               If 'NONE' or blank, then no external inputs
      INCLUDE 'cuser11.i'
!            Variables from:
!              1. Near_field - Logical*4 flag telling whether there are
!                              near-field sources in the database or not.
!                              Default is '.False.' This is for future use.
!
! 3.2.3 PROGRAM SPECIFICATIONS -
!
      Real*8    Xdoy1, Xepoch, X_frac
      Integer*4 I, J, N, NN, Imdoy(12)
      INTEGER*2 KERR(5), LSTRM(40), LOFF(4), LON1(40),LON2(40),NDO(3), &
     &          Intrvl(5,2), KERX, idum2
      CHARACTER*40 C_LSTRM(2), C_OFF(2), C_ON1(2), C_ON2(2)
      EQUIVALENCE (C_LSTRM,LSTRM), (C_OFF,LOFF), (C_ON1,LON1), &
     &            (C_ON2,LON2)
!
      Data Imdoy /0,31,59,90,120,151,181,212,243,273,304,334/
!
      DATA C_LSTRM / 'Star Module - Last modification 98.09.15', &
     &               ', D. Gordon, GSFC                       '/
!
      DATA C_OFF / 'Proper Motion Corrections OFF.          ', &
     &             '                                        '/
!
      DATA C_ON1 / 'Proper Motion Corrections ON, but NOT in', &
     &             'cluded in theoreticals.                 '/
!
      DATA C_ON2 / 'Proper Motion Corrections ON, AND includ', &
     &             'ed in theoreticals.                     '/
!
! 3.2.4 DATA BASE ACCESS -
!          'GET' VARIABLES:
!             1. LNSTAR(4,MAX_ARC_SRC) - THE EIGHT ALPHANUMMERIC CHARACTER
!                                        NAMES OF THE STARS IN THE STAR CATALOG
!                                        (ALPHANUMERIC).
!             2. NUMSTR                - THE NUMBER OF STARS IN THE STAR
!                                        CATALOG.
!             3. RADEC(2,MAX_ARC_SRC)  - THE RIGHT ASCENSIONS AND DECLINATIONS
!                                        OF THE STARS IN THE STAR CATALOG
!                                        (RAD, RAD).
!           'PUT' VARIABLES:
!             1. LSTRM(40)  -  THE STAR MODULE TEXT MESSAGE.
!
!           ACCESS CODES:
!             1. 'STR MESS' - THE DATA BASE ACCESS CODE FOR THE STAR MODULE
!                             TEXT MESSAGE.
!             2. 'STAR2000' - THE DATA BASE ACCESS CODE FOR THE ARRAY OF STAR
!                             RIGHT ASCENSIONS AND DECLINATIONS IN J2000.0
!                             COORDINATES.
!             3. 'STRNAMES' - THE DATA BASE ACCESS CODE FOR THE ARRAY OF STAR
!                             NAMES.
!             4. '# STARS ' - DATA BASE ACCESS CODE FOR THE NUMBER OF STARS.
!             5. 'PRMOTION' - The data base access code for the proper
!                             motion array. First variable runs over
!                             RA velocity (arc-sec/year), Declination
!                             velocity (arc-sec/year), and epoch for
!                             which the J2000 coordinates in 'STAR2000'
!                             precessed to date are correct (epoch for
!                             which corrections should be zero). Second
!                             index runs over the sources, same order as
!                             in 'STRNAMES'. Zeros imply unknown.
!             6. 'DISTPSEC' - The data base access code for the source
!                             distance array. Units are parsecs; zero
!                             implies unknown.
!             7. 'RADECADD' - Proper motion offsets (if proper motion turned
!                             on) in RA and Dec. Add to RA and Dec to get
!                             corrected values. Used if KSTRC = 1.
!             8. 'STARPRMO' - RA and Declinations after correcting for proper
!                             motion. Used if KSTRC = 2.
!
! 3.2.6 SUBROUTINE INTERFACE -
!             CALLER SUBROUTINES: INITL
!             CALLED SUBROUTINES: GETA, GETI, GET4, TERMINATE_CALC, PUTA, STRIN
!
! 3.2.8 PROGRAM VARIABLES -
!             1.  KERR(3) - THE DATA BASE ERROR RETURN FLAGS.
!             2.  NDO(3)  - THE DATA BASE RETURN ARRAY INDICES.
!
! 3.2.9 PROGRAMMER - DALE MARKHAM   01/13/77
!                    PETER DENATALE 07/14/77
!                    CHOPO MA       08/05/81
!                    Jim Ryan 89.07.09 Documentation simplied.
!                    Jim Ryan 89.12.12 UNIX-like database interface
!                        implimented.
!                    David Gordon 94.04.15 Converted to Implicit None
!                    B. Archinal  95.11.13 Max # sources set in cmxsr.i.
!                    David Gordon 98.03.19 Mods for external file input of
!                        source coordinates
!                    David Gordon 98.09.15 Mods for proper motions and
!                        distances.
!                    David Gordon 98.11.25 Moved proper motion source offset
!                        computation here so that the RA/Dec offsets or
!                        corrected RA/Dec's could be output for capture by
!                        correlator users. Added Lcodes 'RADECADD' and
!                        'STARPRMO'.
!                    Jim Ryan Sept 2002 Integer*2/4 mods.
!
!     STRI PROGRAM STRUCTURE
!
!   PUT the Star Module text message.
      CALL PUTA ( 'STR MESS      ', LSTRM, int2(40), int2(1),int2(1))
!   PUT the module flow control message.
      IF (KSTRC .EQ. 0) CALL PUTA('STR CFLG      ', LOFF, int2(40), &
     &                  int2(1), int2(1))
      IF (KSTRC .EQ. 1) CALL PUTA('STR CFLG      ', LON1, int2(40), &
     &                  int2(1), int2(1))
      IF (KSTRC .EQ. 2) CALL PUTA('STR CFLG      ', LON2, int2(40), &
     &                  int2(1), int2(1))
!
! Set Near_field flag to 'false'.
       Near_field = .False.
!
!   GET the star catalog from the input database.
      CALL GETI('# STARS       ',NUMSTR,int2(1),int2(1),int2(1),NDO, &
     & KERR(1))
      CALL GETA('STRNAMES      ',LNSTAR,int2(4),int2((NUMSTR)),int2(1), &
     & NDO, KERR(2))
!
       Pmotion = 0
       Dpsec   = 0
!  Get source positions either from the data base or from an external file
      IF (Input_stars) Then                         !Get Star Info
        Call STRIN(Kerr)
      ELSE                                          !Get Star Info
        CALL GET4('STAR2000      ',RADEC,int2(2),NUMSTR,int2(1),NDO, &
     &   KERR(3))
!
!  Also get proper motions, if they are in the data base
       IF(KSTRC.eq.1 .or. KSTRC.eq.2) Then          !Get proper motions
        CALL GET4('PRMOTION      ', P_motion, int2(3), NUMSTR, &
     &       int2(1), NDO, KERR(4))
        If (Kerr(4) .eq. 0) Then
!   Lcode exists, but check that values are not all zero
         Do N=1,Numstr
          If((P_motion(3,N).ge.1900.D0) .AND. &
     &     (DABS(P_motion(1,N)) .gt. 1.D-12  .or. &
     &      DABS(P_motion(2,N)) .gt. 1.D-12)) Pmotion = Pmotion + 1
         Enddo
        Else
!  No Lcode, therefore no proper motions, zero out the array.
         Do N=1,Numstr
          P_motion(1,N) = 0.D0
          P_motion(2,N) = 0.D0
          P_motion(3,N) = 0.D0
         Enddo
          Pmotion = 0
        Endif
       ENDIF                                        !Get proper motions
!
!  Also get distances, if they are in the data base
       IF(KPLXC.eq.1) Then                          !Get distances
        CALL GET4('DISTPSEC      ',D_Psec,NUMSTR,int2(1),int2(1),NDO, &
     &   KERR(5))
        If (Kerr(5) .eq. 0) Then
!   Check that values not all zero
         Do N=1,Numstr
          If( D_psec(N).ge.1.D0 ) Dpsec = Dpsec + 1
         Enddo
        Else
!  No distances, zero out the array.
         Do N=1,Numstr
          D_psec(N) = 0.D0
         Enddo
          Dpsec = 0
        Endif
       ENDIF                                        !Get distances
!
      ENDIF                                         !Get Star Info
!      print *,' STRI: Pmotion, Dpsec = ', Pmotion, Dpsec
!
!  Proper Motions: If proper motions applied, do the work here. RA and Dec
!   offsets will be computed once for each source, and will remain constant
!   throughout this Calc run. If proper motions are added to the source
!   coordinates (KSTRC = 2), those new coordinates will be output in the
!   type 1 Lcode 'STARPRMO' for capture and later use. If proper motions
!   are used only to compute a proper motion contribution but not added
!   to the source coordinates (KSTRC = 1), then those offsets will be
!   output in the type 1 Lcode 'RADECADD'.
!
       IF (KSTRC.eq.1 .or. KSTRC.eq.2) THEN           !Do proper motions
!
        IF (Pmotion .eq. 0) Then
         Do N = 1, Numstr
          PRcorr(1,N) = 0.D0
          PRcorr(2,N) = 0.D0
         Enddo
         Go to 250
        ENDIF
!
!           Look for 4-digit year interval
          CALL GETI ('INTRVAL4      ',Intrvl,int2(5),int2(2),int2(1), &
     &     NDO, KERX)
!           If no 4-digit year interval, get 2-digit year interval
         If (KERX.ne.0) then
          CALL GETI ('INTERVAL      ',Intrvl,int2(5),int2(2),int2(1), &
     &     NDO, KERX)
!            Convert 2-digit year to 4-digit year
          If (Intrvl(1,1) .ge. 70 .and. Intrvl(1,1) .le. 99) &
     &        Intrvl(1,1) = Intrvl(1,1)+1900
          If (Intrvl(1,1) .ge.  0 .and. Intrvl(1,1) .le. 69) &
     &        Intrvl(1,1) = Intrvl(1,1)+2000
         Endif
!
!  Convert start time to year and fraction, not worrying about leap years:
           xdoy1 = imdoy(Intrvl(2,1)) + Intrvl(3,1) + Intrvl(4,1)/24.d0 &
     &             + Intrvl(5,1)/1440.d0
           Xepoch = Intrvl(1,1) + xdoy1/365.D0
             Do N = 1, Numstr
              X_frac = Xepoch - P_motion(3,N)
               If (P_motion(3,N).eq.0.D0) X_frac = 0.D0
              PRcorr(1,N) = P_motion(1,N)*X_frac*CONVDS/DCOS(RADEC(2,N))
              PRcorr(2,N) = P_motion(2,N)*X_frac*CONVDS
                If (KSTRC.eq.2) Then
                 RADEC(1,N) = RADEC(1,N) + PRcorr(1,N)
                 RADEC(2,N) = RADEC(2,N) + PRcorr(2,N)
                 PRcorr(1,N) = -PRcorr(1,N)
                 PRcorr(1,N) = -PRcorr(1,N)
                Endif
             Enddo
 250    Continue
        IF (KSTRC.eq.1) CALL PUT4 ('RADECADD      ', PRcorr, int2(2), &
     &      Numstr, int2(1))
        IF (KSTRC.eq.2) CALL PUT4 ('STARPRMO      ', RADEC, int2(2), &
     &      Numstr, int2(1))
       ENDIF                                        !Do proper motions
!
      DO 300  N = 1,3
        NN = N
        IF ( KERR(N) .EQ. 0 ) GO TO 300
           CALL TERMINATE_CALC ('STRI  ', NN, KERR(NN) )
  300 CONTINUE
!
!   Check KSTRD for debug output.
      IF ( KSTRD .EQ. 0 ) GO TO 500
      WRITE ( 6, 9)
    9 FORMAT (1X, "Debug output for subroutine STRI." )
      WRITE(6,8)' RADEC   ',RADEC
    8 FORMAT(A,4D25.16/(7X,5D25.16))
      WRITE(6,7)' NUMSTR  ',NUMSTR
    7 FORMAT(A6,15I8/(9X,15I8))
      WRITE ( 6, 9200 )  LNSTAR
 9200 FORMAT (1X, "LNSTAR = ", /, 1X, 10 ( 10 ( 4A2, 2X ), /,1X))
      If (KSTRC.eq.1 .or. KSTRC.eq.2)  WRITE(6,8)' P_motion ', &
     &       ((P_motion(I,J), I=1,3), J=1,Numstr)
      If (KPLXC.eq.1)  WRITE(6,8)' D_Psec ', (D_Psec(J),J=1,Numstr)
!
!     Normal conclusion.
  500 RETURN
      END
!
!************************************************************************
      SUBROUTINE ATMI()
      Implicit none
!
!     ATMI is the atmosphere module input and initialization section.
!
      INCLUDE 'ccon.i'
!       Variables 'from':
!          1. KATMC - Atmosphere module flow control flag.
!                     = 0 (default), compute Niell dry and
!                       wet partials and contributions, but don't apply them to
!                       the theoreticals.
!                     = 1, as above but DO apply Niell dry to the theoreticals.
!
!     Program specifications -
      INTEGER*2      LATMM(40),      LON(40),    LOFF(40)
      CHARACTER*40 C_LATMM(2) ,    C_LON(2) ,  C_LOFF(2)
      EQUIVALENCE (C_LATMM,LATMM),(C_LON,LON),(C_LOFF,LOFF)
!
      DATA C_LATMM / &
     &'Atmosphere Module - Last modification 99', &
     &'OCT05, D. Gordon, GSFC.                 '/
!
      DATA C_LON / &
     &'Atmosphere Module turned on, Niell dry c', &
     &'ontributions applied to theoreticals.   '/
!
      DATA C_LOFF / &
     &'Atmosphere Module is turned on - Contrib', &
     &'utions NOT applied to theoreticals.     '/
!
!     Database access -
!            'Put' variables:
!              1.  LATMM(40)  -  The atmosphere module text message.
!              2.  LON(40)    -  Module flow control 'ON' message.
!              3.  LOFF(40)   -  Module flow control 'OFF' message.
!            Access codes:
!              1.  'ATM MESS'  -  Access code for module text message.
!              2.  'ATM CFLG'  -  Access code for module status message.
!
!      Subroutine Interface -
!         Caller subroutines: INITL
!         Called subroutines: PUTA
!
!      Programmer - Dale Markham  01/13/77
!       77.07.07  Peter Denatale
!       78.01.03  Bruce Schupler
!       85.01.03  David Grodon  Changed FLAG=1 definition.
!       87.03.06  Savita Goel   CDS for A900.
!       89.06.29  Jim Ryan      Character strings used.
!       89.12.12  Jim Ryan      UNIX-like database interface implimented.
!       91.05.24  Jim Ryan      Documentation simplified.
!       94.02.03  David Gordon  Mods for Nhmf2 and Whmf2 computations.
!       Sept 2002 Jim Ryan      Interger*2/4 mods.
!
!     PUT Atmosphere module text message.
      CALL PUTA ('ATM MESS      ',LATMM,int2(40),int2(1),int2(1))
!
!     PUT module control flag status message according to KATMC.
      IF (KATMC .EQ. 1) CALL PUTA('ATM CFLG      ',LON,int2(40), &
     &                  int2(1),int2(1))
      IF (KATMC .NE. 1) CALL PUTA('ATM CFLG      ',LOFF,int2(40), &
     &                  int2(1),int2(1))
!
      RETURN
      END
!
!************************************************************************
      SUBROUTINE AXOI()
      IMPLICIT None
!
! 2.    AXOI
!
! 2.1   AXOI PROGRAM SPECIFICATION
!
! 2.1.1 AXOI is the Axis Offset and Feedbox Rotation Module input and
!       initialization section.
!
! 2.2   AXOI PROGRAM INTERFACE
!
! 2.2.2 COMMON BLOCKS USED -
!
       INCLUDE 'ccon.i'
!      VARIABLES 'FROM'
!        1. KAXOC - THE AXIS OFFSET MODULE FLOW CONTROL FLAG
!                   = 0 ==> Axis offset corrections applied to theoreticals.
!                   = 1 ==> Axis offset corrections NOT applied to theoreticals.
!        1.  KPANC  -  The Feedbox Rotation flow control flag.
!
! 2.2.3 PROGRAM SPECIFICATIONS -
      INTEGER*2       LAXOM(40),      LON(40),    LOFF(40)
      CHARACTER*40  C_LAXOM(2),     C_LON(2),   C_LOFF(2)
      EQUIVALENCE(  C_LAXOM,LAXOM),(C_LON,LON),(C_LOFF,LOFF)
      INTEGER*2      LPANG_ON(40),  LPANG_OF(40)
      CHARACTER*40 C_LPANG_ON(2), C_LPANG_OF(2)
      EQUIVALENCE (  LPANG_ON, C_LPANG_ON)
      EQUIVALENCE (  LPANG_OF, C_LPANG_OF)
!
      DATA C_LAXOM / &
     &'Axis Offset Module - Last modified 2004.', &
     &'05.19, D. Gordon/GSFC.                  '/
      DATA C_LON  / &
     &'Axis Offset Module is turned ON in CALC.', &
     &'                                        '/
      DATA C_LOFF / &
     &'Axis Offset Module is turned OFF in CALC', &
     &'.                                       '/
!
      DATA C_LPANG_ON  / &
     &'Feedbox Rotation Angle Module, Last Modi', &
     &'fied 98NOV12, D. Gordon/GSFC.           '/
      DATA C_LPANG_OF  / &
     &'Feedbox Rotation angle zeroed out.      ', &
     &'                                        '/
!
! 2.2.4 DATA BASE ACCESS -
!           'PUT' VARIABLES:
!              1. LAXOM(40)    - THE AXIS OFFSET MODULE TEXT MESSAGE.
!              2. LON(40)      - THE AXIS OFFSET MODULE TURNED ON MESSAGE
!              3. LOFF(40)     - THE AXIS OFFSET MODULE TURNED OFF MESSAGE.
!              4. LPANG_ON(40) - The feedbox rotation (parallactic) angle
!                                text message.
!              5. LPANG_OF(40) - The feedbox rotation (parallactic) angle
!                                turned OFF message.
!           ACCESS CODES PUT:
!              1. 'AXO MESS'   -  THE DATA BASE ACCESS CODE FOR THE
!                                 AXIS OFFSET MODULE TEXT MESSAGE.
!              2. 'AXO CFLG'   -  THE DATA BASE ACCESS CODE FOR THE AXIS
!                                 OFFSET MODULE FLOW CONTROL MESSAGE.
!              3. 'PAN MESS'   -  The data base access code for the feedbox
!                                 rotation text message.
!
! 2.2.6 SUBROUTINE INTERFACE -
!             CALLER SUBROUTINES: INITL
!             CALLED SUBROUTINES: PUTA
!
! 2.2.9 PROGRAMMER - DALE MARKHAM  01/13/77
!                    PETER DENATALE 07/11/77
!                    BRUCE SCHUPLER 03/07/78
!                    Jim Ryan 89.06.29 Character stings used.
!                    Jim Ryan 89.12.12 UNIX-like database interface
!                             implemented.
!                    D. Gordon 98.11.12 Merged in code from subroutine PANI,
!                             the feedbox rotation (parallactic) angle
!                             initialization routine.
!                    Jim Ryan 02.Sept Integer*2/4 updates.
!                    D.Gordon 2004.05.19 Calc 10 updates.
!
! 2.3   AXOI PROGRAM STRUCTURE
!
!     Put Axis Offset Module text message in database
      CALL PUTA ( 'AXO MESS      ', LAXOM, int2(40), int2(1), int2(1))
!
!     Put Axis Offset Module flow control message into database header.
      IF (KAXOC .NE. 1) CALL PUTA('AXO CFLG      ',LON,int2(40), &
     &                  int2(1),int2(1))
      IF (KAXOC .EQ. 1) CALL PUTA('AXO CFLG      ',LOFF,int2(40), &
     &                  int2(1),int2(1))
!
!     PUT for the feedbox rotation (paralactic) angle routine text message.
      IF (KPANC .NE. 1) CALL PUTA ('PAN MESS      ',LPANG_ON,int2(40), &
     &                  int2(1), int2(1))
      IF (KPANC .EQ. 1) CALL PUTA ('PAN MESS      ',LPANG_OF,int2(40), &
     &                  int2(1), int2(1))
!
!     Normal conclusion.
      RETURN
      END
!
!************************************************************************
      SUBROUTINE THERI()
      IMPLICIT None
!
!     THERI is the Theory input and initialization section.
!
!     THERI program interface:
!
!     Common blocks used -
       INCLUDE 'ccon.i'
!          Variables 'from':
!            1.  KTHEC  -  The Theory routine flow control flag.
!                          (No longer has any function.)
!            2.  KTHED  -  The Theory routine debug control flag.
!            3.  KRELC  -  Relativity module flow control flag.
!
!     Program specifications -
!
      INTEGER*2      LTHEU(40)
      CHARACTER*40 C_LTHEU(2)
      INTEGER*2      L_on(30),  L_off(30)
      CHARACTER*30 C_L_on(2), C_L_off(2)
      EQUIVALENCE (LTHEU, C_LTHEU), (L_on, C_L_on), (L_off, C_L_off)
!
      DATA C_LTHEU / &
     & 'THEORY ROUTINE: Eubanks Consensus Relati', &
     & 'vity Model, Last mod 98DEC16, D. Gordon '/
!
      DATA C_L_on  / &
     & 'Relativistic bending turned ON', &
     & ' in theoreticals computation. '/
!
      DATA C_L_off / &
     & 'Relativistic bending turned OF', &
     & 'F in theoreticals computation.'/
!
!    Database access -
!          'PUT' variables:
!            1. LTHEU(40)  - The theory routine text message.
!          Access codes:
!            1. 'THE MESS' - The database access code for the Theory routine
!                            text message.
!
!    Subroutine interface -
!          Caller subroutines: INITL
!          Called subroutines: PUTA
!
!    Programmer - DALE MARKHAM  01/17/77
!                 PETER DENATALE 07/20/77
!                 CHOPO MA /DAVID GORDON 04/12/84
!                 JIM RYAN 89/06/06 Some character string used.
!                 Jim Ryan 89/10/08 Relativity module code moved here.
!                 Jim Ryan 89.12.12 UNIX-like database interface
!                          implimented.
!                 Jim Ryan 90:11:20 Comments cleaned up and debug fixed.
!                 David Gordon 93AUG02 Data base message revised for Consensus
!                              model.
!                 David Gordon 93DEC23 Fixed up debug output.
!                 David Gordon 94.04.18 Converted to Implicit None.
!                 David Gordon 94.10.05 Updated database text message. Added
!                              character variables for 'REL CFLG' message.
!                 David Gordon 94.12.15 Corrected dimensioning of C_L_on and
!                              C_L_off.
!                 David Gordon 96.01.30 Changed text message date.
!                 David Gordon 98.08.18 Changed text message.
!                 Jim Ryan     Sept 2002 Integer*2/4 mods.
!
!     THERI program structure
!
!     PUT the Theory Module text message into the database.
      CALL PUTA ('THE MESS      ', LTHEU, int2(40), int2(1), int2(1))
!
!     PUT the relativistic bending status into the database.
      IF(KRELC .eq. 0)  then
        CALL PUTA ('REL CFLG      ', L_on, int2(30), int2(1), int2(1))
      else
        CALL PUTA ('REL CFLG      ', L_off, int2(30), int2(1), int2(1))
      Endif
!
      RETURN
      END
!
!************************************************************************
      SUBROUTINE SITBLK(Kerr)
      Implicit None
!
      INCLUDE 'cmxst11.i'

      INCLUDE 'input11.i'
!
      Integer*2 Kerr(10)
      Integer*4 I, II, Jsite(Max_stat), Get4unit, Iunit, An_typ, ios,    &
     &          Iquit, Index
      Real*8 Sit_X, Sit_Y, Sit_Z, Ax_off, S_zen
!
      Character*80 Inbuf
      Character*8  Dbsites(Max_stat)
      Equivalence (LNSITE(1,1), Dbsites(1))
!
      Character*4 T_plate(Max_stat), plate
      Integer*2   L_plate(2,Max_stat)
      Equivalence (T_plate,L_plate)
!
!  Programmer/History:
!       David Gordon 1998.03.17 Program created.
!       David Gordon 2001.01.05 Code to PUT Lcode TECTPLNM.
!       David Gordon 2006.03.30 Revised missing station message.
!       David Gordon 2006.04.03 Revised missing station message again.
!
!   Initialize station counter
       Do I = 1, Max_stat
         Jsite(i) = 0
       Enddo
!
!  Open the input sites data file
       Iunit = get4unit()
      Open (Unit=Iunit, File=Ex_sites, Status='old', Action='Read', &
     &       Err=240, Iostat=ios)
!
      If ( Index(Ex_sites,'blokq') .gt. 0) Then
!  If it is a blokq.dat file, find the site catalog section
  50   Continue
       Read(iunit,'(A80)',end=991) Inbuf
!      If (Inbuf(1:18) .eq. '$$ STATION CATALOG') Go to 100
       If (Inbuf(1:4) .eq. '$$//') Go to 100
       Go to 50
 100   Continue
      Endif
!
 110   Continue
       Read(iunit,'(A80)',end=200) Inbuf
!   Skip comments and illegal lines
!      If (Inbuf(1:2) .eq. '$$'  ) Go to 110
       If (Inbuf(1:4) .ne. '    ') Go to 110
       If (Inbuf(13:13) .ne. ' ' ) Go to 110
!
!   Finished site catalog
       If (Inbuf(1:2) .eq. '//') Go to 200
!
       Do I = 1, Numsit
         If (Inbuf(5:12) .eq. Dbsites(I)) Then
!  Matched I'th station in data base station list, save station particulars
           II = I
           Read(Inbuf(14:80),*) Sit_X, Sit_Y, Sit_Z, An_typ, Ax_off, &
     &                          S_zen, plate
!
           SITXYZ(1,II) = Sit_X
           SITXYZ(2,II) = Sit_Y
           SITXYZ(3,II) = Sit_Z
           SITAXO(II)   = Ax_off
           KTYPE(II)    = An_typ
           SITZEN(II)   = S_zen
           T_plate(II)  = plate
!
           Jsite(II)    = II
!
         Endif
!
       Enddo
!
       Go to 110
!
 200   Continue
!
       Close(Iunit)
!
!   Verify that we have site a priori's for all stations. If not, we must
!    quit here and tell the user to fix the problem.
!
        Iquit = 0
      DO I = 1, Numsit
        If (Jsite(i) .eq. 0) Then
          If (iquit.eq.0) Write(6,'(/)')
          Write(6,'(" SITBLK: Missing station: ",A8)') Dbsites(I)
          Iquit = Iquit + 1
        Endif
         If (Iquit .gt. 0) Then
             Write(6,'(/,"!!! Missing sites!!! Update file ",/,10X,A80,  &
     &        /,3X, " and rerun Calc!!!",/ )') Ex_sites
             Call TERMINATE_CALC( 'SITBLK',int2(0), int2(0))
         Endif
      ENDDO
!
       If (Iquit.eq.0) Then
        Kerr(3) = 0
        Kerr(4) = 0
        Kerr(5) = 0
        Kerr(6) = 0
       Else
        Call TERMINATE_CALC( 'SITBLK', int2(0), int2(0))
       Endif
!
!       print *, ' SITXYZ ', SITXYZ
!       print *, ' SITAXO ', SITAXO
!       print *, ' KTYPE  ', KTYPE
!       print *, ' SITZEN ', SITZEN
!
!  Now we must replace the site a priori's in the data base
      CALL PUTR('SITERECS      ', SITXYZ, int2(3), Numsit, int2(1))
      CALL PUTR('SITEZENS      ', SITZEN, Numsit, int2(1), int2(1))
      CALL PUTI('AXISTYPS      ', KTYPE, Numsit, int2(1), int2(1))
      CALL PUTR('AXISOFFS      ', SITAXO, Numsit, int2(1), int2(1))
      CALL PUTA('TECTPLNM      ', L_plate, int2(2), Numsit, int2(1))
!
       Go to 270
!
!   error on OPEN
 240  Continue
      WRITE ( 6, * )  'Error on OPEN of blokq file '
      Call TERMINATE_CALC( 'SITBLK', int2(0), int2(0))
       Go to 270
!
 991  Continue
      WRITE ( 6, * )  'Cant find STATION section in blokq file '
      Call TERMINATE_CALC( 'SITBLK', int2(0), int2(0))
!
 270  Continue
      Return
      End
!
!*************************************************************************
      SUBROUTINE OCNIN(Kerr)
      Implicit None
!
      INCLUDE 'cmxst11.i'
!
      INCLUDE 'input11.i'
!
      Real*8           PI, TWOPI, HALFPI, CONVD, CONVDS, CONVHS, SECDAY
      COMMON / CMATH / PI, TWOPI, HALFPI, CONVD, CONVDS, CONVHS, SECDAY
!         VARIABLES 'FROM':
!            1. CONVD - THE CONVERSION FACTOR FROM DEGREES TO RADIANS (RAD/DEG)
!
      Real*8    OC11(11)
      Integer*4 I, II, Jsite(Max_stat), Iunit, ios, Iquit, Index, K
      Integer*4 Get4unit
      Integer*2 Kerr(10)
!
      Character*80 Inbuf
      Character*8  Dbsites(Max_stat)
      Equivalence (LNSITE(1,1), Dbsites(1))
!
      Character*4 Ocestat(Max_stat)
      Integer*2 L_ocestat(2,Max_stat)
      Equivalence (Ocestat,L_ocestat)
!
!  Programmer/History:
!       David Gordon 98.03.17 - Program created.
!       David Gordon 99.11.19 - Bug fix - corrected ocean loading external
!                               file site name.
!       David Gordon 2000.01.05 - Determine ocean loading statuses (must be
!                               'YES' for all stations) and PUT them into
!                               the type 1 records, Lcode 'OCE STAT'.
!       David Gordon 2006.03.30 Revised missing station message.
!       David Gordon 2006.04.03 Revised missing station message again.
!
!   Initialize station counter
       Do I = 1, Max_stat
         Jsite(i) = 0
       Enddo
!
!  Open the Ocean loading data file
       Iunit = get4unit()
       Open (Unit=Iunit, File=Ex_ocean, Status='old', Action='Read', &
     &       Err=240, Iostat=ios)
!
      If ( Index(Ex_ocean,'blokq') .gt. 0) Then
!  Blokq.dat file, find the site catalog
  50   Continue
       Read(iunit,'(A80)') Inbuf
       If (Inbuf(1:2) .eq. '//') Go to 100
       Go to 50
 100   Continue
      Endif
!
 110   Continue
       Read(iunit,'(A80)',end=200) Inbuf
!   Skip comments and illegal lines
       If (Inbuf(1:2) .eq. '$$') Go to 110
       If (Inbuf(1:2) .ne. '  ') Go to 110
!      If (Inbuf(11:11) .ne. ' ' ) Go to 110
!
!   Finished site catalog
       If (Inbuf(1:2) .eq. '//') Go to 200
!
! See if this station is in the database list
       Do I = 1, Numsit
         If (Inbuf(3:10) .eq. Dbsites(I)) Then
!         print *, 'Site matched: ', Dbsites(I)
!  Matched I'th station in data base station list, save station particulars
           II = I
!
!  Skip comments
 170    Continue
        Read(iunit,'(A80)',end=200) Inbuf
        If (Inbuf(1:2) .eq. '$$') Go to 170
!
!   Read 6 data lines
        Read(Inbuf,*,err=180) OC11
          Do k=1,11
           SITOAM(k,II) = OC11(k)
          Enddo
!
        Read(Iunit,*,err=180) OC11
          Do k=1,11
           SITHOA(k,1,II) = OC11(k)
          Enddo
        Read(Iunit,*,err=180) OC11
          Do k=1,11
           SITHOA(k,2,II) = OC11(k)
          Enddo
!
        Read(Iunit,*,err=180) OC11
          Do k=1,11
           SITOPH(k,II) = OC11(k) * CONVD
          Enddo
        Read(Iunit,*,err=180) OC11
          Do k=1,11
           SITHOP(k,1,II) = OC11(k) * CONVD
          Enddo
        Read(Iunit,*,err=180) OC11
          Do k=1,11
           SITHOP(k,2,II) = OC11(k) * CONVD
          Enddo
!
           Jsite(II)    = II
!
         Endif
       Enddo
!
       Go to 110
!
 200   Continue
!
       Close(Iunit)
!
!   Verify that we have ocean loading a priori's for all stations, except any
!    site at the geocenter. If not, we must quit here and tell the user to fix
!    the problem.
!
        Iquit = 0
!
      DO I = 1, Numsit
        If (Jsite(i) .eq. 0) Then
           If (I .ne. Zero_site) Then
            If (iquit.eq.0) Write(6,'(/)')
            Write(6,'(" OCNIN: No ocean loading for ",A8)') Dbsites(I)
            Iquit = Iquit + 1
           Endif
        Endif
         If (Iquit .gt. 0) Then
             Write(6,'(/,"!!! Missing ocean loading!!! Update file ",/,   &
     &       10X, A80,/,3X, " and rerun Calc!!!",/ )') Ex_ocean
             Call TERMINATE_CALC( 'OCNIN ',int2(0), int2(0))
         Endif
      ENDDO
!
       If (Iquit.eq.0) Then
        Kerr(7)  = 0
        Kerr(8)  = 0
        Kerr(9)  = 0
        Kerr(10) = 0
         Do I = 1, Numsit
          Ocestat(I) = 'YES '
         Enddo
       Else
        Call TERMINATE_CALC( 'OCNIN ', int2(0), int2(0))
       Endif
!
!  Now we must replace the ocean loading a priori's in the data base
      CALL PUTR('SITOCAMP      ', SITOAM, int2(11), Numsit, int2(1))
      CALL PUTR('SITOCPHS      ', SITOPH, int2(11), Numsit, int2(1))
      CALL PUTR('SITHOCAM      ', SITHOA, int2(11), int2(2), Numsit)
      CALL PUTR('SITHOCPH      ', SITHOP, int2(11), int2(2), Numsit)
      CALL PUTA('OCE STAT      ', L_ocestat, int2(2), Numsit, int2(1))
!
       Go to 270
!
!   Error on Read
 180  Continue
      WRITE ( 6, * )  'OCNIN: Error on read of ocean loading file '
        Call TERMINATE_CALC('OCNIN ', int2(0), int2(0))
!
!   Error on OPEN
 240  Continue
      WRITE ( 6, * )  'OCNIN: Error on OPEN of ocean loading file '
        Call TERMINATE_CALC('OCNIN ', int2(0), int2(0))
!
 270  Continue
      Return
      End
!
!*************************************************************************
      SUBROUTINE ANTILT(Krr)
      Implicit None
!
      INCLUDE 'cmxst11.i'
!        Variables 'from':
!           1. LNSITE(4,Max_Stat)  - THE EIGHT CHARACTER SITE NAMES
!                                    IN THE DATABASE. (ALPHAMERIC)
!           2. NUMSIT             -  THE NUMBER OF SITES IN THE DATABASE.
!        Variables 'to':
!           2. Dbtilt(2,Max_Stat)  - Antenna fixed axis tilts, in arc-minutes.
!                                    For alt-az mounts, 1 => East tilt,
!                                    2 => North tilt.
      INCLUDE 'input11.i'
!        Variables 'from':
!           1. Ex_tilts -  Ascii name of the antenna fixed axis tilts file.
!
      INCLUDE 'cmxut11.i'
!        Variables 'to':
!           1. Intrvl(5,2) - First and last time tag of data in the current
!                            data base. (First index: year, month, day,
!                            hour, minute. Second index: first, last.)
!
      Character*80 Inbuf
      Character*8  Dbsites(Max_stat), Asite, Bsite, slash, Ablnk, &
     &             Bblnk
      Equivalence (LNSITE(1,1), Dbsites(1))
!
      Real*4 Tyear, Tdate, Tilt1, Tilt2, Tilt1a, Tilt2a, Tilt1b, &
     &       Tilt2b, Tdatea, Tdateb
      Real*4 XDOY, XYR
      Integer*4 Iyear, Imonth, Iday, Krr, Nsites, Mstat
      Integer*4 I, II, Iunit, Ios, Index, I4, ICAL(12), LCAL(12)
      Integer*4 Get4unit
!
      DATA ICAL /0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334/
      DATA LCAL /0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335/
!
!  Programmer/History:
!       David Gordon 2004.05.17 Program created.
!
!   Use Integer*4 quantities
       Nsites = Numsit
       Mstat = Max_stat
!
!   Initialize station tilts
       Do I = 1, Mstat
         Dbtilt(1,I) = 0.0D0
         Dbtilt(2,I) = 0.0D0
       Enddo
!
        Iyear  = Intrvl(1,1)
        Imonth = Intrvl(2,1)
        Iday   = Intrvl(3,1)
         If (Iyear .ge.  0 .and. Iyear .le. 69) Iyear = Iyear+2000
         If (Iyear .ge. 70 .and. Iyear .le. 99) Iyear = Iyear+1900
!         Write ( 6, * ) ' Intrvl ', Intrvl
!         Write ( 6, * ) ' Iyear,Imonth,Iday ', Iyear,Imonth,Iday
!
!   Find epoch, in years at start of session.
!    Get day of year number
        IF (MOD(Iyear,4).NE. 0) Then
          XDOY = ICAL(Imonth) + Iday
          XYR = 365.
        Else
          XDOY = LCAL(Imonth) + Iday
          XYR = 366.
        Endif
         Tyear = Float(Iyear) + XDOY/XYR
!          Write ( 6, * ) ' XDOY,XYR,Tyear ', XDOY,XYR,Tyear
!
!  Open the input file of antenna tilts
       Iunit = get4unit()
       Open (Unit=Iunit, File=Ex_tilts, Status='old', Action='Read', &
     &       Err=240, Iostat=ios)
!
!   Skip first 4 lines
       Do I = 1, 4
        Read (Iunit,'(A80)') Inbuf
       Enddo
!
 110   Continue
       Read(iunit,1001,end=300) Asite, Bsite, Tdate, Tilt1, &
     &                                  Tilt2, slash
 1001   Format(2X,A8,1X,A8,F11.3,6X,F8.2,7X,F8.2,A8)
!
       IF ( Index(slash, '/') .gt. 0) Then
          Tilt1a = Tilt1
          Tilt2a = Tilt2
          Tdatea = Tdate
          Tilt1b = Tilt1
          Tilt2b = Tilt2
          Tdateb = Tdate
          Go to 200
       Endif
!
!             More lines for this station
          Tilt1a = Tilt1
          Tilt2a = Tilt2
          Tdatea = Tdate
       If (Tyear .le. Tdate) Then
          Tilt1b = Tilt1
          Tilt2b = Tilt2
          Tdateb = Tdate
!           Read till '/' found
  180     Continue
          Read(iunit,1001) Ablnk, Bblnk, Tdate, Tilt1, Tilt2, slash
          IF (Index(slash, '/') .gt. 0) Go to 200
          Go to 180
       Endif
!
  182      Continue
          Read(Iunit,1001) Ablnk, Bblnk, Tdate, Tilt1, Tilt2, slash
          If (Tyear .le. Tdate) Then
          Tilt1b = Tilt1
          Tilt2b = Tilt2
          Tdateb = Tdate
          IF (Index(slash, '/') .gt. 0) Go to 200
  184      Continue
           Read(iunit,1001) Ablnk, Bblnk, Tdate, Tilt1, Tilt2, slash
           IF (Index(slash, '/') .gt. 0) Go to 200
           Go to 184
!
          Else
            IF (Index(slash, '/') .le. 0) Then
             Tilt1a = Tilt1
             Tilt2a = Tilt2
             Tdatea = Tdate
             Go to 182
            Else
             Tilt1b = Tilt1
             Tilt2b = Tilt2
             Tdateb = Tdate
            Endif
!
          Endif
!
 200   Continue
!       Write (6, *) ' Asite, Bsite, Tyear: ', Asite, Bsite, Tyear
!       Write (6, *) ' Tdatea,Tilt1a,Tilt2a: ', Tdatea,Tilt1a,Tilt2a
!       Write (6, *) ' Tdateb,Tilt1b,Tilt2b: ', Tdateb,Tilt1b,Tilt2b
!
!  Match I'th station and find its tilt for this epoch.
      Do I = 1, Nsites
       If (Asite .eq. Dbsites(I) .or. Bsite .eq. Dbsites(I)) Then
         II = I
         Go to 155
       Endif
      Enddo
       Go to 110
!
 155  Continue
        If ( ABS(Tdateb-Tdatea) .lt. .002 ) Then
          Dbtilt(1,II) = Tilt1a
          Dbtilt(2,II) = Tilt2a
        Else
          Dbtilt(1,II) = Tilt1a + (Tyear-Tdatea)*(Tilt1b-Tilt1a)/ &
     &                   (Tdateb-Tdatea)
          Dbtilt(2,II) = Tilt2a + (Tyear-Tdatea)*(Tilt2b-Tilt2a)/ &
     &                   (Tdateb-Tdatea)
        Endif
!       Write (6, *) ' Interpolated tilt: ', Dbtilt(1,II),Dbtilt(2,II)
!
       Go to 110
!
 300   Continue
       Close(Iunit)
!
!  Now we place the tilts in the data base
      CALL PUTR ('AXISTILT      ', Dbtilt, int2(2), Numsit, int2(1))
       Krr = 0
       Go to 270
!
!   Error on OPEN
 240  Continue
      Krr = -1
      WRITE ( 6, * )  'Error on OPEN of blokq file '
!
 270  Continue
      Return
      End
!
!*************************************************************************
      SUBROUTINE OPTLIN(Kerr)
      Implicit None
!
      INCLUDE 'cmxst11.i'
!
      INCLUDE 'input11.i'
!
      Character*80 Inbuf
      Integer*4 I, II, Jsite(Max_stat), Iunit, Ios, Iquit, Index, J
      Integer*4 Get4unit
!     Real*8    OPTL6(6,Max_stat)
      Character*8  Vsite, Dbsites(Max_stat)
      Equivalence (LNSITE(1,1), Dbsites(1))
      Integer*2 Kerr(10)
!
      Real*8 Vlon, Vlat, XYZ(3), u_rR, u_rI, u_nR, u_nI, u_eR, u_eI
!
!     Character*4 Ocestat(Max_stat)
!     Integer*2 L_ocestat(2,Max_stat)
!     Equivalence (Ocestat,L_ocestat)
!
!  Programmer/History:
!       David Gordon 2012.11.28 - Program created.
!
!   Initialize station counter
       Do I = 1, Max_stat
         Jsite(i) = 0
         Do J = 1,6
          OPTL6(J,I) = 0.0D0
         Enddo
       Enddo
!
!  Open the Ocean pole tide loading data file
       Iunit = get4unit()
       Open (Unit=Iunit, File=Ex_OPTL , Status='old', Action='Read',         &
     &       Err=240, Iostat=Ios)
!
  50   Continue
       Read(Iunit,'(A80)',Err=180,End=250) Inbuf
!       Write(6,*) Inbuf
       If (Inbuf(1:36) .eq. 'Ocean Pole Tide Loading Coefficients')     &
     &     Go to 100
       Go to 50
 100   Continue
!  Skip 4 lines
       Do I = 1,4
        Read(iunit,'(A80)') Inbuf
       Enddo
!
 110   Continue
       Read(iunit,1001,err=110,end=200)  Vsite, Vlat, Vlon, u_rR, u_rI, &
     &        u_nR, u_nI, u_eR, u_eI
 1001  Format(1x,A8,3X,F8.2,3X,F8.2,6(2X,F10.6))
!
! See if this station is in the database list
       Do I = 1, Numsit
         If (Vsite .eq. Dbsites(I)) Then
!*        print *, 'OPTLIN: Site matched - ', Dbsites(I)
!  Matched I'th station in data base station list, save station particulars
           II = I
          OPTL6(1,II) =  u_rR
          OPTL6(2,II) =  u_rI
          OPTL6(3,II) =  u_nR
          OPTL6(4,II) =  u_nI
          OPTL6(5,II) =  u_eR
          OPTL6(6,II) =  u_eI
           Jsite(II) = II
!
         Endif
       Enddo
!
       Go to 110
!
 200   Continue
!
       Close(Iunit)
!
!   Verify that we have ocean pole tide loading coefficients for all stations,
!    except any site at the geocenter. If not, we must quit here and tell the 
!    user to fix the problem.
!
        Iquit = 0
!
      DO I = 1, Numsit
        If (Jsite(I) .eq. 0 .and. I .ne. Zero_site) Then
          If (Iquit.eq.0) Write(6,'(/)')
          Write(6,'(" OPTLIN: No ocean pole tide loading for ",A8)')    &
     &       Dbsites(I)
          Iquit = Iquit + 1
        Endif
         If (Iquit .gt. 0) Then
             Write(6,'(/,"!!! Missing ocean pole tide loading!!! You"   &
     &       " really should update file ",/, A80, /,                   &
     &       " and rerun Calc!!!",/ )') Ex_OPTL 
!!!!!        Call TERMINATE_CALC( 'OPTLIN ',int2(0), int2(0))
         Endif
      ENDDO
!
!
!*     Write(6,'(" OPTLIN: Site, Ocean loading coefficients: ")')
!*    Do I=1,Numsit
!*     Write (6,1073) Dbsites(I), OPTL6(1,I), OPTL6(2,I), OPTL6(3,I),   &
!*   &                OPTL6(4,I), OPTL6(5,I), OPTL6(6,I)
 1073  Format(1x,A8,3X,6(2X,F10.6))
!*    Enddo
!   
!  Now we must replace/create the ocean pole tide loading in the data base
      CALL PUTR('OPTLCOEF      ', OPTL6, int2(6), Numsit, int2(1))
!
       Go to 270
!
!   Error on Read
 180  Continue
      WRITE (6,*)  'OPTLIN: Error on read of ocean loading file '
        Call TERMINATE_CALC('OPTLIN ', int2(0), int2(0))
!
!   Error on OPEN
 240  Continue
       Close (Iunit)
      WRITE (6,*)  'OPTLIN: Ocean Pole Tide Loading not found '
        Call TERMINATE_CALC('OPTLIN ', int2(0), int2(0))
!
 250  CONTINUE
       Close (Iunit)
      WRITE(6,*) 'OPTLIN: Ocean Pole Tide Loading section was not found'
        Call TERMINATE_CALC('OPTLIN ', int2(0), int2(0))
!
 270  Continue
       Close (Iunit)
      Return
      End
!
!************************************************************************
      SUBROUTINE STRIN(Kerr)
      Implicit None
!
!    Subroutine to open external source catalog and get source a priori's.
!
!     98.03.19 D. Gordon/GSFC - Subroutine created
!     98.09.11 D. Gordon/GSFC - Expanded to read a non-blokq.dat file which
!                               may optionally contain proper motion rates,
!                               proper motion epochs, and source distances.
!     99.10.27 D. Gordon/GSFC - Extraneous printout removed.
!     Sept 2002 Jim Ryan      - Integer*2/4 mods.
!     2006.03.30 D. Gordon    - Improve missing star message.
!     2006.04.03 D. Gordon    - Improve missing star message again.
!     2008.04.14 D.Gordon     - Modified to list all missing sources.
!     2010.03.08 D.Gordon     - Modified to read a Solve source mod file.
!                               The file should contain the string '.src',
!                               '.mod', or 'icrf2'. 
!
      INCLUDE 'cmxsr11.i'
!
      INCLUDE 'input11.i'
!
      INCLUDE 'ccon.i'
!            VARIABLES 'FROM':
!             1. KSTRC  -  THE STAR MODULE FLOW CONTROL FLAG.
!                          0 => Do not add proper motion constants to data
!                               base when in external file mode  (Lcode
!                               'PRMOTION'). Do not compute any proper
!                               motion contributions. Remove any previous
!                               proper motion contribution Lcodes.
!                          1 => Add proper motion constants ('PRMOTION') if
!                               in external file input mode. Compute proper
!                               motion contributions if proper motion
!                               constants are available and insert in data
!                               base under Lcode 'PMOTNCON'. Do NOT add
!                               these contributions to the theoretical.
!                               [They can be ADDED in SOLVE to get proper
!                               motion corrected delays and rates.]
!                          2 => Add proper motion constants ('PRMOTION') if
!                               in external file input mode. Also, ADD the
!                               proper motions to source vector and use the
!                               proper motion corrected source vector
!                               throughout all computations. Compute a
!                               contribution (ADD to theoretical) that will
!                               (approximately) return the delays and rates
!                               to their non-proper motion corrected values,
!                               and put in Lcode 'PMOT2CON'. For cases where
!                               there is a large accumulated proper motion
!                               (greater than ~1 arcsec). Intended for
!                               correlator useage only. USE WITH CAUTION!!
!             2. KSTRD  -  THE STAR MODULE DEBUG OUTPUT FLAG.
!             3. KPLXC  -  THE PARALLAX MODULE FLOW CONTROL FLAG.
!                          0 => Do not insert source distances into data
!                               base when in external file input mode, even
!                               if distances given in external source file.
!                          1 => Insert source distances into data base in
!                               external file input mode, even if all zeros,
!                               using Lcode 'DISTPSEC'.
!
      Real*8           PI, TWOPI, HALFPI, CONVD, CONVDS, CONVHS, SECDAY
      COMMON / CMATH / PI, TWOPI, HALFPI, CONVD, CONVDS, CONVHS, SECDAY
!
      Real*8    RA, Dec, RA_sec, Dec_sec, Dsign, PM_RA, PM_Dec, PM_ep,  &
     &          Psecs
      Integer*4 I, II, I2, Jstar(Max_arc_src), Get4unit,  Iunit, ios,   &
     &          Iquit, Index, K, RA_hr, RA_min, Dec_deg, Dec_min,       &
     &          Itype
      Integer*2 Kerr(3), Lstref(10,Max_arc_src)
      Character*1 Csign, dummy(3)
      Character*20 Cstref(Max_arc_src), Staref
      Equivalence ( Lstref, Cstref )
!
      Character*120 Inbuf
      Character*8  Dbstars(Max_arc_src), Star
      Equivalence (LNSTAR(1,1), Dbstars(1))
!
!   Initialize star counter
       Do I = 1, Max_arc_src
         Jstar(i) = 0
       Enddo
!
        Itype = 1
        Dpsec = 0
        Pmotion = 0
!
!  Open the Star catalog data file
       Iunit = get4unit()
       Open (Unit=Iunit, File=Ex_stars, Status='old', Action='Read', &
     &       Err=240, Iostat=ios)
!
      If (Index(Ex_stars,'blokq') .gt. 0) Then
!  Blokq.dat file, find the star catalog
        Itype = 2
        I2 = 0
  50   Continue
       Read(iunit,'(A120)') Inbuf
       If (Inbuf(1:2) .eq. '//') Then
        I2 = I2 + 1
        If (I2 .eq. 2) Go to 100
       Endif
       Go to 50
 100   Continue
      Endif
!
!  Solve source mod file
      If (Index(Ex_stars,'.src' ) .gt. 0 .or.                           &
     &    Index(Ex_stars,'.mod' ) .gt. 0 .or.                           &
     &    Index(Ex_stars,'icrf2') .gt. 0)           Then
        Itype = 3
!          print *, 'Using a Solve source mod file !!! '
      Endif
!
 130   Continue
       Read(iunit,'(A120)',end=200) Inbuf
!   Skip comments and illegal lines
       If (Inbuf(1:2) .eq. '//')   Go to 200
       If (Inbuf(1:2) .eq. '$$'  ) Go to 130
       If (Inbuf(1:4) .ne. '    ') Go to 130
!      If (Inbuf(13:13) .ne. ' ' ) Go to 130
!
       IF (Itype .eq. 2) THEN
!  blokq.dat file
        Read(Inbuf,1010,err=200,end=200) Star, RA_hr, RA_min, RA_sec, &
     &       Csign, Dec_deg, Dec_min, Dec_sec, Staref
 1010   Format(4X,A8,2X,2(I2,X),F13.10,1X,A1,2(I2,X),F13.10,3X,A20)
       ENDIF
!
       IF (Itype .eq. 3) THEN
! Solve source mod file
        Read(Inbuf,1011,err=200,end=200) Star, RA_hr, RA_min, RA_sec,   &
     &       Csign, Dec_deg, Dec_min, Dec_sec,     Staref
 1011   Format (4X,A8,2X,I2.2,1X,I2.2,1X,F9.6,5X,A1,I2.2,1X,I2.2,1X,    &
     &          F8.5,3X, 6X ,2X,1X, 1X, A20)
       ENDIF
       
       IF (Itype .eq. 1) THEN
!  Non-blokq.dat file
        Read(Inbuf,1015,err=200,end=200) Star, RA_hr, RA_min, RA_sec, &
     &       Csign, Dec_deg, Dec_min, Dec_sec, PM_RA, PM_Dec, PM_ep, &
     &       Psecs, Staref
 1015   Format(4X,A8,2X,2(I2,X),F13.10,1X,A1,2(I2,X),F13.10,3X, &
     &         F8.7,2X,F8.7,2X,F8.3,2X,F8.1,3X,A20)
       ENDIF
!
! See if this source is in the database list
       Do I = 1, NUMSTR
         If (Star .eq. Dbstars(I)) Then
!          print *, 'Source matched: ', Dbstars(I)
           II = I
           Jstar(II) = II
!
           Dsign = 0.D0
           If(Csign .eq. '-') Dsign = -1.D0
           If(Csign .eq. '+') Dsign =  1.D0
           If(Csign .eq. ' ') Dsign =  1.D0
           If(Dsign .eq. 0.D0) Then
            WRITE ( 6, * )  '!!! No plus/minus sign for Declination !!! '
           Endif
!
!  [We use 2.D0*PI below instead of TWOPI for consistency with the
!   Dbedit/Apriori/Skeleton programs. Difference is: TWOPI - 2.D0*PI = +1.D-16.
!   Don't change or re-order these equations, or they may no longer
!    give identical results to Dbedit/Apriori/Skeleton.]
           RADEC(1,II) = (RA_hr/24.D0 + RA_min/1440.D0 + RA_sec/8.64D4) &
     &           * 2.D0*PI
           RADEC(2,II) = ( Dec_deg + (Dec_min + Dec_sec/60.D0)/60.D0 ) &
     &           * Dsign * 2.D0*PI / 360.D0
!
           Cstref(II) = Staref
!           print *,'RA/Dec/ref ', RADEC(1,II), RADEC(2,II), Cstref(II)
!
           IF (Itype .eq. 1) THEN
            P_motion(1,II) = PM_RA
            P_motion(2,II) = PM_Dec
            P_motion(3,II) = PM_ep
            D_psec(II)     = Psecs
            If((P_motion(3,II).ge.1900.D0) .AND. &
     &         (DABS(P_motion(1,II)) .gt. 1.D-12  .or. &
     &          DABS(P_motion(2,II)) .gt. 1.D-12) ) &
     &                Pmotion = Pmotion + 1
            If(D_psec(II).ge.1.D0) Dpsec = Dpsec + 1
           ENDIF
!
         Endif
       Enddo
!
       Go to 130
!
 200   Continue
!
       Close(Iunit)
!
!   Verify that we have a priori's for all stars
        Iquit = 0
!
      DO I = 1, NUMSTR
        If (Jstar(i) .eq. 0) Then
            If (iquit.eq.0) Write(6,'(/)')
            Write(6,'(" STRIN: Missing source: ",A8)') Dbstars(I)
            Iquit = Iquit + 1
        Endif
      ENDDO
         If (Iquit .gt. 0) Then
             Write(6,'(/,"!!! Missing sources!!! Update file ",/,10X,A80,  &
     &        /,3X, " and rerun Calc!!!",/ )') Ex_stars
             Call TERMINATE_CALC( 'STRIN ',int2(0), int2(0))
         Endif
!
       If (Iquit.eq.0) Kerr(3) = 0
!
!  Now we must replace the star a priori's in the data base
      CALL PUTR('STAR2000      ', RADEC, int2(2), NUMSTR, int2(1))
      CALL PUTA('STAR REF      ', LSTREF, int2(10), NUMSTR, int2(1))
      IF (KSTRC.eq.1 .or. KSTRC.eq.2) CALL PUTR('PRMOTION      ', &
     &     P_motion, int2(3), NUMSTR, int2(1))
      IF (KPLXC.eq.1) CALL PUTR('DISTPSEC      ',D_psec,NUMSTR, &
     &     int2(1), int2(1))
!
       Go to 270
!
!   Error on Read
 180  Continue
      WRITE ( 6, * )  'STRIN: Error on read of star catalog '
        Call TERMINATE_CALC('STRIN ', int2(0), int2(0))
!
!   Error on OPEN
 240  Continue
      WRITE ( 6, * )  'STRIN: Error on OPEN of star catalog '
        Call TERMINATE_CALC('STRIN ', int2(0), int2(0))
!
 270  Continue
      Return
      End
!
!************************************************************************
