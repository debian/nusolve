      SUBROUTINE UTCTM ( UTC, XJD )
      IMPLICIT None
!
! 1.1.1 UTCTM is the utility which fetches the UTC time tag of the observation
!       and computes the Julian date at 0 hours UTC of the date of the obs.
!
! 1.1.2 RESTRICTIONS - Computation of Julian date correct for 1901 to 2099 only.
!
! 1.1.3 REFERENCES - ALMANAC FOR COMPUTERS - 1981, Nautical Almanac Office,
!                    United States Naval Observatory, Washington, D.C., 20390
!
! 1.2   UTCTM PROGRAM INTERFACE
!
! 1.2.1 CALLING SEQUENCE -
!           INPUT VARIABLES: None
!           OUTPUT VARIABLES:
!             1. UTC  -  THE UTC TIME AS A FRACTION OF THE UTC DAY. (SEC/SEC)
!             2. XJD  -  THE JULIAN DATE AT ZERO HOURS UTC OF THE DATE IN
!                        QUESTION. (DAYS)
!
! 1.2.2 COMMON BLOCKS USED -
!
      Real*8           PI, TWOPI, HALFPI, CONVD, CONVDS, CONVHS, SECDAY
      COMMON / CMATH / PI, TWOPI, HALFPI, CONVD, CONVDS, CONVHS, SECDAY
!           VARIABLES 'FROM':
!             1. SECDAY  -  THE CONVERSION FACTOR OF COORDINATE TIME SECONDS
!                           PER COORDINATE TIME DAY. (SEC/DAY)
!           VARIABLES 'TO': NONE
!
      INCLUDE 'ccon.i'
!           VARIABLES 'FROM':
!             1. KUTCC  -  THE UTCTM UTILITY ROUTINE FLOW CONTROL FLAG.
!             2. KUTCD  -  THE UTCTM UTILITY ROUTINE FLOW CONTROL FLAG.
!           VARIABLES 'TO': NONE
!
      INCLUDE 'get2s.i'
!       Variables from:
!          1. ITAG(5)     - THE ARRAY CONTAINING THE YEAR/MONTH/DAY/HOUR/MINUTE
!                           PORTION OF THE OBSERVATION TIME TAG.
!          2. TAGSEC      - THE SECONDS PORTION OF THE OBSERVATION TIME TAG.
!
! 1.2.3 PROGRAM SPECIFICATIONS -
!     Integer*2 ITAG(5), KERR(3), NDO(3), ndum
      Integer*2          KERR(3), NDO(3), ndum
!     Real*8    TAGSEC, UTC, XJD, JDY2K
      Real*8            UTC, XJD, JDY2K
      Integer*4 N, NN, IYY, IM, ID
!
! 1.2.6 SUBROUTINE INTERFACE -
!          CALLER SUBROUTINES: DRIVG
!          CALLED SUBROUTINES: JDY2K
!
! 1.2.7 CONSTANTS USED - SECDAY
!
! 1.2.9 PROGRAMMER - DALE MARKHAM  01/17/77
!                    PETER DENATALE 07/18/77
!                    JIM RYAN 09/14/81
!                         CHANGED TO COMPUTE XJD, RATHER THAN 'GET' IT.
!                    SAVITA GOEL 06/04/87 (CDS FOR A900)
!                    Jim Ryan 89.07.26 Documentation simplified.
!                    Jim Ryan 89.12.12 UNIX-like database interface
!                    implimented.
!                    D. Gordon 94.04.13 - Implicit none instituted. DFLOT
!                              changed to Fortran 77 DFLOTI.
!                    D. Gordon 98.07.29 - Convert to use JDY2K to convert year,
!                              month, day to Julian date. Year can be 2-digit
!                              or 4-digit.
!                    D. Gordon 98.11.04 Added GETI of 'UTC TAG4' (4-digit
!                              year). If not there (will not be there in Mark
!                              III system for a while) will get 'UTC TAG '
!                              (2-digit year) as before.
!                    Jim Ryan Sept 2002 Integer*2/4 mods.
!                    Jim Ryan 03.03.10 Kill replaced with terminate_solve
!                    D. Gordon Jan. 2013. Database GET's moved to sub. GET_G.
!
!     UTCTM Program Structure
!
!     GET the time tag information from the database.
!       ==> Moved to subroutine GET_G. 
!
!  Compute the Julian date at 0 hours UTC for the year, month, day.
!  Use function JDY2K to convert year, month, day to Julian date.
!  Year can be either 2-digit or 4-digit.
      IYY = ITAG(1)
      IM  = ITAG(2)
      ID  = ITAG(3)
      XJD = JDY2K(IYY,IM,ID)
!     write(6,'("UTCTM: ITAG(1), New XJD = ",i5,f12.2)') IYY,XJD
!
!     Compute the UTC time as a fraction of the UTC day.
      UTC = ( DFLOAT ( ITAG(4) ) * 3600.D0 &
     &      + DFLOAT ( ITAG(5) ) * 60.D0 &
     &      + TAGSEC ) / SECDAY
!
!     Check for debug.
      IF ( KUTCD .EQ. 0 )  GO TO 500
      WRITE ( 6, 9)
    9 FORMAT (1X, "Debug output for subroutine UTCTM." )
      WRITE(6,7)' ITAG    ',ITAG
    7 FORMAT(A,15I8/(9X,15I8))
      WRITE(6,8) 'SECDAY',SECDAY
    8 FORMAT(A,4D25.16/(7X,5D25.16))
      WRITE(6,8)' TAGSEC  ',TAGSEC
      WRITE ( 6, 9200 )  UTC, XJD
 9200 FORMAT (1X, "UTC  = ", D30.16, /, 1X, &
     &            "XJD  = ", D30.16 )
!
  500 RETURN
      END
