      SUBROUTINE ALL_ADDS()
      IMPLICIT None
!
!   ALL_ADDS
!    ALL_ADDS ADDS ENTRIES TO THE TABLE OF CONTENTS FOR ALL Modules.
!
!     Common blocks used:
      INCLUDE 'ccon.i'
!
      INCLUDE 'cuser11.i'
!          1. Calc_user - Analysis center or Correlator user
!
      INCLUDE 'input11.i'
!       Variables from:
!          1. Input_sites - T/F logical flag telling whether to use
!                           external site a priori input.
!          2. Input_stars - T/F logical flag telling whether to use
!                           external star (radio sources) a priori input.
!          3. Input_EOP   - T/F logical flag telling whether to use
!                           external EOP file input.
!          4. Input_Ocean - T/F logical flag telling whether to use
!                           Ocean loading from an external file.
!          5. Input_Tilts - T/F logical flag telling whether to use
!                           antenna tilt information from an external file.
!          6. Input_OPTL  - T/F logical flag telling whether to use
!                           Ocean pole tided loading from an external file.
! From SITA:
      INCLUDE 'cmxst11.i'
!       Variables from:
!          1. NUMSIT   - Number of sites in the data base.
!          2. Max_Stat - Maximun number of stations allowed.
!
! From STAA:
      Real*8    XCALC
      Integer*2 NFLAG,NFLAGC,loadm(8),LFILE(3)
      Character*64 Computing_center
      COMMON /STACM/ Computing_center,XCALC,NFLAG,NFLAGC,loadm,LFILE
!       Variables 'from' -
!          1. NFLAG - THE TOTAL NUMBER OF CALC FLOW CONTROL AND DEBUG FLAGS.
!       Variables 'to' -
!          1. NFLAGC - THE NUMBER OF CALC FLOW CONTROL FLAGS (= NFLAG/2).
!
! From UT1A:
      INCLUDE 'cmxut11.i'
!       Variables 'from':
!          1. Ndays - number of days in the ROTEPH array.
!       Variables 'to':
!          1. IEPOCH - The number of epochs at which TAI - UT1 is desired.
!          2. ASKKER - The database return code from ASK for 'UT1EPOCH'
!
      Integer*2 LTEXT(16)
      CHARACTER*32 LTEXT_chr
      EQUIVALENCE (LTEXT,LTEXT_chr)
      Integer*2 ND1, ND3, NVER, KTYP, Nut1, idm8
!       Program variables -
!          1. LTEXT(16) - A dummy array used to hold the message from ASK.
!
! From WOBA:
      INCLUDE 'cmwob11.i'
!       Variables 'to':
!          1. KERASK - The database error return code from the 'ASK'
!                      for the rotation epoch array.
!          2. NEPOCH - The number of epochs at which the interpolated
!                       polar motions are desired.
!       Variables 'from':
!          1. WOBIF(3) - The wobble information array. Contains
!                        respectively: 1) The Julian date of the first
!                        tabular point, 2) The increment in days of the
!                        tabular points, 3) The number of tabular points.
!                        (days, days, unitless)
!    Program Specifications -
!!    Integer*2 LTEXT(16), nd1, nd3, nver, ktype, nut1
!!    CHARACTER*32 LTEXT_chr
!!    EQUIVALENCE (LTEXT,LTEXT_chr)
!
!
! From STRA:
      INCLUDE 'cmxsr11.i'
!       Variables 'to':
!          1. NUMSTR - The number of stars (radio sources) in the Star
!                      catalog.
!
! From NUTA:
!?    Real*8 CENTJ, DJ2000, EC(4), ARGP(2,6)
!?    Integer*4 NOT, NOP, IDP(6)
!?    COMMON / NUTCM / CENTJ, DJ2000, EC, ARGP, NOT, NOP, IDP
!
!   DATA BASE ACCESS -
!    ACCESS CODES:
!        1. 'CALCFLGN' - THE DATA BASE ACCESS CODE FOR THE ARRAY OF 
!                        THE CALC FLOW CONTROL FLAG NAMES.
!        2. 'CALCFLGV' - THE DATA BASE ACCESS CODE FOR THE ARRAY OF
!                        CALC FLOW CONTROL FLAG VALUES.
!        3. 'CALC VER' - THE DATA BASE ACCESS CODE FOR THE CURRENT
!                        VERSION NUMBER OF PROGRAM CALC.
!
!        4. 'ATI MESS' - THE DATA BASE ACCESS CODE FOR THE
!                        ATIME UTILITY ROUTINE TEXT MESSAGE.
!        5. 'ATI CFLG' - THE DATA BASE ACCESS CODE FOR THE ATIME
!                        UTILITY ROUTINE FLOW CONTROL MESSAGE.
!
!        6. 'CTI MESS' - THE DATA BASE ACCESS CODE FOR THE CTIMG
!                        UTILITY ROUTINE TEXT MESSAGE.
!        7. 'CTI CFLG' - THE DATA BASE ACCESS CODE FOR THE CTIMG
!                        UTILITY ROUTINE FLOW CONTROL MESSAGE.
!        8. 'CT SITE1' - THE DATA BASE ACCESS CODE FOR THE CTIMG
!                        UTILITY ROUTINE CT TIME FRACTION AT SITE 1.
!
!        9. 'PEP MESS' - THE DATA BASE ACCESS CODE FOR THE
!                         PEP UTILITY ROUTINE TEXT MESSAGE.
!
!       10. 'NUT MESS' - THE DATA BASE ACCESS CODE FOR THE
!                        NUTATION MODULE TEXT MESSAGE.
!       11. 'NUT CFLG' - THE DATA BASE ACCESS CODE FOR THE
!                        NUTATION MODULE FLOW CONTROL MESSAGE.
!       12. 'NUT06XYS' - THE DATA BASE ACCESS CODE FOR THE
!                        CEO-based X,Y,S IAU2000/2006 Nutation/
!                        Precession values.
!       13. 'NUT06XYP' - THE DATA BASE ACCESS CODE FOR THE NUTATION
!                        MODULE X and Y PARTIAL DERIVATIVE ARRAY,
!                        based on the IAU2000/2006 models.
!      13.1 'NUT2006A' - The database access code for the IAU2006/2000
!                        Precession/Nutation classical nutation
!                        offsets.
!      13.2 'NUT WAHR' - The database access code for the Wahr
!                        model nutation values.
!
!       14. 'WOB MESS' - Wobble module message code.
!       15. 'WOB PART' - Wobble partials array code.
!       16. 'WOBXCONT' - X-Wobble contributions array code.
!       17. 'WOBYCONT' - Y-Wobble contributions array code.
!       18. 'WOB CFLG' - Wobble flow control message code.
!       19. 'POLAR XY' - Observation wobble values code.
!       20. 'ROTEPOCH' - Rotation epochs array code.
!       21. 'WOBEPOCH' - Interpolated wobble array code.
!       22. 'WOBINTRP' - Access code for polar motion interpolation
!                         message.
!
!       23. 'UT1 MESS' - Module text message access code.
!       24. 'UT1 PART' - The partial derivatives access code.
!       25. 'UT1 CFLG' - Module control flag access code.
!       26. 'UT1 -TAI' - Observation dependent UT1 access code.
!       27. 'UT1EPOCH' - TAI-UT1 array access code.
!       28. 'ROTEPOCH' - Access code for the array containing
!                        the epoch at which TAI-UT1 is desired.
!       29. 'UT1INTRP' - Access code for the type of interpolation
!                         used in UT1MU.
!
!       30. 'SIT MESS' - THE DATA BASE ACCESS CODE FOR THE
!                        SITE MODULE TEXT MESSAGE.
!       31. 'SIT PART' - THE DATA BASE ACCESS CODE FOR THE SITE
!                        MODULE TEXT MESSAGE.
!
!       32. 'STR MESS' - THE DATA BASE ACCESS CODE FOR THE
!                        STAR MODULE TEXT MESSAGE.
!       33. 'STR CFLG' - THE DATA BASE ACCESS CODE FOR THE
!                        STAR MODULE FLOW CONTROL MESSAGE.
!       34. 'STR PART' - THE DATA BASE ACCESS CODE FOR THE
!                        STAR MODULE PARTIAL DERIVATIVES ARRAY.
!       35. 'PRMOTION' - The data base access code for the proper
!                        motion array. First variable runs over
!                        RA velocity (arc-sec/year), Declination
!                        velocity (arc-sec/year), and epoch for
!                        which the J2000 coordinates in 'STAR2000'
!                        precessed to date are correct (epoch for
!                        which corrections should be zero). Second
!                        index runs over the sources, same order as
!                        in 'STRNAMES'. Zeros imply unknown.
!       36. 'DISTPSEC' - The data base access code for the source
!                        distance array. Units are parsecs; zero
!                        implies unknown.
!       37. 'PMOTNCON' - Proper motion contributions (sec, sec/sec).
!                        Add to normal delays and rates to correct for
!                        proper motions to date of observation. Exists
!                        only if KSTRC=1.
!       38. 'PMOT2CON' - Proper motion contributions (sec, sec/sec)
!                        to return to non-proper motion delays and
!                        rates. Exists only if KSTRC=2. Add to proper
!                        motion corrected delays and rates to return
!                        to the uncorrected values.
!       39. 'RADECADD' - Proper motion offsets (if proper motion turned
!                        on) in RA and Dec. Add to RA and Dec to get
!                        corrected values. Used if KSTRC = 1.
!       40. 'STARPRMO' - RA and Declinations after correcting for proper
!                        motion. Used if KSTRC = 2.
!
!       41. 'UVF/ASEC' - The data base access code for the
!                        U, V coordinates.
!       42. 'UVF/MHz ' - The correlator code for the
!                        U, V coordinates in fringes per arcsec,
!                        per MHz.
!
!       43. 'ETD MESS' - The database access code for the Earth Tide
!                        Module Text Message.
!       44. 'ETD CFLG' - The database access code for the Earth Tide
!                        Module flow control message.
!       45. 'ETD CONT' - The database access code for the Earth Tide
!                        Module contributions array.
!
!       46. 'OCE MESS' - THE DATA BASE ACCESS CODE FOR THE OCEAN
!                        LOADING MODULE TEXT MESSAGE.
!       47. 'OCE CONT' - THE DATA BASE ACCESS CODE FOR THE OCEAN
!                        LOADING CONTRIBUTIONS ARRAY.
!       48. 'OCE CFLG' - THE DATA BASE ACCESS CODE FOR THE OCEAN
!                        LOADING MODULE FLOW CONTROL MESSAGE.
!       49. 'OCE DELD' - THE DATA BASE ACCESS CODE FOR THE SITE
!                        DEPENDENT OCEAN LOADING DISPLACEMENTS AND
!                        VELOCITIES.
!       50. 'OCE HORZ  - THE DATABASE ACCESS CODE FOR THE SITE
!                        DEPENDENT CONTRIBUTIONS TO THE DELAY AND
!                        RATE FOR THE HORIZONTAL CORRECTIONS.
!       51. 'OCE VERT  - THE DATABASE ACCESS CODE FOR THE SITE
!                        DEPENDENT CONTRIBUTIONS TO THE DELAY AND
!                        RATE FOR THE VERTICAL CORRECTIONS.
!
!       52. 'PTD MESS' - THE DATA BASE ACCESS CODE FOR THE
!                        POLE TIDE MODULE TEXT MESSAGE.
!       53. 'PTD CFLG' - THE DATA BASE ACCESS CODE FOR THE POLE
!                        TIDE MODULE FLOW CONTROL MESSAGE.
!       54. 'PTD CONT' - THE DATA BASE ACCESS CODE FOR THE POLE
!                        TIDE MODULE CONTRIBUTIONS ARRAY.
!       55. 'PTDXYPAR' - The data base access code for the pole tide
!                        delay and rate partials w.r.t. X-pole and
!                        Y-pole.
!       56. 'PTOLDCON' - The data base access code for the contribution
!                        to restore the X_mean and Y_mean portion of
!                        the pole tide.
!
!       57. 'ATM MESS' - Access code for the atmosphere module message.
!       58. 'ATM CFLG' - Access code for ATM module control flag message.
!       59. 'EL-THEO ' - Access code for source elevation array.
!       60. 'AZ-THEO ' - Access code for the source azimuth array.
!       61. 'NDRYPART' - Access code for the NHMF2 (Niell) dry partial.
!       62. 'NWETPART' - Access code for the WHMF2 (Niell) wet partial.
!       63. 'NDRYCONT' - Access code for the NHMF2 (Niell) dry
!                        contributions.
!       64. 'NWETCONT' - Access code for the WHMF2 (Niell) wet
!                        contributions.
!       65. 'NGRADPAR' - Access code for the gradient partials using
!                        Niell dry scaling.
!
!       66. 'AXO MESS' - THE DATA BASE ACCESS CODE FOR THE
!                        AXIS OFFSET MODULE TEXT MESSAGE.
!       67. 'AXO CFLG' - THE DATA BASE ACCESS CODE FOR THE AXIS
!                        OFFSET CONTROL FLAG MESSAGE.
!       68. 'AXO PART' - THE DATA BASE ACCESS CODE FOR THE AXIS
!                        OFFSET MODULE PARTIAL DERIVATIVES ARRAY.
!                        (Using new, simple axis offset model)
!       69. 'AXO CONT' - The database access code for the new axis
!                        offset module contributions array.
!                        (Using the new, simple axis offset model.)
!       70. 'PAN MESS' - The database access code for the
!                        feedhorn rotation routine text message.
!       71. 'PARANGLE' - The database access code for the
!                        feedhorn rotation angles.
!       72. 'FEED.COR' - The database access code for the feedbox
!                        rotation corrections for group delay and
!                        phase delay rate.
!
!       73. 'CF2J2000' - The data base access code for the complete crust
!                        fixed to J2000 rotation matrix and its first two
!                        time derivatives.
!
!       74. 'PLX MESS' - THE DATA BASE ACCESS CODE FOR THE
!                        PARALLAX MODULE TEXT MESSAGE.
!       75. 'PLX PART' - THE DATA BASE ACCESS CODE FOR THE PARALLAX
!                        MODULE PARTIAL DERIVATIVES ARRAY.
!       76. 'PLX CFLG' - THE DATA BASE ACCESS CODE FOR THE
!                        PARALLAX MODULE FLOW CONTROL MESSAGE.
!       77. 'PLX1PSEC' - New with Calc 9.x. Has two possible usages.
!                        1) Parallax delay and rate contributions for
!                        the source at 1 parsec distance. Divide by
!                        actual distance (parsecs) to get delay and
!                        rate corrections. 2) Partial derivatives of
!                        delay and rate w.r.t. inverse distance in
!                        parsecs. Use to solve for inverse distance.
!       78. 'PRLXCONT' - Optional/New with Calc 9.x. Delay and rate
!                        contributions for parallax (sec, sec/sec).
!                        Computed only if KPLXC=1. If distance not
!                        supplied, these will be zeros. Intended for
!                        correlator usage.
!
!       79. 'THE MESS' - The database access code for the Theory routine
!                        text message.
!       80. 'CONSNDEL' - The database access code for the VLBI delay
!                        using the Eubanks' Consensus model.
!       81. 'CONSNRAT' - The database access code for the VLBI delay
!                        rate using the Eubanks' Consensus model.
!       82. 'CONSPART' - Database access code for partial derivatives
!                        of the Consensus model delays and rates with
!                        respect to Gamma.
!       83. 'CON CONT' - Database access code for the TOTAL relativistic
!                        bending contributions based on the Consensus
!                        model.
!       84. 'SUN CONT' - Database access code for the Sun's relativistic
!                        bending contributions based on the Consensus
!                        model.
!       85. 'BENDPART' - Database access code for partial derivatives
!                        of the Consensus gravitational bending portion
!                        of the delays and rates with respect to Gamma.
!
! 1.2.6 SUBROUTINE INTERFACE -
!             CALLER SUBROUTINES: TOCUP
!             CALLED SUBROUTINES: ADDA
! 1.2.9 PROGRAMMER -  D. Gordon, Dec. 2012, All database 'ADD' suroutines
!                     consolidated into one subroutine.
!                     ALL_ADDS = ATIMA + ATMA + AXOA + CTIMA + ETDA +
!                                M2000A + NUTA + OCEA + PEPA + PLXA +
!                                PTDA + SITA + STRA +  THERA + UT1A +
!                                UVA + WOBA + STAA
!                     D. Gordon, 2014 Jan. 8, Adding 'NUT2006A' access code
!                                for classical nutation values.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ADDS for the start module (STAA):
!     Compute NFLAGC
      NFLAGC = NFLAG / 2
!
!   Do the ADD for the flag names
      CALL ADDA (int2(1),'CALCFLGN','CALC flow control flags name def', &
     & int2(2), NFLAGC, int2(1))
!
!   Do the ADD for the flag values
      CALL ADDI (int2(1),'CALCFLGV','CALC flow control flags valu def', &
     & NFLAGC, int2(1), int2(1))
!
!   Do the ADD for the CALC version number
      CALL ADDR (int2(1),'CALC VER','CALC version number             ', &
     & int2(1), int2(1), int2(1))
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ADDS for the atomic time module (ATMA):
!     ADD for ATIME utility text message.
      CALL ADDA (int2(1),'ATI MESS','ATIME Message Definition        ', &
     & int2(40), int2(1), int2(1))
!     ADD for ATIME utility flow control message.
      CALL ADDA (int2(1),'ATI CFLG','ATIME Flow Control Message Def. ', &
     & int2(40), int2(1), int2(1))
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ADDS for the coordinate time module (CTIMA):
!   ADD for the CTIMG utility text message.
      CALL ADDA (int2(1),'CTI MESS','CTIMG Message Definition        ', &
     & int2(40), int2(1), int2(1))
!   ADD for the CTIMG flow contrl message.
      CALL ADDA (int2(1),'CTI CFLG','CTIMG Flow Control Message Der  ', &
     & int2(40), int2(1), int2(1))
!   ADD for putting the CT fraction of the day into the database.
      CALL ADDR (int2(2),'CT SITE1','CT fraction at obs site 1       ', &
     & int2(1), int2(1), int2(1))
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ADDS for the planetary ephemeris module (PEPA):
!     ADD for the PEP utility text message.
      CALL ADDA (int2(1),'PEP MESS','PEP Utility Message Definition  ', &
     & int2(40), int2(1), int2(1))
!
!    ADD's to insert/replace Earth, Sun, Moon coordinates
      CALL ADDR (int2(2),'EARTH CE','Earth barycentric coordinates...', &
     & int2(3), int2(3), int2(1))
      CALL ADDR (int2(2),'SUN DATA','Solar geocentric coordinates....', &
     & int2(3), int2(2), int2(1))
      CALL ADDR (int2(2),'MOONDATA','Lunar geocentric coordinates....', &
     & int2(3), int2(2), int2(1))
!
!   Remove obsolete Lcode
       CALL DELA (int2(1), 'PEP TAPE')
!      CALL DELA (int2(1), 'PEP 2000')
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ADDS for the Nutation/Precession module (NUTA):
!   ADD for the module text message.
      CALL ADDA (int2(1),'NUT MESS','Nutation message definition     ', &
     & int2(40), int2(1), int2(1))
!   ADD for  module flow control message.
      CALL ADDA (int2(1),'NUT CFLG','Nutation flow control mess def. ', &
     & int2(40), int2(1), int2(1))
!
!   Nutation from data base no longer used, remove old Lcodes
      CALL DELR (int2(2), 'DPSI    ')
      CALL DELR (int2(2), 'DEPS    ')
      CALL DELR (int2(2), 'FUNDARGS')
      CALL DELR (int2(2), 'NUT PART')
      CALL DELR (int2(2), 'NUT 1996')
      CALL DELR (int2(2), 'GDNUTCON')
!
      CALL DELR (int2(2), 'NUT2000A')
!     CALL DELR (int2(2), 'NUT WAHR')
      CALL DELR (int2(2), 'WAHRCONT')
      CALL DELR (int2(2), 'NUT2000P')
      CALL DELR (int2(2), 'NUT2KXYS')
      CALL DELR (int2(2), 'NUT2KXYP')
!
!  Remove the old precession Lcodes. There is no longer a separate 
!   precession module. 2012-Nov-08
      CALL DELR (int2(1), 'PRE MESS')
      CALL DELR (int2(1), 'PRE CFLG')
      CALL DELR (int2(2), 'PRE PART')
!
!  Lcodes for the nutation values
      IF ( KNUTC .eq. 0) then
!   IAU2000A/2006A Nutation/Precession:
       CALL ADDR(int2(2),'NUT06XYS','2000/2006 Nut/Prec X,Y,S & Rates', &
     &  int2(3), int2(2), int2(1))
!   IAU2000A/2006A Nutation/Precession partials w.r.t. X and Y.
       CALL ADDR(int2(2),'NUT06XYP','2000/2006 Nut/Prec X,Y Partials ', &
     &  int2(2), int2(2), int2(1))
!
! New, added 2014-Jan-09.
!   IAU2000A/2006A Nutation/Precession classical values:
       CALL ADDR(int2(2),'NUT2006A','2000/2006 Nut/Prec psi/epsilon  ', &
     &  int2(2), int2(2), int2(1))
!   Wahr model classical nutation values:
       CALL ADDR (int2(2),'NUT WAHR','Wahr nut vals  - Dpsi,Deps&rates', &
     &  int2(2), int2(2), int2(1))
      Endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ADDS for the polar motion (obble) module (WOBA):
!    ADD for wobble module text message.
       CALL ADDA(int2(1),'WOB MESS','Wobble message definition.      ', &
     & int2(40),int2(1),int2(1))
!
!    Add for wobble interpolation scheme message.
       Call ADDA(int2(1),'WOBINTRP','Interp. scheme for polar motion.', &
     & int2(30),int2(1),int2(1))
!
!    ADD for wobble module flow control message.
      CALL ADDA (int2(1),'WOB CFLG','Wobble flow control mess def.   ', &
     & int2(40),int2(1),int2(1))
!
!    ADD for wobble partials.
      CALL ADDR (int2(2),'WOB PART','Wobble partial derivatives def. ', &
     & int2(2),int2(2),int2(1))
!
!    ADD's for wobble contributions.
      CALL ADDR (int2(2),'WOBXCONT','X Wobble contribution definition', &
     & int2(2),int2(1),int2(1))
      CALL ADDR (int2(2),'WOBYCONT','Y Wobble contribution definition', &
     & int2(2),int2(1),int2(1))
!
!    ADDS for short period (nutation and tidal) wobble contributions.
      CALL ADDR (int2(2),'WOBLIBRA','High freq libration wobble contr', &
     & int2(2),int2(1),int2(1))
      CALL ADDR (int2(2),'WOBORTHO','ORTHO_EOP tidal wobble contribtn', &
     & int2(2),int2(1),int2(1))
!
!   Delete old X and Y contributions from pre-Calc 8.0 databases.
      CALL DELR (int2(2), 'WOB CONT')
      CALL DELR (int2(2), 'WOBNUTAT')
!
!    ADD for X and Y wobble values.
      CALL ADDR (int2(2),'POLAR XY','Polar motion X & Y for obs (rad)', &
     & int2(2),int2(1),int2(1))
!
!**   IF (.not. Input_EOP) THEN       ! Skip check if external EOP input
!       Check to see if there is a rotation epoch array in the database.
        CALL ASK ('ROTEPOCH',int2(1),ND1,NEPOCH,ND3,NVER,LTEXT_chr, &
     &   KTYP,KERASK)
!**   ELSE
!**      KERASK = 0
!**   ENDIF                           ! Skip check if external EOP input
!
!     If there is no rotation epoch array in the database,
!     skip on.  Otherwise ADD for interpolated wobble values.
      IF (KERASK .NE. 0) GO TO 100
      CALL ADDR (int2(1),'WOBEPOCH','Interpolated wobble array def   ', &
     & int2(2),NEPOCH,int2(1))
  100 CONTINUE
!
!  ADD's for optional EOP input via external file
      IF (Input_EOP) THEN
        nut1 = WOBIF(3) + 0.01
       CALL ADDR(int2(1),'FWOB INF','Final Value wobble array descr. ', &
     &  int2(3),int2(1),int2(1))
       CALL ADDR(int2(1),'FWOBX&YT','Final wobble X,Y component value', &
     &  int2(2),nut1,int2(1))
       CALL ADDA(int2(1),'FWOBTEXT','Final Value wobble origin text. ', &
     &  int2(40),int2(1),int2(1))
!! Added 2014-May-15
      CALL ADDR (int2(1),'WOBEPOCH','Interpolated wobble array def   ', &
     & int2(2),ndays ,int2(1))
!
       CALL DELR (int2(1), 'PWOB INF')
       CALL DELR (int2(1), 'PWOBX&YT')
       CALL DELA (int2(1), 'PWOBTEXT')
       CALL DELR (int2(1), 'EWOB INF')
       CALL DELR (int2(1), 'EWOBX&YT')
       CALL DELA (int2(1), 'EWOBTEXT')
      ENDIF
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ADDS for the UT1 module (UT1A):
!     ADD for the UT1 module text message.
      CALL ADDA (int2(1),'UT1 MESS','UT1 Module message definition   ', &
     & int2(40), int2(1), int2(1))
!
!     ADD for the UT1 partial derivatives.
      CALL ADDR (int2(2),'UT1 PART','UT1 partial derivatives def.    ', &
     & int2(2), int2(2), int2(1))
!
!     ADD for the UT1 module flow control message.
      CALL ADDA (int2(1),'UT1 CFLG','UT1 control flag message def.   ', &
     & int2(40), int2(1), int2(1))
!
!     ADD for the UT1 values actually used for each observation.
      CALL ADDR (int2(2),'UT1 -TAI','UT1 time of day for this obsvr. ', &
     & int2(1), int2(1), int2(1))
!
!     ASK in order to obtain the number of rotation epochs for UT1.
      CALL ASK('ROTEPOCH',int2(1),ND1,IEPOCH,ND3,NVER,LTEXT_chr,KTYP,   &
     & ASKKER)
!
!     If there already is an array of epochs in the database, call 'ADDR' so
!     that we may place the values in later. Otherwise skip this step.
      IF (ASKKER .NE. 0) GO TO 101
      CALL ADDR (int2(1),'UT1EPOCH','TAI - UT1 epoch value definition', &
     & int2(2), IEPOCH, int2(1))
  101 CONTINUE
!
      Call ADDA (int2(1),'UT1INTRP','Message for UT1 interp. scheme  ', &
     & int2(30), int2(1), int2(1))
!
!     ADD for the UT1 ocean tidal contribution.
      CALL ADDR (int2(2),'UT1ORTHO','ORTHO_EOP Tidal UT1 contribution', &
     & int2(2),int2(1),int2(1))
!
!     ADD for the UT1 libration contribution.
      CALL ADDR (int2(2),'UT1LIBRA','UT1 Libration contribution.     ', &
     & int2(2),int2(1),int2(1))
!
!------------------------------------------------------------------
!  ADD's for optional EOP input via external file
      If (Input_EOP) Then
!
! ADD's for 'TIDALUT1', 'FUT1 INF', 'FUT1 PTS', and 'EOPSCALE'.
! DEL's for 'PUT1 INF', 'EUT1 INF', 'PUT1 PTS', and 'EUT1 PTS'.
!
      CALL ADDI (int2(1),'TIDALUT1','Flag for tidal terms in UT1 sers', &
     & int2(1), int2(1), int2(1))
!
      CALL ADDR (int2(1),'FUT1 INF','Final Value TAI-UT1 array descrp', &
     & int2(4), int2(1), int2(1))
       Nut1 = UT1IF(3) + .01
      CALL ADDR (int2(1),'FUT1 PTS','Final Value TAI-UT1 data points.', &
     & Nut1, int2(1), int2(1))
!
      CALL ADDR (int2(1),'A1 - UTC','FJD, A1-UTC (sec),rate off.(sec)', &
     & int2(3), int2(1), int2(1))
!
!   'TAI- UTC' used in catiu.f
      CALL ADDR (int2(1),'TAI- UTC','FJD,TAI-UTC (sec),rate off.(sec)', &
     & int2(3), int2(1), int2(1))
!
!   'AI - TAI' used in cctiu.f
      CALL ADDR (int2(1),'A1 - TAI','TAI USNO- TAI BIH = 0.03439 sec.', &
     & int2(3), int2(1), int2(1))
!      Ndays from Subroutine START and cmxut.i
      CALL ADDR (int2(1),'ROTEPOCH','UT1,pole interp.reference epochs', &
     & int2(2), ndays, int2(1))
      CALL ADDR (int2(1),'UT1EPOCH','TAI - UT1 epoch value definition', &
     & int2(2), ndays, int2(1))
!
      CALL ADDA (int2(1),'FUT1TEXT','Final Value TAI-UT1 origin text.', &
     & int2(40), int2(1), int2(1))
      CALL ADDA (int2(1),'EOPSCALE','Time scale of EOP table epochs. ', &
     & int2(4), int2(1), int2(1))
!
      CALL DELR (int2(1), 'PUT1 PTS')
      CALL DELR (int2(1), 'PUT1 INF')
!     CALL DELR (int2(1), 'UT1 PART')
      CALL DELA (int2(1), 'PUT1TEXT')
      CALL DELR (int2(1), 'EUT1 PTS')
      CALL DELR (int2(1), 'EUT1 INF')
      CALL DELA (int2(1), 'EUT1TEXT')
!
      Endif
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ADDS for the site module (SITA):
!     ADD for module text message.
      CALL ADDA (int2(1),'SIT MESS','Site Module Message Definition  ', &
     & int2(40), int2(1), int2(1))
!
!     ADD for Site module partials.
      CALL ADDR (int2(2),'SIT PART','Site partial derivative def.    ', &
     & int2(3), int2(2), int2(2))
!
!   Do adds to replace the site a priori's in the data base in the case of
!    external file inputs
      If (Input_sites) Then
       CALL ADDR(int2(1),'SITERECS','Site cartesian coords (m).      ', &
     &  int2(3), Numsit, int2(1))
       CALL ADDR(int2(1),'SITEZENS','Site zenith path delays (nsec). ', &
     &  Numsit, int2(1), int2(1))
       CALL ADDA(int2(1),'TECTPLNM','4-char tectonic plate names.    ', &
     &  int2(2), Numsit, int2(1))
       CALL ADDI(int2(1),'AXISTYPS','Axis type (1-eq,2-xy,3-azel,4,5)', &
     &  Numsit, int2(1), int2(1))
       CALL ADDR(int2(1),'AXISOFFS','Axis offsets (m).               ', &
     &  Numsit, int2(1), int2(1))
      Endif
!
!   Do adds to replace the ocean loading coefficients in the data base in
!    the case of external file inputs
      If (Input_ocean) Then
       CALL ADDR(int2(1),'SITOCAMP','Vert ocean loading ampltudes (m)', &
     &  int2(11), Numsit, int2(1))
       CALL ADDR(int2(1),'SITOCPHS','Vert ocean loading phases (rad).', &
     &  int2(11), Numsit, int2(1))
       CALL ADDR(int2(1),'SITHOCAM','Horz ocean loading ampltudes (m)', &
     &  int2(11), int2(2), Numsit)
       CALL ADDR(int2(1),'SITHOCPH','Horz ocean loading phases (rad).', &
     &  int2(11), int2(2), Numsit)
       CALL ADDA(int2(1),'OCE STAT','Ocean loading station status.   ', &
     &  int2(2), Numsit, int2(1))
      Endif
!
!   Do adds to create/replace the axis tilts in the data base in the case of
!    external file inputs
      If (Input_tilts) Then
       CALL ADDR(int2(1),'AXISTILT','Fixed axis tilts (arc-sec).     ', &
     &  int2(2), Numsit, int2(1))
      Endif
!
!   Do adds to create/replace the ocean pole tide loading coefficients in
!    the database in the case of external file inputs
      If (Input_OPTL ) Then
       CALL ADDR(int2(1),'OPTLCOEF','Ocean pole tide loading coefs.  ', &
     &  int2(6), Numsit, int2(1))
      Endif
!
! Do add for modified/unmodified site positions for correlator usage.
!  Correlator users must capture this access code for downstream analysis
!  (in AIPS, HOPS, etc.) (in place of 'SITERECS') if they use the 'SITERECV'
!  access code for site input. The number of stations will probably not be
!  known at this time. To avoid unnecessary complications, we allow for the
!  maximum number of stations, as defined in 'cmxst.i'.
      If (calc_user .eq. 'C') Then
       CALL ADDR(int2(1),'SITEXYZS','Site Coords To Date (meters)    ', &
     &  int2(3), Max_Stat, int2(1))
      Endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ADDS for the Star module (STRA):
!     ADD for the Star Module text message.
      CALL ADDA (int2(1),'STR MESS','Star module message definition  ', &
     & int2(40), int2(1), int2(1))
!
!   ADD for the Star Module flow control message.
      CALL ADDA (int2(1),'STR CFLG','Parallax flow control mess def  ', &
     & int2(40), int2(1), int2(1))
!
!     ADD for the STAR Module partial derivatives.
      CALL ADDR (int2(2),'STR PART','Star partial derivatives def.   ', &
     & int2(2), int2(2), int2(1))
!
!   Do ADDR to replace the source positions in the data base in the case of
!    external file input
      IF (Input_stars) THEN
       CALL ADDR(int2(1),'STAR2000','J2000 source RAs, decs (rd,rd). ', &
     &  int2(2), NUMSTR, int2(1))
       CALL ADDA(int2(1),'STAR REF','Source of coordinate values.    ', &
     &  int2(10), NUMSTR, int2(1))
!
       IF (KSTRC.eq.1 .or. KSTRC.eq.2) CALL ADDR(int2(1), 'PRMOTION', &
     &  'Proper motions/Epoch (a-sec, yr)', int2(3), NUMSTR, int2(1))
!
       IF (KPLXC.eq.1) CALL ADDR(int2(1), 'DISTPSEC', &
     &  'Source Distances (parsecs)      ', NUMSTR, int2(1), int2(1))
      ENDIF
!
       IF (KSTRC.eq.0) Then
        CALL DELR( int2(2), 'PMOTNCON')
        CALL DELR( int2(2), 'PMOT2CON')
       ENDIF
!
       If (KSTRC.eq.1) Then
        CALL ADDR(int2(1),'RADECADD','Add to RA/Dec for proper motion ', &
     &   int2(2), Numstr, int2(1))
        CALL ADDR(int2(2),'PMOTNCON','Proper Motion contribs, sec, s/s', &
     &   int2(2), int2(1), int2(1))
        CALL DELR( int2(1), 'STARPRMO')
        CALL DELR( int2(2), 'PMOT2CON')
       Endif
!
       If (KSTRC.eq.2) Then
        CALL ADDR(int2(1),'STARPRMO','Proper motion corrected RA/Dec  ', &
     &   int2(2), Numstr, int2(1))
        CALL ADDR(int2(2),'PMOT2CON','Proper Motion Remover,  sec, s/s', &
     &   int2(2), int2(1), int2(1))
        CALL DELR( int2(1), 'RADECADD')
        CALL DELR( int2(2), 'PMOTNCON')
       Endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ADDS for the U/V coordinates module (STRA):
!   ADD for U,V coordinates
      If (KASTC .eq. 0) Then
       If (Calc_user .eq. 'A') CALL ADDR (int2(2),'UVF/ASEC',           &
     &  'U,V in FR per arcsec, from CALC ',int2(2),int2(1),int2(1))
       If (Calc_user .eq. 'C') CALL ADDR (int2(2),'UVF/MHz ',           &
     &  'U,V in FR per arcsec per MHz    ',int2(2),int2(1),int2(1))
      Endif
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ADDS for the Earth tide module (ETDA):
!   ADD for Earth Tide module text message.
      CALL ADDA(int2(1),'ETD MESS', 'Earth Tide message definition   ', &
     & int2(40), int2(1), int2(1))
!   ADD for Earth Tide module flow control message.
      CALL ADDA(int2(1),'ETD CFLG', 'Earth Tide flow control mess def', &
     & int2(40), int2(1), int2(1))
!   ADD for Earth Tide contributions.
      CALL ADDR(int2(2),'ETD CONT', 'Earth tide contributions def.   ', &
     & int2(2), int2(1), int2(1))
      IF (Calc_user .eq. 'A') THEN
!   Deletes for obsolete Calc 9.x Lcodes.
        CALL DELR (int2(2), 'ETJMGPAR')
        CALL DELR (int2(2), 'C82ETCON')
        CALL DELR (int2(2), 'PERMDEF ')
        CALL DELR (int2(2), 'ELASTCON')
!   Deletes for obsolete Calc 8.x Lcodes.
        CALL DELR (int2(2), 'ETD PART')
        CALL DELR (int2(2), 'ETD3CONT')
        CALL DELR (int2(2), 'ETDKCONT')
        CALL DELR (int2(2), 'ETD2CONT')
      ENDIF
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ADDS for the ocean loading module (OCEA):
!   Add for ocean loading module text message.
      CALL ADDA (int2(1),'OCE MESS','Ocean loading message definition', &
     & int2(40),int2(1),int2(1))
!
!   Add for module flow control message.
      CALL ADDA (int2(1),'OCE CFLG','Ocean load flow control mess def', &
     & int2(40),int2(1),int2(1))
!
!   Add for module contributions array. 
!   This is the contribution from the Hardisp model.
      CALL ADDR (int2(2),'OCE CONT','Ocean loading contributions def.', &
     & int2(2),int2(1),int2(1))
!   This is the contribution from the old ocean loading model.
      CALL ADDR (int2(2),'OCE_OLD ','Old Ocean loading contribution. ', &
     & int2(2),int2(1),int2(1))
!
!   Add for site depending displacement and velocity array.
      CALL ADDR (int2(2),'OCE DELD','Ocean load site depndnt displace', &
     & int2(3),int2(2),int2(2))
!
!   Add for site dependent contribution to the delay and rate for the
!   horizontal corrections.
      CALL ADDR (int2(2),'OCE HORZ','Site-dep ocean cont - horizontal', &
     & int2(2),int2(2),int2(1))
!
!   Add for site dependent contribution to the delay and rate for the vertical
!   corrections.
      CALL ADDR (int2(2),'OCE VERT','Site-dep ocean cont - vertical  ', &
     & int2(2),int2(2),int2(1))
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ADDS for pole tide and ocean pole tide  module (PTDA):
!     ADD for pole tide module text message.
      CALL ADDA (int2(1),'PTD MESS','Pole tide message definition    ', &
     & int2(40), int2(1), int2(1))
!
!     ADD for pole tide module flow control flag status.
      CALL ADDA (int2(1),'PTD CFLG','Pole tide flow control mess def ', &
     & int2(40), int2(1), int2(1))
!
!     ADD for pole tide contributions.
      CALL ADDR (int2(2),'PTD CONT','Pole tide contributions def.    ', &
     & int2(2), int2(1), int2(1))
!
!     ADD for pole tide delay and rate partials w.r.t. X-wobble and Y-wobble.
      CALL ADDR (int2(2),'PTDXYPAR','Pole Tide Partials w.r.t. X & Y ', &
     & int2(2), int2(2), int2(1))
!
!     ADD for old pole tide restorer contributions.
      CALL ADDR (int2(2),'PTOLDCON','Old Pole Tide Restorer Contrib. ', &
     & int2(2), int2(1), int2(1))
!
!     ADD for ocean pole tide contributions.
      CALL ADDR (int2(2),'OPTLCONT','Ocean Pole Tide Load Contributn ', &
     & int2(2), int2(1), int2(1))
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ADDS for the atmosphere module (ATMA):
!     ADD for Atmosphere module test message.
      CALL ADDA (int2(1),'ATM MESS','Atmosphere message definition   ', &
     & int2(40), int2(1), int2(1))
!     ADD for Atmosphere module flow control message.
      CALL ADDA (int2(1),'ATM CFLG','Atmosphere control flag mess def', &
     & int2(40), int2(1), int2(1))
!     ADD for Niell Dry (NHMF2) partial derivatives.
      CALL ADDR (int2(2),'NDRYPART','Nhmf2 dry partial deriv. def.   ', &
     & int2(2), int2(2), int2(1))
!     ADD for Niell Wet (WHMF2) partial derivatives.
      CALL ADDR (int2(2),'NWETPART','Whmf2 wet partial deriv. def.   ', &
     & int2(2), int2(2), int2(1))
!     ADD for Niell Dry (NHMF2) contributions.
      CALL ADDR (int2(2),'NDRYCONT','Nhmf (dry) atm. contribution    ', &
     & int2(2), int2(2), int2(1))
!     ADD for Niell Wet (WHMF2) contributions.
      CALL ADDR (int2(2),'NWETCONT','Whmf (wet) atm. contribution    ', &
     & int2(2), int2(2), int2(1))
!     ADD for Niell Dry atmosphere gradient partials.
      CALL ADDR (int2(2),'NGRADPAR','Niell dry atm. gradient partials', &
     & int2(2), int2(2), int2(2))
!     ADD for source elevation array.
      CALL ADDR (int2(2),'EL-THEO ','Elevation array definition      ', &
     & int2(2), int2(2), int2(1))
!     ADD for source azimuth array.
      CALL ADDR (int2(2),'AZ-THEO ','Azimuth array definition        ', &
     & int2(2), int2(2), int2(1))
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ADDS for the axis offset module (AXOA):
!   ADD for axis offset module text message.
      CALL ADDA (int2(1),'AXO MESS','Axis Offset Message Definition  ', &
     & int2(40), int2(1), int2(1))
!   ADD for axis offset module control flag message.
      CALL ADDA (int2(1),'AXO CFLG','Axis Offset Control flag mes def', &
     & int2(40), int2(1), int2(1))
!   ADD for axis offset partial derivatives.
      CALL ADDR (int2(2),'AXO PART','Axis Offset partial deriv. def. ', &
     & int2(2), int2(2), int2(1))
!   ADD for new axis offset contributions (one-term formula).
      CALL ADDR (int2(2),'AXO CONT','New Axis Offset Contributions   ', &
     & int2(2), int2(2), int2(1))
!   Delete for incorrect Calc 9 contribution
       CALL DELR ( int2(2), 'AXO2CONT')
!     ADDA for feedbox rotation (paralactic angle) text message.
      CALL ADDA (int2(1),'PAN MESS','Feedhorn rot. angle mod. ident. ', &
     & int2(40), int2(1), int2(1))
!     ADD for feedbox rotation angle (paralactic angle).
      CALL ADDR (int2(2),'PARANGLE','Feedhorn rot. angle, ref and rem', &
     & int2(2), int2(1), int2(1))
!     ADD for feedbox corrections for standard observables.
      CALL ADDR (int2(2),'FEED.COR','Feedhorn corr. in CORFIL scheme ', &
     & int2(2), int2(1), int2(1))
!     ADD for corrections to remove fixed axis tilt corrections.
      CALL ADDR (int2(2),'TILTRMVR','Axis Tilt Contribution Remover  ', &
     & int2(2), int2(1), int2(1))
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!   Deletes for the diurnal spin module (DIRNA)
        CALL DELR (int2(2), 'EQE DIFF')
        CALL DELR (int2(2), 'EQE CONT')
        CALL DELR (int2(2), 'OLDEQCON')
        CALL DELR (int2(2), 'NEWEQCON')
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!  ADD's for J2000 rotation matrix (M2000A):
!  Remove previous CRF=>TRF rotation matrix.
        CALL DELR ( int2(2), 'CF2J2000')
!     ADD for Crust-fixed-to-J2000 rotation matrix. IERS (2003) version.
      CALL ADDR (int2(2),'CF2J2K  ','Crust-fixed-to-J2000 Rot. Matrix', &
     &           int2(3), int2(3), int2(3))
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!  ADD's for the parallax module (PLXA):
!   ADD for the module text message.
      CALL ADDA (int2(1),'PLX MESS','Parallax message definition     ', &
     & int2(40), int2(1), int2(1))
!
!   ADD for module flow control message.
      CALL ADDA (int2(1),'PLX CFLG','Parallax flow control mess def  ', &
     & int2(40), int2(1), int2(1))
!
!   ADD for parallax partial derivatives. (Old partial, for annual parallax
!        of 1 radian.)
      CALL ADDR (int2(2),'PLX PART','Parallax partial deriv. def.    ', &
     & int2(2), int2(1), int2(1))
!
!   New ADD for parallax partials. Use to solve for inverse distance (parsecs)
!     or divide by distance (in parsecs) to get delay and rate corrections.
      CALL ADDR (int2(2),'PLX1PSEC','Parallax partial/contr, 1 parsec', &
     & int2(2), int2(1), int2(1))
!
!   Optional add for total parallax contributions.
      If (KPLXC .eq. 1) CALL ADDR (int2(2), 'PRLXCONT', &
     & 'Parallax Contributions, sec, s/s', int2(2), int2(1), int2(1))
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!  ADD's for the theory module (THERA):
!   Do ADD for THEORY routine text message.
      CALL ADDA (int2(1),'THE MESS','Theory module identification    ', &
     & int2(40), int2(1), int2(1))
!
!   Remove old Shapiro model Lcodes.
      CALL DELR (int2(2), 'SHAP DEL')
      CALL DELR (int2(2), 'SHAP RAT')
      CALL DELR (int2(2), 'SHAP T62')
      CALL DELR (int2(2), 'SHAPCONT')
      CALL DELR (int2(2), 'REL PART')
      CALL DELR (int2(2), 'REL CONT')
!
!   Remove old Hellings model Lcodes.
      CALL DELR (int2(2), 'HELL DEL')
      CALL DELR (int2(2), 'HELL RAT')
      CALL DELR (int2(2), 'HELL EMS')
!
!   Do ADD's for the Eubanks' Consensus model information. 07/06/93
      CALL ADDR (int2(2),'CONSNDEL','Consensus theo. delay (microsec)', &
     &     int2(2), int2(1), int2(1))
      CALL ADDR (int2(2),'CONSNRAT','Consensus theo. rate (sec/sec)  ', &
     &     int2(1), int2(1), int2(1))
!   Remove old Consensus correction to Hellings
      CALL DELR (int2(2), 'CONSCONT')
!
!   Do DEL's to delete any vestige of the old (incorrect) Shapiro relativistic
!   delay correction. Also delete the old Robertson delay and rate theoretical
!   lcodes. Also delete 'CON PART' from Calc 8.1 databases.
      CALL DELA (int2(1), 'REL MESS')
      CALL DELA (int2(1), 'RGD MESS')
      CALL DELR (int2(2), 'RGD CONT')
      CALL DELR (int2(2), 'THODELAY')
      CALL DELR (int2(2), 'THODRATE')
      CALL DELR (int2(2), 'CON PART')
!
!   Do the Add's for the relativity partials, contributions, and
!   relativity application status.
      CALL ADDA (int2(1),'REL CFLG','Relativisitc bending use status ', &
     & int2(30), int2(1), int2(1))
      CALL ADDR (int2(2),'CONSPART','Consensus partial w.r.t. Gamma  ', &
     & int2(2), int2(1), int2(1))
      CALL ADDR (int2(2),'CON CONT','Consensus bending contrib. (sec)', &
     & int2(2), int2(1), int2(1))
      CALL ADDR (int2(2),'SUN CONT','Consensus bending contrib. (sec)', &
     & int2(2), int2(1), int2(1))
      CALL ADDR (int2(2),'SUN2CONT','High order bending contrib.(sec)', &
     & int2(2), int2(1), int2(1))
      CALL ADDR (int2(2),'BENDPART','Grav. bend. partial w.r.t. Gamma', &
     & int2(2), int2(1), int2(1))
!
!     Normal Program Conclusion.
      RETURN
      END
!
!*********************************************************************!
      SUBROUTINE GET_G()
      IMPLICIT None
!
!     Common blocks used:
!
      INCLUDE 'cuser11.i'
!       Variables to:
!          1. Calc_user -
!          2. C_mode    -
!
      INCLUDE 'get2s.i'
!       Variables to:
!          1. LNBASE(4,2) - THE EIGHT CHARACTER SITE NAMES OF THE BASELINE
!                           OF THE CURRENT OBSERVATION. (ALPHAMERIC)
!          2. ITAG(5)     - THE ARRAY CONTAINING THE YEAR/MONTH/DAY/HOUR/MINUTE
!                           PORTION OF THE OBSERVATION TIME TAG.
!          3. TAGSEC      - THE SECONDS PORTION OF THE OBSERVATION TIME TAG.
!          4. LSTRNM(4)   - THE EIGHT ALPHAMERIC CHARACTER STAR NAME FOR THE
!                           CURRENT OBSERVATION. (ALPHAMERIC)
!          5. REF_FREQ    - For databases, should be the correct reference
!                           frequency. For correlator usage, set to 1.0 MHz,
!                           should be multiplied later by the correct
!                           frequency or frequencies.
!
!
!       ACCESS CODES:
!          1. 'UTC TAG ' - THE DATA BASE ACCESS CODE FOR THE
!                          YEAR/MONTH/DAY/HOUR/MINUTE PORTION OF THE
!                          OBSERVATION TIME TAG ARRAY. (Year is 2-digit
!                          in the Mark III analysis system.)
!          2. 'UTC TAG4' - THE DATA BASE ACCESS CODE FOR THE YEAR, MONTH,
!                          DAY, HOUR, AND MINUTE PORTION OF THE UTC
!                          OBSERVATION TIME TAG ARRAY. (New proposed
!                          L-code. Year will be 2-digits in the Mark
!                          III analysis system.)
!          3. 'SEC TAG ' - THE DATA BASE ACCESS CODE FOR THE SECONDS
!                          PORTION OF THE OBSERVATION TIME TAG.
!          4. 'STAR ID ' - THE DATA BASE ACCESS CODE FOR THE STAR NAME OF
!                          THE CURRENT OBSERVATION.
!          5. 'REF FREQ' - The database access code for the reference 
!                          frequency. (MHz)
!
! PROGRAMMER -  D. Gordon, Dec. 2012, All database GET's from the geometry 
!               subroutines consolidated into one subroutine.
!
!     Real*8    
      Integer*4 N, NN
      Integer*2 NDO(3), KERR(20)
!
!  From SITG:
!     GET the baseline name. Check for errors.
      CALL GETA ('BASELINE      ',LNBASE,int2(4),int2(2),int2(1),NDO,   &
     & KERR(1))
      IF (KERR(1) .NE. 0) CALL TERMINATE_CALC ('SITG  ',int2(1),KERR(1))
!
!  From UTCTM:
!     GET the time tag information from the database. Check for db errors.
!    First try to get new time tag with 4-digit year. If not there, get old
!     time tag with 2-digit year. Doesn't really matter though.
      CALL GETI ('UTC TAG4      ', ITAG, int2(5), int2(1), int2(1),     &
     &     NDO, KERR(2))
       If (KERR(2) .ne. 0) CALL GETI ('UTC TAG       ', ITAG, int2(5),  &
     &     int2(1), int2(1), NDO, KERR(2))
      CALL GET4 ('SEC TAG       ', TAGSEC, int2(1), int2(1), int2(1),   &
     &     NDO, KERR(3))
      DO 200  N = 2,3
        NN = N
        IF ( KERR(N) .EQ. 0 )  GO TO 200
        CALL TERMINATE_CALC ('UTCTM ', NN, KERR(NN) )
  200 CONTINUE
!
!  From STRG:
!     GET the star name of the current observation.
      CALL GETA ('STAR ID       ',LSTRNM,int2(4),int2(1),int2(1),NDO,   &
     & KERR(4))
      IF (KERR(4) .NE. 0) CALL TERMINATE_CALC ('STRG  ',int2(1),KERR(4))
!
! From UVG:
!  Also from AXOG: ?????
      If (Calc_user .eq. 'A') Then
!   Get the reference frequency.
       CALL GET4('REF FREQ      ', REF_FREQ, int2(1), int2(1), &
     &      int2(1), NDO, KERR(5))
       IF(KERR(5) .NE. 0) then
         write(6,'("UVG: Failure to obtain ref frequency.")')
         CALL TERMINATE_CALC( 'UVG   ', int2(1), KERR(5))
       Endif
!    Convert from MHz to Hz.
       REF_FREQ = REF_FREQ*1.D6
      Endif
!
!  Set frequency to 1 MHz for correlators.
      If (Calc_user .eq. 'C') Then
       REF_FREQ = 1.D6
      Endif
!
!     Normal Program Conclusion.
      RETURN
      END
!
!*********************************************************************!
      SUBROUTINE GET_P()
      IMPLICIT None
!
!     Common blocks used:
!
      INCLUDE 'cuser11.i'
!       Variables to:
!          1. Calc_user -
!          2. C_mode    -
!
      INCLUDE 'get2s.i'
!       Variables to:
!        1. SurPR(2,2) - Surface pressure from the database (mbars). 
!        2. SurTP(2,2) - Surface temperature from the database (Centigrade).
!        3. SurHM(2,2) - Surface humidity from the database (percent).
!        4. metPR      - Integer flag telling whether to use measured 
!                        surface pressures (=1) or not (=0).
!        5. metTP      - Integer flag telling whether to use measured 
!                        surface temperatures (=1) or not (=0).
!        6. metHM      - Integer flag telling whether to use measured 
!                        surface humidities (=1) or not (=0).
!
!       ACCESS CODES:
!          1. 'ATM PRES' - The database access code for the surface 
!                          pressure (mbars(.
!          2. 'TEMP C  ' - The database access code for the surface 
!                          temperature (Centigrade).
!          3. 'REL.HUM.' - The database access code for the surface 
!                          relative humidity (percent).
!
! PROGRAMMER -  D. Gordon, Dec. 2012, All database GET's from the geometry 
!               subroutines consolidated into one subroutine.
!     Real*8    
      Integer*4 N, NN
      Integer*2 NDO(3), KERR(20)
!
!  From ATMP:
!  Check to see if we are to use surface met data:
      metPR = 0
      metTP = 0
      metHM = 0
       IF (Calc_user .eq. 'C') Then
         CALL GET4 ('ATM PRES      ',SurPR,int2(2),int2(2),int2(1),NDO, &
     &    Kerr(1))
           If (Kerr (1).eq. 0) metPR = 1
         CALL GET4 ('TEMP C        ',SurTP,int2(2),int2(2),int2(1),NDO, &
     &    Kerr(2))
           If (Kerr(2) .eq. 0) metTP = 1
         CALL GET4 ('REL.HUM.      ',SurHM,int2(2),int2(2),int2(1),NDO, &
     &    Kerr(3))
           If (Kerr(3) .eq. 0) metHM = 1
       ENDIF
!
!     Normal Program Conclusion.
      RETURN
      END
!
!*********************************************************************!
!     SUBROUTINE PUT_I()
!     IMPLICIT None
!
!     Common blocks used:
!
!      INCLUDE 'ccon.i'
!       VARIABLES 'FROM':
!         1. KOCEC - THE OCEAN LOADING MODULE FLOW CONTROL FLAG
!
!      INCLUDE 'cuser11.i'
!       VARIABLES 'FROM':
!         1. Calc_user - 
!
!       Database 'PUT' access codes:
!          1. 'CT SITE1' - THE DATA BASE ACCESS CODE FOR THE COORDINATE TIME
!
! PROGRAMMER -  D. Gordon, Dec/Jan. 2012/2013, All database PUT's 
!               from the geometry subroutines consolidated into 
!               this subroutine.
!
!     Real*8 
!     Integer*4 N, NN
!     Integer*2 NDO(3), KERR(20)
!
! 
!  From ATMC: 
!
!     Normal Program Conclusion.
!     RETURN
!     END
!
!*********************************************************************!
      SUBROUTINE PUT_G()
      IMPLICIT None
!
!     Common blocks used:
      INCLUDE 'put2s.i'
!       Variables from:
!          1. TDBgeo      - TDB at the geocenter. (days)
!          2. EARTH1(3,3) - THE J2000.0 BARYCENTRIC EARTH POSITION, VELOCITY,
!                           AND ACCELERATION VECTORS. (M, M/SEC, M/SEC**2)
!                           (THE FIRST INDEX RUNS OVER THE VECTOR
!                           COMPONENTS, THE SECOND RUNS OVER THE POSITION,
!                           VELOCITY, AND ACCELERATION RESPECTIVELY.)
!          3. SUN1(3,2)   - THE J2000.0 GEOCENTRIC SUN POSITION AND VELOCITY
!                           VECTORS. (M, M/SEC)
!          4. XMOON1(3,2) - THE J2000.0 GEOCENTRIC MOON POSITION AND VELOCITY
!                           VECTORS. (M, M/SEC)
!          5. ATMUT_1     - The interpolated value of 'TAI - UT1'. (sec)
!          6. NUT6XYS(3,2) -Variable used to hold the X, Y, and S nutation
!                           values computed using the IAU2000/2006 
!                           Nutation/Precession models, and 'PUT' into the
!                          'NUT06XYS' database access code.
!         6.5 NUT20006(2,2)-Variable used to hold the IAU2006/2000A 
!                           classical nutation values (DPSI and DEPS) and 
!                           their time derivatives. (rad, rad/sec)
!          7. WOBIN       - The X and Y polar motion values interpolated to
!                           the time of this observation in the conventional
!                           left-handed system. (rad)
!          8. R2Kput(3,3,3)-THE COMPLETE CRUST FIXED TO J2000.0 ROTATION
!                           MATRIX AND ITS FIRST TWO CT TIME DERIVATIVES.
!                           (UNITLESS, 1/SEC, 1/SEC**2)
!          9. XHOLD(3,2,2) -THE TOTAL OCEAN LOADING DISPLACEMENT VECTORS IN
!                           J2000.0 COORDINATES. THE INDICES RUN OVER (X,Y,Z),
!                           (POSITION, VELOCITY), (SITE 1, SITE 2) (METERS)
!         10. U_V(2)      - Baseline coordinates in the (U,V) plane, in
!                           units of fringes per arcsec. [Multiply by
!                           206264.81 to get the more conventional values.]
!                           Scaled by the value of REF_FREQ. 
!         11. ELEV(2,2)   - The elevation angle of the source corrrected for
!                           aberration and its CT time derivative at each
!                           site. (rad,rad/sec)
!         12. AZ(2,2)     - The azimuth angle of the source corrrected for
!                           aberration and its CT time derivative at each
!                           site. (rad,rad/sec)
!         13. PANGL(2)    - Feedbox rotation angles at site 1 and 2 in
!                           degrees of phase.
!         14. FCONT(2)    - The corrections to group delay and phase delay 
!                           rate for the baseline due to feedbox rotations. 
!
!
       INCLUDE 'ccon.i'
!       VARIABLES 'FROM':
!         1. KOCEC - THE OCEAN LOADING MODULE FLOW CONTROL FLAG
!
       INCLUDE 'cuser11.i'
!       VARIABLES 'FROM':
!         1. Calc_user - 
!
!       Database 'PUT' access codes:
!          1. 'CT SITE1' - THE DATA BASE ACCESS CODE FOR THE COORDINATE TIME
!                          FRACTION OF THE COORDINATE TIME DAY AT SITE #1.
!          2. 'EARTH CE' - THE DATA BASE ACCESS CODE FOR THE Barycentric 
!                          Earth position, velocity, and acceleration. First
!                          index runs over X,Y,Z. Second index runs over
!                          position, velocity, and acceleration.
!          3. 'SUN DATA' - THE DATA BASE ACCESS CODE FOR THE geocentric Sun
!                          position and velocity. 
!          4. 'MOONDATA' - THE DATA BASE ACCESS CODE FOR THE geocentric Moon
!                          position and velocity. 
!          5. 'UT1 -TAI' - Database access code for the interpolated value 
!                          of 'UT1 - TAI'. (sec)
!          6. 'POLAR XY' - Database access code for the X and Y polar 
!                          motion values interpolated to the time of this
!                          observation in the conventional left-handed
!                          system. (rad)
!          7. 'CF2J2K  ' - Database access code for the Terrestrial-to-
!                          Celestial, J2000, rotation matrix.
!          8. 'OCE DELD' - Database access code for the ocean loading
!                          J2000 displacement vectors.
!          9. 'UVF/ASEC' - The data base access code for the U, V 
!                          coordinates in fringes per arcsec.
!         10. 'UVF/MHz ' - The correlator code for the U, V coordinates
!                          in fringes per arcsec per MHz.
!         11. 'EL-THEO ' - Access code for source elevation array.
!         12. 'AZ-THEO ' - Access code for the source azimuth array.
!         13. 'PARANGLE' - The database access code for the feedbox
!                          rotation angles.
!         14. 'FEED.COR' - The database access code for the feedbox
!                          rotation correction to the group delay and
!                          the phase delay rate.
!
! PROGRAMMER -  D. Gordon, Dec/Jan. 2012/2013, All database PUT's 
!               from the geometry subroutines consolidated into 
!               this subroutine.
!               2014.Jan.09 D. Gordon, Added PUT4's for the classical 
!                           nutation quantities from the IAU2006/2000A
!                           Nutation/Precession models and from the very
!                           old Wahr model. These are needed in Solve in
!                           order to generate classical nutation 
!                           adjustments with respect to the Wahr model.
!
      Integer*4 N, NN
      Integer*2 NDO(3), KERR(20)
! 
!  From CTIMG:
!     PUT the coordinate time fracton of the coordinate time day into
!     the database.
!!! This needs to be renamed!!!!!!
      CALL PUT4 ('CT SITE1      ',TDBgeo, int2(1), int2(1), int2(1))
!
! From PEP:
!   Do the PUT's for the Earth, Sun, and Moon coordinates
      CALL PUT4 ('EARTH CE      ', EARTH1, int2(3), int2(3), int2(1))
      CALL PUT4 ('SUN DATA      ', SUN1, int2(3), int2(2), int2(1))
      CALL PUT4 ('MOONDATA      ', XMOON1, int2(3), int2(2), int2(1))
!
!     PUT the UT1 value into the database for this observation.
!     NOTE: This is UT1-TAI, not the opposite sense.
      CALL PUT4 ('UT1 -TAI      ',-ATMUT_1,int2(1),int2(1),int2(1))
!
! From NUTG:
!  'PUT' the nutation/precession X,Y,S quantities and their rates.
      CALL PUT4 ('NUT06XYS      ', NUT6XYS,int2(3),int2(2),int2(1))
! Added 2014-Jan-08
!  'PUT' the 2006/2000A nutation/precession psi, epsilon quantities 
!      and their rates.
      CALL PUT4 ('NUT2006A      ', NUT2006,int2(2),int2(2),int2(1))
!  'PUT' the Wahr model nutation/precession psi, epsilon quantities 
!      and their rates.
      CALL PUT4 ('NUT WAHR      ',NUTWAHR,int2(2),int2(2),int2(1))
!
! From WOBG:
!    'PUT' the left-handed wobble values (radians) into the database.
      CALL PUT4 ('POLAR XY      ', WOBIN, int2(2), int2(1), int2(1))
!
! From M2K:
!    PUT the Crust-fixed-to-J2000 rotation matrix into the data base. We do
!     it here for convenience, there really isn't a correct place to do it.
!     96APR02  -DG-
      CALL PUT4 ('CF2J2K        ', R2Kput, int2(3), int2(3), int2(3))
!
! From OCEG:
!  Put the site dependent topocentric ocean loading displacement and velocity in
!  the database only if the module is turned on.
!   Note the use of the indices: (Up, East, North) by (P,V) by (site1,site2).
      IF (KOCEC .NE. 1) THEN
        CALL PUT4 ('OCE DELD      ', XHOLD, int2(3), int2(2), int2(2))
      ENDIF
!
! From UVG: 
!  PUT the U,V coordinates in the database.
      If (Calc_user .eq. 'A') CALL PUT4 ('UVF/ASEC      ', U_V, &
     &    int2(2), int2(1), int2(1))
      If (Calc_user .eq. 'C') CALL PUT4 ('UVF/MHz       ', U_V, &
     &    int2(2), int2(1), int2(1))
!
! From ATMG:
!   PUT the elevation and its time derivative into the data base.
      CALL PUT4 ('EL-THEO       ', ELEV, int2(2), int2(2), int2(1))
!   PUT the azimuth and its time derivative into the data base.
      CALL PUT4 ('AZ-THEO       ', AZ, int2(2), int2(2), int2(1))
!
! From AXOG:
      CALL PUT4 ('PARANGLE      ', PANGL, int2(2), int2(1), int2(1))
      CALL PUT4 ('FEED.COR      ', FCONT, int2(2), int2(1), int2(1))
!
!     Normal Program Conclusion.
      RETURN
      END
!
!*********************************************************************!
      SUBROUTINE PUT_P()
      IMPLICIT None
!
!     Common blocks used:
      INCLUDE 'put2s.i'
!       Variables from:
!         1. DNUXY(2,2)   - Partial derivatives of the delay and rate
!                           w.r.t. X and Y CEO-based nutation.
!         2. DSITP(3,2,2) - THE PARTIAL DERIVATIVES OF THE DELAY AND THE
!                           DELAY RATE WITH RESPECT TO THE CRUST FIXED SITE
!                           COORDINATES AT EACH OBSERVATION SITE. THE FIRST
!                           INDEX RUNS OVER THE SITE COORDINATES, THE SECOND
!                           INDEX RUNS OVER THE SITES, AND THE THIRD RUNS
!                           OVER THE DELAY AND THE DELAY RATE.
!                           (SEC/M, SEC/SEC-M)
!         3. DSTRP(2,2)   - THE PARTIAL DERIVATIVES OF THE DELAY AND THE DELAY
!                           RATE WITH RESPECT TO THE SOURCE RIGHT ASCENSION AND
!                           DECLINATION. (sec/rad, sec/sec-rad) THE FIRST INDEX
!                           RUNS OVER RA AND DEC, THE SECOND RUNS OVER DELAY 
!                           AND DELAY RATE.
!         4. DUT1P(2,2)   - The partial derivatives of the delay and rate
!                           with respect to the instantaneous value of
!                           'A1-UT1'. (s/s, s/s**2) The second index runs
!                           over the first and second derivatives.
!         5. DPLXP(2)     - THE PARTIAL DERIVATIVES OF THE DELAY AND RATE
!                           w.r.t.  ???????????????????????????????
!
      INCLUDE 'cmwob11.i'
!       Variables 'from':
!         1. DWOBP(2,2) - The partial derivatives of the delay and rate
!                         with respect to the X and Y polar motion. The
!                         first index runs over the X and Y offsets
!                         and the second runs over delay and rate.
!                         (s/radian, (s/s)/radian)
!
!
      Real*8 RF(2), RTRACE(2), wetcon(2), Datmp_hmf(2,2),               &
     &       Datmp_wmf(2,2), Zen_dry(2,2), Zen_wet(2,2), Ngrad(2,2,2)
      COMMON /ATMCM/ RF, RTRACE, wetcon, Datmp_hmf, Datmp_wmf, Zen_dry, &
     &               Zen_wet, Ngrad
!
      Real*8  DAXOP(2,2), DCOMP(2,2), RICHM(2), &
     &        Tpartl(2), uaxis2000(3,2), duaxis2000(3,2)
      COMMON / AXOCM / DAXOP, DCOMP, RICHM, &
     &                 Tpartl, uaxis2000, duaxis2000
!
      Real*8  ZPLTDP(3,2), ZPLTDV(3,2), ZPLDPX(3,2), ZPLDVX(3,2),       &
     &        ZPLDPY(3,2), ZPLDVY(3,2), X_mean, Y_mean, DPTDP(2,2)
      COMMON / PTDCM / ZPLTDP, ZPLTDV, ZPLDPX, ZPLDVX, ZPLDPY, ZPLDVY,  &
     &                 X_mean, Y_mean, DPTDP
!
      Real*8 Dparsec, t_prlx(2)
      Common /PRLX/ Dparsec, t_prlx
!
!
       INCLUDE 'ccon.i'
!       VARIABLES 'FROM':
!         1. KOCEC - THE OCEAN LOADING MODULE FLOW CONTROL FLAG
!
       INCLUDE 'cuser11.i'
!       VARIABLES 'FROM':
!         1. Calc_user - 
!
!     Database access -
!        'PUT' variables:
!          1. Datmp_hmf(2,2) - The Niell (Nhmf) dry atmosphere partial
!                          derivatives of the delay and rate with respect to
!                          the zenith path delays at each site. The first
!                          index runs over the sites and the second runs
!                          over the delay and rate. (sec,sec/sec)
!          2. Datmp_wmf(2,2) - Same as Datmp_hmf, except for the wet Niell
!                          (Whmf) atmosphere partials.
!          3. Ngrad(2,2,2) - Atmosphere gradient partials, scaled using
!                          the Niell dry mapping function. First
!                          index runs over sites, second over North
!                          and East components, third over delay and
!                          rate.
!          4. DAXOP(2,2) - THE PARTIAL DERIVATIVES OF THE DELAY AND THE
!                          DELAY RATE WITH RESPECT TO THE ANTENNA AXIS
!                          OFFSETS AT EACH SITE (modified for new model).
!                          THE FIRST INDEX RUNS OVER THE OBSERVATION SITES
!                          AND THE SECOND RUNS OVER THE DELAY AND DELAY
!                          RATE. (SEC/M, SEC/SEC-M)
!          5. DPTDP(2,2) - The pole tide partial derivatives of the delay
!                          the delay rate w.r.t. X-pole and Y-pole. First
!                          index runs over the delay partial w.r.t. X and
!                          Y, second runs over rate partial w.r.t X and Y.
!                          (seconds/arcsecond, seconds/sec/arcsec)
!          6. DNUXY(2,2) - Partial derivatives of the delay and rate
!                          w.r.t. X and Y CEO-based nutation.
!          7. DSITP(3,2,2) - THE PARTIAL DERIVATIVES OF THE DELAY AND THE
!                          DELAY RATE WITH RESPECT TO THE CRUST FIXED SITE
!                          COORDINATES AT EACH OBSERVATION SITE. THE FIRST
!                          INDEX RUNS OVER THE SITE COORDINATES, THE SECOND
!                          INDEX RUNS OVER THE SITES, AND THE THIRD RUNS
!                          OVER THE DELAY AND THE DELAY RATE.
!                          (SEC/M, SEC/SEC-M)
!          8. DSTRP(2,2) - THE PARTIAL DERIVATIVES OF THE DELAY AND THE DELAY
!                          RATE WITH RESPECT TO THE SOURCE RIGHT ASCENSION AND
!                          DECLINATION. (sec/rad, sec/sec-rad) THE FIRST INDEX
!                          RUNS OVER RA AND DEC, THE SECOND RUNS OVER DELAY AND
!                          DELAY RATE.
!          9. DUT1P(2,2) - The partial derivatives of the delay and rate
!                          with respect to the instantaneous value of
!                          'A1-UT1'. (s/s, s/s**2) The second index runs
!                          over the first and second derivatives.
!         10. DWOBP(2,2) - The partial derivatives of the delay and rate
!                          with respect to the X and Y polar motion. The
!                          first index runs over the X and Y offsets
!                          and the second runs over delay and rate.
!                          (s/radian, (s/s)/radian)
!         11.  DPLXP(2)  - THE PARTIAL DERIVATIVES OF THE DELAY AND RATE
!                          w.r.t. ???????????????????????????????
!         12.  t_prlx(2) - The partial derivatives of the delay and rate
!                          with respect to the inverse distance in parsecs.
!
!        Access codes:
!          1. 'NDRYPART' - Access code for dry Niell (Nhmf) partials.
!          2. 'NWETPART' - Access code for wet Niell (Whmf) partials.
!          3. 'NGRADPAR' - Access code for dry Niell atmosphere gradient
!                          partials.
!          4. 'AXO PART' - THE DATA BASE ACCESS CODE FOR THE AXIS OFFSET
!                          MODULE PARTIAL DERIVATIVES ARRAY.
!          5. 'PTDXYPAR' - The data base access code for the pole tide
!                          X and Y partials array.
!          6. 'NUT06XYP' - The database access code for the CEO-based
!                          nutation partial derivatives array.
!          7. 'NUT06XYP' - The database access code for the CEO-based
!                          nutation partial derivatives array.
!          8. 'SIT PART' - THE DATA BASE ACCESS CODE FOR THE SITE MODULE
!                          PARTIAL DERIVATIVES ARRAY.
!          9. 'STR PART' - THE DATA BASE ACCESS CODE FOR THE STAR MODULE
!                          PARTIAL DERIVATIVES ARRAY.
!         10. 'UT1 PART' - THE DATA BASE ACCESS CODE FOR the UT1 partial
!                          derivatives.
!         11. 'WOB PART' - THE DATA BASE ACCESS CODE FOR the X and Y 
!                          polar motion partial derivatives.
!         12. 'PLX PART' - THE DATA BASE ACCESS CODE FOR THE PARALLAX
!                          MODULE PARTIAL DERIVATIVES ARRAY.
!         13. 'PLX1PSEC' - The data base access code for the parallax
!                          partial with respect to inverse distance in
!                          parsecs.
!
! PROGRAMMER -  D. Gordon, Dec/Jan. 2012/2013, All database PUT's 
!               from the partials subroutines consolidated into 
!               this subroutine.
!
      Integer*4 N, NN
      Integer*2 NDO(3), KERR(20)
! 
!  From ATMP: 
!   'PUT' the Niell partials.
      CALL PUT4 ('NDRYPART      ',Datmp_hmf,int2(2),int2(2),int2(1))
      CALL PUT4 ('NWETPART      ',Datmp_wmf,int2(2),int2(2),int2(1))
      CALL PUT4 ('NGRADPAR      ',Ngrad,int2(2),int2(2),int2(2))
!
!  From AXOP:
!   'PUT' the axis offset partial derivatives array.
      CALL PUT4 ('AXO PART      ',DAXOP,int2(2),int2(2),int2(1))
!
!  From PTDP:
!   'PUT' the pole tide partials.
      CALL PUT4 ('PTDXYPAR      ',DPTDP,int2(2),int2(2),int2(1))
!
!  From NUTP:
!   Call 'PUT4' to place the IERS 2003 Nutation partials into the database.
      CALL PUT4 ('NUT06XYP      ',DNUXY,int2(2),int2(2),int2(1))
!
!  From SITP:
!    PUT the site module partials.
      CALL PUT4 ('SIT PART      ',DSITP,int2(3),int2(2),int2(2))
!
!  From STRP:
!   PUT the star partials into the database.
      CALL PUT4 ('STR PART      ', DSTRP, int2(2), int2(2), int2(1))
!
!  From UT1P:
!   PUT the UT1 partial derivatives in the database.
      CALL PUT4 ('UT1 PART      ', DUT1P, int2(2), int2(2), int2(1))
!
!  From WOBP:
!  'PUT' the wobble partial derivatives.
      CALL PUT4 ('WOB PART      ', DWOBP, int2(2), int2(2), int2(1))
!
!  From STRP:
!   PUT the parallax partials.
      CALL PUT4 ('PLX PART      ',DPLXP,int2(2),int2(1),int2(1))
      CALL PUT4 ('PLX1PSEC      ',t_prlx,int2(2),int2(1),int2(1))
!
!     Normal Program Conclusion.
      RETURN
      END
!
!*********************************************************************!
      SUBROUTINE PUT_C()
      IMPLICIT None
!
!     Common blocks used:
      INCLUDE 'put2s.i'
!       Variables from:
!         1. DAXOC(2,2)  - THE CONTRIBUTIONS TO THE DELAY AND TO THE DELAY
!                          RATE DUE TO ANTENNA AXIS OFFSETS AT EACH SITE.
!                          THE FIRST INDEX RUNS OVER THE OBSERVATION SITES
!                          AND THE SECOND RUNS OVER THE DELAY AND THE DELAY
!                          RATE. (SEC, SEC/SEC)
!         2. Tcorrmv(2)  - Contribution to undue the fixed axis tilt correction
!                          (sec, sec/sec???).
!         3. DETDC(2)    - The total Earth tide contribution to the delay and
!                          rate. Value depends on the value of KETDC. Default
!                          is the IERS model (second order and K1 terms).
!         4. DPTDC(2)    - THE POLE TIDE CONTRIBUTION TO THE DELAY AND TO
!                          THE DELAY RATE (SEC, SEC/SEC)
!         5. PTOLD(2)    - The contribution that will effectively convert
!                          the pole tides to the non-mean-offset-removed
!                          values, i.e. to the old (Calc 8.2 and earlier
!                          versions) values.
!         6. DOCEC(2)    - THE OCEAN LOADING CONTRIBUTION TO THE DELAY
!                          AND TO THE DELAY RATE. (SEC, SEC/SEC)
!         7. CONTRIB_HOR(2,2) - THE SITE-DEPENDENT CONTRIBUTION TO THE DELAY
!                          AND RATE FROM HORIZONTAL DISPLACEMENTS. THE
!                          first index runs over the sites and the second
!                          over the delay and rate. (sec, sec/sec)
!         8. CONTRIB_VER(2,2) - THE SITE-DEPENDENT CONTRIBUTION TO THE DELAY
!                          AND RATE FROM VERTICAL DISPLACEMENTS. THE
!                          first index runs over the sites and the second
!                          over the delay and rate. (sec, sec/sec)
!         9. DOPTLC(2)   - THE OCEAN POLE TIDE CONTRIBUTION TO THE DELAY AND TO
!                          THE DELAY RATE (SEC, SEC/SEC)
!        10. PMCONT(2)   - If KSTRC = 1, these are the contributions to
!                          the delay and rate to correct for the effect
!                          of proper motion; add to theoreticals.
!                          If KSTRC = 2, these are the contributions to
!                          return the delay and rate to their non-proper
!                          motion values; add to theoreticals. (sec, sec/sec).
!        11. UT1tid(2)   - UT1 delay and rate contributions from short
!                          period UT1 corrections (sec, sec/sec).
!        12. UT1lib(2)   - UT1 delay and rate contributions from short
!                          period libration corrections (sec, sec/sec).
!        13. DWOBXC(2)  - The contributions to the delay and rate due to
!                         the X-pole offset. (s, s/s)
!        14. DWOBYC(2)  - The contributions to the delay and rate due to
!                         the Y-pole offset. (s, s/s)
!        15. DWOBlib(2) - The contributions to the delay and rate due to
!                         the X and Y short period libration. (s, s/s)
!        16. DWOBtid(2) - The contributions to the delay and rate due to
!                         the X and Y ocean tides. (s, s/s
!        17. PLXCON(2)  - Optional parallax contribution (???).
!        18. Datmc_hmf(2,2) - The contributions to the delay and rate due
!                         to the Niell (Nhmf) dry tropospheric refraction 
!                         at each site. The first index runs over the sites
!                         and the second over the delay and rate.
!                         (sec, sec/sec)
!        19. Datmc_wmf(2,2) - The contributions to the delay and rate due
!                         to the Niell (Whmf) wet tropospheric refraction
!                         at each site. The first index runs over the sites
!                         and the second over the delay and rate.
!                         (sec, sec/sec)
!
       INCLUDE 'ccon.i'
!       VARIABLES 'FROM':
!          1. KSTRC - THE STAR MODULE FLOW CONTROL FLAG
!          2. KPLXC - THE PARALLAX MODULE FLOW CONTROL FLAG.
!
       INCLUDE 'cuser11.i'
!       VARIABLES 'FROM':
!         1. Calc_user - 
!
      Real*8 RF(2), RTRACE(2), wetcon(2), Datmp_hmf(2,2),               &
     &       Datmp_wmf(2,2), Zen_dry(2,2), Zen_wet(2,2), Ngrad(2,2,2)
      COMMON /ATMCM/ RF, RTRACE, wetcon, Datmp_hmf, Datmp_wmf, Zen_dry, &
     &               Zen_wet, Ngrad
!       Variables 'from':
!         1. Datmp_hmf(2,2) - The Niell (Nhmf) dry atmosphere partial
!                             derivatives of the delay and rate with respect to
!                             the zenith path delays at each site. The first
!                             index runs over the sites and the second runs
!                             over the delay and rate. (sec,sec/sec)
!         2. Datmp_wmf(2,2) - Same as Datmp_hmf, except for the wet Niell
!                             (Whmf) atmosphere partials.
!
!    Database access:
!      'PUT' variables:
!         1. Datmp_hmf(2,2)- The Niell (Nhmf) dry atmosphere partial
!                            derivatives of the delay and rate with respect to
!                            the zenith path delays at each site. The first
!                            index runs over the sites and the second runs
!                            over the delay and rate. (sec,sec/sec)
!         2. Datmp_wmf(2,2)- Same as Datmp_hmf, except for the wet Niell
!                            (Whmf) atmosphere partials.
!         3. DAXOC(2,2)    - THE CONTRIBUTIONS TO THE DELAY AND THE DELAY
!                            RATE DUE TO ANTENNA AXIS OFFSETS AT EACH SITE.
!                            THE FIRST INDEX RUNS OVER THE OBSERVATION
!                            SITES AND THE SECOND RUNS OVER THE DELAY AND
!                            THE DELAY RATE. (SEC, SEC/SEC)
!         4. Tcorrmv(2)    - Contribution to undue the fixed axis tilt 
!                            correction (sec, sec/sec???).
!         5. DETDC(2)      - The total Earth tide contribution to the delay and
!                            rate. Value depends on the value of KETDC. Default
!                            is the IERS model (second order and K1 terms).
!         6. DPTDC(2)      - THE POLE TIDE CONTRIBUTION TO THE DELAY AND TO
!                            THE DELAY RATE (SEC, SEC/SEC)
!         7. PTOLD(2)      - The contribution that will effectively convert
!                            the pole tides to the non-mean-offset-removed
!                            values, i.e. to the old (Calc 8.2 and earlier
!                            versions) values.
!         8. DOCEC(2)      - THE OCEAN LOADING CONTRIBUTION TO THE DELAY
!                            AND TO THE DELAY RATE. (SEC, SEC/SEC)
!         9. CONTRIB_HOR(2,2) - THE SITE-DEPENDENT CONTRIBUTION TO THE DELAY
!                            AND RATE FROM HORIZONTAL DISPLACEMENTS. THE
!                            first index runs over the sites and the second
!                            over the delay and rate. (sec, sec/sec)
!        10. CONTRIB_VER(2,2) - THE SITE-DEPENDENT CONTRIBUTION TO THE DELAY
!                            AND RATE FROM VERTICAL DISPLACEMENTS. THE
!                            first index runs over the sites and the second
!                            over the delay and rate. (sec, sec/sec)
!        11. DOPTLC(2) -     THE OCEAN POLE TIDE CONTRIBUTION TO THE DELAY 
!                            AND TO THE DELAY RATE (SEC, SEC/SEC)
!        12. PMCONT(2)     - If KSTRC = 1, these are the contributions to
!                            the delay and rate to correct for the effect
!                            of proper motion; add to theoreticals.
!                            If KSTRC = 2, these are the contributions to
!                            return the delay and rate to their non-proper
!                            motion values; add to theoreticals. 
!                            (sec, sec/sec).
!        13. UT1tid(2)     - UT1 delay and rate contributions from short
!                            period UT1 corrections (sec, sec/sec).
!        14. UT1lib(2)     - UT1 delay and rate contributions from short
!                            period libration corrections (sec, sec/sec).
!        15. DWOBXC(2)     - The contributions to the delay and rate due to
!                            the X-pole offset. (s, s/s)
!        16. DWOBYC(2)     - The contributions to the delay and rate due to
!                            the Y-pole offset. (s, s/s)
!        17. DWOBlib(2)    - The contributions to the delay and rate due to
!                            the X and Y short period libration. (s, s/s)
!        18. DWOBtid(2)    - The contributions to the delay and rate due to
!                            the X and Y ocean tides. (s, s/s).
!        19. PLXCON(2)     - Optional parallax contribution (???).
!
!       Access codes:
!         1. 'NDRYCONT' - Access code for the NHMF2 (Niell) dry contributions.
!         2. 'NWETCONT' - Access code for the WHMF2 (Niell) wet contributions.
!         3. 'AXO CONT' - The database access code for the new, simple
!                         version, axis offset contributions array.
!         4. 'TILTRMVR' - Sccess code for the tilt remover contribution. 
!         5. 'ETD CONT' - Database access code for the total Earth tide
!                         contributions.
!         6. 'PTD CONT' - THE DATA BASE ACCESS CODE FOR THE POLE TIDE
!                         MODULE CONTRIBUTIONS ARRAY.
!         7. 'PTOLDCON' - The data base access code for the old pole-
!                         tide-restored contribution.
!         8. 'OCE CONT' - THE DATA BASE ACCESS CODE FOR THE OCEAN LOADING
!                         MODULE CONTRIBUTIONS ARRAY.
!         9. 'OCE HORZ' - THE DATABASE ACCESS CODE FOR THE SITE DEPENDENT
!                         CONTRIBUTIONS TO THE DELAY AND RATE FOR THE
!                         HORIZONTAL CORRECTIONS.
!        10. 'OCE VERT' - THE DATABASE ACCESS CODE FOR THE SITE DEPENDENT
!                         CONTRIBUTIONS TO THE DELAY AND RATE FOR THE VERTICAL
!                         CORRECTIONS.
!        11. 'OPTLCONT' - THE DATA BASE ACCESS CODE FOR THE OCEAN POLE TIDE
!                         MODULE CONTRIBUTIONS ARRAY.
!        12. 'PMOTNCON' - The data base access code for the proper motion
!                         contribution.
!        13. 'PMOT2CON' - The data base access code for the proper motion
!                         removal contribution.
!        14. 'UT1ORTHO' - Access code for the UT1 tidal contribution. 
!        15. 'UT1LIBRA' - Access code for the UT1 libration contribution. 
!        16. 'WOBXCONT' - Access code for the X polar motion contribution. 
!        17. 'WOBYCONT' - Access code for the Y polar motion contribution. 
!        18. 'WOBORTHO' - Access code for the polar motion tidal contribution. 
!        19. 'WOBLIBRA' - Access code for the polar motion libration 
!                         contribution. 
!        20. 'PRLXCONT' - Access code for the optional parallaxcontribution. 
!
! PROGRAMMER -  D. Gordon, Dec/Jan. 2012/2013, All database PUT's 
!               from the geometry subroutines consolidated into 
!               this subroutine.
!
      Integer*4 N, NN
      Integer*2 NDO(3), KERR(20)
!
! 
!  From ATMC: 
!  'PUT' the Niell dry and wet contributions.
      CALL PUT4 ('NDRYCONT      ',Datmc_hmf,int2(2),int2(2),int2(1))
      CALL PUT4 ('NWETCONT      ',Datmc_wmf,int2(2),int2(2),int2(1))
!
!  From AXOC:
!   PUT the axis offset contributions.
      CALL PUT4 ('AXO CONT      ',DAXOC,int2(2),int2(2),int2(1))
      CALL PUT4 ('TILTRMVR      ',Tcorrmv,int2(2),int2(1),int2(1))
!
!  From ETDC:
!   PUT the Earth tide contributions.
      CALL PUT4 ('ETD CONT      ', DETDC, int2(2), int2(1), int2(1))
!
!  From PTDC:
!   PUT the pole tide contributions.
      CALL PUT4 ('PTD CONT      ',DPTDC,int2(2),int2(1),int2(1))
      CALL PUT4 ('PTOLDCON      ',PTOLD,int2(2),int2(1),int2(1))
!
!  From OPTLC:
!   PUT the ocean pole tide loading contribution.
      CALL PUT4 ('OPTLCONT      ',DOPTLC,int2(2),int2(1),int2(1))
!
!  From OCEC:
!   'PUT' the ocean loading contributions into the database.
      CALL PUT4 ('OCE CONT      ',DOCEC,int2(2),int2(1),int2(1))
      CALL PUT4 ('OCE HORZ      ',CONTRIB_HOR,int2(2),int2(2),int2(1))
      CALL PUT4 ('OCE VERT      ',CONTRIB_VER,int2(2),int2(2),int2(1))
      CALL PUT4 ('OCE_OLD       ',DOCECold,int2(2),int2(1),int2(1))
!
      IF (KSTRC.eq.1) CALL PUT4 ('PMOTNCON      ', PMCONT, int2(2), &
     &      int2(1), int2(1))
      IF (KSTRC.eq.2) CALL PUT4 ('PMOT2CON      ', PMCONT, int2(2), &
     &      int2(1), int2(1))
!
!  From UT1C:
!   PUT the short period UT1 contributions
      CALL PUT4 ('UT1ORTHO      ',UT1tid,int2(2),int2(1),int2(1))
      CALL PUT4 ('UT1LIBRA      ',UT1lib,int2(2),int2(1),int2(1))
!
!  From WOBC:
!   PUT the wobble contributions.
      CALL PUT4 ('WOBXCONT      ', DWOBXC, int2(2), int2(1), int2(1))
      CALL PUT4 ('WOBYCONT      ', DWOBYC, int2(2), int2(1), int2(1))
      CALL PUT4 ('WOBLIBRA      ', DWOBlib, int2(2), int2(1), int2(1))
      CALL PUT4 ('WOBORTHO      ', DWOBorth, int2(2), int2(1), int2(1))
!
!   PUT the parallax contributions.
      IF (KPLXC.eq.1) CALL PUT4 ('PRLXCONT      ',PLXCON,int2(2),       &
     &    int2(1),int2(1))
!
!     Normal Program Conclusion.
      RETURN
      END
!
!*********************************************************************!
      SUBROUTINE PUT_TH()
      IMPLICIT None
!
!     Common blocks used:
      INCLUDE 'put2s.i'
!       Variables from:
!          1. CONDEL(2)    - The theoretical delay from the Consensus
!                            model in two pieces in units of MICROSECONDS.
!                            The 1st is the integer microseconds and the
!                            2nd is the submicroseconds portion.
!          2. CONRAT       - The theoretical delay rate from the Consensus
!                            model/ (sec/sec).
!          3. SUN_CNTRB(2) - The solar gravitational bending delay and rate
!                            terms from the Consensus model. (sec, sec/sec)
!          4. CON_CNTRB(2) - The total gravitational bending delay and rate
!                            terms from the Consensus model. (sec, sec/sec)
!          5. CON_PART(2)  - The total Consensus delay and rate partials
!                            with respect to Gamma. (sec, sec/sec)
!          6. Sunplus(2)   - Higher order solar bending delay and rate
!                            contributions, as defined in IERS Conventions
!                            (1996), page 91, eqn. 14. (sec, sec/sec) It
!                            has also been added to the total solar bending
!                            delay and rate contibutions.
!          7. Bend_par(2)  - The relativistic bending delay and rate partials
!                            with respect to Gamma. (sec, sec/sec)
!
!
       INCLUDE 'ccon.i'
!       VARIABLES 'FROM':
!          1. KSTRC - THE STAR MODULE FLOW CONTROL FLAG
!          2. KPLXC - THE PARALLAX MODULE FLOW CONTROL FLAG.
!
       INCLUDE 'cuser11.i'
!       VARIABLES 'FROM':
!          1. Calc_user - 
!
!    Database access:
!      'PUT' variables:
!         1. CONDEL(2)    - The theoretical delay from the Consensus
!                           model in two pieces in units of MICROSECONDS.
!                           The 1st is the integer microseconds and the
!                           2nd is the submicroseconds portion.
!         2. CONRAT       - The theoretical delay rate from the Consensus
!                           model/ (sec/sec).
!         3. CON_CNTRB(2) - The total gravitational bending delay and rate
!                           terms from the Consensus model. (sec, sec/sec)
!         4. SUN_CNTRB(2) - The solar gravitational bending delay and rate
!                           terms from the Consensus model. (sec, sec/sec)
!                           terms from the Consensus model. (sec, sec/sec)
!         5. CON_PART(2)  - The total Consensus delay and rate partials
!                           with respect to Gamma. (sec, sec/sec)
!         6. BEND_PAR(2)  - The relativistic bending delay and rate partials
!                            with respect to Gamma. (sec, sec/sec)
!         7. Sunplus(2)   - Higher order solar bending delay and rate
!                           contributions, as defined in IERS Conventions
!                           (1996), page 91, eqn. 14. (sec, sec/sec). It
!                           has also been added to the total solar bending
!                           delay and rate contibutions.
!
!       Access codes:
!         1. 'CONSNDEL' - The database access code for the VLBI delay
!                         using the Eubanks' Consensus model.
!         2. 'CONSNRAT' - The database access code for the VLBI delay
!         3. 'CON CONT' - Database access code for the TOTAL relativistic
!                         bending contributions based on the Consensus model.
!         4. 'SUN CONT' - Database access code for the Sun's relativistic
!                         model.
!         5. 'CONSPART' - Database access code for partial derivatives
!                         of the Consensus model delays and rates with
!                         respect to Gamma.
!         6. 'BENDPART' - Database access code for partial derivatives
!                         of the Consensus gravitational bending portion
!                         of the delays and rates with respect to Gamma.
!         7. 'SUN2CONT' - Database access code for the additional solar
!                         bending due to higher order relativistic effects.
!                         This term is already in the theoretical, so to
!                         remove it, SUBTRACT the values in this access code.
!
! PROGRAMMER -  D. Gordon, Dec/Jan. 2012/2013, All database PUT's 
!               from the Theory subroutines consolidated into 
!               this subroutine.
      Integer*4 N, NN
      Integer*2 NDO(3), KERR(20)
! 
!  From THERY: 
!
!   Put the Consensus model theoretical delays and rates into the database.
      CALL PUT4 ('CONSNDEL      ', CONDEL, int2(2), int2(1), int2(1))
      CALL PUT4 ('CONSNRAT      ', CONRAT, int2(1), int2(1), int2(1))
!
      CALL PUT4 ('CON CONT      ', CON_CNTRB, int2(2), int2(1),int2(1))
      CALL PUT4 ('SUN CONT      ', SUN_CNTRB, int2(2), int2(1),int2(1))
      CALL PUT4 ('CONSPART      ', CON_PART, int2(2), int2(1), int2(1))
      CALL PUT4 ('BENDPART      ', BEND_PAR, int2(2), int2(1), int2(1))
!
!  The following is the higher order solar bending from the 1996 Conventions.
!   It is already included in the total theoreticals ('CONSNDEL' and
!   'CONSNRAT') and the total solar bending contribution ('SUN CONT').
      CALL PUT4 ('SUN2CONT      ', Sunplus, int2(2), int2(1), int2(1))
!
!     Normal Program Conclusion.
      RETURN
      END
!
!*********************************************************************!
