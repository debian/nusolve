      SUBROUTINE UVG (STAR, EPBASE )
      IMPLICIT None
!
!       UVG computes the (U,V) coordinates of the baseline.
!
!      CALLING SEQUENCE -
!        INPUT VARIABLES:
!          1. STAR(3)     - The J2000.0 Source unit vector.
!          2. EPBASE(3,2) - The J2000.0 Geocentric baseline position and
!                           velocity vectors. (m, m/sec)
!
!   COMMON BLOCKS USED -
!
      INCLUDE 'ccon.i'
!        VARIABLES 'FROM':
!          1. KASTC - The Dave Shaffer switch to turn on the computation
!                     of (U,V) coordinates. (Logic reversed 2001.01.12)
!                      = 0 ==> Switched ON. (default)
!                      = 1 ==> Switched OFF
!
      INCLUDE 'cphys11.i'
!        VARIABLES 'FROM':
!          1. VLIGHT - The velocity of light in a vacuum (m/sec).
!
      INCLUDE 'cuser11.i'
!        VARIABLES 'FROM'
!          1. Calc_user - 'A' for analysis centers, 'C' for correlators.
!
      INCLUDE 'get2s.i'
!       Variables from:
!          1. REF_FREQ - For databases, should be the correct reference
!                        frequency. For correlator usage, set to 1.0 MHz,
!                        should be multiplied later by the correct
!                        frequency or frequencies.
!
      INCLUDE 'put2s.i'
!       Variables to:
!          1. U_V(2) - Baseline coordinates in the (U,V) plane, in
!                      units of fringes per arcsec. [Multiply by
!                      206264.81 to get the more conventional values.]
!                      Scaled by the value of REF_FREQ. 
!
      Real*8         PI,TWOPI,HALFPI,CONVD,CONVDS,CONVHS,SECDAY
      COMMON /CMATH/ PI,TWOPI,HALFPI,CONVD,CONVDS,CONVHS,SECDAY
!        VARIABLES 'FROM':
!          1. CONVDS - THE CONVERSION FACTOR FROM ARCSECONDS TO RADIANS
!                      (RAD/ARCSECOND)
!
      Real*8 STAR(3), EPBASE(3,2)
      Real*8 DOTP, B(3), NCP(3), vectr(3), Bpr(3), NCPpr(3), W,         &
     &       VECMG
!     Integer*2 KERR, NDO(3)
!
!        Program variables:
!          1. U_V(2) -   Baseline coordinates in the (U,V) plane, in
!                        units of fringes per arcsec. [Multiply by
!                        206264.81 to get the more conventional values.]
!                        Scaled by the value of REF_FREQ.
!          2. REF_FREQ - For databases, should be the correct reference
!                        frequency. For correlator usage, set to 1.0 MHz,
!                        should be multiplied later by the correct
!                        frequency or frequencies.
!
!       PROGRAMMER:
!             David Gordon 1998.11.17 Subroutine created
!             David Gordon 2001.01.12 Logic reversed, default is to
!                          compute and add U/V coordinates, since the
!                          Mark IV correlators are not computing them.
!                          New access code ('UVF/MHz ', U/V coordinates in
!                          fringes per arcsec per MHz) added for Mark IV
!                          correlator usage.
!             Jim Ryan Sept 2002 Integer*2/4 mods.
!             David Gordon Dec. 2012 Moved GET's to GET_G and PUT's to
!                          PUT_G.
!
!       UVG PROGRAM STRUCTURE
!
      IF (KASTC .ne. 0) Go to 800
!
!  Set frequency to 1 MHz for correlators.
      If (Calc_user .eq. 'C')  REF_FREQ = 1.D6
      If (C_mode .eq. 'difx  ') REF_FREQ = 1.D6
!
!   Baseline vector
       B(1) = EPBASE(1,1) * REF_FREQ/VLIGHT*CONVDS
       B(2) = EPBASE(2,1) * REF_FREQ/VLIGHT*CONVDS
       B(3) = EPBASE(3,1) * REF_FREQ/VLIGHT*CONVDS
!
      If (C_mode .eq. 'difx  ') Then
       B(1) = EPBASE(1,1) 
       B(2) = EPBASE(2,1)
       B(3) = EPBASE(3,1)
      Endif
!
!   NCP unit vector
       NCP(1) = 0.D0
       NCP(2) = 0.D0
       NCP(3) = 1.D0
!
! Get component of baseline vector projected into the plane
!   perpendicular to the STAR vector:
        CALL CROSP (STAR,      B, vectr)
        CALL CROSP (vectr,  STAR,   Bpr)
! Get component of NCP vector projected into the plane perpendicular
!   to the STAR vector:
        CALL CROSP (STAR,   NCP, vectr)
        CALL CROSP (vectr, STAR, NCPpr)
! Convert to a unit vector
        CALL VUNIT (NCPpr, NCP)
!
        U_V(2) = DOTP (Bpr, NCP)
        CALL CROSP (Bpr, NCP, vectr)
        U_V(1) = VECMG (vectr)
         If (DOTP (STAR,vectr) .lt. 0.d0) U_V(1) = -U_V(1)
! Add third component
        W = DOTP(B, STAR)
        Wb = W
!
!      Write (6,*) 'UVG:STAR: U,V,W  ', U_V, W
!
!     Normal conclusion.
  800 CONTINUE
      RETURN
      END
!
!***********************************************************************
!
      SUBROUTINE UVG_ab (STAR_ab, EPBASE )
      IMPLICIT None
!
!       UVG computes the (U,V) coordinates of the baseline, using
!        the aberrated source unit vector.
!
!      CALLING SEQUENCE -
!        INPUT VARIABLES:
!          1. STAR_ab(3,2) - The J2000.0 aberrated source unit vector.
!          2. EPBASE(3,2)  - The J2000.0 Geocentric baseline position and
!                            velocity vectors. (m, m/sec)
!
!   COMMON BLOCKS USED -
!
      INCLUDE 'ccon.i'
!        VARIABLES 'FROM':
!          1. KASTC - The Dave Shaffer switch to turn on the computation
!                     of (U,V) coordinates. (Logic reversed 2001.01.12)
!                      = 0 ==> Switched ON. (default)
!                      = 1 ==> Switched OFF
!
      INCLUDE 'cphys11.i'
!        VARIABLES 'FROM':
!          1. VLIGHT - The velocity of light in a vacuum (m/sec).
!
      INCLUDE 'cuser11.i'
!        VARIABLES 'FROM'
!          1. Calc_user - 'A' for analysis centers, 'C' for correlators.
!
      INCLUDE 'get2s.i'
!       Variables from:
!          1. REF_FREQ - For databases, should be the correct reference
!                        frequency. For correlator usage, set to 1.0 MHz,
!                        should be multiplied later by the correct
!                        frequency or frequencies.
!
      INCLUDE 'put2s.i'
!       Variables to:
!          1. U_V(2) - Baseline coordinates in the (U,V) plane, in
!                      units of fringes per arcsec. [Multiply by
!                      206264.81 to get the more conventional values.]
!                      Scaled by the value of REF_FREQ. 
!
      Real*8         PI,TWOPI,HALFPI,CONVD,CONVDS,CONVHS,SECDAY
      COMMON /CMATH/ PI,TWOPI,HALFPI,CONVD,CONVDS,CONVHS,SECDAY
!        VARIABLES 'FROM':
!          1. CONVDS - THE CONVERSION FACTOR FROM ARCSECONDS TO RADIANS
!                      (RAD/ARCSECOND)
!
      Real*8 STAR_ab(3,2), EPBASE(3,2)
      Real*8 DOTP, B(3), NCP(3), vectr(3), Bpr(3), NCPpr(3), W,         &
     &       VECMG, Starxx(3), Starab(3)
!     Integer*2 KERR, NDO(3)
!
!        Program variables:
!          1. U_V(2) -   Baseline coordinates in the (U,V) plane, in
!                        units of fringes per arcsec. [Multiply by
!                        206264.81 to get the more conventional values.]
!                        Scaled by the value of REF_FREQ.
!          2. REF_FREQ - For databases, should be the correct reference
!                        frequency. For correlator usage, set to 1.0 MHz,
!                        should be multiplied later by the correct
!                        frequency or frequencies.
!
!       PROGRAMMER:
!             David Gordon 1998.11.17 Subroutine created
!             David Gordon 2001.01.12 Logic reversed, default is to
!                          compute and add U/V coordinates, since the
!                          Mark IV correlators are not computing them.
!                          New access code ('UVF/MHz ', U/V coordinates in
!                          fringes per arcsec per MHz) added for Mark IV
!                          correlator usage.
!             Jim Ryan Sept 2002 Integer*2/4 mods.
!             David Gordon Dec. 2012 Moved GET's to GET_G and PUT's to
!                          PUT_G.
!
!       UVG PROGRAM STRUCTURE
!
!     IF (KASTC .ne. 0) Go to 800
!
!  Set frequency to 1 MHz for correlators.
      If (Calc_user .eq. 'C') REF_FREQ = 1.D6
      If (C_mode .eq. 'difx  ') REF_FREQ = 1.D6
!
! Take average of the two abberrated star vectors and renormalize
      Call VECAD(Star_ab(1,1), Star_ab(1,2), Starxx)
      Call Vunit(Starxx,Starab)
!
!   Baseline vector
       B(1) = EPBASE(1,1) * REF_FREQ/VLIGHT*CONVDS
       B(2) = EPBASE(2,1) * REF_FREQ/VLIGHT*CONVDS
       B(3) = EPBASE(3,1) * REF_FREQ/VLIGHT*CONVDS
!
      If (C_mode .eq. 'difx  ') Then
       B(1) = EPBASE(1,1) 
       B(2) = EPBASE(2,1)
       B(3) = EPBASE(3,1)
      Endif
!
!   NCP unit vector
       NCP(1) = 0.D0
       NCP(2) = 0.D0
       NCP(3) = 1.D0
!
! Get component of baseline vector projected into the plane
!   perpendicular to the STAR vector:
        CALL CROSP (STARab,      B, vectr)
        CALL CROSP (vectr,  STARab,   Bpr)
! Get component of NCP vector projected into the plane perpendicular
!   to the STAR vector:
        CALL CROSP (STARab,   NCP, vectr)
        CALL CROSP (vectr, STARab, NCPpr)
! Convert to a unit vector
        CALL VUNIT (NCPpr, NCP)
!
        U_V(2) = DOTP (Bpr, NCP)
        CALL CROSP (Bpr, NCP, vectr)
        U_V(1) = VECMG (vectr)
         If (DOTP (STARab,vectr) .lt. 0.d0) U_V(1) = -U_V(1)
! Add third component
        W = DOTP(B, STARab)
        Wb = W
!
!      Write (6,*) 'UVG:STARab: U,V,W  ', U_V, W
!
!     Normal conclusion.
  800 CONTINUE
      RETURN
      END
