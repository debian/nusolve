/*
 *
 *    This file is a part of Space Geodetic Library. The library is used by
 *    nuSolve, a part of CALC/SOLVE system, and designed to make analysis of
 *    geodetic VLBI observations.
 *    Copyright (C) 2010-2020 Sergei Bolotin.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <stdlib.h>


#include <QtCore/QDataStream>
#include <QtCore/QVector>


#include <SgLogger.h>
#include <SgMathSupport.h>
#include <SgTaskConfig.h>
#include <SgVector.h>
#include <SgVlbiBand.h>
#include <SgVlbiObservable.h>
#include <SgVlbiObservation.h>
#include <SgVlbiSession.h>



//
// declarations (just to use them only here):
//void subroutineParab(const double* pCXD, int n, double &dD, double &dA, double &dP);
void subroutineParab(const double* pX, const double* pY, int n, double &dD, double &dA, double &dP);



//
bool evaluatePhaseCals(const SgVector& pcPhases, const SgVector& pcAmplitudes,
  const SgVector& pcFreqByChan, double refFreq,
  double& phaseCalGrd, double& phaseCalGrdAmbig, 
  double& phaseCalPhd, double& phaseCalPhdAmbig,
  const QString& oId);
//bool evaluatePhaseCals(const SgVector& pcPhaseByChan, const SgVector& pcFreqByChan,
//  double& phaseCal4grd, const QString& oId);



/*=======================================================================================================
*
*                           METHODS:
* 
*======================================================================================================*/
//
// static first:
const QString SgVlbiObservable::className()
{
  return "SgVlbiObservable";
};



// CONSTRUCTORS:
//
// An empty constructor:
SgVlbiObservable::SgVlbiObservable(SgVlbiObservation* obs) :
  bandKey_(""),
  sbDelay_("Single Band Delay", SgTaskConfig::VD_SB_DELAY, SgTaskConfig::VR_NONE),
  grDelay_("Group Delay", SgTaskConfig::VD_GRP_DELAY, SgTaskConfig::VR_NONE),
  phDelay_("Phase Delay", SgTaskConfig::VD_PHS_DELAY, SgTaskConfig::VR_NONE),
  phDRate_("Phase Rate", SgTaskConfig::VD_NONE, SgTaskConfig::VR_PHS_RATE),
  errorCode_(""),
  tapeQualityCode_(""),
  nonUsableReason_(),
  fourfitOutputFName_(""),
  fourfitControlFile_(""),
  fourfitCommandOverride_(""),
  epochOfCorrelation_(tZero),
  epochOfFourfitting_(tZero),
  epochOfScan_(tZero),
  epochCentral_(tZero),
  tStart_(tZero),
  tStop_(tZero)
{
  mediaIdx_ = -1;
  owner_ = obs;
  band_ = NULL;
  baseline_ = NULL;
  stn_1_ = NULL;
  stn_2_ = NULL;
  src_ = NULL;
  referenceFrequency_ = 0.0;
  totalPhase_ = 0.0;
  corrCoeff_ = 0.0;
  snr_ = 0.0;
  qualityFactor_ = 0;
  numOfChannels_ = 0;
  isUsable_ = true;
//  nonUsableReason_ = NUR_UNDEF;
  activeDelay_ = &sbDelay_;
  activeRate_  = NULL;
  fourfitVersion_[0] = -1;
  fourfitVersion_[1] = -1;
  //
  startOffset_ = 0;
  stopOffset_ = 0;
  centrOffset_ = 0.0;
  sampleRate_ = 0.0;
  bitsPerSample_ = 0;
  effIntegrationTime_ = 0.0;
  acceptedRatio_ = 0.0;
  discardRatio_ = 0.0;
  incohChanAddAmp_ = 0.0;
  incohSegmAddAmp_ = 0.0;
  probabOfFalseDetection_ = 0.0;
  geocenterTotalPhase_ = 0.0;
  geocenterResidPhase_ = 0.0;
  aPrioriDra_[0] = aPrioriDra_[1] = aPrioriDra_[2] = 0.0;
  calcFeedCorrDelay_ = calcFeedCorrRate_ = 0.0;
  correlStarElev_1_ = correlZdelay_1_ = 0.0;
  correlStarElev_2_ = correlZdelay_2_ = 0.0;

  //
  for (int i=0; i<2; i++)
  {
    phaseCalGrDelays_[i] = phaseCalPhDelays_[i] = 
    phaseCalGrAmbigSpacings_[i] = phaseCalPhAmbigSpacings_[i] =
    phaseCalRates_[i] = uvFrPerAsec_[i] = uRvR_[i] = corrClocks_[i][0] = 
    corrClocks_[i][1] = instrDelay_[i] = 0.0;
    phaseCalGrAmbigMultipliers_[i] = phaseCalPhAmbigMultipliers_[i] = 0;
  };
  nLags_ = 0;
  for (int i=0; i<6; i++)
    fourfitSearchParameters_[i] = 0.0;
  hopsRevisionNumber_ = 0;

  numOfAccPeriodsByChan_USB_ = NULL;
  numOfAccPeriodsByChan_LSB_ = NULL;
  numOfSamplesByChan_USB_ = NULL;
  numOfSamplesByChan_LSB_ = NULL;
  refFreqByChan_ = NULL;
  fringeAmplitudeByChan_ = NULL;
  fringePhaseByChan_ = NULL;
  polarization_1ByChan_ = NULL;
  polarization_2ByChan_ = NULL;
  phaseCalData_1ByChan_ = NULL;
  phaseCalData_2ByChan_ = NULL;
  vDlys_ = NULL;
  vAuxData_ = NULL;
  corelIndexNumUSB_ = NULL;
  corelIndexNumLSB_ = NULL;

  chanIdByChan_     = NULL;
  loFreqByChan_1_   = NULL;
  loFreqByChan_2_   = NULL;
  bbcIdxByChan_1_   = NULL;
  bbcIdxByChan_2_   = NULL;


  phaseCalModes_ = -1;
  //
  // temporary/tests:
  sbdDiffBand_ = 0.0;
  grdDiffBand_ = 0.0;
  phrDiffBand_ = 0.0;
  sbdQ_ = 0.0;
  grdQ_ = 0.0;
  phrQ_ = 0.0;
  dTauS_= 0.0;
  dTest_ = dTest2_ = 0.0;
};



//
SgVlbiObservable::SgVlbiObservable(SgVlbiObservation* obs, SgVlbiBand* band) :
  bandKey_(band?band->getKey():""),
  sbDelay_("Single Band Delay", SgTaskConfig::VD_SB_DELAY, SgTaskConfig::VR_NONE),
  grDelay_("Group Delay", SgTaskConfig::VD_GRP_DELAY, SgTaskConfig::VR_NONE),
  phDelay_("Phase Delay", SgTaskConfig::VD_PHS_DELAY, SgTaskConfig::VR_NONE),
  phDRate_("Phase Rate", SgTaskConfig::VD_NONE, SgTaskConfig::VR_PHS_RATE),
  errorCode_(""),
  tapeQualityCode_(""),
  nonUsableReason_(),
  fourfitOutputFName_(""),
  fourfitControlFile_(""),
  fourfitCommandOverride_(""),
  epochOfCorrelation_(tZero),
  epochOfFourfitting_(tZero),
  epochOfScan_(tZero),
  epochCentral_(tZero),
  tStart_(tZero),
  tStop_(tZero)
{
  mediaIdx_ = -1;
  owner_ = obs;
  band_ = band;
  baseline_ = NULL;
  stn_1_ = NULL;
  stn_2_ = NULL;
  src_ = NULL;
  referenceFrequency_ = 0.0;
  totalPhase_ = 0.0;
  corrCoeff_ = 0.0;
  snr_ = 0.0;
  qualityFactor_ = 0;
  numOfChannels_ = 0;
  isUsable_ = true;
//  nonUsableReason_ = NUR_UNDEF;
  activeDelay_ = &sbDelay_;
  activeRate_ = NULL;
  fourfitVersion_[0] = 0;
  fourfitVersion_[1] = 0;
  //
  startOffset_ = 0;
  stopOffset_ = 0;
  centrOffset_ = 0.0;
  sampleRate_ = 0.0;
  bitsPerSample_ = 0;
  effIntegrationTime_ = 0.0;
  acceptedRatio_ = 0.0;
  discardRatio_ = 0.0;
  incohChanAddAmp_ = 0.0;
  incohSegmAddAmp_ = 0.0;
  probabOfFalseDetection_ = 0.0;
  geocenterTotalPhase_ = 0.0;
  geocenterResidPhase_ = 0.0;
  aPrioriDra_[0] = aPrioriDra_[1] = aPrioriDra_[2] = 0.0 ;
  calcFeedCorrDelay_ = calcFeedCorrRate_ = 0.0;
  correlStarElev_1_ = correlZdelay_1_ = 0.0;
  correlStarElev_2_ = correlZdelay_2_ = 0.0;
  //
  for (int i=0; i<2; i++)
  {
    phaseCalGrDelays_[i] = phaseCalPhDelays_[i] = 
    phaseCalGrAmbigSpacings_[i] = phaseCalPhAmbigSpacings_[i] =
    phaseCalRates_[i] = uvFrPerAsec_[i] = uRvR_[i] = corrClocks_[i][0] = 
    corrClocks_[i][1] = instrDelay_[i] = 0.0;
    phaseCalGrAmbigMultipliers_[i] = phaseCalPhAmbigMultipliers_[i] = 0;
  };
  nLags_ = 0;
  for (int i=0; i<6; i++)
    fourfitSearchParameters_[i] = 0.0;
  hopsRevisionNumber_ = 0;
  //
  numOfAccPeriodsByChan_USB_ = NULL;
  numOfAccPeriodsByChan_LSB_ = NULL;
  numOfSamplesByChan_USB_ = NULL;
  numOfSamplesByChan_LSB_ = NULL;
  refFreqByChan_ = NULL;
  fringeAmplitudeByChan_ = NULL;
  fringePhaseByChan_ = NULL;
  polarization_1ByChan_ = NULL;
  polarization_2ByChan_ = NULL;
  phaseCalData_1ByChan_ = NULL;
  phaseCalData_2ByChan_ = NULL;
  vDlys_ = NULL;
  vAuxData_ = NULL;
  corelIndexNumUSB_ = NULL;
  corelIndexNumLSB_ = NULL;

  chanIdByChan_     = NULL;
  loFreqByChan_1_   = NULL;
  loFreqByChan_2_   = NULL;
  bbcIdxByChan_1_   = NULL;
  bbcIdxByChan_2_   = NULL;

  phaseCalModes_ = -1;
  //
  // temporary/tests:
  sbdDiffBand_ = 0.0;
  grdDiffBand_ = 0.0;
  phrDiffBand_ = 0.0;
  sbdQ_ = 0.0;
  grdQ_ = 0.0;
  phrQ_ = 0.0;
  dTauS_= 0.0;
  dTest_ = dTest2_ = 0.0;
};






// A copy constructor:
SgVlbiObservable::SgVlbiObservable(SgVlbiObservation* obs, const SgVlbiObservable& o) :
  bandKey_(o.getBandKey()),
  sbDelay_(o.sbDelay_),
  grDelay_(o.grDelay_),
  phDelay_(o.phDelay_),
  phDRate_(o.phDRate_),
  errorCode_(o.getErrorCode()),
  tapeQualityCode_(o.getTapeQualityCode()),
  fourfitOutputFName_(o.getFourfitOutputFName()),
  fourfitControlFile_(o.getFourfitControlFile()),
  fourfitCommandOverride_(o.getFourfitCommandOverride()),
  epochOfCorrelation_(o.getEpochOfCorrelation()),
  epochOfFourfitting_(o.getEpochOfFourfitting()),
  epochOfScan_(o.getEpochOfScan()),
  epochCentral_(o.getEpochCentral()),
  tStart_(o.getTstart()),
  tStop_(o.getTstop())
{
  owner_ = obs;
  band_ = o.band_;
  baseline_ = o.getBaseline();
  stn_1_ = o.getStn_1();
  stn_2_ = o.getStn_2();
  src_ = o.getSrc();
  setMediaIdx(o.getMediaIdx());
  setReferenceFrequency(o.getReferenceFrequency());
  setTotalPhase(o.getTotalPhase());
  setCorrCoeff(o.getCorrCoeff());
  setSnr(o.getSnr());
  setQualityFactor(o.getQualityFactor());
  setNumOfChannels(o.getNumOfChannels());
  activeDelay_ = &sbDelay_;
  activeRate_  = NULL;
  fourfitVersion_[0] = o.getFourfitVersion(0);
  fourfitVersion_[1] = o.getFourfitVersion(1);
  //
  setStartOffset(o.getStartOffset());
  setStopOffset(o.getStopOffset());
  setCentrOffset(o.getCentrOffset());
  setSampleRate(o.getSampleRate());
  setBitsPerSample(o.getBitsPerSample());
  setEffIntegrationTime(o.getEffIntegrationTime());
  setAcceptedRatio(o.getAcceptedRatio());
  setDiscardRatio(o.getDiscardRatio());
  setIncohChanAddAmp(o.getIncohChanAddAmp());
  setIncohSegmAddAmp(o.getIncohSegmAddAmp());
  setProbabOfFalseDetection(o.getProbabOfFalseDetection());
  setGeocenterTotalPhase(o.getGeocenterTotalPhase());
  setGeocenterResidPhase(o.getGeocenterResidPhase());
  setAprioriDra(0, o.getAprioriDra(0));
  setAprioriDra(1, o.getAprioriDra(1));
  setAprioriDra(2, o.getAprioriDra(2));
  setCalcFeedCorrDelay(o.getCalcFeedCorrDelay());
  setCalcFeedCorrRate(o.getCalcFeedCorrRate());
  setCorrelStarElev_1(o.getCorrelStarElev_1());
  setCorrelStarElev_2(o.getCorrelStarElev_2());
  setCorrelZdelay_1(o.getCorrelZdelay_1());
  setCorrelZdelay_2(o.getCorrelZdelay_2());

  for (int i=0; i<2; i++)
  {
    setPhaseCalGrDelays(i, o.getPhaseCalGrDelays(i));
    setPhaseCalPhDelays(i, o.getPhaseCalPhDelays(i));
    setPhaseCalRates(i, o.getPhaseCalRates(i));
    setUvFrPerAsec(i, o.getUvFrPerAsec(i));
    setUrVr(i, o.getUrVr(i));
    setCorrClocks(i, 0, o.getCorrClocks(i, 0));
    setCorrClocks(i, 1, o.getCorrClocks(i, 1));
    setInstrDelay(i, o.getInstrDelay(i));
  }
  setNlags(o.getNlags());
  for (int i=0; i<6; i++)
    setFourfitSearchParameters(i, o.getFourfitSearchParameters(i));
  setHopsRevisionNumber(o.getHopsRevisionNumber());
    
  //
  if (o.numOfAccPeriodsByChan_USB())
    numOfAccPeriodsByChan_USB_ = new SgVector(*o.numOfAccPeriodsByChan_USB());
  else
    numOfAccPeriodsByChan_USB_ = NULL;
    
  if (o.numOfAccPeriodsByChan_LSB())
    numOfAccPeriodsByChan_LSB_ = new SgVector(*o.numOfAccPeriodsByChan_LSB());
  else
    numOfAccPeriodsByChan_LSB_ = NULL;
    
  if (o.numOfSamplesByChan_USB())
    numOfSamplesByChan_USB_ = new SgVector(*o.numOfSamplesByChan_USB());
  else
    numOfSamplesByChan_USB_ = NULL;
    
  if (o.numOfSamplesByChan_LSB())
    numOfSamplesByChan_LSB_ = new SgVector(*o.numOfSamplesByChan_LSB());
  else
    numOfSamplesByChan_LSB_ = NULL;
    
  if (o.refFreqByChan())
    refFreqByChan_ = new SgVector(*o.refFreqByChan());
  else
    refFreqByChan_ = NULL;
    
  if (o.fringeAmplitudeByChan())
    fringeAmplitudeByChan_ = new SgVector(*o.fringeAmplitudeByChan());
  else
    fringeAmplitudeByChan_ = NULL;

  if (o.fringePhaseByChan())
    fringePhaseByChan_ = new SgVector(*o.fringePhaseByChan());
  else
    fringePhaseByChan_ = NULL;

  if (o.polarization_1ByChan())
    polarization_1ByChan_ = new QVector<char>(*o.polarization_1ByChan());
  else
    polarization_1ByChan_ = NULL;

  if (o.polarization_2ByChan())
    polarization_2ByChan_ = new QVector<char>(*o.polarization_2ByChan());
  else
    polarization_2ByChan_ = NULL;

  if (o.phaseCalData_1ByChan())
    phaseCalData_1ByChan_ = new SgMatrix(*o.phaseCalData_1ByChan());
  else
    phaseCalData_1ByChan_ = NULL;

  if (o.vDlys())
    vDlys_ = new SgVector(*o.vDlys());
  else
    vDlys_ = NULL;
  
  if (o.vAuxData())
    vAuxData_ = new SgVector(*o.vAuxData());
  else
    vAuxData_ = NULL;

  if (o.phaseCalData_2ByChan())
    phaseCalData_2ByChan_ = new SgMatrix(*o.phaseCalData_2ByChan());
  else
    phaseCalData_2ByChan_ = NULL;

  setPhaseCalModes(o.getPhaseCalModes());

  if (o.corelIndexNumUSB())
    corelIndexNumUSB_ = new QVector<int>(*o.corelIndexNumUSB());
  else
    corelIndexNumUSB_ = NULL;
    
  if (o.corelIndexNumLSB())
    corelIndexNumLSB_ = new QVector<int>(*o.corelIndexNumLSB());
  else
    corelIndexNumLSB_ = NULL;
  
  //
  if (o.chanIdByChan())
    chanIdByChan_ = new QVector<char>(*o.chanIdByChan());
  else
    chanIdByChan_ = NULL;

  if (o.loFreqByChan_1())
    loFreqByChan_1_ = new SgVector(*o.loFreqByChan_1());
  else
    loFreqByChan_1_ = NULL;
  if (o.loFreqByChan_2())
    loFreqByChan_2_ = new SgVector(*o.loFreqByChan_2());
  else
    loFreqByChan_2_ = NULL;
  
  bbcIdxByChan_1_ = o.bbcIdxByChan_1() ? new QVector<int>(*o.bbcIdxByChan_1()) : NULL;
  bbcIdxByChan_2_ = o.bbcIdxByChan_2() ? new QVector<int>(*o.bbcIdxByChan_2()) : NULL;
  //
  // temporary/tests:
  setSbdDiffBand(o.getSbdDiffBand());
  setGrdDiffBand(o.getGrdDiffBand());
  setPhrDiffBand(o.getPhrDiffBand());
  setSbdQ(o.getSbdQ());
  setGrdQ(o.getGrdQ());
  setPhrQ(o.getPhrQ());
  dTauS_ = o.getTauS();
  dTest_ = o.dTest_;
  dTest2_ = o.dTest2_;
};



//
void SgVlbiObservable::setupActiveMeasurements(const SgTaskConfig* cfg)
{
  switch (cfg->getUseDelayType())
  {
  default:
  case SgTaskConfig::VD_NONE:
    activeDelay_ = NULL;
    break;
  case SgTaskConfig::VD_SB_DELAY:
    activeDelay_ = &sbDelay_;
    break;
  case SgTaskConfig::VD_GRP_DELAY:
    activeDelay_ = &grDelay_;
    break;
  case SgTaskConfig::VD_PHS_DELAY:
    activeDelay_ = &phDelay_;
    break;
  };
  switch (cfg->getUseRateType())
  {
  default:
  case SgTaskConfig::VR_NONE:
    activeRate_ = NULL;
    break;
  case SgTaskConfig::VR_PHS_RATE:
    activeRate_ = &phDRate_;
    break;
  };
};



// returns true if the obs is potentially good:
bool SgVlbiObservable::isRestorable_old(const SgTaskConfig* cfg)
{
  bool                          isOk=true;
  // quality code:
  if (cfg && getQualityFactor()<cfg->getQualityCodeThreshold())
    isOk = false;
  // deselected station, baseline, source:
  if (owner_->stn_1()->isAttr(SgVlbiStationInfo::Attr_NOT_VALID))
    return false;
  if (owner_->stn_2()->isAttr(SgVlbiStationInfo::Attr_NOT_VALID))
    return false;
  if (owner_->src()->isAttr(SgVlbiSourceInfo::Attr_NOT_VALID))
    return false;
  if (owner_->baseline()->isAttr(SgVlbiBaselineInfo::Attr_NOT_VALID))
    return false;
  // ionosphere:
  //if (isOk && owner_->session()->isAttr(SgVlbiSession::Attr_FF_ION_C_CALCULATED))
  if (isOk && owner_->session()->isAttr(SgVlbiSession::Attr_HAS_IONO_CORR))
  {
    if (owner_->observableByKey().size() == 1) //missed obs on the other band
      isOk = false;
    if (isOk && cfg && owner_->minQualityFactor() < cfg->getQualityCodeThreshold()) //not enough quality
      isOk = false;
  };
  return isOk;
};



void SgVlbiObservable::checkUsability(const SgTaskConfig* cfg)
{
  isUsable_ = true;
  nonUsableReason_.clearAll();
  //
  if (cfg && getQualityFactor()<cfg->getQualityCodeThreshold())
  {
    isUsable_ = false;
    nonUsableReason_.addAttr(NUR_LOW_QF);
  };
  if (1<band_->getMaxNumOfChannels() && numOfChannels_ < 2)
  {
    isUsable_ = false;
    nonUsableReason_.addAttr(NUR_ONE_CHANNEL);
  };
  if (owner_->stn_1()->isAttr(SgVlbiStationInfo::Attr_NOT_VALID))
  {
    isUsable_ = false;
    nonUsableReason_.addAttr(NUR_DESELECTED_STATION);
  };
  if (owner_->stn_2()->isAttr(SgVlbiStationInfo::Attr_NOT_VALID))
  {
    isUsable_ = false;
    nonUsableReason_.addAttr(NUR_DESELECTED_STATION);
  };
  if (owner_->src()->isAttr(SgVlbiSourceInfo::Attr_NOT_VALID))
  {
    isUsable_ = false;
    nonUsableReason_.addAttr(NUR_DESELECTED_SOURCE);
  };
  if (owner_->baseline()->isAttr(SgVlbiBaselineInfo::Attr_NOT_VALID))
  {
    isUsable_ = false;
    nonUsableReason_.addAttr(NUR_DESELECTED_BASELINE);
  };
  if (errorCode_.simplified().size() &&
      !((cfg->getUseQualityCodeG() && errorCode_.contains('G')) ||
        (cfg->getUseQualityCodeH() && errorCode_.contains('H'))  )  )
  {
    isUsable_ = false;
    nonUsableReason_.addAttr(NUR_HAS_ERROR_CODE);
  };
  //
  // dual band combinations, check other band:
  if (owner_->session()->bands().size()>1 &&
//      owner_->session()->isAttr(SgVlbiSession::Attr_FF_ION_C_CALCULATED) &&
      owner_->session()->isAttr(SgVlbiSession::Attr_HAS_IONO_CORR) &&
      ( cfg->getUseRateType()==SgTaskConfig::VR_PHS_RATE ||
        cfg->getUseDelayType()==SgTaskConfig::VD_SB_DELAY ||
        (cfg->getUseDelayType()==SgTaskConfig::VD_GRP_DELAY &&
                owner_->baseline()->isAttr(SgVlbiBaselineInfo::Attr_USE_IONO4GRD)) ||
        (cfg->getUseDelayType()==SgTaskConfig::VD_PHS_DELAY && 
                owner_->baseline()->isAttr(SgVlbiBaselineInfo::Attr_USE_IONO4PHD))  )
      )
  {
    if (owner_->observableByKey().size() == 1)
    {
      isUsable_ = false;
      nonUsableReason_.addAttr(NUR_UNMATED);
    }
    else
      for (int i=0; i<owner_->passiveObses().size(); i++)
      {
        SgVlbiObservable       *o=owner_->passiveObses().at(i);
        if (o->getQualityFactor() < cfg->getQualityCodeThreshold())
        {
          isUsable_ = false;
          nonUsableReason_.addAttr(NUR_MATE_LOW_QF);
        };
        if (1<o->band()->getMaxNumOfChannels() && o->getNumOfChannels() < 2)
        {
          isUsable_ = false;
          nonUsableReason_.addAttr(NUR_MATE_ONE_CHANNEL);
        };
        if (o->getErrorCode().simplified().size() && 
            !((cfg->getUseQualityCodeG() && o->getErrorCode().contains('G')) ||
              (cfg->getUseQualityCodeH() && o->getErrorCode().contains('H'))  ) )
        {
          isUsable_ = false;
          nonUsableReason_.addAttr(NUR_MATE_HAS_ERROR_CODE);
        };
      };
  };
};



//
const SgMJD& SgVlbiObservable::epoch() const
{
  return owner_?(*owner_):tZero;
};



//
QString SgVlbiObservable::strId() const
{
  QString                       srcN("?"), blnN("?:?");
  SgVlbiSourceInfo             *si=src_;
  SgVlbiBaselineInfo           *bi=baseline_;
  if (!si)
    si = owner_->session()->lookupSourceByIdx(owner_->getSourceIdx());
  if (!bi)
    bi = owner_->session()->lookupBaselineByIdx(owner_->getBaselineIdx());
  if (si)
    srcN = si->getKey();
  if (bi)
    blnN = bi->getKey();
  return 
    " #" + QString("").setNum(mediaIdx_) + " on " + bandKey_ + "-band, " + 
    epoch().toString(SgMJD::F_YYYYMMDDHHMMSSSS) + " of " + srcN + " at " + blnN + 
    " [" + owner_->getScanName() + "]";
};



//
void SgVlbiObservable::allocateChannelsSetupStorages(int numOfChans)
{
  releaseChannelsSetupStorages();
  if ((numOfChannels_=numOfChans) > 0)
  {
    numOfAccPeriodsByChan_USB_ = new SgVector(numOfChannels_);
    numOfAccPeriodsByChan_LSB_ = new SgVector(numOfChannels_);
    numOfSamplesByChan_USB_ = new SgVector(numOfChannels_);
    numOfSamplesByChan_LSB_ = new SgVector(numOfChannels_);
    refFreqByChan_ = new SgVector(numOfChannels_);
    fringeAmplitudeByChan_ = new SgVector(numOfChannels_);
    fringePhaseByChan_ = new SgVector(numOfChannels_);
    polarization_1ByChan_ = new QVector<char>(numOfChannels_);
    polarization_2ByChan_ = new QVector<char>(numOfChannels_);
    phaseCalData_1ByChan_ = new SgMatrix(5, numOfChannels_);
    phaseCalData_2ByChan_ = new SgMatrix(5, numOfChannels_);
    //
    vDlys_ = new SgVector(3);
    vAuxData_ = new SgVector(5);
    // fixed size (mimic dbedit behavior):
    corelIndexNumUSB_ = new QVector<int>(numOfChannels_);
    corelIndexNumLSB_ = new QVector<int>(numOfChannels_);
    //
    chanIdByChan_ = new QVector<char>(numOfChannels_);
    loFreqByChan_1_ = new SgVector(numOfChannels_);
    loFreqByChan_2_ = new SgVector(numOfChannels_);
    bbcIdxByChan_1_ = new QVector<int>(numOfChannels_);
    bbcIdxByChan_2_ = new QVector<int>(numOfChannels_);
  }
  else
    logger->write(SgLogger::WRN, SgLogger::IO_DBH, className() +
      ": allocateChannelsSetupStorages(): an attempt to allocate data with zero number of channels");
};



//
void SgVlbiObservable::releaseChannelsSetupStorages()
{
  if (numOfAccPeriodsByChan_USB_)
  {
    delete numOfAccPeriodsByChan_USB_;
    numOfAccPeriodsByChan_USB_ = NULL;
  };
  if (numOfAccPeriodsByChan_LSB_)
  {
    delete numOfAccPeriodsByChan_LSB_;
    numOfAccPeriodsByChan_LSB_ = NULL;
  };
  if (numOfSamplesByChan_USB_)
  {
    delete numOfSamplesByChan_USB_;
    numOfSamplesByChan_USB_ = NULL;
  };
  if (numOfSamplesByChan_LSB_)
  {
    delete numOfSamplesByChan_LSB_;
    numOfSamplesByChan_LSB_ = NULL;
  };
  if (refFreqByChan_)
  {
    delete refFreqByChan_;
    refFreqByChan_ = NULL;
  };
  if (fringeAmplitudeByChan_)
  {
    delete fringeAmplitudeByChan_;
    fringeAmplitudeByChan_ = NULL;
  };
  if (fringePhaseByChan_)
  {
    delete fringePhaseByChan_;
    fringePhaseByChan_ = NULL;
  };
  if (polarization_1ByChan_)
  {
    delete polarization_1ByChan_;
    polarization_1ByChan_ = NULL;
  };
  if (polarization_2ByChan_)
  {
    delete polarization_2ByChan_;
    polarization_2ByChan_ = NULL;
  };
  if (phaseCalData_1ByChan_)
  {
    delete phaseCalData_1ByChan_;
    phaseCalData_1ByChan_ = NULL;
  };
  if (phaseCalData_2ByChan_)
  {
    delete phaseCalData_2ByChan_;
    phaseCalData_2ByChan_ = NULL;
  };
  if (vDlys_)
  {
    delete vDlys_;
    vDlys_ = NULL;
  };
  if (vAuxData_)
  {
    delete vAuxData_;
    vAuxData_ = NULL;
  };
  if (corelIndexNumUSB_)
  {
    delete corelIndexNumUSB_;
    corelIndexNumUSB_ = NULL;
  }; 
  if (corelIndexNumLSB_)
  {
    delete corelIndexNumLSB_;
    corelIndexNumLSB_ = NULL;
  }; 

  if (chanIdByChan_)
  {
    delete chanIdByChan_;
    chanIdByChan_ = NULL;
  }; 
  if (loFreqByChan_1_)
  {
    delete loFreqByChan_1_;
    loFreqByChan_1_ = NULL;
  }; 
  if (loFreqByChan_2_)
  {
    delete loFreqByChan_2_;
    loFreqByChan_2_ = NULL;
  }; 
  if (bbcIdxByChan_1_)
  {
    delete bbcIdxByChan_1_;
    bbcIdxByChan_1_ = NULL;
  }; 
  if (bbcIdxByChan_2_)
  {
    delete bbcIdxByChan_2_;
    bbcIdxByChan_2_ = NULL;
  }; 
};



//
/*
int SgVlbiObservable::qualityFactor() const
{
  int                           qFactor;
  bool                          isOk;
  qFactor = qualityCode_.toInt(&isOk);
  if (!isOk)
    qFactor = -1;
  return qFactor;
};
*/



//
int SgVlbiObservable::errorCode2Int() const
{
  int                           n=-29;
  if (errorCode_ == "" || errorCode_ == " " || errorCode_ == "  ")
    n = 0;
  else if (errorCode_ == "A")
    n = -1;
  else if (errorCode_ == "B")
    n = -2;
  else if (errorCode_ == "C")
    n = -3;
  else if (errorCode_ == "D")
    n = -4;
  else if (errorCode_ == "E")
    n = -5;
  else if (errorCode_ == "F")
    n = -6;
  else if (errorCode_ == "G")
    n = -7;
  else if (errorCode_ == "H")
    n = -8;
  else if (errorCode_ == "I")
    n = -9;
  else if (errorCode_ == "J")
    n = -10;
  else if (errorCode_ == "K")
    n = -11;
  else if (errorCode_ == "L")
    n = -12;
  else if (errorCode_ == "M")
    n = -13;
  else if (errorCode_ == "N")
    n = -14;
  else if (errorCode_ == "O")
    n = -15;
  else if (errorCode_ == "P")
    n = -16;
  else if (errorCode_ == "Q")
    n = -17;
  else if (errorCode_ == "R")
    n = -18;
  else if (errorCode_ == "S")
    n = -19;
  else if (errorCode_ == "T")
    n = -20;
  else if (errorCode_ == "U")
    n = -21;
  else if (errorCode_ == "V")
    n = -22;
  else if (errorCode_ == "W")
    n = -23;
  else if (errorCode_ == "X")
    n = -24;
  else if (errorCode_ == "Y")
    n = -25;
  else if (errorCode_ == "Z")
    n = -26;
  return n;
};



//
void SgVlbiObservable::resetAllEditings()
{
  sbDelay_.resetAllEditings();
  grDelay_.resetAllEditings();
  phDelay_.resetAllEditings();
  phDRate_.resetAllEditings();
  if (sbDelay_.getSigma() < 5.0e-12)
    owner()->addAttr(SgVlbiObservation::Attr_SBD_NOT_VALID);
  if (grDelay_.getSigma() < 1.0e-15)
    owner()->addAttr(SgVlbiObservation::Attr_NOT_VALID);
};



//
bool SgVlbiObservable::saveIntermediateResults(QDataStream& s) const
{
  s << bandKey_ << mediaIdx_ << isUsable_ << nonUsableReason_.getAttributes();
  if (s.status() == QDataStream::Ok &&
      sbDelay_.saveIntermediateResults(s) &&
      grDelay_.saveIntermediateResults(s) &&
      phDelay_.saveIntermediateResults(s) &&
      phDRate_.saveIntermediateResults(s))
    return s.status() == QDataStream::Ok;
  else
    return false;
};



//
bool SgVlbiObservable::loadIntermediateResults(QDataStream& s)
{
  QString                       bandKey;
  int                           mediaIdx;
  bool                          is;
  unsigned int                  attributes;
  s >> bandKey >> mediaIdx >> is >> attributes;
  if (s.status() == QDataStream::Ok)
  {
    if (bandKey_ != bandKey)
    {
      logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
        ": loadIntermediateResults(): error reading " + bandKey_ + 
        "-band data: bandKey mismatch: got [" + bandKey + "], expected [" + bandKey_ + "]");
      return false;
    };
    if (mediaIdx_ != mediaIdx)
    {
      logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
        ": loadIntermediateResults(): error reading " + bandKey_ + 
        "-band data: media index mismatch: got [" + 
        QString("").setNum(mediaIdx) + "], expected [" + QString("").setNum(mediaIdx_) + "]");
      return false;
    };
    isUsable_ = is;
    nonUsableReason_.setAttributes(attributes);
    if (sbDelay_.loadIntermediateResults(s) &&
        grDelay_.loadIntermediateResults(s) &&
        phDelay_.loadIntermediateResults(s) &&
        phDRate_.loadIntermediateResults(s))
      return true;
  };
  logger->write(SgLogger::WRN, SgLogger::IO_BIN, className() +
    ": loadIntermediateResults(): error reading data: " +
    (s.status()==QDataStream::ReadPastEnd?"read past end of the file":"read corrupt data"));
  return false;
};



//
void SgVlbiObservable::calcPhaseDelay()
{
  if (snr_ <= 0.0) // just noise
    return;
  
  if (!owner_)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": calcPhaseDelay(): cannot calculate phase delay: the owner is NULL");
    return;
  };
  if (!band_)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": calcPhaseDelay(): cannot calculate phase delay: the band is NULL");
    return;
  };
  SgVlbiAuxObservation         *aux_1=owner_->auxObs_1(), *aux_2=owner_->auxObs_2();
  if (!(aux_1 && aux_2))
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": calcPhaseDelay(): cannot calculate phase delay: the aux obss are NULL");
    return;
  };
  double                        vPhDly, ePhDly, aPhDly, dNumAmbigs;

  vPhDly = (totalPhase_ - (aux_2->getParallacticAngle() - aux_1->getParallacticAngle()))*1.0e-6
                                                                        /(2.0*M_PI*referenceFrequency_);
  if (1.0e-10 < snr_)
    ePhDly = 1.0e-6/(2.0*M_PI*referenceFrequency_*snr_);
  else
  {
    logger->write(SgLogger::WRN, SgLogger::IO_BIN, className() +
      ": calcPhaseDelay(): got unusual value for SNR: " + QString("").setNum(snr_));
    ePhDly = 1.0e-6/(2.0*M_PI*referenceFrequency_);
  };
  aPhDly = 1.0e-6/referenceFrequency_; // refFreq is in Mhz
  dNumAmbigs = round((grDelay_.getValue() + grDelay_.ambiguity() - vPhDly)/aPhDly);

  phDelay_.setValue(vPhDly + dNumAmbigs*aPhDly);
  phDelay_.setSigma(ePhDly);
  phDelay_.setAmbiguitySpacing(aPhDly);
};



//
void SgVlbiObservable::calcPhaseCalDelay()
{
  if (!band_)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": calcPhaseCalDelay(): cannot calculate phase cal effects: band is NULL");
    return;
  };
  if (!refFreqByChan_)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": calcPhaseCalDelay(): cannot calculate phase cal effects: ref.freq storage is NULL");
    return;
  };
  if (!phaseCalData_1ByChan_)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": calcPhaseCalDelay(): cannot calculate phase cal effects: pcal phases @1 storage is NULL");
    return;
  };
  if (!phaseCalData_2ByChan_)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": calcPhaseCalDelay(): cannot calculate phase cal effects: pcal phases @2 storage is NULL");
    return;
  };
  if (refFreqByChan_->n() != phaseCalData_1ByChan_->nCol())
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": calcPhaseCalDelay(): cannot calculate phase cal effects: size mismatch for @1: " +
      QString("").sprintf("#chan(%d) != #PCchan(%d)", 
        refFreqByChan_->n(), phaseCalData_1ByChan_->nCol()));
    return;
  };
  if (refFreqByChan_->n() != phaseCalData_2ByChan_->nCol())
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": calcPhaseCalDelay(): cannot calculate phase cal effects: size mismatch for @2");
    return;
  };
  int                           n=refFreqByChan_->n();
  SgVector                     *pcPhases_1 = new SgVector(n);
  SgVector                     *pcPhases_2 = new SgVector(n);
  SgVector                     *pcAmplitudes_1 = new SgVector(n);
  SgVector                     *pcAmplitudes_2 = new SgVector(n);

  for (int i=0; i<n; i++)
  {
    pcPhases_1    ->setElement(i, phaseCalData_1ByChan_->getElement(PCCI_PHASE, i) + 
                                  phaseCalData_1ByChan_->getElement(PCCI_OFFSET, i));
    pcPhases_2    ->setElement(i, phaseCalData_2ByChan_->getElement(PCCI_PHASE, i) + 
                                  phaseCalData_2ByChan_->getElement(PCCI_OFFSET, i));
//    pcAmplitudes_1->setElement(i, phaseCalData_1ByChan_->getElement(PCCI_AMPLITUDE, i)*1.0e-4);
//    pcAmplitudes_2->setElement(i, phaseCalData_2ByChan_->getElement(PCCI_AMPLITUDE, i)*1.0e-4);
    pcAmplitudes_1->setElement(i, phaseCalData_1ByChan_->getElement(PCCI_AMPLITUDE, i));
    pcAmplitudes_2->setElement(i, phaseCalData_2ByChan_->getElement(PCCI_AMPLITUDE, i));
    pcAmplitudes_1->setElement(i, 1.0);
    pcAmplitudes_2->setElement(i, 1.0);
  };

  double                        phaseCalGrd=0.0;
  double                        phaseCalGrdAmbig=0.0;
  double                        phaseCalPhd=0.0;
  double                        phaseCalPhdAmbig=0.0;

  if (evaluatePhaseCals(*pcPhases_1, *pcAmplitudes_1, *refFreqByChan_, referenceFrequency_,
    phaseCalGrd, phaseCalGrdAmbig, phaseCalPhd, phaseCalPhdAmbig, strId()))
  {
    phaseCalGrDelays_[0] = phaseCalGrd*1.0e-9;
    phaseCalPhDelays_[0] = phaseCalPhd*1.0e-9;
    phaseCalGrAmbigSpacings_[0] = phaseCalGrdAmbig*1.0e-9;
    phaseCalPhAmbigSpacings_[0] = phaseCalPhdAmbig*1.0e-9;
  };

  if (evaluatePhaseCals(*pcPhases_2, *pcAmplitudes_2, *refFreqByChan_, referenceFrequency_,
    phaseCalGrd, phaseCalGrdAmbig, phaseCalPhd, phaseCalPhdAmbig, strId()))
  {
    phaseCalGrDelays_[1] = phaseCalGrd*1.0e-9;
    phaseCalPhDelays_[1] = phaseCalPhd*1.0e-9;
    phaseCalGrAmbigSpacings_[1] = phaseCalGrdAmbig*1.0e-9;
    phaseCalPhAmbigSpacings_[1] = phaseCalPhdAmbig*1.0e-9;
  };
  //
  delete pcPhases_1;
  delete pcPhases_2;
  delete pcAmplitudes_1;
  delete pcAmplitudes_2;
};
/*=====================================================================================================*/



/*=====================================================================================================*/
//
//                           FRIENDS:
// 
/*=====================================================================================================*/
//



/*=====================================================================================================*/
//
// aux functions:
//
/*=====================================================================================================*/
//
bool evaluatePhaseCals(const SgVector& pcPhases, const SgVector& pcAmplitudes,
  const SgVector& pcFreqByChan, double refFreq,
  double& phaseCalGrd, double& phaseCalGrdAmbig, 
  double& phaseCalPhd, double& phaseCalPhdAmbig, 
  const QString& oId)
{
  int                           nChan=pcFreqByChan.n();
  phaseCalGrdAmbig = 0.0;
  phaseCalPhdAmbig = 0.0;
  if (nChan == 0)
  {
    logger->write(SgLogger::INF, SgLogger::PREPROC,
      "evaluatePhaseCals: number of actual channels is zero, nothing to do for the obs " + oId);
    return false;
  };
  if (nChan == 1)
  {
    logger->write(SgLogger::INF, SgLogger::PREPROC,
      "evaluatePhaseCals: have only one actual channel, nothing to do for the obs " + oId);
    return false;
  };
  // $MK5_ROOT/progs/dbcal/pcclc.f:
//--
//      COMPLEX*16  CDD(16), CD(256)
  double                       *pX=NULL, *pY=NULL;
  double                       *pXsvd=NULL, *pYsvd=NULL;
//      REAL*8 SUMI, SUMR, SPP, CPP, SINP, COSP, FFF, AWOPI
//  double                        cosPh, sinPh;
//      REAL*8 REFFR, PCGRP, PCPHA, GPAMB, PHAMB
//      REAL*8 RFREQ(16), RPCP(16), PHRES(16)
//      REAL*8 A, B, RFMIN, TWOPI
  double                        rfMin, diffMin, d;
//  int                          *idxs=new int[nChan], nData=256;
  int                          *idxs=new int[nChan], nData=512;
//      REAL*8 D, A4, P
  double                        dD, dA4, dP;
//      INTEGER*4 I, J, K, IERR, luout, IELEM(16), I256, I1
//      INTEGER*2 NFREQ
//
//      DATA TWOPI /6.2831853071796D0/
// !
// !     GET LOWEST FREQUENCY.  GET SMALLEST SPACING.
// !
//       A=1.D6
//       RFMIN=1.D15
  diffMin = 1.0e6;
  diffMin = 1.0e12;
  rfMin   = 1.0e15;
//       DO I=1,NFREQ
//          RFREQ(I)=DABS(RFREQ(I))
//          IF (RFREQ(I).LT.RFMIN) RFMIN=RFREQ(I)
//          ENDDO
  for (int i=0; i<nChan; i++)
    if ( (d=fabs(pcFreqByChan.getElement(i))) < rfMin)
      rfMin = d;
// !
//       DO I=1,NFREQ
//          K=I+1
//          DO J=K,NFREQ
//             B=DABS(RFREQ(J)-RFREQ(I))
//             IF (A.GT.B .AND. B.GT.0.D0)  A=B
//             ENDDO
//          ENDDO
  for (int i=0; i<nChan; i++)
    for (int j=i+1; j<nChan; j++)
    {
      d = fabs(pcFreqByChan.getElement(j) - pcFreqByChan.getElement(i));
      if (0.0 < d && d < diffMin)
        diffMin = d;
    };
// !
// !     GROUP DELAY AMBIGUITY IN NANOSEC (FREQS ARE MHZ)
// !
//       GPAMB=1.D0/A
//       GPAMB=GPAMB*1000.D0
  phaseCalGrdAmbig = 1.0/diffMin*1.0e3;
// !
// !     PHASE DELAY AMBIGUITY IN NANOSEC (FREQS ARE MHZ)
// !
//       PHAMB=1.D0/REFFR
//       PHAMB=PHAMB*1000.D0
  phaseCalPhdAmbig = 1.0/refFreq*1.0e3;
// !
// !     ASSIGN ARRAY ELEMENTS;
// !
//       DO I=1,NFREQ
//          IELEM(I)=((RFREQ(I)-RFMIN)/A)+1.D0
//          IF (IELEM(I).GT.256) THEN
//             Write (luout,'(" Array overrun ")')
//             IERR=1
//             RETURN
//             ENDIF
//          ENDDO
  for (int i=0; i<nChan; i++)
    if (nData <= (idxs[i]=((fabs(pcFreqByChan.getElement(i)) - rfMin)/diffMin)))
    {
      logger->write(SgLogger::INF, SgLogger::PREPROC,
        "evaluatePCal4GrpDelay: Array overrun for the obs " + oId +
        QString("").sprintf(": idxs[%d]=%d for %.2f and Fmin=%.2f Dmin=%.2f",
        i, idxs[i], pcFreqByChan.getElement(i), rfMin, diffMin));
      delete[] idxs;
      return false;
    };
// !
// !     PUT PHASES IN THE APPROPRIATE BINS
// !     (AMPLITUDES ARE NOT USED).
// !
//       DO I=1,256
//          CD(I)=DCMPLX(0.D0,0.D0)
//          ENDDO
// !
//       DO I=1,16
//          CDD(I)=DCMPLX(0.D0,0.D0)
//          ENDDO
  pX    = new double[nData];
  pY    = new double[nData];
  pXsvd = new double[nChan];
  pYsvd = new double[nChan];
  for (int i=0; i<nChan; i++)
    pXsvd[i] = pYsvd[i] = 0.0;
  for (int i=0; i<nData; i++)
    pX[i] = pY[i] = 0.0;
// !
//       DO I=1 ,NFREQ
//          COSP=DCOS(RPCP(I))
//          SINP=DSIN(RPCP(I))
//          CD(IELEM(I))=DCMPLX(COSP,SINP)
//          CDD(I)=DCMPLX(COSP,SINP)
//          ENDDO
  for (int i=0; i<nChan; i++)
  {
    pX[idxs[i]] = pXsvd[i] = pcAmplitudes.getElement(i)*cos(pcPhases.getElement(i));
    pY[idxs[i]] = pYsvd[i] = pcAmplitudes.getElement(i)*sin(pcPhases.getElement(i));
  };
// !
// !     COMPUTE DELAY RESOLUTION FUNCTION
// !
//        I256 = 256
//        I1   = 1
//       CALL FOUR1 ( CD, I256, I1 )

//std::cout << "------------\n";
//for (int i=0; i<nData; i++)
//std::cout << i << "  " << dData[2*i] << "  " << dData[2*i + 1]  << "\n";

//->   four1(dData, nData, 1);
  fft(pX, pY, nData, -1);

//for (int i=0; i<nData; i++)
//std::cout << i << "  " << dData[2*i] << "  " << dData[2*i + 1]  << "\n";
//std::cout << "------------\n";

// !
// !     LOCATE MAXIMUM
// !
//       CALL PARAB ( CD, I256, D, A4, P )
  subroutineParab(pX, pY, nData, dD, dA4, dP);
// !
// !     FIND GROUP DELAY EFFECT IN NANOSECONDS;
// !
//       D=D-1.D0
//       PCGRP=GPAMB* D/256.D0
//       PCGRP=DMOD(PCGRP+GPAMB*2.5D0, GPAMB) - 0.5D0*GPAMB
  phaseCalGrd = phaseCalGrdAmbig*dD/nData;
  phaseCalGrd = fmod(phaseCalGrd + phaseCalGrdAmbig*2.5, phaseCalGrdAmbig) - 0.5*phaseCalGrdAmbig;
// !
// !
// !     FIND PHASE DELAY EFFECT IN NANOSECONDS.
// !     P is the best estimate for the fringe phase at RFMIN
// !     i.e.      P = < phase(F) - gpdly * (F-RFMIN) >
// !     where phase(F) =observed phasecal phase at frequency F.
// !     But we want the phase at REFFR, which may not = RFMIN
// !
//       PCPHA = ((P)/TWOPI)*PHAMB + (REFFR-RFMIN)*0.001D0*PCGRP
  phaseCalPhd = dP/(2.0*M_PI)*phaseCalPhdAmbig + (refFreq - rfMin)*1.0e-3*phaseCalGrd;

//std::cout 
//<< "  -- dP=" << dP << "  "
//<< "  phaseCalPhdAmbig=" << phaseCalPhdAmbig << "  "
//<< "  refFreq=" << refFreq << "  "
//<< "  rfMin=" << rfMin << "  "
//<< "  phaseCalGrd=" << phaseCalGrd << "  \n";


  // will add the rest later, when/if need it:
// !
// !      CALCULATE RESIDUAL PHASES
// !
//       AWOPI=TWOPI
//       DO I=1,NFREQ
// !XX      AA=IELEM(I)
//          FFF=(RFREQ(I)-RFMIN)*0.001D0
//          CPP=DCOS( AWOPI*FFF*(PCGRP-0.5D0*GPAMB))
//          SPP=DSIN( AWOPI*FFF*(PCGRP-0.5D0*GPAMB))
//          CDD(I)=CDD(I)*DCMPLX(CPP,SPP)
//          ENDDO
// !
// !
// !     AVERAGE THE VECTORS AND TAKE OUT THE MEAN PHASE
// !
//       SUMR=0.D0
//       SUMI=0.D0
//       DO I=1,NFREQ
//          SUMR=SUMR+DREAL(CDD(I))
//          SUMI=SUMI+DIMAG(CDD(I))
//          ENDDO
// !
//       DO I=1,NFREQ
//          CDD(I)=CDD(I)*DCMPLX(-SUMR,-SUMI)
//          PHRES(I)=DATAN2(DIMAG(CDD(I)),DREAL(CDD(I)))
//          ENDDO
// !
// !     ALL IS DONE.  GO HOME.
// !
//       IERR = 0
//       RETURN
//       END
//--
  delete[] idxs;
  delete[] pX;
  delete[] pY;
  delete[] pXsvd;
  delete[] pYsvd;
  return true;
};




// just translation of $MK5_ROOT/progs/dbcal/parab.f from FORTRAN to C:
//       SUBROUTINE PARAB (CXD, N, D, A, P)
void subroutineParab(const double* pX, const double* pY, int n, double &dD, double &dA, double &dP)
{
// !
// !***Modifications:
// !       91.09.25  Darin Miller    Converted all variables to Double
// !                                 Precision.
// !       92.07.27  Brent Archinal  "(1)" dimensions => "(*)".
// !       93.02.10  MSW             General cleanup.
// !
// !***Flow chart for PARAB:
// !     DBCAL|
// !          |-> PUTDA|
// !                   |-> PUPCL|
// !                            |-> PCCLC|
// !                                     |-> PARAB
// !
//       IMPLICIT NONE
// !
//       COMPLEX*16 CXD(*)
//       REAL*8    A, D, P
//       REAL*8    P1, P2, P3,  A1, A2, A3    ! PHASE, AMP AT MAX CHANNELS
  double                        dP1, dP2, dP3, dA1, dA2, dA3;
//       REAL*8    PI, DREL, AA, AMAX
  double                        dDrel, dAa, dAmax;
//       INTEGER*4 I1, I2, I3, I, N
  int                           nI1=0, nI2=0, nI3=0;
// !
// !     GIVEN A COMPLEX ARRAY CXD, FIND THE POSITION WITH
// !     MAXIMAL AMPLITUDE, AND THE AMP AND PHASE AT THAT POSITION.
// !     FIRST, FIND THE CELL WITH HIGHEST AMPLITUDE.
// !     SUBROUTINE PERFORMS PARABOLIC INTERPOLATION WITH CELL ON
// !     EITHER SIDE OF MAX, TO OBTAIN REFINED ESTIMATE OF
// !     POSITION AND AMPLITUDE AT MAX, AND DOES A LINEAR INTERPOLATION
// !     TO OBTAIN THE PHASE OF CDD AT THE POSITION OF MAXIMUM AMP.
// !
// !     THE CHANNEL OF HIGHEST AMP CORRESPONDS TO THE LAG CHANNEL
// !     WITH HIGHEST AUTOCORRELATION. THE REFINED ESTIMATE
// !     IS OUR GROUP DELAY, IN UNITS  GPAMB/N. THE PHASE AT
// !     THIS LAG IS THE PHASE OFFSET (AT ZERO FREQ?, RFMIN?,REFFR?)
// !     (I THINK; MAY????).
// !     THUS, THE TOTAL PHASE IS
// !            PTOT = P + TWOPI * FREQ * GPAMB * (DD-1)/N
// !
// !     SEARCH FOR MAX AMP
// !
//       AMAX=-1000.D0
//       DO I=1,N
//          AA=CDABS(CXD(I))
//          IF (AA.LT.AMAX) GO TO 1
//          AMAX=AA
//          I2=I
//          A2=AMAX
// 1        CONTINUE
//          ENDDO
  dA2 = dAmax = -1000.0;
  for (int i=0; i<n; i++)
  {
    dAa = hypot(pX[i], pY[i]);
    if (dAa >= dAmax)
    {
      dAmax = dAa;
      nI2 = i;
      dA2 = dAmax;
    };
  };
//       I1=I2-1
//       IF (I1.LT.1) I1=I1+N
//       I3=I2+1
//       IF (I3.GT.N) I3=I3-N
//       A1=CDABS(CXD(I1))
//       A3=CDABS(CXD(I3))
  nI1 = nI2 - 1;
  if (nI1 < 0) 
    nI1 = nI1 + n;
  nI3 = nI2 + 1;
  if (nI3 >= n) 
    nI3 = nI3 - n;
  dA1 = hypot(pX[nI1], pY[nI1]);
  dA3 = hypot(pX[nI3], pY[nI3]);
// !
// !     POSITION OF MAXIMUM
// !     (in cells, relative to I2)
// !
//       DREL=(A3-A1)/(2.D0*(2.D0*A2-A1-A3))
//       IF (DREL.GT. 1.D0) DREL=1.D0
//       IF (DREL.LT.-1.D0) DREL=-1.D0
//       AA=I2
//       D=AA+DREL
  dDrel = (dA3 - dA1)/(2.0*(2.0*dA2 - dA1 - dA3));
  if (dDrel > 1.0) 
    dDrel = 1.0;
  if (dDrel < -1.0) 
    dDrel = -1.0;
  dAa = nI2;              // wtf??
  dD = dAa + dDrel;
// !
// !     AMPLITUDE AT MAXIMUM
// !     (THIS IS EXACT QUADRATIC FIT!)
// !
//       A=A2+DREL*(A3-A1)/4.D0
  dA = dA2 + dDrel*(dA3 - dA1)/4.0;
// !
// !     PHASE AT MAXIMUM (POSSIBLE PROBLEM HERE IF PHASE CROSSES
// !     TWOPI IN THE RANGE P1-P3)
// !
// !***Warning generated in compilation: "Argument has same name as
// !***INTRINSIC function" for following statements.
// !
//       P1=DATAN2(DIMAG(CXD(I1)),DREAL(CXD(I1)))
//       P2=DATAN2(DIMAG(CXD(I2)),DREAL(CXD(I2)))
//       P3=DATAN2(DIMAG(CXD(I3)),DREAL(CXD(I3)))
  dP1 = atan2(pY[nI1], pX[nI1]);
  dP2 = atan2(pY[nI2], pX[nI2]);
  dP3 = atan2(pY[nI3], pX[nI3]);
// !
// !     (BETTER FIXUP FOR PHASE AMBIGUITY PROBLEM)
// !
//       PI = 3.14159265358979323846264338328D0
//       IF (DABS(P1-P2).GE.PI .OR. DABS(P2-P3).GE.PI ) THEN
//          P1 = P2 + DMOD( P1-P2 + 5.D0*PI, 2.D0*PI ) - PI
//          P3 = P2 + DMOD( P3-P2 + 5.D0*PI, 2.D0*PI ) - PI
//          ENDIF
  if (fabs(dP1-dP2)>=M_PI || fabs(dP2-dP3)>=M_PI)
  {
    dP1 = dP2 + fmod(dP1 - dP2 + 5.0*M_PI, 2.0*M_PI) - M_PI;
    dP3 = dP2 + fmod(dP3 - dP2 + 5.0*M_PI, 2.0*M_PI) - M_PI;
  };
// !
//       P=P2
  dP = dP2;
// !     IF (ABS(P2-0.5D0*(P3+P1)).GT.0.1D0) RETURN
//       P=P2 + 0.5D0*(P3-P1)*DREL + 0.5D0*(P3-P2-P2+P1)*DREL**2
  dP = dP2 + 0.5*(dP3 - dP1)*dDrel + 0.5*(dP3 - dP2 - dP2 + dP1)*dDrel*dDrel;
// !
//       RETURN
//       END
  return;
};




/*=====================================================================================================*/
//
// statics:
//
/*=====================================================================================================*/




/*=====================================================================================================*/
