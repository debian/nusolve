/*
 *
 *    This file is a part of Space Geodetic Library. The library is used by
 *    nuSolve, a part of CALC/SOLVE system, and designed to make analysis of
 *    geodetic VLBI observations.
 *    Copyright (C) 2010-2020 Sergei Bolotin.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <math.h>


#include <QtCore/QFile>
#include <QtCore/QStringList>
#include <QtCore/QTextStream>

#include <SgModelEop_JMG_96_hf.h>

#include <SgLogger.h>
#include <SgMatrix.h>
#include <SgVector.h>


void fundArgs_Old(const SgMJD&, double[6]);


/*=======================================================================================================
*
*                          SgModelEop_JMG_96_hf's METHODS:
* 
*======================================================================================================*/
//
// static first:
const QString SgModelEop_JMG_96_hf::className()
{
  return "SgModelEop_JMG_96_hf";
};



//
bool SgModelEop_JMG_96_hf::readFile(const QString& fileName)
{
  QString             str;
  QFile               f((fileName_=fileName));
  bool                isOk;
  isOk_ = false;
  if (!f.exists())
  {
    logger->write(SgLogger::DBG, SgLogger::IO_TXT, className() + 
      ": the file [" + fileName + "] with a priori data does not exist");
    return false;
  };
  //
  // clear the stuff:
  numUt_ = 0;
  numPm_ = 0;
  if (baseModel_)
  {
    delete baseModel_;
    baseModel_ = NULL;
  };
  utModel_ = NULL;
  pmModel_ = NULL;
  //
  if (f.open(QFile::ReadOnly))
  {
    QTextStream       s(&f);
    int               idx=0;
    while (!s.atEnd())
    {
      str = s.readLine();
      if (str.at(0)==' ')
      {
        if (numUt_*numPm_ == 0) // first non-comments string, determine number of arrays:
        {
          QStringList l = str.simplified().split(' ', QString::SkipEmptyParts);
          if (l.size() != 2)
          {
            logger->write(SgLogger::WRN, SgLogger::IO_TXT, className() + 
              ": the file [" + fileName + "]: cannot guess numbers of arrays from the string: [" +
              str + "]");
            return false;
          };
          numUt_ = l.at(0).toInt(&isOk);
          if (!isOk)
          {
            numUt_ = 0;
            logger->write(SgLogger::WRN, SgLogger::IO_TXT, className() + 
              ": the file [" + fileName + "]: failed to get the array size for UT1, the string: [" +
              str + "]");
            return false;
          };
          numPm_ = l.at(1).toInt(&isOk);
          if (!isOk)
          {
            numUt_ = 0;
            numPm_ = 0;
            logger->write(SgLogger::WRN, SgLogger::IO_TXT, className() + 
              ": the file [" + fileName + "]: failed to get the array size for PM, the string: [" +
              str + "]");
            return false;
          };
          // arrange some space for the data:
          baseModel_ = new HfEopRec[numUt_ + numPm_];
          utModel_ = baseModel_;
          pmModel_ = baseModel_ + numUt_;
          logger->write(SgLogger::DBG, SgLogger::IO_TXT, className() + 
            ": the file [" + fileName + "]: allocated " + QString("").setNum(numUt_) + 
            " records for UT1 and " + QString("").setNum(numPm_) + " records for PM");
        }
        else
        {
          int           n[6];
          double        a_c, a_s;
          QStringList l = str.simplified().split(' ', QString::SkipEmptyParts);
          if (l.size() != 10)
          {
            logger->write(SgLogger::WRN, SgLogger::IO_TXT, className() + 
              ": the file [" + fileName + "]: cannot guess format of the string: [" +
              str + "]");
            return false;
          };
          for (int i=0; i<6; i++)
          {
            n[i] = l.at(i).toInt(&isOk);
            if (!isOk)
            {
              logger->write(SgLogger::WRN, SgLogger::IO_TXT, className() + 
                ": the file [" + fileName + "]: failed to acquire info (n_" + QString("").setNum(i) +
                "), the string: [" + str + "]");
              return false;
            };
          };
          a_c = l.at(6).toDouble(&isOk);
          if (!isOk)
          {
            logger->write(SgLogger::WRN, SgLogger::IO_TXT, className() + 
              ": the file [" + fileName + "]: failed to acquire info (a_c), the string: [" + str + "]");
            return false;
          };
          a_s = l.at(7).toDouble(&isOk);
          if (!isOk)
          {
            logger->write(SgLogger::WRN, SgLogger::IO_TXT, className() + 
              ": the file [" + fileName + "]: failed to acquire info (a_s), the string: [" + str + "]");
            return false;
          };
          for (int i=0; i<6; i++)
            baseModel_[idx].n_[i] = n[i];
          baseModel_[idx].a_cos_ = a_c;
          baseModel_[idx].a_sin_ = a_s;
          idx++;
        };
      };
    };
    f.close();
    s.setDevice(NULL);
  };

  if (0<numUt_ && 0<numPm_)
    isOk_ = true;

  return isOk_;
};



//
void SgModelEop_JMG_96_hf::calcCorrections(const SgMJD& t, double& dUt1, double& dPx, double& dPy)
{
  QString                       str;
  double                        fundArgs[6];
  double                        arg;
  
  // zerofy values:
  fundArgs[0] = fundArgs[1] = fundArgs[2] = fundArgs[3] = fundArgs[4] = fundArgs[5] = 0.0;
  dUt1 = dPx = dPy = 0.0;
  
  if (!isOk_)
    return;

  fundArgs_Old(t, fundArgs);

  for (int i=0; i<numUt_; i++)
  {
    //         Get the argument
    arg = 0.0;
    for (int j=0; j<6; j++)
      arg += utModel_[i].n_[j]*fundArgs[j];
    //         Increment the change to UT1
    dUt1 += utModel_[i].a_cos_*cos(arg) + utModel_[i].a_sin_*sin(arg);
  };
  // convert from mas to mts:
  // WTF?
  // dUt1 /= 15.0;

  //****  Now do polar motion
  for (int i=0; i<numPm_; i++)
  {
    //         Get the argument
    arg = 0.0;
    for (int j=0; j<6; j++)
      arg += pmModel_[i].n_[j]*fundArgs[j];
    //         Increment the change to the X anf Y positions
    // dx = dx  + (-xy_val(1,i)* cos(arg) + xy_val(2,i)* sin(arg))
    dPx += -pmModel_[i].a_cos_*cos(arg) + pmModel_[i].a_sin_*sin(arg);
    // dy = dy + (xy_val(1,i)* sin(arg) + xy_val(2,i) * cos(arg))
    dPy +=  pmModel_[i].a_cos_*sin(arg) + pmModel_[i].a_sin_*cos(arg);
  };
  // ***** That is all.
  //        return
  //        end
};
/*=====================================================================================================*/





/*=======================================================================================================
*
*                           FRIENDS:
* 
*======================================================================================================*/
//



/*=====================================================================================================*/
//
// aux functions:
//
void fundArgs_Old_another_verison(const SgMJD& t, double args[6])
{
  static const double argsConsts[5][5] =
  { //0,0      0,1          0,2          0,3       0,4
    { 0.064,  31.310,  715922.633,  485866.733, 1325.0},
    {-0.012,  -0.577, 1292581.224, 1287099.804,   99.0},
    { 0.011, -13.257,  295263.137,  335778.877, 1342.0},
    { 0.019,  -6.891, 1105601.328, 1072261.307, 1236.0},
    { 0.008,   7.455, -482890.539,  450160.280,   -5.0} 
  };

  double              tc((t - tEphem)/36525.0);
  double              ts[4];
  ts[0] = 1.0;
  ts[1] = tc;
  ts[2] = tc*tc;
  ts[3] = tc*tc*tc;

  for (int i=0; i<5; i++)
  {
    args[i] = 0.0;
    for (int j=0; j<4; j++)
      args[i] += argsConsts[i][j]*ts[3-j];
    args[i] += fmod(argsConsts[i][4]*tc, 1.0)*1296000.0;
    args[i] = fmod(args[i], 1296000.0)*SEC2RAD;
  };
  
  //***** Now compute GMST.  (CALC 7.1 Algorithm)
  //     Remove the fractional part of the julian date
  //     Get fjday at 0:00 UT
  //  fjday_0hr = aint(epoch-0.5) + 0.5;
  //                         ! Days since J2000.0
  //  t_0hr = fjday_0hr - dj2000;
  //                         ! 0:00 hrs at start of day
  //  cent = t_0hr / 36525.0;
  tc = (t.getDate() - tEphem)/36525.0;
  ts[1] = tc;
  ts[2] = tc*tc;
  ts[3] = tc*tc*tc;
  //
  //                         ! Fraction of a day
  double fract = t.getTime();
  double diurnv = (1.002737909350795 + 5.9006e-11*ts[1] - 5.9e-15*ts[2]);

  //**** COMPUTE GST in cycles
  double gstd = (24110.54841 + 8640184.81266*ts[1] + 0.093104*ts[2] - 6.2e-6*ts[3])/86400.0;
  gstd = fmod(gstd, 1.0);
  args[5] = (gstd + diurnv*fract)*2.0*M_PI + M_PI;
};



// it mimicks TIDE_ANGLES for compatibility
void fundArgs_Old(const SgMJD& t, double args[6])
{
  double        sec360(1296000.0);
  double        pi(3.1415926535897932);
  double        twopi(2.0*pi);
  double        arcsec2rad(twopi/sec360);
  double        elc [5] = { 0.064,  31.310,   715922.633,  485866.733,   1325.0};
  double        elpc[5] = {-0.012,  -0.577,  1292581.224, 1287099.804,     99.0};
  double        fc  [5] = { 0.011, -13.257,   295263.137,  335778.877,   1342.0};
  double        dc  [5] = { 0.019,  -6.891,  1105601.328, 1072261.307,   1236.0};
  double        omc [5] = { 0.008,   7.455,  -482890.539,  450160.280,     -5.0};
  double        cent((t - tEphem)/36525.0);
  double        cent2, cent3;
  double        el, elp, f, d, om, gst;

  cent2 = cent*cent;
  cent3 = cent2*cent;

  el = elc[0]*cent3   + elc[1]*cent2  + elc[2]*cent  + elc[3]  +  fmod(elc[4]*cent, 1.0)*sec360;
  el = fmod(el, sec360);
  //
  elp = elpc[0]*cent3 + elpc[1]*cent2 + elpc[2]*cent + elpc[3] +  fmod(elpc[4]*cent, 1.0)*sec360;
  elp = fmod(elp, sec360);
  //
  f = fc[0]*cent3     + fc[1]*cent2   + fc[2]*cent   + fc[3]   +   fmod(fc[4]*cent, 1.0)*sec360;
  f = fmod(f, sec360);
  //
  d = dc[0]*cent3     + dc[1]*cent2   + dc[2]*cent   + dc[3]   +   fmod(dc[4]*cent, 1.0)*sec360;
  d = fmod(d, sec360);
  //
  om = omc[0]*cent3   + omc[1]*cent2  + omc[2]*cent  + omc[3]  +   fmod(omc[4]*cent, 1.0)*sec360;
  om = fmod(om, sec360);


  //!***** Now compute GMST.  (CALC 7.1 Algorithm)
  //!     Remove the fractional part of the julian date
  //!                         ! Days since J2000.0
  double t_0hr = (t.getDate() - tEphem);
  //!                         ! 0:00 hrs at start of day
  cent  = t_0hr/36525.0;
  cent2 = cent*cent;
  cent3 = cent2*cent;
  //!
  //!                         ! Fraction of a day
  double fract = t.getTime();
  //!
  double diurnv = (1.002737909350795 + 5.9006e-11*cent - 5.9e-15*cent2);
  //!
  //!**** COMPUTE GST in cycles
  double gstd = (24110.54841 + 8640184.81266*cent + 0.093104*cent2 - 6.2e-6*cent3 )/86400.0;
  //!
  gstd = fmod(gstd, 1.0);
  //!                                             ! Rads
  gst = (gstd + diurnv*fract)*twopi;


  args[0] = el;
  args[1] = elp;
  args[2] = f;
  args[3] = d;
  args[4] = om;
  for (int i=0; i<5; i++)
    args[i] *= arcsec2rad;
  args[5] = gst + pi;
};



/*
 * 
 * 
 * 
!TITLE TIDE_ANGLES
      SUBROUTINE tide_angles( epoch, fund_arg )
      IMPLICIT NONE                         !Added by IMP/jwr
!
!-----The following type specification statements added automatically
!     by imp/jwr July 2002
!
      INTEGER*2 i
      REAL*4 gstdot
!-----END of imp added lines.
!
!     Routine to compute the value of the fundamental argument
!     for Brown's arguments.  The sixth entry is returned as GST
!     plus pi.  The additional pi is used for compatability with
!     Doodson's Tide argument.
!
! original routine From Tom Herring.
! modified by JMGipson to compute derivatives of tide angles.
! Tide angles are returned in radians.
! Derivatives in radians/sec.
!
! PHYSICAL CONSTANTS NEEDED FOR SD_COMP
!
!   pi          - Define here to full precision
!   rad_to_deg  - Conversion from radians to degs.
!   DJ2000      - Julian date of J2000
!   sec360      - number of seconds in 360 degreees.
!
        real*8 pi, rad_to_deg, DJ2000, sec360,arcsec2rad,twopi
        real*8 century2sec
!
        parameter ( pi            = 3.1415926535897932D0 )
        parameter ( twopi         = 2.d0*pi)
        parameter ( DJ2000        = 2451545.d0           )
        parameter ( sec360        = 1296000.d0           )
!
!     Computed quanities
        parameter ( rad_to_deg    = 180.d0   /pi         )
! conversion from arcseconds to radians.
        parameter (arcsec2rad= twopi/sec360)
        parameter(century2sec=86400.d0*36525.d0)
!
!-------------------------------------------------------------------
!
! PASSED VARIABLES
!
! INPUT
! epoch  - Julian date for arguments (fjday + fraction of day)
!
! OUTPUT
! fund_arg(6,2) -  Brown's arguments plus GST+pi (rads)
!
        real*8 epoch, fund_arg(6,2)
!
!
! LOCAL VARIABLES
!      cent             - Julian centuries to DJ2000.
!      el,eld           - Mean longitude of moon minus mean
!                       - longitude of moon's perigee (arcsec)
!      elc(5)           - Coefficients for computing el
!      elp,elpd         - Mean longitude of the sun minus mean
!                       - longitude of sun perigee (arcsec)
!      elpc(5)          - Coeffiecents for computing elp
!      f,fd             - Moon's mean longitude minus omega (sec)
!      fc(5)            - Coefficients for computing f
!      d,dd             - Mean elongation of the moon from the
!                       - sun (arcsec)
!      dc(5)            - coefficients for computing d
!      om,omd           - longitude of the ascending node of the
!                       - moon's mean orbit on the elliptic
!                       - measured from the mean equinox of date
!      omc(5)           - Coefficients for computing om.
!      gst              - Greenwich mean sidereal time (rad)
!
        real*8 cent, el,eld, elc(5), elp, elpd, elpc(5), &
     &    f,fd, fc(5), d,dd, dc(5), om,omd, omc(5), gst
!      fract        - fraction of a day from 0:00 hrs UT.
!      Jd_0hr       - Julian date at zero hours UT
!      t_0hr        - Days since DJ2000 at 0:00 hrs UT
!      gstd         - GMST at 0:00 hrs UT1 of day being evaluated
!      diurnv       - Ratio of solar days to sidreal days on
!                     day of evalution.
!
        real*8 fract, t_0hr, gstd, diurnv, fjday_0hr
!
!****  DATA statements for the fundamental arguments.
!
        data elc    /     0.064d0,    31.310d0,    715922.633d0, &
     &             485866.733d0,    1325.0d0 /
        data elpc   /    -0.012d0,    -0.577d0,   1292581.224d0, &
     &            1287099.804d0,      99.0d0 /
        data fc     /     0.011d0,   -13.257d0,    295263.137d0, &
     &             335778.877d0,    1342.0d0/
        data dc     /     0.019d0,    -6.891d0,    1105601.328d0, &
     &            1072261.307d0,    1236.0d0/
        data omc    /     0.008d0,     7.455d0,    -482890.539d0, &
     &             450160.280d0,      -5.0d0/
!
!****  Get the number of centuries to current time
!
        cent = (epoch-dj2000) / 36525.d0
!
!****  Compute angular arguments
        el = elc(1) * cent**3 + elc(2) * cent**2 + elc(3) * cent &
     &          + elc(4) + dmod( elc(5) * cent, 1.d0 ) * sec360
        el = dmod( el, sec360 )
        eld = 3.d0 * elc(1) * cent**2 + 2.d0 * elc(2) * cent + elc(3) &
     &      + elc(5) * sec360
!
        elp = elpc(1) * cent**3 + elpc(2) * cent**2 + elpc(3) * cent &
     &     + elpc(4) + dmod( elpc(5) * cent, 1.d0 ) * sec360
        elp = dmod( elp, sec360 )
        elpd = 3.d0 * elpc(1) * cent**2 + 2.d0 * elpc(2) * cent + elpc(3) &
     &       + elpc(5) * sec360
!
        f = fc(1) * cent**3 + fc(2) * cent**2 + fc(3) * cent &
     &     + fc(4) + dmod( fc(5) * cent, 1.d0 ) * sec360
        f = dmod( f, sec360 )
        fd = 3.d0 * fc(1) * cent**2 + 2.d0 * fc(2) * cent + fc(3) &
     &     + fc(5) * sec360
!
        d = dc(1) * cent**3 + dc(2) * cent**2 + dc(3) * cent &
     &     + dc(4) + dmod( dc(5) * cent, 1.d0 ) * sec360
        d = dmod( d, sec360 )
        dd = 3.d0 * dc(1) * cent**2 + 2.d0 * dc(2) * cent + dc(3) &
     &     + dc(5) * sec360
!
        om = omc(1) * cent**3 + omc(2) * cent**2 + omc(3) * cent &
     &     + omc(4) + dmod( omc(5) * cent, 1.d0 ) * sec360
        om = dmod( om, sec360 )
        omd = 3.d0 * omc(1) * cent**2 + 2.d0 * omc(2) * cent + omc(3) &
     &      + omc(5) * sec360
!
!
!***** Now compute GMST.  (CALC 7.1 Algorithm)
!     Remove the fractional part of the julian date
!     Get fjday at 0:00 UT
        fjday_0hr = aint(epoch-0.5d0) + 0.5d0
!                         ! Days since J2000.0
        t_0hr = fjday_0hr - dj2000
!                         ! 0:00 hrs at start of day
        cent = t_0hr / 36525.d0
!
!                         ! Fraction of a day
        fract = epoch - fjday_0hr
!
        diurnv = ( 1.002737909350795d0 + 5.9006d-11*cent &
     &                               - 5.9d-15*cent**2 )
!
!**** COMPUTE GST in cycles
        gstd = ( 24110.54841d0  + 8640184.81266d0*cent &
     &                        + 0.093104d0*cent**2 &
     &                        - 6.2d-6*cent**3 ) /86400.d0
!
        gstd = dmod(gstd,1.d0)
!                                             ! Rads
        gst = (gstd + diurnv*fract) * twopi
!
        gstdot=(diurnv/86400.+ &
     &     (8640184.81266d0+2.*0.093104d0*cent - 3.d0*6.2d-6*cent*3 ) &
     &    /86400.d0/century2sec)
!
!
!****  Now save the values.  Convert values from arcseconds to radians
!
        fund_arg(1,1) = el
        fund_arg(2,1) = elp
        fund_arg(3,1) = f
        fund_arg(4,1) = d
        fund_arg(5,1) = om
!
        fund_arg(1,2) = eld
        fund_arg(2,2) = elpd
        fund_arg(3,2) = fd
        fund_arg(4,2) = dd
        fund_arg(5,2) = omd
!
        do i=1,5
          fund_arg(i,1)=fund_arg(i,1)*arcsec2rad
          fund_arg(i,2)=fund_arg(i,2)*arcsec2rad/century2sec
        end do
!
        fund_arg(6,1) = gst + pi
        fund_arg(6,2) = gstdot*twopi
!***** Thats all
        return
        end
!
!********************************************************

*/

// i/o:


/*=====================================================================================================*/
//
// constants:
//
/*=====================================================================================================*/








/*=====================================================================================================*/
