/*
 *
 *    This file is a part of Space Geodetic Library. The library is used by
 *    nuSolve, a part of CALC/SOLVE system, and designed to make analysis of
 *    geodetic VLBI observations.
 *    Copyright (C) 2010-2020 Sergei Bolotin.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef SG_CONSTANTS_H
#define SG_CONSTANTS_H

#include <math.h>

class SgMJD;

// some physical constants:

extern const double vLight;


void calcCip2IAU1980(const SgMJD& epoch, double dX, double dY, double dPsi_1980, double dEps_1980,
  double dPsi_2000, double dEps_2000, double& diffPsi, double& diffEps);





#endif //SG_CONSTANTS_H
