/*
 *
 *    This file is a part of Space Geodetic Library. The library is used by
 *    nuSolve, a part of CALC/SOLVE system, and designed to make analysis of
 *    geodetic VLBI observations.
 *    Copyright (C) 2010-2020 Sergei Bolotin.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SG_MATH_DEFINITIONS_H
#define SG_MATH_DEFINITIONS_H


#include <math.h>


//!< radians to degrees:
#ifndef RAD2DEG
#define RAD2DEG         (180.0/M_PI)
#endif  
//!< radians to hours:
#ifndef RAD2HR
#define RAD2HR          (12.0 /M_PI)
#endif
//!< degrees to radians:
#ifndef DEG2RAD
#define DEG2RAD         (M_PI/180.0)
#endif
//!< hours to radians:
#ifndef HR2RAD
#define HR2RAD          (M_PI/ 12.0)
#endif
//!< arc seconds to radians:
#ifndef SEC2RAD
#define SEC2RAD         (DEG2RAD/3600.0)
#endif
//!< radians to arc seconds:
#ifndef RAD2SEC
#define RAD2SEC         (RAD2DEG*3600.0)
#endif
//!< seconds in one day:
#ifndef DAY2SEC
#define DAY2SEC         (86400.0)
#endif
//!< radians to mas:
#ifndef RAD2MAS
#define RAD2MAS         (RAD2SEC*1000.0)
#endif
//!< radians to ms:
#ifndef RAD2MS
#define RAD2MS          (RAD2HR*3600.0*1000.0)
#endif


enum DIRECTION {X_AXIS=0, VERTICAL=0, Y_AXIS=1, EAST=1, Z_AXIS=2, NORTH=2};


inline double signum(const double x) {return x<0.0 ? -1.0 : 1.0;};
inline double cpsign(const double a1, const double a2) {return fabs(a1)*signum(a2);};// copysign(a1, a2)?
inline void swap(double &a1, double &a2) {double tmp=a1; a1=a2; a2=tmp;};


class Sg3dVector;
class Sg3dMatrix;


// these constants are defined in GPGeoMath3dVector.cpp:
extern const Sg3dVector v3Zero;
extern const Sg3dVector v3Unit;

// these constants are defined in GPGeoMath3dMatrix.cpp:
extern const Sg3dMatrix m3E;
extern const Sg3dMatrix m3Zero;


// Fast Fourier transformation:
void fft(double *x, double *y, int n, int dir);






#endif //SG_MATH_DEFINITIONS_H
