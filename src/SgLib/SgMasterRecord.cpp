/*
 *
 *    This file is a part of Space Geodetic Library. The library is used by
 *    nuSolve, a part of CALC/SOLVE system, and designed to make analysis of
 *    geodetic VLBI observations.
 *    Copyright (C) 2010-2020 Sergei Bolotin.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <stdlib.h>

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QStringList>
#include <QtCore/QTextStream>


#include <SgMasterRecord.h>
#include <SgLogger.h>



/*=======================================================================================================
*
*                           METHODS:
* 
*======================================================================================================*/
//
// static first:
const QString SgMasterRecord::className()
{
  return "SgMasterRecord";
};



//
SgMasterRecord& SgMasterRecord::operator=(const SgMasterRecord& r)
{
  isValid_ = r.isValid_;
  name_ = r.name_;
  code_ = r.code_;
  date_ = r.date_;
  stations_ = r.stations_;
  dbcCode_ = r.dbcCode_;
  scheduledBy_ = r.scheduledBy_;
  correlatedBy_ = r.correlatedBy_;
  submittedBy_ = r.submittedBy_;
  return *this;
};



//
void SgMasterRecord::parseString(const QString& str)
{
  isValid_ = false;
  if (str.at(0) != QChar('|') || 
      str.at(str.size()-1)!=QChar('|') ||
      str.count(QChar('|')) != 16)
  {
    //complain (?):
    //logger->write(SgLogger::DBG, SgLogger::IO_TXT, className() + 
    //  ": got a not valid MR-string: [" + str + "]; skipped");
    return;
  };
  QStringList fields = str.split("|");
  name_ = fields.at(1).simplified();
  code_ = fields.at(2).simplified();
  date_ = fields.at(3).simplified();
  stations_ = fields.at(7).simplified();
  dbcCode_ = fields.at(12).simplified();
  scheduledBy_ = fields.at(8).simplified();
  correlatedBy_ = fields.at(9).simplified();
  submittedBy_ = fields.at(13).simplified();
  if (name_.size() && code_.size() && dbcCode_.size() && date_.size())
    isValid_ = true;
};



//
bool SgMasterRecord::lookupRecordByName(const QString& nickName, const QString& path)
{
  QDir                          dir(path);
  if (!dir.exists())
  {
    logger->write(SgLogger::ERR, SgLogger::IO_TXT, className() + 
      "::lookupRecordByName(): the masterfile directory \"" + path + 
      "\" does not exist; master file records are unavailable");
    return false;
  };
  // nickName: 12FEB27XA
  QString                       sYear(nickName.mid(0, 2));
  QString                       sDate(nickName.mid(2, 5));
  QString                       sCode(nickName.mid(7, 2));
  if (findRecordByName(sDate, sCode, path + "/" + "master" + sYear + "-loc.txt")  ||
      findRecordByName(sDate, sCode, path + "/" + "master" + sYear + ".txt")      ||
      findRecordByName(sDate, sCode, path + "/" + "master" + sYear + "-int.txt")  ||
      findRecordByName(sDate, sCode, path + "/" + "master" + sYear + "-vgos.txt")  )
    return true;
  return false;
};



//
bool SgMasterRecord::findRecordByName(const QString& date, const QString& dbcCode,
  const QString& fileName)
{
  SgMasterRecord                mr;
  QString                       str;
  QFile                         f(fileName);
  if (!f.exists())
  {
    logger->write(SgLogger::DBG, SgLogger::IO_TXT, className() + 
      "::findRecordByName(): the masterfile \"" + fileName + "\" does not exist");
    return false;
  };
  //
  if (f.open(QFile::ReadOnly))
  {
    QTextStream                 s(&f);
    while (!s.atEnd())
    {
      str = s.readLine();
      if (str.size()>16)
      {
        mr.parseString(str);
        if (mr.getDate() == date && mr.getDbcCode() == dbcCode)
        {
          *this = mr;
          f.close();
          s.setDevice(NULL);
          return true;
        };
      };
    };
    f.close();
    s.setDevice(NULL);
  };
  return false;
};



//
bool SgMasterRecord::lookupRecordByCode(const QString& sessionCode, const QString& sYear, 
  const QString& path)
{
  QDir                          dir(path);
  if (!dir.exists())
  {
    logger->write(SgLogger::WRN, SgLogger::IO_TXT, className() + 
      "::lookupRecordByCode(): the masterfile directory \"" + path + 
      "\" does not exist; master file records are unavailable");
    return false;
  };
  if (findRecordByCode(sessionCode, path + "/" + "master" + sYear + "-loc.txt")   ||
      findRecordByCode(sessionCode, path + "/" + "master" + sYear + ".txt")       ||
      findRecordByCode(sessionCode, path + "/" + "master" + sYear + "-int.txt")   ||
      findRecordByCode(sessionCode, path + "/" + "master" + sYear + "-vgos.txt")   )
    return true;
  return false;
};



//
bool SgMasterRecord::findRecordByCode(const QString& sessionCode, const QString& fileName)
{
  SgMasterRecord                mr;
  QString                       str;
  QFile                         f(fileName);
  if (!f.exists())
  {
    logger->write(SgLogger::DBG, SgLogger::IO_TXT, className() + 
      "::findRecordByCode(): the masterfile \"" + fileName + "\" does not exist");
    return false;
  };
  //
  if (f.open(QFile::ReadOnly))
  {
    logger->write(SgLogger::DBG, SgLogger::IO_TXT, className() + 
      "::findRecordByCode(): processing the masterfile \"" + fileName + "\"");
    QTextStream                 s(&f);
    while (!s.atEnd())
    {
      str = s.readLine();
      if (str.size()>16)
      {
        mr.parseString(str);
        if (mr.getCode() == sessionCode.toUpper()) // some correlators provide lower case here
        {
          *this = mr;
          f.close();
          s.setDevice(NULL);
          return true;
        };
      };
    };
    f.close();
    s.setDevice(NULL);
  };
  return false;
};
/*=====================================================================================================*/
//
//                           FRIENDS:
// 
/*=====================================================================================================*/
//

/*=====================================================================================================*/
//
// aux functions:
//

/*=====================================================================================================*/
//
// constants:
//

/*=====================================================================================================*/
