/*
 *
 *    This file is a part of Space Geodetic Library. The library is used by
 *    nuSolve, a part of CALC/SOLVE system, and designed to make analysis of
 *    geodetic VLBI observations.
 *    Copyright (C) 2010-2020 Sergei Bolotin.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <stdlib.h>

#include <QtCore/QDataStream>


#include <SgLogger.h>
#include <SgTaskConfig.h>
#include <SgTaskManager.h>
#include <SgVlbiBand.h>
#include <SgVlbiObservable.h>
#include <SgVlbiObservation.h>



/*=======================================================================================================
*
*               SgVlbiBand's METHODS:
* 
*======================================================================================================*/
// An empty constructor:
SgVlbiBand::SgVlbiBand() :
  SgObjectInfo(-1, "_"),
  observables_(),
  history_(),
  stationsByName_(),
  baselinesByName_(),
  sourcesByName_(),
  stationsByIdx_(),
  baselinesByIdx_(),
  sourcesByIdx_(),
  sampleRateByCount_(),
  bitsPerSampleByCount_(),
  recordMode_(""),
  grdAmbigsBySpacing_(),
  strGrdAmbigsStat_(""),
  phdAmbigsBySpacing_(),
  strPhdAmbigsStat_(""),
  phCalOffset_1ByBln_(),
  phCalOffset_2ByBln_()
{
  frequency_ = 0.0;
  tCreation_ = tZero;
  inputFileName_ = "null";
  inputFileVersion_ = 0;
  correlatorType_ = "Mk0";
  maxNumOfChannels_ = 0;
  sampleRate_ = 0.0;
  bitsPerSample_ = 0;
  typicalGrdAmbigSpacing_ = 0.0;
  typicalPhdAmbigSpacing_ = 0.0;
};



// destructor:
SgVlbiBand::~SgVlbiBand()
{
  stationsByIdx_.clear();
  baselinesByIdx_.clear();
  sourcesByIdx_.clear();
  sampleRateByCount_.clear();
  bitsPerSampleByCount_.clear();
  grdAmbigsBySpacing_.clear();
  phdAmbigsBySpacing_.clear();
  phCalOffset_1ByBln_.clear();
  phCalOffset_2ByBln_.clear();

  // clear maps:
  for (QMap<QString, SgVlbiStationInfo*>::iterator it=stationsByName_.begin(); 
    it != stationsByName_.end(); ++it)
    delete it.value();
  stationsByName_.clear();

  for (QMap<QString, SgVlbiBaselineInfo*>::iterator it=baselinesByName_.begin(); 
    it != baselinesByName_.end(); ++it)
    delete it.value();
  baselinesByName_.clear();

  for (QMap<QString, SgVlbiSourceInfo*>::iterator it=sourcesByName_.begin(); 
    it != sourcesByName_.end(); ++it)
    delete it.value();
  sourcesByName_.clear();

  // clear observations container:
  observables_.clear();
};



//
void SgVlbiBand::resetAllEditings()
{
  // attributes:
  delAttr(Attr_NOT_VALID);
  //
  for (QMap<QString, SgVlbiStationInfo*>::iterator it=stationsByName_.begin(); 
    it!=stationsByName_.end(); ++it)
    it.value()->resetAllEditings();
  for (QMap<QString, SgVlbiSourceInfo*>::iterator it=sourcesByName_.begin(); 
    it!=sourcesByName_.end(); ++it)
    it.value()->resetAllEditings();
  for (QMap<QString, SgVlbiBaselineInfo*>::iterator it=baselinesByName_.begin(); 
    it!=baselinesByName_.end(); ++it)
    it.value()->resetAllEditings();
  //
  SgObjectInfo::resetAllEditings();
};



//
bool SgVlbiBand::saveIntermediateResults(QDataStream& s) const
{
  s << getKey() << getAttributes() << getSigma2add(DT_DELAY);
  if (s.status() != QDataStream::Ok)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": saveIntermediateResults(): error writting data");
    return false;
  };
  //
  for (QMap<QString, SgVlbiStationInfo*>::const_iterator it=stationsByName_.begin(); 
    it!=stationsByName_.end(); ++it)
    if (s.status() == QDataStream::Ok)
    {
      s << it.value()->getKey(); // put a station name to check order
      it.value()->clockBreaks().saveIntermediateResults(s);
    }
    else
    {
      logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
        ": saveIntermediateResults(): error writting clock break data: " + it.value()->getKey());
      return false;
    };
  //
  if (s.status() != QDataStream::Ok)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": saveIntermediateResults(): error writting data");
    return false;
  };
  return s.status() == QDataStream::Ok;
};



//
bool SgVlbiBand::loadIntermediateResults(QDataStream& s)
{
  QString                       key;
  unsigned int                  attributes;
  double                        sigma2add;
  s >> key >> attributes >> sigma2add;
  if (s.status() != QDataStream::Ok)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": loadIntermediateResults(): error reading data: " +
      (s.status()==QDataStream::ReadPastEnd?"read past end of the file":"read corrupt data"));
    return false;
  };
  if (getKey() != key)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": loadIntermediateResults(): error reading data: wrong order, key mismatch: got [" + key +
      "], expected [" + getKey() + "]");
    return false;
  };

  for (QMap<QString, SgVlbiStationInfo*>::iterator it=stationsByName_.begin(); 
    it!=stationsByName_.end(); ++it)
    if (s.status() == QDataStream::Ok)
    {
      s >> key; // first, check the order:
      if (it.value()->getKey() != key)
      {
        logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
          ": loadIntermediateResults(): error reading data: wrong order, key mismatch: got [" + key +
          "], expected [" + it.value()->getKey() + "]");
        return false;
      }
      if (!it.value()->clockBreaks().loadIntermediateResults(s))
      {
        logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
          ": loadIntermediateResults(): error reading data: clock break data for " +
          it.value()->getKey());
        return false;
      }
    }
    else
    {
      logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
        ": loadIntermediateResults(): error writting clock break data for " + it.value()->getKey() + 
        ": " + (s.status()==QDataStream::ReadPastEnd?"read past end of the file":"read corrupt data"));
      return false;
    };
  setAttributes(attributes);
  setSigma2add(DT_DELAY, sigma2add);
  //
  return s.status()==QDataStream::Ok;
};



//
bool SgVlbiBand::selfCheck()
{
  QMap<QString, QMap<QString, int> >
                                numByScanIdBySrc;
  QString                       str("");
  bool                          isOk=true;
  int                           numOfZCodes=0;
  int                           num;
  maxNumOfChannels_ = 0;
  sampleRate_ = 0.0;
  bitsPerSample_ = 0;
  sampleRateByCount_.clear();
  bitsPerSampleByCount_.clear();
  grdAmbigsBySpacing_.clear();
  phdAmbigsBySpacing_.clear();
  
  // set up "typical" group delay ambiguity spacing for the band:
  for (int i=0; i<observables_.size(); i++)
  {
    SgVlbiObservable           *o=observables_.at(i);
    grdAmbigsBySpacing_[o->grDelay().getAmbiguitySpacing()]++;
    phdAmbigsBySpacing_[o->phDelay().getAmbiguitySpacing()]++;
    if (maxNumOfChannels_ < o->getNumOfChannels())
      maxNumOfChannels_ = o->getNumOfChannels();
    if (o->getErrorCode() == "Z")
      numOfZCodes++;
    numByScanIdBySrc[o->owner()->src()->getKey()][o->owner()->getScanId()]++;
    sampleRateByCount_[o->getSampleRate()]++;
    bitsPerSampleByCount_[o->getBitsPerSample()]++;
  };
  //
  // set up the typical sample rate:
  if (sampleRateByCount_.size() == 0) //?
    logger->write(SgLogger::WRN, SgLogger::PREPROC, className() +
      "::selfCheck(): cannot find the sample rate for the " + getKey() + "-band");
  else if (sampleRateByCount_.size() == 1)
  {
    sampleRate_ = sampleRateByCount_.begin().key();
    logger->write(SgLogger::DBG, SgLogger::PREPROC, className() +
      "::selfCheck(): set up the sample rate to " + QString("").sprintf("%.2f", sampleRate_) + 
      " for the " + getKey() + "-band");
  }
  else
  {
    num = 0;
    logger->write(SgLogger::WRN, SgLogger::PREPROC, className() +
      "::selfCheck(): found multiple values of the sample rate at the " + getKey() + "-band:");
    for (QMap<double, int>::iterator it=sampleRateByCount_.begin(); it!=sampleRateByCount_.end(); ++it)
    {
      logger->write(SgLogger::WRN, SgLogger::PREPROC, className() +
        "::selfCheck(): " + QString("").sprintf("%.2f -> %d times", it.key(), it.value()));
      if (it.value() > num)
      {
        num = it.value();
        sampleRate_ = it.key();
      };
    };
    logger->write(SgLogger::DBG, SgLogger::PREPROC, className() +
      "::selfCheck(): set up the typical sample rate to " + QString("").sprintf("%.2f", sampleRate_) + 
      " for the " + getKey() + "-band");
  };
  //
  // set up the typical bits per sample:
  if (bitsPerSampleByCount_.size() == 0) //?
    logger->write(SgLogger::WRN, SgLogger::PREPROC, className() +
      "::selfCheck(): cannot find the bits per sample for the " + getKey() + "-band");
  else if (bitsPerSampleByCount_.size() == 1)
  {
    bitsPerSample_ = bitsPerSampleByCount_.begin().key();
    logger->write(SgLogger::DBG, SgLogger::PREPROC, className() +
      "::selfCheck(): set up the bits per sample to " + QString("").sprintf("%d", bitsPerSample_) + 
      " for the " + getKey() + "-band");
  }
  else
  {
    num = 0;
    logger->write(SgLogger::WRN, SgLogger::PREPROC, className() +
      "::selfCheck(): found multiple values of the bits per sample at the " + getKey() + "-band:");
    for (QMap<int, int>::iterator it=bitsPerSampleByCount_.begin(); it!=bitsPerSampleByCount_.end(); 
      ++it)
    {
      logger->write(SgLogger::WRN, SgLogger::PREPROC, className() +
        "::selfCheck(): " + QString("").sprintf("%d -> %d times", it.key(), it.value()));
      if (it.value() > num)
      {
        num = it.value();
        bitsPerSample_ = it.key();
      };
    };
    logger->write(SgLogger::DBG, SgLogger::PREPROC, className() +
      "::selfCheck(): set up the typical bits per sample to " + 
      QString("").sprintf("%d", bitsPerSample_) + " for the " + getKey() + "-band");
  };
  recordMode_.sprintf("NChan:%d Rbw:%.1f Bps:%d", 
    maxNumOfChannels_, maxNumOfChannels_*sampleRate_*1.0e-6, bitsPerSample_);

  logger->write(SgLogger::DBG, SgLogger::PREPROC, className() +
    "::selfCheck(): the record mode has been set to \"" + recordMode_ + 
    "\" for the " + getKey() + "-band:");

  //
  //
  num = 0;
  // group delay:
  for (QMap<double, int>::iterator it=grdAmbigsBySpacing_.begin(); it!=grdAmbigsBySpacing_.end(); ++it)
  {
    if (it.value() > num)
    {
      num = it.value();
      typicalGrdAmbigSpacing_ = it.key();
    };
  };
  if (grdAmbigsBySpacing_.size() == 1)
    strGrdAmbigsStat_.sprintf("%.1f", grdAmbigsBySpacing_.begin().key()*1.0e9);
  else
  {
    strGrdAmbigsStat_ = "";
    num = observables_.size();
    for (QMap<double, int>::iterator it=grdAmbigsBySpacing_.begin(); it!=grdAmbigsBySpacing_.end(); ++it)
      strGrdAmbigsStat_ += QString("").sprintf("%.1f (%.1f%%), ", it.key()*1.0e9, it.value()*100.0/num);
    strGrdAmbigsStat_ = strGrdAmbigsStat_.left(strGrdAmbigsStat_.size() - 2);
  };
  //
  // phase delay:
  num = 0;
  for (QMap<double, int>::iterator it=phdAmbigsBySpacing_.begin(); it!=phdAmbigsBySpacing_.end(); ++it)
  {
    if (it.value() > num)
    {
      num = it.value();
      typicalPhdAmbigSpacing_ = it.key();
    };
  };
  if (phdAmbigsBySpacing_.size() == 1)
    strPhdAmbigsStat_.sprintf("%.3f", phdAmbigsBySpacing_.begin().key()*1.0e9);
  else
  {
    strPhdAmbigsStat_ = "";
    num = observables_.size();
    for (QMap<double, int>::iterator it=phdAmbigsBySpacing_.begin(); it!=phdAmbigsBySpacing_.end(); ++it)
      strPhdAmbigsStat_ += QString("").sprintf("%.3f (%.1f%%), ", it.key()*1.0e9, it.value()*100.0/num);
    strPhdAmbigsStat_ = strPhdAmbigsStat_.left(strPhdAmbigsStat_.size() - 2);
  };
  //
  //
  if (typicalGrdAmbigSpacing_ > 0.0)
  {
    addAttr(Attr_HAS_AMBIGS);
    logger->write(SgLogger::DBG, SgLogger::PREPROC, className() +
      "::selfCheck(): the typical ambig.spacing for the " + getKey() + "-band was set to " + 
      QString("").sprintf("%.2fns", typicalGrdAmbigSpacing_*1.0e9));
  }
  else
  {
    delAttr(Attr_HAS_AMBIGS);
    logger->write(SgLogger::WRN, SgLogger::PREPROC, className() +
      "::selfCheck(): set up of the typical ambig.spacing for the " + getKey() + "-band failed");
  };
  // adjust error codes: Z code was added by the import procedure
  // if all codes are "Z", that is an old DSN session, remove them:
  if (numOfZCodes == observables_.size() && !isAttr(Attr_HAS_AMBIGS))
  {
    for (int i=0; i<observables_.size(); i++)
      observables_.at(i)->setErrorCode(" ");
    logger->write(SgLogger::WRN, SgLogger::PREPROC, className() +
      "::selfCheck(): looks like an old DSN session, the error codes have been cleared");
  };
  //
  // baselines:
  for (BaselinesByName_it it=baselinesByName_.begin(); it!=baselinesByName_.end(); ++it)
    isOk = isOk && it.value()->selfCheck();
  //
  // sources:
  for (SourcesByName_it it=sourcesByName_.begin(); it!=sourcesByName_.end(); ++it)
  {
    SgVlbiSourceInfo           *si=it.value();
    if (numByScanIdBySrc.contains(si->getKey()))
      si->setTotalScanNum(numByScanIdBySrc.value(si->getKey()).size());
    else
    {
      str = si->getKey();
      // the source have no any observations on the primary band, remove it from the maps:
      --it;
      sourcesByName_.remove(str);
      delete si;
      logger->write(SgLogger::INF, SgLogger::PREPROC, className() +
        "::selfCheck(): the source \"" + str + "\" has no any good observations in the primary band"
        ", it was removed from the " + getKey() + "-band too");
    };
  };
  logger->write(SgLogger::DBG, SgLogger::PREPROC, className() +
    "::selfCheck(): numbers of scans per a source were calculated", true);
  //
  return isOk;
};
/*=====================================================================================================*/




/*=====================================================================================================*/
