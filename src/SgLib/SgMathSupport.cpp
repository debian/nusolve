/*
 *
 *    This file is a part of Space Geodetic Library. The library is used by
 *    nuSolve, a part of CALC/SOLVE system, and designed to make analysis of
 *    geodetic VLBI observations.
 *    Copyright (C) 2010-2020 Sergei Bolotin.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <stdlib.h>
#include <complex>



//#include <SgLogger.h>
#include <SgMathSupport.h>





#define SWAP(a, b) {typeof(a) t; t = a; a = b; b = t;}



/*=====================================================================================================*/
//
// just aux functions:
//
//
/*=====================================================================================================*/
// https://rosettacode.org/wiki/Fast_Fourier_transform
// it is usable too:
void _fft(std::complex<double> buf[], std::complex<double> out[], int n, int step)
{
  std::complex<double>          zI(0.0, 1.0);
  if (step < n) 
  {
    _fft(out, buf, n, step*2);
    _fft(out + step, buf + step, n, step*2);
    for (int i=0; i<n; i+=2*step)
    {
      std::complex<double>      t=std::exp(-zI*M_PI*double(i)/double(n))*out[i + step];
      buf[i / 2]     = out[i] + t;
      buf[(i + n)/2] = out[i] - t;
    };
  };
};
void fft(std::complex<double> buf[], int n)
{
  std::complex<double>          out[n];
  for (int i=0; i<n; i++) 
    out[i] = buf[i];
   _fft(buf, out, n, 1);
};



// This function is originally written by Paul Bourke (http://paulbourke.net/miscellaneous/dft),
// then modified a little bit.
// 
/*
   This computes an in-place complex-to-complex FFT 
   x and y are the real and imaginary arrays of n=2^m points.
   dir =  1 gives forward transform
   dir = -1 gives reverse transform 
*/
void fft(double *x, double *y, int n, int dir)
{
  int                           m, i1, j, k, i2, l1, l2;
  double                        c1, c2, tx, ty, t1, t2, u1, u2, z;

  m = 0;
  while (1<<m<n && m<32)
    m++;
  
  if (1<<m != n)
  {
    std::cerr << "FFT: number of points, " << n << ", is not a power of 2.\n";
    return;
  };

  /* Do the bit reversal */
  i2 = n >> 1;
  j = 0;
  for (int i=0; i<n-1; i++)
  {
    if (i < j)
    {
      tx = x[i];
      ty = y[i];
      x[i] = x[j];
      y[i] = y[j];
      x[j] = tx;
      y[j] = ty;
    };
    k = i2;
    while (k <= j)
    {
      j -= k;
      k >>= 1;
    };
    j += k;
  };
  /* Compute the FFT */
  c1 = -1.0; 
  c2 = 0.0;
  l2 = 1;
  for (int l=0; l<m; l++)
  {
    l1 = l2;
    l2 <<= 1;
    u1 = 1.0; 
    u2 = 0.0;
    for (j=0; j<l1; j++)
    {
      for (int i=j; i<n; i+=l2)
      {
        i1 = i + l1;
        t1 = u1*x[i1] - u2*y[i1];
        t2 = u1*y[i1] + u2*x[i1];
        x[i1] = x[i] - t1; 
        y[i1] = y[i] - t2;
        x[i] += t1;
        y[i] += t2;
      };
      z =  u1*c1 - u2*c2;
      u2 = u1*c2 + u2*c1;
      u1 = z;
    };
    c2 = sqrt((1.0 - c1)/2.0);
    if (dir == 1)
      c2 = -c2;
    c1 = sqrt((1.0 + c1)/2.0);
  };
  /* Scaling for forward transform */
  if (dir == 1)
  {
    for (int i=0; i<n; i++)
    {
      x[i] /= n;
      y[i] /= n;
    };
  };
  //
  return;
};





/*=====================================================================================================*/
//
// statics:
//
/*=====================================================================================================*/




/*=====================================================================================================*/
