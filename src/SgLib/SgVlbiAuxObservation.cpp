/*
 *
 *    This file is a part of Space Geodetic Library. The library is used by
 *    nuSolve, a part of CALC/SOLVE system, and designed to make analysis of
 *    geodetic VLBI observations.
 *    Copyright (C) 2010-2020 Sergei Bolotin.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <stdlib.h>


#include <SgVlbiAuxObservation.h>
#include <SgLogger.h>



/*=======================================================================================================
*
*                           METHODS:
* 
*======================================================================================================*/
//
// static first:



//
bool SgVlbiAuxObservation::selfCheck()
{
  bool isOk = true;
  
  // make selfcheck here:
  
  return isOk;
};



//  
bool SgVlbiAuxObservation::isEligible(const SgTaskConfig* cfg)
{
  bool isOk = SgObservation::isEligible(cfg);
  
  // make selfcheck here:
  
  return isOk;
};



//
void SgVlbiAuxObservation::prepare4Analysis(SgTaskManager*)
{
};



//
void SgVlbiAuxObservation::evaluateResiduals(SgTaskManager*, SgVlbiBand*, bool)
{
};



//
void SgVlbiAuxObservation::evaluateResiduals(SgTaskManager*)
{
};



//
void SgVlbiAuxObservation::evaluateTheoreticalValues(SgTaskManager*)
{
};



//
const SgVector& SgVlbiAuxObservation::o_c()
{
  logger->write(SgLogger::WRN, SgLogger::OBS | SgLogger::ESTIMATOR, className() + 
    ": call to (O-C)");  
  return vZero;
};



//
const SgVector& SgVlbiAuxObservation::sigma()
{
  logger->write(SgLogger::WRN, SgLogger::OBS | SgLogger::ESTIMATOR, className() + 
    ": call to sigma");  
  return vZero;
};






/*=====================================================================================================*/
//
//                           FRIENDS:
//
/*=====================================================================================================*/
//

/*=====================================================================================================*/
//
// aux functions:
//

/*=====================================================================================================*/
//
// constants:
//
/*=====================================================================================================*/




/*=====================================================================================================*/
