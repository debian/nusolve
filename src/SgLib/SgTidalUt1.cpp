/*
 *
 *    This file is a part of Space Geodetic Library. The library is used by
 *    nuSolve, a part of CALC/SOLVE system, and designed to make analysis of
 *    geodetic VLBI observations.
 *    Copyright (C) 2010-2020 Sergei Bolotin.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <math.h>


#include <QtCore/QFile>
#include <QtCore/QStringList>
#include <QtCore/QTextStream>

#include <SgTidalUt1.h>
#include <SgLogger.h>


void fundArgs_stub   (const SgMJD&, double[5]);
void fundArgs_UT1ZT  (const SgMJD&, double[5]);
void fundArgs_NUTFA  (const SgMJD&, double[5]);
void fundArgs_NUTF96 (const SgMJD&, double[5]);
void fundArgs_NUTFA10(const SgMJD&, double[5]);


void calcUT1stub(const double[5], double&);
void calcUT1ZT  (const double[5], double&);
void calcUT1S_82(const double[5], double&);
void calcUT1S_83(const double[5], double&);
void calcUT1S2K (const double[5], double&);





/*=======================================================================================================
*
*                           METHODS:
* 
*======================================================================================================*/
//
// static first:
const QString SgTidalUt1::className()
{
  return "SgTidalUt1";
};



// An empty constructor:
SgTidalUt1::SgTidalUt1(UT1TideContentType tc, double v)
{
  tideContent_ = tc;
  calcVersionValue_ = v;
  
  if (tideContent_ == CT_SHORT_TERMS_REMOVED)           // UT1R
  {
    fundArgs    = &fundArgs_UT1ZT;
    calcUT1_UTC = &calcUT1ZT;
  }
  else if (tideContent_ == CT_ALL_TERMS_REMOVED)        // UT1S
  {
    if (calcVersionValue_ <= 8.200001)
    {
      fundArgs    = &fundArgs_NUTFA;
      calcUT1_UTC = &calcUT1S_82;
    }
    else if (9.0 <= calcVersionValue_ && calcVersionValue_ < 9.9)
    {
      fundArgs    = &fundArgs_NUTF96;
      calcUT1_UTC = &calcUT1S_83;
    }
    else if (9.99 < calcVersionValue_ && calcVersionValue_ < 99.99)
    {
      fundArgs    = &fundArgs_NUTFA10;
      calcUT1_UTC = &calcUT1S2K;
    } 
    else
    {
      fundArgs    = &fundArgs_stub;
      calcUT1_UTC = &calcUT1stub;
      logger->write(SgLogger::ERR, SgLogger::TIME, className() +
        ": the CALC version number, " + QString("").setNum(calcVersionValue_) +
        ", is undocumented. Removing of zonal tides from UT1 is impossible");
    };
  }
  else
  {
    fundArgs    = &fundArgs_stub;
    calcUT1_UTC = &calcUT1stub;
  };
};



// A destructor:
SgTidalUt1::~SgTidalUt1()
{
};



//
double SgTidalUt1::calc(const SgMJD& t)
{
  double                        dUt(0.0);
  double                        fArgs[5];
  
  // evaluate the fundamental arguments:
  (*fundArgs)(t, fArgs);
  
  // calculate the zonal tidal correction:
  (*calcUT1_UTC)(fArgs, dUt);
  return dUt;
};
/*=====================================================================================================*/






/*=======================================================================================================
*
*                           FRIENDS:
* 
*======================================================================================================*/
//



/*=====================================================================================================*/
//
// aux functions:
//
// 
//
void fundArgs_stub(const SgMJD&, double args[5])
{
  args[0] = args[1] = args[2] = args[3] = args[4] = 0.0;
};



//
void fundArgs_UT1ZT(const SgMJD& t, double args[5])
{
  //  
  //  ! 1.  UT1ZT PROGRAM SPECIFICATION
  //
  //  ! 1.1 Evaluate the effects of zonal earth tides on the rotation
  //  !     of the earth.
  //
  //  ! 1.2 REFERENCES:
  //  !                    1.  YODER, WILLIAMS, AND PARKE, 1981, "TIDAL
  //  !                        VARIATIONS OF EARTH ROTATION", J. GEOPHYS.
  //  !                        RES., VOL. 86, P. 881-891.
  //  !                    2.  PROJECT MERIT STANDARDS, W. MELBOURNE, CHAIRMAN,
  //  !                        U.S. NAVAL OBSERVATORY, WASHINGTON, D.C.,
  //  !                        DEC. 27, 1983.
  //
  
  // It is a first part of UT1ZT:
  
  const double                  seccon(206264.8062470964);
  const double                  sec360(1296000.0);
  double                        el, elp, f, d, om;
  double                        cent((t - tEphem)/36525.0);
  // l:
  el  = (( 0.064*cent + 31.310)*cent +  715922.633)*cent +  485866.733 + 
    fmod(1325.0*cent, 1.0)*1296000.0;
  args[0] = fmod(el,  sec360);
  // l':
  elp = ((-0.012*cent -  0.577)*cent + 1292581.224)*cent + 1287099.804 + 
    fmod(  99.0*cent, 1.0)*1296000.0;
  args[1] = fmod(elp, sec360);
  // F:
  f   = (( 0.011*cent - 13.257)*cent +  295263.137)*cent +  335778.877 + 
    fmod(1342.0*cent, 1.0)*1296000.0;
  args[2] = fmod(f,   sec360);
  // D:
  d   = (( 0.019*cent -  6.891)*cent + 1105601.328)*cent + 1072261.307 + 
    fmod(1236.0*cent, 1.0)*1296000.0;
  args[3] = fmod(d,   sec360);
  // Omega:
  om  = (( 0.008*cent +  7.455)*cent -  482890.539)*cent  + 450160.280 - 
    fmod(   5.0*cent, 1.0)*1296000.0;
  args[4] = fmod(om,  sec360);
  //
  for (int i=0; i<5; i++)
    args[i] /= seccon;  // sec=>rad
};



//
void fundArgs_NUTFA(const SgMJD& t, double args[5])
{
  //
  //  !  NUTFA computes the number of Julian centuries since J2000 and the fundamental
  //  !  arguments and derivatives to be used in nutation series.
  //  !
  //  !  References: D.McCarthy, IERS Technical Note 13, Paris 1992
  //  !              T.C. van Flandern, Lunar Occult. Work (fundam.argum.)
  //
  const double elc [5] = { 0.064, 31.310,  715922.633,  485866.733, 1325.0};
  const double elpc[5] = {-0.012, -0.577, 1292581.224, 1287099.804,   99.0};
  const double fc  [5] = { 0.011,-13.257,  295263.137,  335778.877, 1342.0};
  const double dc  [5] = { 0.019, -6.891, 1105601.328, 1072261.307, 1236.0};
  const double omc [5] = { 0.008,  7.455, -482890.539,  450160.280,   -5.0};
  const double                  sec360(1296000.0);
  const double                  convds(4.8481368110953599E-06);
  double                        cent((t - tEphem)/36525.0);
  double                        cent2, cent3;
  double                        el, elp, f, d, om;
  cent2 = cent*cent;
  cent3 = cent2*cent;
  // l:
  el  = elc [0]*cent3 + elc [1]*cent2 + elc [2]*cent + elc [3] + fmod(elc [4]*cent, 1.0)*sec360;
  args[0] = fmod(el,  sec360);
  // l':
  elp = elpc[0]*cent3 + elpc[1]*cent2 + elpc[2]*cent + elpc[3] + fmod(elpc[4]*cent, 1.0)*sec360;
  args[1] = fmod(elp, sec360);
  // F:
  f   = fc  [0]*cent3 + fc  [1]*cent2 + fc  [2]*cent + fc  [3] + fmod(fc  [4]*cent, 1.0)*sec360;
  args[2] = fmod(f,   sec360);
  // D:
  d   = dc  [0]*cent3 + dc  [1]*cent2 + dc  [2]*cent + dc  [3] + fmod(dc  [4]*cent, 1.0)*sec360;
  args[3] = fmod(d,   sec360);
  // Omega:
  om  = omc [0]*cent3 + omc [1]*cent2 + omc [2]*cent + omc [3] + fmod(omc [4]*cent, 1.0)*sec360;
  args[4] = fmod(om,  sec360);
  //
  for (int i=0; i<5; i++)
    args[i] *= convds;  // sec=>rad
};



//
void fundArgs_NUTF96(const SgMJD& t, double args[5])
{
  //
  //  !  NUTF96 computes the number of Julian centuries since J2000 and the
  //  !  fundamental arguments and derivatives to be used in the nutation series.
  //  !
  //  !  NUTF96 is taken from Calc 9, and comforms with the 1996 IERS Conventions.
  //  !   It should be used to duplicate the UT1 => UT1S conversion for Calc 9+
  //  !   databases, and for ALL modfile UT1 => UT1S conversions. Added to SOLVE
  //  !   99.12.14 -DG-
  //
  //  !  References: D.McCarthy, IERS Technical Note 13, 'IERS Conventions (1992)',
  //  !                          Paris 1992
  //  !              T.C. van Flandern, Lunar Occult. Work (fundam.argum.)
  //  !              D.McCarthy, IERS Technical Note 21, 'IERS Conventions (1996)',
  //  !                          Paris 1996
  //
  const double elc [5] = {-0.00024470,  0.051635,  31.8792, 1717915923.2178,  485868.249036};
  const double elpc[5] = {-0.00001149, -0.000136,  -0.5532,  129596581.0481, 1287104.79305};
  const double fc  [5] = { 0.00000417, -0.001037, -12.7512, 1739527262.8478,  335779.526232};
  const double dc  [5] = {-0.00003169,  0.006593,  -6.3706, 1602961601.2090, 1072260.70369};
  const double omc [5] = {-0.00005939,  0.007702,   7.4722,   -6962890.2665,  450160.398036};
  const double                  sec360(1296000.0);
  const double                  convds(4.8481368110953599E-06);
  double                        cent((t - tEphem)/36525.0);
  double                        cent2, cent3, cent4;
  double                        el, elp, f, d, om;
  cent2 = cent*cent;
  cent3 = cent2*cent;
  cent4 = cent2*cent2;
  // l:
  el  = elc [0]*cent4 + elc [1]*cent3 + elc [2]*cent2 + elc [3]*cent + elc[4];
  args[0] = fmod(el, sec360);
  // l':
  elp = elpc[0]*cent4 + elpc[1]*cent3 + elpc[2]*cent2 + elpc[3]*cent + elpc[4];
  args[1] = fmod(elp, sec360);
  // F:
  f   = fc  [0]*cent4 + fc  [1]*cent3 + fc  [2]*cent2 + fc  [3]*cent + fc[3];
  args[2] = fmod(f, sec360);
  // D:
  d   = dc  [0]*cent4 + dc  [1]*cent3 + dc  [2]*cent2 + dc  [3]*cent + dc[4];
  args[3] = fmod(d, sec360);
  // Omega:
  om  = omc [0]*cent4 + omc [1]*cent3 + omc [2]*cent2 + omc [3]*cent + omc[4];
  args[4] = fmod(om, sec360);

  for (int i=0; i<5; i++)
    args[i] *= convds;  // sec=>rad
};



//
void fundArgs_NUTFA10(const SgMJD& t, double args[5])
{
  //  !    From Calc 10.0, 2005.12.21, DG
  //  !
  //  !  NUTFA computes the number of Julian centuries since J2000 and the fundamental
  //  !  arguments and derivatives to be used in the nutation series.
  //  !
  //  !  References: D.McCarthy, IERS Technical Note 32, IERS Conventions (2003).
  //
  const double elc2 [5] = {-0.00024470, 0.051635, 31.8792, 1717915923.2178,  485868.249036};
  const double elpc2[5] = {-0.00001149, 0.000136, -0.5532,  129596581.0481, 1287104.793048};
  const double fc2  [5] = { 0.00000417,-0.001037,-12.7512, 1739527262.8478,  335779.526232};
  const double dc2  [5] = {-0.00003169, 0.006593, -6.3706, 1602961601.2090, 1072260.703692};
  const double omc2 [5] = {-0.00005939, 0.007702,  7.4722,   -6962890.5431,  450160.398036};
  const double                  sec360(1296000.0);
  const double                  convds(4.8481368110953599E-06);
  double                        cent((t - tEphem)/36525.0);
  double                        cent2, cent3, cent4;
  double                        el, elp, f, d, om;
  cent2 = cent*cent;
  cent3 = cent2*cent;
  cent4 = cent2*cent2;
  // l:
  el  =  elc2[0]*cent4  + elc2[1]*cent3  + elc2[2]*cent2  + elc2[3]*cent  + elc2[4];
  args[0] = fmod(el, sec360);
  // l':
  elp =  elpc2[0]*cent4 + elpc2[1]*cent3 + elpc2[2]*cent2 + elpc2[3]*cent + elpc2[4];
  args[1] = fmod(elp, sec360);
  // F:
  f   =  fc2[0]*cent4   + fc2[1]*cent3   + fc2[2]*cent2   + fc2[3]*cent   + fc2[3];
  args[2] = fmod(f, sec360);
  // D:
  d =    dc2[0]*cent4   + dc2[1]*cent3   + dc2[2]*cent2   + dc2[3]*cent   + dc2[4];
  args[3] = fmod(d, sec360);
  // Omega:
  om =   omc2[0]*cent4  + omc2[1]*cent3  + omc2[2]*cent2  + omc2[3]*cent  + omc2[4];
  args[4] = fmod(om, sec360);
  //
  for (int i=0; i<5; i++)
    args[i] *= convds;  // sec=>rad
};



//
void calcUT1stub(const double[5], double& dUt1)
{
  dUt1 = 0.0;
};



//
void calcUT1ZT(const double args[5], double& dUt1)
{
  //  
  //  ! 1.  UT1ZT PROGRAM SPECIFICATION
  //
  //  ! 1.1 Evaluate the effects of zonal earth tides on the rotation
  //  !     of the earth.
  //
  //  ! 1.2 REFERENCES:
  //  !                    1.  YODER, WILLIAMS, AND PARKE, 1981, "TIDAL
  //  !                        VARIATIONS OF EARTH ROTATION", J. GEOPHYS.
  //  !                        RES., VOL. 86, P. 881-891.
  //  !                    2.  PROJECT MERIT STANDARDS, W. MELBOURNE, CHAIRMAN,
  //  !                        U.S. NAVAL OBSERVATORY, WASHINGTON, D.C.,
  //  !                        DEC. 27, 1983.
  //
  
  // This is a second part of UT1ZT:
  
  // make summation:
  double                        arg;
  dUt1 = 0.0;
  for (int i=0; i<numOfRecs_Ut1cm; i++)
  {
    arg = ut1Ttable_Ut1cm[i].n_[0] * args[0] +
          ut1Ttable_Ut1cm[i].n_[1] * args[1] +
          ut1Ttable_Ut1cm[i].n_[2] * args[2] +
          ut1Ttable_Ut1cm[i].n_[3] * args[3] +
          ut1Ttable_Ut1cm[i].n_[4] * args[4] ;
    arg = fmod(arg, M_PI*2.0);
    dUt1 += ut1Ttable_Ut1cm[i].ut1_sin_*sin(arg);
  };
  dUt1 *= 1.0E-4; // convert to seconds
};



//
void calcUT1S_82(const double args[5], double& dUt1)
{
  //  
  //  !     Purpose: This subroutine evaluates the effects of zonal Earth tides on
  //  !     the rotation of the Earth. The model used is from Yoder, Williams, and
  //  !     Park (1981) and modified by the ocean effects as given in Dickman (1991)
  //  !     as recommended by the IERS Standards, p. 117, 119-120 (1992).
  //
  //  !     Special Note: Under the UT1S definition, and as done by this routine,
  //  !     zonal tides of _all_ periods are evaluated, including even those of
  //  !     18.6 year period. Results will be substantially different (tenths of
  //  !     seconds) from those evaluated with the "UT1R" algorithm, which only
  //  !     includes the 41 terms for periods under 35 days. If you wish to determine
  //  !     the effects from only those periods, please use the original Luzum
  //  !     "zontids" routine, with N set to 41.  (B.A.)
  //
  double                        arg, sinArg, cosArg;
  dUt1 = 0.0;
  for (int i=0; i<numOfRecs_Ut1s82; i++)
  {
    arg = 0.0;
    for (int j=0; j<5; j++)
      arg += args[j]*ut1Ttable_Ut1s82[i].n_[j];
    arg = fmod(arg, M_PI*2.0);
    sincos(arg,   &sinArg, &cosArg);
    dUt1 += ut1Ttable_Ut1s82[i].ut1_sin_*sinArg + ut1Ttable_Ut1s82[i].ut1_cos_*cosArg;
  };
  dUt1 *= 1.0E-4; // convert to seconds
};



//
void calcUT1S_83(const double args[5], double& dUt1)
{
  //  !     Purpose: This subroutine evaluates the effects of zonal Earth tides on
  //  !     the rotation of the Earth. The model used is from Yoder, Williams, and
  //  !     Park (1981) and modified by the ocean effects as given in Dickman (1991)
  //  !     as recommended by the IERS Standards, p. 117, 119-120 (1992).
  //
  //  !     Special Note: Under the UT1S definition, and as done by this routine,
  //  !     zonal tides of _all_ periods are evaluated, including even those of
  //  !     18.6 year period. Results will be substantially different (tenths of
  //  !     seconds) from those evaluated with the "UT1R" algorithm, which only
  //  !     includes the 41 terms for periods under 35 days. If you wish to determine
  //  !     the effects from only those periods, please use the original Luzum
  //  !     "zontids" routine, with N set to 41.  (B.A.)
  //
  //  ! Note added by JMGipson
  //  !     This routine was substantially rewritten to calculate DUT_DOT,DLOD,
  //  !     and DOMEGA using the series expansion of DUT.  This was done becuase
  //  !     the old definition of DUT_DOT in terms of a separate series was
  //  !     inconsistent.  I modified it so that it used the same table to calculate
  //  !     the two values.
  //  !     I also removed dlod and domega from the argument list, which solve/calc
  //  !     does not use.  Instead I put in DUT_DOT, which is used.
  double                        arg, sinArg, cosArg;
  dUt1 = 0.0;
  for (int i=0; i<numOfRecs_Ut1s83; i++)
  {
    arg = 0.0;
    for (int j=0; j<5; j++)
      arg += args[j]*ut1Ttable_Ut1s83[i].n_[j];
    arg = fmod(arg, M_PI*2.0);
    sincos(arg,   &sinArg, &cosArg);
    dUt1 += ut1Ttable_Ut1s83[i].ut1_sin_*sinArg + ut1Ttable_Ut1s83[i].ut1_cos_*cosArg;
  };
  dUt1 *= 1.0E-4; // convert to seconds
};



//
void calcUT1S2K(const double args[5], double& dUt1)
{
  //
  //  !    From Calc 10.0, 2005.12.21, DG
  //  !
  //  !     Purpose: This subroutine evaluates the effects of zonal Earth tides on
  //  !     the rotation of the Earth. The model used is from Defraigne and Smits,
  //  !     1999, as recommended by the IERS Conventions (2003).
  //
  double                        arg, sinArg, cosArg;
  dUt1 = 0.0;
  for (int i=0; i<numOfRecs_Ut1s2k; i++)
  {
    arg = 0.0;
    for (int j=0; j<5; j++)
      arg += args[j]*ut1Ttable_Ut1s2k[i].n_[j];
    arg = fmod(arg, M_PI*2.0);
    sincos(arg,   &sinArg, &cosArg);
    dUt1 += ut1Ttable_Ut1s2k[i].ut1_sin_*sinArg + ut1Ttable_Ut1s2k[i].ut1_cos_*cosArg;
  };
  dUt1 *= 1.0E-4; // convert to seconds
};




// i/o:


/*=====================================================================================================*/
//
// constants:
//
/*=====================================================================================================*/








/*=====================================================================================================*/
