/*
 *
 *    This file is a part of Space Geodetic Library. The library is used by
 *    nuSolve, a part of CALC/SOLVE system, and designed to make analysis of
 *    geodetic VLBI observations.
 *    Copyright (C) 2010-2020 Sergei Bolotin.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <stdlib.h>
#include <math.h>


#include <QtCore/QDataStream>
#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QRegExp>
#include <QtCore/QStringList>
#include <QtCore/QTextStream>


#include <Sg3dMatrixR.h>
#include <SgEccRec.h>
#include <SgLogger.h>
#include <SgVlbiStationInfo.h>
#include <SgVlbiObservation.h>



const int SgVlbiStationInfo::maxNumOfPolynomials_=MAX_NUMBER_OF_POLYNOMIALS;


/*=======================================================================================================
*
*                           METHODS:
* 
*======================================================================================================*/
//
// static first:
const QString SgVlbiStationInfo::className()
{
  return "SgVlbiStationInfo";
};



//
int SgVlbiStationInfo::mntType2int(MountingType mType)
{
  int                           mnt=0;
  switch (mType)
  {
    case MT_EQUA:
      mnt = 1;
      break;
    case MT_X_YN:
      mnt = 2;
      break;
    case MT_AZEL:
      mnt = 3;
      break;
    case MT_RICHMOND:
      mnt = 4;
      break;
    case MT_X_YE:
      mnt = 5;
      break;
    case MT_UNKN:
    default:
      mnt = 0;
      break;
  };
  return mnt;
};



//
SgVlbiStationInfo::MountingType SgVlbiStationInfo::int2mntType(int iMnt)
{
  MountingType                  mnt=MT_UNKN;
  switch (iMnt)
  {
    case 1:
      mnt = MT_EQUA;
      break;
    case 2:
      mnt = MT_X_YN;
      break;
    case 3:
      mnt = MT_AZEL;
      break;
    case 4:
      mnt = MT_RICHMOND;
      break;
    case 5:
      mnt = MT_X_YE;
      break;
    default:
      logger->write(SgLogger::WRN, SgLogger::IO_NCDF, className() +
        "::int2mntType(): got unknown value of the axis type (" + QString("").setNum(iMnt) + ")");
      break;
    };
  return mnt;
};




#pragma GCC push_options
#pragma GCC optimize ("O0")

//
void SgVlbiStationInfo::calcRLF(double &latitude, double& longitude, double& height,
  const Sg3dVector& r, bool useOldEllipsoid)
{
/* Downloaded from from ftp://tai.bipm.org/iers/conv2010/chapter4/GCONV2.F
 * URL to the site: http://tai.bipm.org/iers/conv2010/conv2010_c4.html
 * 
      SUBROUTINE GCONV2 (A,F,X,Y,Z,PHI,LAMBDA,H)
*+
*  - - - - - - - - - - -
*   G C O N V 2
*  - - - - - - - - - - -
*
*  This routine is part of the International Earth Rotation and
*  Reference Systems Service (IERS) Conventions software collection.
*
*  This subroutine transforms from geocentric rectangular to 
*  geodetic coordinates.
*  [...]
*/

  //*  -------------
  //*  Preliminaries
  //*  -------------
  //*       Default Values: GRS1980 System (recommended in IERS Conventions)
  //  double       dA(6378137.0), dF(0.00335281068118);
  useOldEllipsoid = false;
  useOldEllipsoid = true;

  double       dA, dF;
  dA = 6378137.0;
  dF = 0.00335281068118;
  
  if (useOldEllipsoid)
  {
    dA = 6378136.3;
    dF = 0.0033528918690;
  };

  //*  Functions of ellipsoid parameters
  double            dAEPS2, dE2, dE4T, dEP2, dEP, dAEP;
  double            dP2;
  dAEPS2 = dE2 = dE4T = dEP2 = dEP = dAEP = 0.0;
  dP2 = 0.0;
  
  dAEPS2 = dA*dA*1.0e-32;
  dE2    = (2.0 - dF)*dF;
  dE4T   = dE2*dE2*1.5;
  dEP2   = 1.0 - dE2;
  dEP    = sqrt(dEP2);
  dAEP   = dA*dEP;

  //* ---------------------------------------------------------
  //*       Compute Coefficients of (Modified) Quartic Equation
  //*
  //*       Remark: Coefficients are rescaled by dividing by 'a'
  //* ---------------------------------------------------------
  //*      Compute distance from polar axis squared
  dP2 = r.at(X_AXIS)*r.at(X_AXIS) + r.at(Y_AXIS)*r.at(Y_AXIS);
  
  //*      Compute longitude lambda 
  if (dP2 != 0.0)
  {
    longitude = atan2(r.at(Y_AXIS), r.at(X_AXIS));
    if (longitude<0.0)
      longitude += M_PI*2.0;
  }
  else
    longitude = 0.0;
    

  double            dABSZ, dP, dS0, dPN, dZP, dC0, dC02, dC03, dS02, dS03, dA02;
  double            dA0, dA03, dD0, dF0, dB0, dS1, dCP, dS12, dCP2;

  dABSZ = dP = dS0 = dPN = dZP = dC0 = dC02 = dC03 = dS02 = dS03 = dA02 = 0.0;
  dA0 = dA03 = dD0 = dF0 = dB0 = dS1 = dCP = dS12 = dCP2 = 0.0;

  //*      Ensure that Z-coordinate is unsigned
  dABSZ = fabs(r.at(Z_AXIS));
  //*      Continue unless at the poles
  if (dP2 > dAEPS2)
  {
    //*      Compute distance from polar axis
    dP = sqrt(dP2);
    //*      Normalize
    dS0 = dABSZ/dA;
    dPN = dP/dA;
    dZP = dEP*dS0;
    //*     Prepare Newton correction factors.
    dC0  = dEP*dPN;
    dC02 = dC0*dC0;
    dC03 = dC02*dC0;
    dS02 = dS0*dS0;
    dS03 = dS02*dS0;
    dA02 = dC02 + dS02;
    dA0  = sqrt(dA02);
    dA03 = dA02*dA0;
    dD0  = dZP*dA03 + dE2*dS03;
    dF0  = dPN*dA03 - dE2*dC03;
    //*     Prepare Halley correction factor.
    dB0 = dE4T*dS02*dC02*dPN*(dA0 - dEP);
    dS1 = dD0*dF0 - dB0*dS0;
    dCP = dEP*(dF0*dF0 - dB0*dC0);
    //*     Evaluate latitude and height.
    latitude = atan(dS1/dCP);
    dS12 = dS1*dS1;
    dCP2 = dCP*dCP;
    height = (dP*dCP + dABSZ*dS1 - dA*sqrt(dEP2*dS12 + dCP2))/sqrt(dS12 + dCP2);
  }
  else
  {
    //*     Special case: pole.
    latitude = M_PI_2;
    height   = dABSZ - dAEP;
  };

  //*  Restore sign of latitude.
  if (r.at(Z_AXIS) < 0.0) 
    latitude = -latitude;
};



//
//void SgVlbiStationInfo::calcRLF_bis(double &latitude, double& longitude, double& height,
//  const Sg3dVector& r, bool useOldEllipsoid)
//{
 /*
    IERS TECHNICAL NOTE 21
    IERS Conventions (1996)
    page 12:
    
    Once the Cartesian coordinates (x,y,z) are known, they can be transformed to "datum" or
    curvilinear geodetic coordinates (lambda, fi, h) reffered to an ellipsoid of semi-major 
    axis a and flattening f, using the following code (Borkowski, 1989). First compute 
    lambda=tan^(-1)(y/x) properly determining the quadrant from x and y (0<=lamda<=2pi)
  */
//
//  longitude = atan2(r.at(Y_AXIS), r.at(X_AXIS));
//  if (longitude<0.0)
//    longitude += M_PI*2.0;
//  
//  /* 
//     and r=sqrt(x^2+y^2).
//  */
//
//  double rho = hypot(r.at(X_AXIS), r.at(Y_AXIS));
//
  /*
        subroutine GEOD(r,z,fi,h)
    c Program to transform Cartesian to geodetic coordinates
    c based on the exact solution (Borkowski, 1989)
    c Input : r, z = equatorial [m] and polar [m] components
    c Output: fi, h = geodetic coord's (latitude [rad], height [m])
        implicit real*8(a-h,o-z)
//  */
//  
//  double b,E,F,P,Q,D,s,v,G,t;
//
  /*
    c GRS80 ellipsoid: semimajor axis (a) and inverse flattening (fr)
        data a,fr /6378137.d0,298.257222101d0/
  */
////  const double a  = 6378137.0;
////  const double fr = 298.257222101;
//  double a  = 6378137.0;
//  double fr = 298.257222101;
//
//  if (useOldEllipsoid)
//  {
//    a  = 6378136.3;
//    fr = 1.0/0.0033528918690;
//  };
//
  /*
        b = dsign(a - a/fr,z)
        E = ((z + b)*b/a - a)/r
        F = ((z - b)*b/a + a)/r
  */
//  b = sign(a - a/fr, r.at(Z_AXIS));
//  E = ((r.at(Z_AXIS) + b)*b/a - a)/rho;
//  F = ((r.at(Z_AXIS) - b)*b/a + a)/rho;
//    
  /*
    c Find solution to:  t**4 + 2*E*t**3 + 2*F*t - 1 = 0
        P = (E*F + 1.)*4.d0/3.d0
        Q = (E*E - F*F)*2.d0
        D = P*P*P + Q*Q
            if(D.ge.0d0) then
        s = dsqrt(D) + Q
        s = dsign(dexp(dlog(dabs(s))/3.d0),s)
        v = P/s - s
    c Improve the accuracy of numeric values of v
        v = -(Q + Q + v*v*v)/(3.d0*P)
            else
        v = 2.d0*dsqrt(-P)*dcos(dacos(Q/P/dsqrt(-P))/3.d0)
            endif
        G = .5d0*(E + dsqrt(E*E + v))
        t = dsqrt(G*G + (F - v*G)/(G + G - E)) - G
  */
//
//  P = (E*F + 1.0)*4.0/3.0;
//  Q = (E*E - F*F)*2.0;
//  D = P*P*P + Q*Q;
//  if (D>=0.0)
//  {
//    s = sqrt(D) + Q;
//    s = sign(exp(log(fabs(s))/3.0), s);
//    v = P/s - s;
//    v = -(Q + Q + v*v*v)/(3.0*P);
//  }
//  else
//    v = 2.0*sqrt(-P)*cos(acos(Q/P/sqrt(-P))/3.0);
//
//  G = 0.5*(E + sqrt(E*E + v));
//  t = sqrt(G*G + (F - v*G)/(G + G - E)) - G;
//  
  /*
        fi = datan((1.d0 - t*t)*a/(2.d0*b*t))
        h = (r - a*t)*dcos(fi) + (z - b)*dsin(fi)
        return
        end
  */
//  latitude = atan((1.0 - t*t)*a/(2.0*b*t));
//  height = (rho - a*t)*cos(latitude) + (r.at(Z_AXIS) - b)*sin(latitude);
//  return;
//};
#pragma GCC pop_options






// An empty constructor:
SgVlbiStationInfo::SgVlbiStationInfo(int idx, const QString& key, const QString& aka) :
  SgObjectInfo(idx, key, aka),
  clockBreaks_(),
  auxObservationByScan_(),
  cableCalsOriginTxt_(""),
  meteoDataOriginTxt_(""),
  r_(v3Zero),
  r_ea_(v3Zero),
  v_ea_(v3Zero),
  v3Ecc_(v3Zero),
  tsysIfFreqs_(),
  tsysIfIds_(),
  tsysIfSideBands_(),
  tsysIfPolarizations_(),
  pcClocks_(),
  pcZenith_()
{
  clocksModelOrder_ = 2;
  cableCalMultiplier_ = 1.0;
  flybyCableCalSource_ = SgTaskConfig::CCS_DEFAULT;
  cableCalMultiplierDBCal_ = 0.0;

  cableCalsOrigin_ = CCO_UNDEF;
  meteoDataOrigin_ = MDO_UNDEF;
  
  axisOffset_ = 0.0;
  axisOffset_ea_ = 0.0;
  //
  gradNorth_ = 0.0;
  gradEast_  = 0.0;
  tilt_[0] = tilt_[1] = 0.0;
  //
  mntType_ = MT_UNKN;
  latitude_= longitude_= height_ = 0.0;
  tectonicPlateName_ = "NONE";
  cdpNumber_ = -1;

  for (int i=0; i<maxNumOfPolynomials_; i++)
  {
    estClockModel_[i] = 0.0;
    estClockModelSigmas_[i] = 0.0;
    pClocks_[i] = NULL;
  };
  aPrioriClockTerm_0_ = 0.0;
  aPrioriClockTerm_1_ = 0.0;
  need2useAPrioriClocks_ = false;
  //
  estWetZenithDelay_ = 0.0;
  estWetZenithDelaySigma_ = 0.0;
  gradientDelay_ = 0.0;

  for (int iWave=0; iWave<11; iWave++)
    for (int iCoord=0; iCoord<3; iCoord++)
    {
      oLoadAmplitudes_[iWave][iCoord] = 0.0;
      oLoadPhases_[iWave][iCoord] = 0.0;
    };
  for (int i=0; i<6; i++)
    optLoadCoeffs_[i] = 0.0;
  cId_ = ' ';
  sId_[0] = ' ';
  sId_[1] = ' ';
  sId_[2] = '\0';
  //
  // parameters:
  pZenithDelay_ = NULL;
  pAtmGradN_ = NULL;
  pAtmGradE_ = NULL;
  pRx_ = NULL;
  pRy_ = NULL;
  pRz_ = NULL;
  pAxisOffset_ = NULL;
  addAttr(Attr_ESTIMATE_COO);
  addAttr(Attr_ESTIMATE_AXO);
  eccRec_ = NULL;
  isEccNonZero_ = false;
  cccIdx_ = SgVlbiAuxObservation::CCT_DFLT;
};



//
// A destructor:
SgVlbiStationInfo::~SgVlbiStationInfo()
{
  releaseParameters();

  QMap<QString, SgVlbiAuxObservation*>::iterator it;
  for (it=auxObservationByScan_.begin(); it!=auxObservationByScan_.end(); ++it)
    delete it.value();
  auxObservationByScan_.clear();
  
  if (eccRec_)
  {
    delete eccRec_;
    eccRec_ = NULL;
  };

  tsysIfFreqs_.clear();
  tsysIfIds_.clear();
  tsysIfSideBands_.clear();
  tsysIfPolarizations_.clear();
};



//
void SgVlbiStationInfo::createParameters()
{
  QString prefix = "Stn " + getKey().leftJustified(8, ' ') + ": ";
  releaseParameters();
  for (int i=0; i<maxNumOfPolynomials_; i++)
    pClocks_[i] = new SgParameter(prefix + QString("").sprintf("Clock_%02d", i));
  pZenithDelay_ = new SgParameter(prefix + "Zenith");
  pAtmGradN_ = new SgParameter(prefix + "Grad_N");
  pAtmGradE_ = new SgParameter(prefix + "Grad_E");
  pRx_ = new SgParameter(prefix + "coord-X");
  pRy_ = new SgParameter(prefix + "coord-Y");
  pRz_ = new SgParameter(prefix + "coord-Z");
  pAxisOffset_ = new SgParameter(prefix + "Axis Offset");
  clockBreaks_.createParameters(prefix + "CBr");
};



//
void SgVlbiStationInfo::releaseParameters()
{
  for (int i=0; i<maxNumOfPolynomials_; i++)
    if (pClocks_[i])
    {
      delete pClocks_[i];
      pClocks_[i] = NULL;
    };
  if (pZenithDelay_)
  {
    delete pZenithDelay_;
    pZenithDelay_ = NULL;
  };
  if (pAtmGradN_)
  {
    delete pAtmGradN_;
    pAtmGradN_ = NULL;
  };
  if (pAtmGradE_)
  {
    delete pAtmGradE_;
    pAtmGradE_ = NULL;
  };
  if (pRx_)
  {
    delete pRx_;
    pRx_ = NULL;
  };
  if (pRy_)
  {
    delete pRy_;
    pRy_ = NULL;
  };
  if (pRz_)
  {
    delete pRz_;
    pRz_ = NULL;
  };
  if (pAxisOffset_)
  {
    delete pAxisOffset_;
    pAxisOffset_ = NULL;
  };
  clockBreaks_.releaseParameters();
};



//
void SgVlbiStationInfo::calcCBEpochs4Export(const QMap<QString, SgVlbiAuxObservation*>& auxObsByScan)
{
  if (!auxObsByScan.size())
  {
    logger->write(SgLogger::WRN, SgLogger::STATION, className() +
      ": calcCBEpochs4Export(): cannot determine the epoch, the size of auxObs map is zero");
    return;
  };
  for (int i=0; i<clockBreaks_.size(); i++)
  {
    SgParameterBreak*   pb=clockBreaks_.at(i);
    SgMJD               tLeft(tZero), tRight(tInf);
    QMap<QString, SgVlbiAuxObservation*>::const_iterator it;
    for (it=auxObsByScan.begin(); it!=auxObsByScan.end(); ++it)
    {
      SgVlbiAuxObservation*   obs=it.value();
//    if (false || obs->isAttr(SgVlbiAuxObservation::Attr_PROCESSED))//ok, right now we use only good obs
      if (true || obs->isAttr(SgVlbiAuxObservation::Attr_PROCESSED)) // use all obs
      {
        if (*obs<*pb && tLeft<*obs)
          tLeft = *obs;
        if (*pb<*obs && *obs<tRight)
          tRight = *obs;
      };
    };
    if (tZero<tLeft && tRight<tInf)
    {
      // calculating mean epoch:
      SgMJD     exportEpoch(tLeft);
      exportEpoch += 0.5*(tRight - tLeft);
      // round it to minutes:
      double d=exportEpoch.getTime();
      d = round(d*24.0*60.0)/24.0/60.0;
      exportEpoch.setTime(d);
      // setup epoch for export:
      pb->setEpoch4Export(exportEpoch);
      logger->write(SgLogger::DBG, SgLogger::STATION, className() +
        ": calcCBEpochs4Export(): station " + name() + ": set up clock break epoch for export: " +
        exportEpoch.toString());
    }
    else
    {
      pb->setEpoch4Export(*pb);
      logger->write(SgLogger::WRN, SgLogger::STATION, className() +
        ": calcCBEpochs4Export(): cannot determine clock break epoch for export: tLeft: " +
        tLeft.toString(SgMJD::F_YYYYMMDDHHMMSSSS) + ", tRight: " +
        tRight.toString(SgMJD::F_YYYYMMDDHHMMSSSS));
    };
  };
};

/*
Sg3dVector SgVlbiStationInfo::r(const SgMJD& t, SgTaskConfig* cfg)
{
  Sg3dVector    r(r_);
  if (cfg->getUseExtAPrioriSitesPositions())
    r = r_ea_ + v_ea_*(t - t0_);
  return r;
};
*/



//
void SgVlbiStationInfo::adjustEccVector()
{
  isEccNonZero_ = true;
  if (eccRec_->getEccType()==SgEccRec::ET_XYZ)
    v3Ecc_ = eccRec_->getDR();
  else if (eccRec_->getEccType()==SgEccRec::ET_NEU)
  {
    Sg3dMatrixR         R_e(EAST), R_n(NORTH);
    Sg3dMatrix          m3M=R_n(-longitude_)*R_e(latitude_);
    v3Ecc_ = m3M*eccRec_->getDR();
  };
};



//
void SgVlbiStationInfo::checkAuxObs(const QMap<QString, QString>& scanNameById)
{
  if (!auxObservationByScan_.size())
  {
    logger->write(SgLogger::WRN, SgLogger::STATION, className() +
        ": checkAuxObs(): station " + name() + ": the size of aux.obs's container is zero");
    return;
  };
  QMap<SgMJD, QString>          scanByTime;
  QMap<QString, SgVlbiAuxObservation*>::iterator
                                it=auxObservationByScan_.begin();
  //
  while (it != auxObservationByScan_.end())
  {
    // check for the "same epoch" case:
    SgVlbiAuxObservation       *auxObs=it.value();
    if (!scanByTime.contains(*auxObs))
      scanByTime.insert(*auxObs, it.key());
    else
    {
      QString                   scanName=scanByTime.value(*auxObs);
      logger->write(SgLogger::WRN, SgLogger::STATION, className() +
        ": WARNING: the station " + name() + ": contains different observations at the same epoch: " +
        auxObs->toString() + "; the scans: " + scanName + " and " + it.key(), true);
    };
    // remove unused data:
    if (!scanNameById.contains(it.key()))
    {
      logger->write(SgLogger::INF, SgLogger::STATION, className() +
        ": the scan " + it.key() + " was removed from the list of known scans of the station " + 
        name());
      it = auxObservationByScan_.erase(it);
    }
    else
      ++it;
  };
/*
  for (; it!=auxObservationByScan_.end(); ++it)
  {
    SgVlbiAuxObservation       *auxObs=it.value();
    if (!scanByTime.contains(*auxObs))
      scanByTime.insert(*auxObs, it.key());
    else
    {
      QString                   scanName=scanByTime.value(*auxObs);
      logger->write(SgLogger::WRN, SgLogger::STATION, className() +
        ": WARNING: the station " + getKey() + ": contains different observations at the same epoch: " +
        auxObs->toString() + "; the scans: " + scanName + " and " + it.key(), true);
    };
  };
*/
};



//
bool SgVlbiStationInfo::saveIntermediateResults(QDataStream& s) const
{
  SgObjectInfo::saveIntermediateResults(s);
  
  s << clocksModelOrder_ << estWetZenithDelay_ << estWetZenithDelaySigma_
    << aPrioriClockTerm_0_ << aPrioriClockTerm_1_ << need2useAPrioriClocks_
    << (unsigned int)flybyCableCalSource_;

  if (s.status() != QDataStream::Ok)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": saveIntermediateResults(): error writting data");
    return false;
  };
  //
  for (int i=0; i<clocksModelOrder_; i++)
    if (s.status() == QDataStream::Ok)
      s << estClockModel_[i] << estClockModelSigmas_[i];
    else
    {
      logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
        ": saveIntermediateResults(): error writting data idx#" + QString("").setNum(i));
      return false;
    };
  //
  if (s.status() == QDataStream::Ok)
    clockBreaks_.saveIntermediateResults(s);

  if (!pcClocks_.saveIntermediateResults(s))
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": saveIntermediateResults(): error writting data for the local clock parameter setup");
    return false;
  };
  if (!pcZenith_.saveIntermediateResults(s))
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": saveIntermediateResults(): error writting data for the local zenith parameter setup");
    return false;
  };
  //
  return s.status() == QDataStream::Ok;
};



//
bool SgVlbiStationInfo::loadIntermediateResults(QDataStream& s)
{
  int                           n;
  double                        v, e, a_0, a_1;
  bool                          is;
  unsigned int                  ccs;

  SgObjectInfo::loadIntermediateResults(s);

  s >> n >> v >> e >> a_0 >> a_1 >> is >> ccs;

  if (s.status() != QDataStream::Ok)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": loadIntermediateResults(): error reading data: " +
      (s.status()==QDataStream::ReadPastEnd?"read past end of the file":"read corrupt data"));
    return false;
  };
  clocksModelOrder_ = n;
  estWetZenithDelay_ = v;
  estWetZenithDelaySigma_ = e;

  aPrioriClockTerm_0_ = a_0;
  aPrioriClockTerm_1_ = a_1;
  need2useAPrioriClocks_ = is;
  flybyCableCalSource_ = (SgTaskConfig::CableCalSource)ccs;
  //
  //
  for (int i=0; i<clocksModelOrder_; i++)
  {
    s >> v >> e;
    if (s.status() == QDataStream::Ok)
    {
      estClockModel_[i] = v;
      estClockModelSigmas_[i] = e;
    }
    else
    {
      logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
        ": loadIntermediateResults(): error reading data idx#" + QString("").setNum(i) + ": " + 
        (s.status()==QDataStream::ReadPastEnd?"read past end of the file":"read corrupt data"));
      return false;
    };
  };
  //
  clockBreaks_.loadIntermediateResults(s);
  if (s.status() != QDataStream::Ok)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": loadIntermediateResults(): error reading clock breaks data: " +
      (s.status()==QDataStream::ReadPastEnd?"read past end of the file":"read corrupt data"));
    return false;
  };
  //
  pcClocks_.loadIntermediateResults(s);
  if (s.status() != QDataStream::Ok)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": loadIntermediateResults(): error reading local clock parameter setup: " +
      (s.status()==QDataStream::ReadPastEnd?"read past end of the file":"read corrupt data"));
    return false;
  };
  //
  pcZenith_.loadIntermediateResults(s);
  if (s.status() != QDataStream::Ok)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": loadIntermediateResults(): error reading local zenith parameter setup: " +
      (s.status()==QDataStream::ReadPastEnd?"read past end of the file":"read corrupt data"));
    return false;
  };
  //
  return s.status()==QDataStream::Ok;
};



//
void SgVlbiStationInfo::resetCable()
{
  for (QMap<QString, SgVlbiAuxObservation*>::iterator it=auxObservationByScan_.begin(); 
    it!=auxObservationByScan_.end(); ++it)
    it.value()->resetCable();
  // clear the attributes too:
  delAttr(Attr_HAS_CABLE_CAL);
};



//
void SgVlbiStationInfo::resetMeteo()
{
  for (QMap<QString, SgVlbiAuxObservation*>::iterator it=auxObservationByScan_.begin(); 
    it!=auxObservationByScan_.end(); ++it)
    it.value()->resetMeteo();
  // clear the attributes too:
  delAttr(Attr_HAS_METEO);
};



//
void SgVlbiStationInfo::resetTsys()
{
  for (QMap<QString, SgVlbiAuxObservation*>::iterator it=auxObservationByScan_.begin(); 
    it!=auxObservationByScan_.end(); ++it)
    it.value()->resetTsys();
  // clear the attributes too:
  delAttr(Attr_HAS_TSYS);
};



//
void SgVlbiStationInfo::checkPresenceOfCableCalibratioCorrections()
{
  delAttr(Attr_HAS_CCC_FSLG);
  delAttr(Attr_HAS_CCC_CDMS);
  delAttr(Attr_HAS_CCC_PCMT);
  bool                          hasFslg, hasCdms, hasPcmt;
  hasFslg = hasCdms = hasPcmt = false;
  //
  for (QMap<QString, SgVlbiAuxObservation*>::iterator it=auxObservationByScan_.begin();
    it!=auxObservationByScan_.end(); ++it)
  {
    SgVlbiAuxObservation*       aux=it.value();
    if (!hasFslg && aux->cableCorrections().getElement(SgVlbiAuxObservation::CCT_FSLG) != 0.0)
      hasFslg = true;
    if (!hasCdms && aux->cableCorrections().getElement(SgVlbiAuxObservation::CCT_CDMS) != 0.0)
      hasCdms = true;
    if (!hasPcmt && aux->cableCorrections().getElement(SgVlbiAuxObservation::CCT_PCMT) != 0.0)
      hasPcmt = true;
  };
  //
  if (hasFslg)
    addAttr(Attr_HAS_CCC_FSLG);
  if (hasCdms)
    addAttr(Attr_HAS_CCC_CDMS);
  if (hasPcmt)
    addAttr(Attr_HAS_CCC_PCMT);
};



//
void SgVlbiStationInfo::prepare2Run(SgTaskConfig::CableCalSource src)
{
  if (flybyCableCalSource_ != SgTaskConfig::CCS_DEFAULT)
    src = flybyCableCalSource_;
  switch (src)
  {
  case SgTaskConfig::CCS_FSLG:
    cccIdx_ = isAttr(Attr_HAS_CCC_FSLG)?SgVlbiAuxObservation::CCT_FSLG:SgVlbiAuxObservation::CCT_DFLT;
    break;
  case SgTaskConfig::CCS_CDMS:
    cccIdx_ = isAttr(Attr_HAS_CCC_CDMS)?SgVlbiAuxObservation::CCT_CDMS:SgVlbiAuxObservation::CCT_DFLT;
    break;
  case SgTaskConfig::CCS_PCMT:
    cccIdx_ = isAttr(Attr_HAS_CCC_PCMT)?SgVlbiAuxObservation::CCT_PCMT:SgVlbiAuxObservation::CCT_DFLT;
    break;
  case SgTaskConfig::CCS_DEFAULT:
  default:
    cccIdx_ = SgVlbiAuxObservation::CCT_DFLT;
  };
  logger->write(SgLogger::DBG, SgLogger::IO_BIN, className() +
    ": prepare2Run(): station " + name() + ": ccc index of cable calibration corrections is set to " +
    QString("").setNum(cccIdx_));
};
/*=====================================================================================================*/




/*=====================================================================================================*/
//
//                           FRIENDS:
// 
/*=====================================================================================================*/
//




/*=====================================================================================================*/
//
// aux functions:
//



/*=====================================================================================================*/
//
// constants:
//












/*=====================================================================================================*/
