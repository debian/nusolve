/*
 *
 *    This file is a part of Space Geodetic Library. The library is used by
 *    nuSolve, a part of CALC/SOLVE system, and designed to make analysis of
 *    geodetic VLBI observations.
 *    Copyright (C) 2010-2020 Sergei Bolotin.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <SgConstants.h>
#include <SgMJD.h>


/*=====================================================================================================*/
//
// constants:
//

const double                    vLight = 299792458.0; // m/s



/*=====================================================================================================*/
// this is an unofficial translation on C of the subroutine "DPSIDEPS1980_DXDY2000(dmjd,dX,dY,dpsi,deps)"
// coded by Christian Bizouard and available at http://hpiers.obspm.fr/iers/models/uai2000.package
//
void calcCip2IAU1980(const SgMJD& epoch, double dX, double dY, double dPsi_1980, double dEps_1980,
  double dPsi_2000, double dEps_2000, double& diffPsi, double& diffEps)
{
  double const                  sineps0(0.3977771559319137), coseps0(0.9174820620691818);
  double                        dPsi_model, dEps_model, dt, dt2, dt3, psi_A, chi_A, f;
  dPsi_model = (dPsi_2000 - dPsi_1980)*RAD2MAS;
  dEps_model = (dEps_2000 - dEps_1980)*RAD2MAS;
  dt = (epoch - tEphem)/36525.0;
  dt2 = dt*dt;
  dt3 = dt2*dt;

  // ! Luni-solar precession
  psi_A = (5038.47875*dt - 1.07259*dt2 -0.001147*dt3)*SEC2RAD;
  // ! Planetary precession
  chi_A = (  10.5526 *dt - 2.38064*dt2 -0.001125*dt3)*SEC2RAD;
  f = (psi_A*coseps0 - chi_A)*(psi_A*coseps0 - chi_A);

  // ! dpsi1980 / deps1980
  diffPsi = (-dX + (psi_A*coseps0 - chi_A)*dY)/(-f*sineps0 - sineps0) + dPsi_model;
  diffEps = (-(psi_A*coseps0 - chi_A)*sineps0*dX - sineps0*dY)/(-f*sineps0 - sineps0) + dEps_model;

  diffPsi+= (-2.9965)*dt*100.0; // values of Herring / UAI 2000
  diffEps+= (-0.2524)*dt*100.0; // values of Herring / UAI 2000

  diffPsi+= - 41.7750 + 40.0e-3; // values of Herring / UAI 2000 corrigees
  diffEps+= -  6.8192 - 40.0e-3; // values of Herring / UAI 2000 corrigees
  diffPsi/= RAD2MAS;
  diffEps/= RAD2MAS;
};
/*=====================================================================================================*/

