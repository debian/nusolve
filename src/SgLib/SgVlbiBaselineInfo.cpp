/*
 *
 *    This file is a part of Space Geodetic Library. The library is used by
 *    nuSolve, a part of CALC/SOLVE system, and designed to make analysis of
 *    geodetic VLBI observations.
 *    Copyright (C) 2010-2020 Sergei Bolotin.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <stdlib.h>


#include <QtCore/QDataStream>


#include <SgVlbiBaselineInfo.h>
#include <SgVlbiObservation.h>
#include <SgVlbiObservable.h>

#include <SgLogger.h>



/*=======================================================================================================
*
*                           METHODS:
* 
*======================================================================================================*/
//
// static first:
const QString SgVlbiBaselineInfo::className()
{
  return "SgVlbiBaselineInfo";
};



//
void SgVlbiBaselineInfo::createParameters()
{
  QString prefix = "Bln: " + getKey() + ": ";
  releaseParameters();
  pClock_ = new SgParameter(prefix + "Clock_0");
  pBx_ = new SgParameter(prefix + "coord-X");
  pBy_ = new SgParameter(prefix + "coord-Y");
  pBz_ = new SgParameter(prefix + "coord-Z");
  dClock_ = 0.0;
  dClockSigma_ = 0.0;
};



//
void SgVlbiBaselineInfo::releaseParameters()
{
  if (pClock_)
  {
    if (pClock_->isAttr(SgParameter::Attr_IS_IN_RUN))
    {
      dClock_ = pClock_->getSolution();
      dClockSigma_ = pClock_->getSigma();
    };
    delete pClock_;
    pClock_ = NULL;
  };
  if (pBx_)
  {
    delete pBx_;
    pBx_ = NULL;
  };
  if (pBy_)
  {
    delete pBy_;
    pBy_ = NULL;
  };
  if (pBz_)
  {
    delete pBz_;
    pBz_ = NULL;
  };
};



//
bool SgVlbiBaselineInfo::selfCheck()
{
  bool                          isOk=true;
  int                           num=0, count=0;
  numOfChanByCount_.clear();
  grdAmbigsBySpacing_.clear();
  phdAmbigsBySpacing_.clear();
  //
  // set up "typical" group delay ambiguity spacing for the band:
  for (int i=0; i<observables_.size(); i++)
  {
    grdAmbigsBySpacing_[observables_.at(i)->grDelay().getAmbiguitySpacing()]++;
    phdAmbigsBySpacing_[observables_.at(i)->phDelay().getAmbiguitySpacing()]++;
    numOfChanByCount_[observables_.at(i)->getNumOfChannels()]++;
  };
  // group delay:
  if (grdAmbigsBySpacing_.size() == 1)
  {
    typicalGrdAmbigSpacing_ = grdAmbigsBySpacing_.begin().key();
    strGrdAmbigsStat_.sprintf("%.1f", typicalGrdAmbigSpacing_*1.0e9);
  }
  else
  {
    strGrdAmbigsStat_ = "";
    num = observables_.size();
    count = 0;
    for (QMap<double, int>::iterator it=grdAmbigsBySpacing_.begin(); it!=grdAmbigsBySpacing_.end(); ++it)
    {
      strGrdAmbigsStat_ += QString("").sprintf("%.1f (%.1f%%), ", it.key()*1.0e9, it.value()*100.0/num);
      if (count < it.value())
      {
        count = it.value();
        typicalGrdAmbigSpacing_ = it.key();
      };
    };
    strGrdAmbigsStat_ = strGrdAmbigsStat_.left(strGrdAmbigsStat_.size() - 2);
  };
  // phase dleay:
  if (phdAmbigsBySpacing_.size() == 1)
  {
    typicalPhdAmbigSpacing_ = phdAmbigsBySpacing_.begin().key();
    strPhdAmbigsStat_.sprintf("%.3f", typicalPhdAmbigSpacing_*1.0e9);
  }
  else
  {
    strPhdAmbigsStat_ = "";
    num = observables_.size();
    count = 0;
    for (QMap<double, int>::iterator it=phdAmbigsBySpacing_.begin(); it!=phdAmbigsBySpacing_.end(); ++it)
    {
      strPhdAmbigsStat_ += QString("").sprintf("%.3f (%.1f%%), ", it.key()*1.0e9, it.value()*100.0/num);
      if (count < it.value())
      {
        count = it.value();
        typicalPhdAmbigSpacing_ = it.key();
      };
    };
    strPhdAmbigsStat_ = strPhdAmbigsStat_.left(strPhdAmbigsStat_.size() - 2);
  };
  //
  // num of channels:
  if (numOfChanByCount_.size() == 1)
    typicalNumOfCannels_ = numOfChanByCount_.begin().key();
  else
  {
    count = 0;
    for (QMap<int, int>::iterator it=numOfChanByCount_.begin(); it!=numOfChanByCount_.end(); ++it)
      if (count < it.value())
      {
        count = it.value();
        typicalNumOfCannels_ = it.key();
      };
  };
  //
  return isOk;
};



//
void SgVlbiBaselineInfo::calculateClockF1(SgTaskConfig*)
{
  const int                   M0=4;
  const double                dS=0.2E-9; // 0.2ns
  QList<SgVlbiObservable*>    obsList;
  SgVlbiObservable           *obs=NULL;

  // first, clear the flag, then, pick up processed observations:
  for (int i=0; i<observables_.size(); i++)
  {
    SgVlbiObservable *obs = observables_.at(i);
    obs->owner()->delAttr(SgVlbiObservation::Attr_PASSED_CL_FN1);
    if (obs->owner()->isAttr(SgVlbiObservation::Attr_PROCESSED))
      obsList.append(obs);
  };

  if (obsList.size()<2*M0)
    return;
  

  double      x, y, w;
  double      A_l, B_l, C_l, P_l, Q_l, R_l;
  double      a_l, b_l, wrms_l;
  double      A_r, B_r, C_r, P_r, Q_r, R_r;
  double      a_r, b_r, wrms_r;
  x = y = w = 0.0;
  A_l = B_l = C_l = P_l = Q_l = R_l = 0.0;
  a_l = b_l = wrms_l = 0.0;
  A_r = B_r = C_r = P_r = Q_r = R_r = 0.0;
  a_r = b_r = wrms_r = 0.0;

  // calc whole wrms residuals:
  for (int i=0; i<obsList.size(); i++)
  {
    obs = obsList.at(i);
    x = obs->epoch().toDouble();
    y = obs->activeDelay()->getResidual();
    w = 1.0/(fabs(obs->activeDelay()->getSigma()) + dS)/
            (fabs(obs->activeDelay()->getSigma()) + dS);
    A_l += w;
    B_l += w*x;
    C_l += w*x*x;
    P_l += w*y;
    Q_l += w*x*y;
    R_l += w*y*y;
  };
  b_l = (P_l*B_l - A_l*Q_l)/(B_l*B_l - A_l*C_l);
  a_l = (Q_l - C_l*b_l)/B_l;
  cbd_total_wrms_ = sqrt(fabs((R_l + a_l*a_l*A_l + b_l*b_l*C_l +
                                2.0*(a_l*b_l*B_l - a_l*P_l - b_l*Q_l))/A_l));

  A_l = B_l = C_l = P_l = Q_l = R_l = 0.0;
  a_l = b_l = 0.0;
  // initial evaluating:
  for (int i=0; i<M0; i++)
  {
    obs = obsList.at(i);
    x = obs->epoch().toDouble();
    y = obs->activeDelay()->getResidual();
    w = 1.0/(fabs(obs->activeDelay()->getSigma()) + dS)/
            (fabs(obs->activeDelay()->getSigma()) + dS);
    A_l += w;
    B_l += w*x;
    C_l += w*x*x;
    P_l += w*y;
    Q_l += w*x*y;
    R_l += w*y*y;
  };
  for (int i=M0; i<obsList.size(); i++)
  {
    obs = obsList.at(i);
    x = obs->epoch().toDouble();
    y = obs->activeDelay()->getResidual();
    w = 1.0/(fabs(obs->activeDelay()->getSigma()) + dS)/
            (fabs(obs->activeDelay()->getSigma()) + dS);
    A_r += w;
    B_r += w*x;
    C_r += w*x*x;
    P_r += w*y;
    Q_r += w*x*y;
    R_r += w*y*y;
  };
  b_l = (P_l*B_l - A_l*Q_l)/(B_l*B_l - A_l*C_l);
  a_l = (Q_l - C_l*b_l)/B_l;
  wrms_l = (R_l + a_l*a_l*A_l + b_l*b_l*C_l + 2.0*(a_l*b_l*B_l - a_l*P_l - b_l*Q_l))/A_l;
  b_r = (P_r*B_r - A_r*Q_r)/(B_r*B_r - A_r*C_r);
  a_r = (Q_r - C_r*b_r)/B_r;
  wrms_r = (R_r + a_r*a_r*A_r + b_r*b_r*C_r + 2.0*(a_r*b_r*B_r - a_r*P_r - b_r*Q_r))/A_r;
//  obsList.at(M0-1)->setBaselineClock_F1 ((sqrt(fabs(wrms_l)) + sqrt(fabs(wrms_r)))/cbd_total_wrms_);
  obsList.at(M0-1)->owner()->setBaselineClock_F1 (sqrt(fabs(wrms_l)) + sqrt(fabs(wrms_r)));
  obsList.at(M0-1)->owner()->setBaselineClock_F1l(sqrt(fabs(wrms_l)));
  obsList.at(M0-1)->owner()->setBaselineClock_F1r(sqrt(fabs(wrms_r)));
//  obsList.at(M0-1)->setBaselineClock_F1(sqrt(fabs(wrms_l + wrms_r)));
  obsList.at(M0-1)->owner()->addAttr(SgVlbiObservation::Attr_PASSED_CL_FN1);
  
  for (int i=M0; i<obsList.size()-M0+1; i++)
  {
    obs = obsList.at(i);
    x = obs->epoch().toDouble();
    y = obs->activeDelay()->getResidual();
    w = 1.0/(fabs(obs->activeDelay()->getSigma()) + dS)/
            (fabs(obs->activeDelay()->getSigma()) + dS);

    A_l += w;
    B_l += w*x;
    C_l += w*x*x;
    P_l += w*y;
    Q_l += w*x*y;
    R_l += w*y*y;

    A_r -= w;
    B_r -= w*x;
    C_r -= w*x*x;
    P_r -= w*y;
    Q_r -= w*x*y;
    R_r -= w*y*y;

    b_l = (P_l*B_l - A_l*Q_l)/(B_l*B_l - A_l*C_l);
    a_l = (Q_l - C_l*b_l)/B_l;
    wrms_l = (R_l + a_l*a_l*A_l + b_l*b_l*C_l + 2.0*(a_l*b_l*B_l - a_l*P_l - b_l*Q_l))/A_l;
    b_r = (P_r*B_r - A_r*Q_r)/(B_r*B_r - A_r*C_r);
    a_r = (Q_r - C_r*b_r)/B_r;
    wrms_r = (R_r + a_r*a_r*A_r + b_r*b_r*C_r + 2.0*(a_r*b_r*B_r - a_r*P_r - b_r*Q_r))/A_r;
//    obsList.at(i)->setBaselineClock_F1 ((sqrt(fabs(wrms_l)) + sqrt(fabs(wrms_r)))/cbd_total_wrms_);
//    obsList.at(i)->setBaselineClock_F1 (sqrt(fabs(wrms_l)) + sqrt(fabs(wrms_r)));
    obs->owner()->setBaselineClock_F1(sqrt(fabs(wrms_l + wrms_r)));
    obs->owner()->setBaselineClock_F1l(sqrt(fabs(wrms_l)));
    obs->owner()->setBaselineClock_F1r(sqrt(fabs(wrms_r)));
    obs->owner()->addAttr(SgVlbiObservation::Attr_PASSED_CL_FN1);
  };

  obsList.clear();
};



//
void SgVlbiBaselineInfo::evaluateCBIndicator()
{
  QList<SgVlbiObservable*>    obsList;
  SgVlbiObservable           *obs=NULL;

  if (!cbIndicator_)
    cbIndicator_ = new SgClockBreakIndicator;
  cbIndicator_->totWrms_ = cbd_total_wrms_;

  // first, clear the flag, then, pick up processed observations:
  for (int i=0; i<observables_.size(); i++)
  {
    obs = observables_.at(i);
    if (obs->owner()->isAttr(SgVlbiObservation::Attr_PROCESSED) &&
        obs->owner()->isAttr(SgVlbiObservation::Attr_PASSED_CL_FN1))
      obsList.append(obs);
  };
  if (obsList.size()<4)
  {
    delete cbIndicator_;
    cbIndicator_ = NULL;
    return;
  };
  //
  double                      minWrms, maxWrms;
  int                         minIdx = -1;
  minWrms = maxWrms = obsList.at(0)->owner()->getBaselineClock_F1();
  for (int i=0; i<obsList.size(); i++)
  {
    obs = obsList.at(i);
    if (minWrms > obs->owner()->getBaselineClock_F1())
    {
      minWrms = obs->owner()->getBaselineClock_F1();
      minIdx = i;
    };
    if (maxWrms < obs->owner()->getBaselineClock_F1())
      maxWrms = obs->owner()->getBaselineClock_F1();
  };
  if (maxWrms==minWrms)
  {
    delete cbIndicator_;
    cbIndicator_ = NULL;
    return;
  };
  if (minIdx==-1)
  {
    delete cbIndicator_;
    cbIndicator_ = NULL;
    return;
  };
  if (minIdx<2)
  {
    delete cbIndicator_;
    cbIndicator_ = NULL;
    return;
  };
  if (minIdx>obsList.size()-3)
  {
    delete cbIndicator_;
    cbIndicator_ = NULL;
    return;
  };
  //
  // ok:
  cbIndicator_->epoch_ = obsList.at(minIdx)->epoch();
  cbIndicator_->minWrms_ = minWrms;
  int                         i, n;
  // right wing:
  n = 0;
  i = minIdx + 1;
  while (i<obsList.size() && 
    obsList.at(i-1)->owner()->getBaselineClock_F1()<=obsList.at(i)->owner()->getBaselineClock_F1())
  {
    n++;
    i++;
  };
  cbIndicator_->numOnRight_ = n;
  cbIndicator_->tOnRight_ = obsList.at(minIdx+n)->epoch();
  // left wing:
  n = 0;
  i = minIdx - 1;
  while (i>0 && 
    obsList.at(i)->owner()->getBaselineClock_F1()<=obsList.at(i-1)->owner()->getBaselineClock_F1())
  {
    n++;
    i--;
  };
  cbIndicator_->numOnLeft_ = n;
  cbIndicator_->tOnLeft_ = obsList.at(minIdx-n)->epoch();
  obsList.clear();
};



//
void SgVlbiBaselineInfo::scan4Ambiguities()
{
  scan4Ambiguities_m1();
//  scan4Ambiguities_m2();
};



//
void SgVlbiBaselineInfo::scan4Ambiguities_m1()
{
  QList<SgVlbiObservable*>      oList;
  SgVlbiObservable             *o=NULL;

  // first, clear the flag, then, pick up processed observations:
  for (int i=0; i<observables_.size(); i++)
  {
    o = observables_.at(i);
    if (o->owner()->isAttr(SgVlbiObservation::Attr_PROCESSED))
      oList.append(o);
  };
  if (!oList.size())
  {
    logger->write(SgLogger::ERR, SgLogger::RUN, className() +
      ": scan4Ambiguities(): the number of processed observations at the baseline [" +
      getKey() + "] is zero; skipping");
    oList.clear();
    return;
  };
  //
  int                           num=oList.size(), numUp=0, numDn=0;
  for (int i=0; i<num; i++)
  {
    o = oList.at(i);
    o->activeDelay()->adjustAmbiguity();
    if (o->activeDelay()->getResidual() > 0.0)
      numUp++;
    if (o->activeDelay()->getResidual() < 0.0)
      numDn++;
  };
  bool                          isUp=numUp>numDn;
  int                           n=std::max(numUp, numDn);
  if (n<4)
  {
    logger->write(SgLogger::WRN, SgLogger::RUN, className() +
      ": scan4Ambiguities(): the number of observations in the good stratum at the baseline [" +
      getKey() + "] less than 4; applying simple correction");
    for (int i=0; i<oList.size()-1; i++)
    {
      double                    closestResidual=oList.at(i)->activeDelay()->getResidual();
      o = oList.at(i+1);
      o->activeDelay()->adjustAmbiguity(closestResidual);
    };
    oList.clear();
    return;
  };
  SgVector                      x(n), y(n), e(n);
  SgMJD                         t0 = (*oList.at(oList.size()-1)->owner() - *oList.at(0)->owner())/2.0;
  int                           j=0;
  for (int i=0; i<num; i++)
  {
    o = oList.at(i);
    if ((isUp && o->activeDelay()->getResidual()>0.0) ||
        (!isUp && o->activeDelay()->getResidual()<0.0)  )
    {
      x(j) = *o->owner() - t0;
      y(j) = o->activeDelay()->getResidual();
      e(j) = o->activeDelay()->getSigma() + 10.0E-12;
      j++;
    };
  };
  // determine mean shift:
  double                        sA=0.0, sB=0.0, sC=0.0, closestResidual, d;
  for (int i=0; i<n; i++)
  {
    sA += 1.0/e.getElement(i)/e.getElement(i);
    sB += (d=y.getElement(i)/e.getElement(i))/e.getElement(i);
    sC += d*d;
  };
  closestResidual = sB/sA;

  oList.clear();
  // reprocess residuals:
  //
  for (int i=0; i<observables_.size(); i++)
  {
    o = observables_.at(i);
    o->activeDelay()->adjustAmbiguity(closestResidual);
  };
};



//
void SgVlbiBaselineInfo::scan4Ambiguities_m2()
{
  QList<SgVlbiObservable*>      obsList;
  SgVlbiObservable             *o=NULL;

  // first, clear the flag, then, pick up processed observations:
  for (int i=0; i<observables_.size(); i++)
  {
    o = observables_.at(i);
    if (o->owner()->isAttr(SgVlbiObservation::Attr_PROCESSED))
      obsList.append(o);
  };
  if (!obsList.size())
  {
    logger->write(SgLogger::DBG, SgLogger::RUN, className() +
      ": scan4Ambiguities(): the number of processed observations at the baseline [" +
      getKey() + "] is zero; skipping");
    obsList.clear();
    return;
  };
  //
  int                           nAvg=5;
  double                        closestResidual;
  if (obsList.size() <= nAvg)
  {
    obsList.at(0)->activeDelay()->adjustAmbiguity();
    closestResidual = obsList.at(0)->activeDelay()->getResidual();
    for (int i=0; i<obsList.size()-1; i++)
    {
      obsList.at(i+1)->activeDelay()->adjustAmbiguity(closestResidual);
      closestResidual = obsList.at(i+1)->activeDelay()->getResidual();
    };
  }
  else
  {
    closestResidual = 0.0;
    for (int i=0; i<nAvg; i++)
      closestResidual += obsList.at(i)->activeDelay()->getResidual();
    closestResidual /= nAvg;
    for (int i=0; i<nAvg; i++)
      obsList.at(i)->activeDelay()->adjustAmbiguity(closestResidual);
    for (int i=nAvg; i<obsList.size(); i++)
    {
      closestResidual = 0.0;
      for (int j=0; j<nAvg; j++)
        closestResidual += obsList.at(i + j - nAvg)->activeDelay()->getResidual();
      closestResidual /= nAvg;

      obsList.at(i)->activeDelay()->adjustAmbiguity(closestResidual);
    };
  };
  return;



  int                           crIdx=0;
  closestResidual = fabs(obsList.at(crIdx)->activeDelay()->getResidual());
  for (int i=0; i<obsList.size(); i++)
  {
    o = obsList.at(i);
    if (fabs(o->activeDelay()->getResidual()) < closestResidual)
    {
      closestResidual = fabs(o->activeDelay()->getResidual());
      crIdx = i;
    };
  };
  closestResidual = obsList.at(crIdx)->activeDelay()->getResidual();
  // adjust the closest residual:
  obsList.at(crIdx)->activeDelay()->adjustAmbiguity(closestResidual);
  closestResidual = obsList.at(crIdx)->activeDelay()->getResidual();
  // process other residuals:
  for (int i=crIdx; i>0; i--)
  {
    closestResidual = obsList.at(i)->activeDelay()->getResidual();
    obsList.at(i-1)->activeDelay()->adjustAmbiguity(closestResidual);
  };
  for (int i=crIdx; i<obsList.size()-1; i++)
  {
    closestResidual = obsList.at(i)->activeDelay()->getResidual();
    o = obsList.at(i + 1);
    obsList.at(i+1)->activeDelay()->adjustAmbiguity(closestResidual);
  };
  obsList.clear();
};



//
void SgVlbiBaselineInfo::evaluateMeanGrDelResiduals()
{
  QList<SgVlbiObservable*>      oList;
  SgVlbiObservable             *o=NULL;
  //
  // first, clear the flag, then, pick up processed observations:
  for (int i=0; i<observables_.size(); i++)
  {
    o = observables_.at(i);
    if (o->owner()->isAttr(SgVlbiObservation::Attr_PROCESSED))
      oList.append(o);
  };
  if (oList.size() == 0)
  {
    logger->write(SgLogger::DBG, SgLogger::RUN, className() +
      ": evaluateMeanGrDelResiduals(): the number of processed observations at the baseline [" +
      getKey() + "] is zero; skipping");
    oList.clear();
    return;
  };
  //
  double                        sA=0.0, sB=0.0, sC=0.0;
  double                        y, w;
  for (int i=0; i<oList.size(); i++)
  {
    o = oList.at(i);
    y = o->activeDelay()->getResidual();
    w = o->activeDelay()->getSigma() + 5.0E-12;
    w = 1.0/w/w;
    sA += w;
    sB += w*y;
    sC += w*y*y;
  };
  meanGrDelResiduals_     = sB/sA;
  meanGrDelResidualsSigma_= (sC - sB)/sA;
  //
  oList.clear();
};



//
void SgVlbiBaselineInfo::shiftAmbiguities(int deltaN)
{
  if (deltaN==0 || observables_.size()==0)
    return; // nothing to do
  
  SgVlbiObservable             *o=observables_.at(0);
  double                        ambigSpacing=typicalGrdAmbigSpacing_, shift;
  if (o->activeDelay()->getDelayType() == SgTaskConfig::VD_PHS_DELAY)
    ambigSpacing = typicalPhdAmbigSpacing_;
  shift = deltaN*ambigSpacing;
  int                           n;
  for (int i=0; i<observables_.size(); i++)
  {
    o = observables_.at(i);
    n = rint(shift/o->activeDelay()->getAmbiguitySpacing());
    o->activeDelay()->setResidual(o->activeDelay()->getResidual() + 
      n*o->activeDelay()->getAmbiguitySpacing());
    o->activeDelay()->setNumOfAmbiguities(o->activeDelay()->getNumOfAmbiguities() + n);
  };
};



//
SgVlbiStationInfo* SgVlbiBaselineInfo::stn_1(QMap<QString, SgVlbiStationInfo*> stations) const
{
  SgVlbiStationInfo            *stn=NULL;
  QString                       name(getKey().left(8));
  if (stations.contains(name))
    stn = stations.value(name);
  return stn;
};



//
SgVlbiStationInfo* SgVlbiBaselineInfo::stn_2(QMap<QString, SgVlbiStationInfo*> stations) const
{
  SgVlbiStationInfo            *stn=NULL;
  QString                       name(getKey().right(8));
  if (stations.contains(name))
    stn = stations.value(name);
  return stn;
};



//
void SgVlbiBaselineInfo::setGrdAmbiguities2min()
{
  QList<SgVlbiObservable*>      oList;
  SgVlbiObservable             *o=NULL;

  // first, clear the flag, then, pick up processed observations:
  for (int i=0; i<observables_.size(); i++)
  {
    o = observables_.at(i);
    if (o->owner()->isAttr(SgVlbiObservation::Attr_PROCESSED))
      oList.append(o);
  };
  if (!oList.size())
  {
    logger->write(SgLogger::ERR, SgLogger::RUN, className() +
      ": setAmbiguities2min(): the number of processed observations at the baseline [" +
      getKey() + "] is zero; skipping");
    oList.clear();
    return;
  };
  //
  int                         num=oList.size(), meanAmbig;
  double                      sum=0.0;
  for (int i=0; i<num; i++)
    sum += oList.at(i)->activeDelay()->getNumOfAmbiguities();
  meanAmbig = round(sum/num);
  if (meanAmbig != 0)
    for (int i=0; i<observables_.size(); i++)
    {
      o = observables_.at(i);
      o->activeDelay()->setNumOfAmbiguities(o->activeDelay()->getNumOfAmbiguities() 
        - meanAmbig);
    };
};



//
bool SgVlbiBaselineInfo::saveIntermediateResults(QDataStream& s) const
{
  SgObjectInfo::saveIntermediateResults(s);
  //
  s << dClock_ << dClockSigma_;
  //
  if (s.status() != QDataStream::Ok)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": saveIntermediateResults(): error writting data");
    return false;
  };
  return s.status() == QDataStream::Ok;
};



//
bool SgVlbiBaselineInfo::loadIntermediateResults(QDataStream& s)
{
  double                        dClock, dClockSigma;
  //
  SgObjectInfo::loadIntermediateResults(s);
  //
  s >> dClock >> dClockSigma;
  //
  if (s.status() != QDataStream::Ok)
  {
    logger->write(SgLogger::ERR, SgLogger::IO_BIN, className() +
      ": loadIntermediateResults(): error reading data: " +
      (s.status()==QDataStream::ReadPastEnd?"read past end of the file":"read corrupt data"));
    return false;
  };
  dClock_ = dClock;
  dClockSigma_ = dClockSigma;
  //
  return s.status()==QDataStream::Ok;
};
/*=====================================================================================================*/




/*=====================================================================================================*/
//
//                           FRIENDS:
// 
/*=====================================================================================================*/
//

/*=====================================================================================================*/
//
// aux functions:
//

/*=====================================================================================================*/
//
// constants:
//

/*=====================================================================================================*/
