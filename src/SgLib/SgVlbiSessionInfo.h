/*
 *
 *    This file is a part of Space Geodetic Library. The library is used by
 *    nuSolve, a part of CALC/SOLVE system, and designed to make analysis of
 *    geodetic VLBI observations.
 *    Copyright (C) 2010-2020 Sergei Bolotin.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SG_VLBI_SESSION_INFO_H
#define SG_VLBI_SESSION_INFO_H


#ifdef HAVE_CONFIG_H
#   include <config.h>
#endif


#include <math.h>


#include <QtCore/QMap>

#include <SgAttribute.h>
#include <SgMJD.h>



/***===================================================================================================*/
/**
 * 
 *
 */
/**====================================================================================================*/
// typedefs:
class SgVlbiSessionInfo;
typedef QMap<QString, SgVlbiSessionInfo*>           SessionInfosByName;
typedef QMap<QString, SgVlbiSessionInfo*>::iterator SessionInfosByName_it;
typedef QList<SgVlbiSessionInfo*>                   SessionInfos;
typedef QList<SgVlbiSessionInfo*>::iterator         SessionInfos_it;
//
//
class SgVlbiSessionInfo : public SgAttribute
{
public:
  enum OriginType
  {
    OT_DBH      = 0,                  //!< observations are from database files provided by correlators;
    OT_NGS      = 1,                  //!< observations are from a NGS file provided by other AC;
    OT_VDB      = 2,                  //!< observations are from vgosDb data tree;
    OT_MK4      = 3,                  //!< observations are from Mk4-compatible correlator output;
    OT_KOMB     = 4,                  //!< observations are from KOMB output (generated by NICT, Japan);
    OT_AGV      = 5,                  //!< observations are in AGV format;
    OT_UNKNOWN  = 0xff,               //!< unknown (=all others) source of import;
  };
  enum Attributes
  {
    Attr_NOT_VALID            = 1<< 0,//!< omit the session;
    Attr_HAS_AUX_OBS          = 1<< 1,//!< session contains aux.observations (e.g.: meteo data, cables);
    Attr_PRE_PROCESSED        = 1<< 2,//!< the observations has been prepared for analysis;
    Attr_HAS_CLOCK_BREAKS     = 1<< 3,//!< there is at least one clock break at one of stations;
    Attr_HAS_DTEC             = 1<< 4,//!< the session contains diffTec values;
    Attr_HAS_CALC_DATA        = 1<< 5,//!< the theoretical values are available;
    Attr_HAS_IONO_CORR        = 1<< 6,//!< the theoretical values are available;
    Attr_HAS_WEIGHTS          = 1<< 7,//!< the theoretical values are available;
    // fulfillments:
    Attr_FF_CREATED           = 1<< 8,//!< the session has been imported from correlator;//vgosDbMake
    Attr_FF_CALC_DATA_MODIFIED= 1<< 9,//!< the theoretical values were modified;         //vgosDbCalc
    Attr_FF_AUX_OBS_MODIFIED  = 1<<10,//!< station log related data were modified;       //vgosDbProcLogs
    Attr_FF_OUTLIERS_PROCESSED= 1<<11,//!< outliers have been processed;
    Attr_FF_AMBIGS_RESOLVED   = 1<<12,//!< ambiguities have been resolved;
    Attr_FF_WEIGHTS_CORRECTED = 1<<13,//!< weights have been corrected;
    Attr_FF_ION_C_CALCULATED  = 1<<14,//!< ionospheric corrections have been calculated;
    Attr_FF_ECC_UPDATED       = 1<<15,//!< eccentricity data for one of stations were updated;
    Attr_FF_AUTOPROCESSED     = 1<<16,//!< automatic data processing performed successfully;
    Attr_FF_EDIT_INFO_MODIFIED= 1<<17,//!< edit info has been modified by user;
    Attr_FF_STN_PWL_MODIFIED  = 1<<18,//!< station clocks or zenith delays were set.
    Attr_FF_PHASE_DEL_USED    = 1<<19,//!< station clocks or zenith delays were set.
    // modifiers:
    Attr_REF_CLOCKS_ADJUSTED  = 1<<20,// for KOMB type input
  };
  enum CorrelatorPostProcSoftware
  {
    CPPS_HOPS     = 0,                //!< HOPS
    CPPS_PIMA     = 1,                //!< PIMA
    CPPS_C5PP     = 2,                //!< C5++
    CPPS_UNKNOWN  = 0xff,             //!< unknown (=all others);
  };


  // Statics:
  /**Returns name of the class (for debug output).
   */
  static QString className();
  
  //
  // constructors/destructors:
  //
  /**A constructor.
   * Creates a default object.
   */
  SgVlbiSessionInfo();

  /**A constructor.
   * Creates a copy of the object.
   */
  SgVlbiSessionInfo(const SgVlbiSessionInfo&);

  /**A destructor.
   * Frees allocated memory.
   */
  virtual ~SgVlbiSessionInfo();



  //
  // Interfaces:
  //
  SgVlbiSessionInfo& operator=(const SgVlbiSessionInfo&);
  
  // gets:
  /**Returns type of origin of the session.
   */
  inline OriginType getOriginType() const;

  /**Returns epoch of the creation of original file.
   */
  inline const SgMJD& getTCreation() const;

  /**Returns epoch of the first observation.
   */
  inline const SgMJD& getTStart() const;

  /**Returns epoch of the last observation.
   */
  inline const SgMJD& getTFinis() const;

  /**Returns mean epoch of observations.
   */
  inline const SgMJD& getTMean() const;

  /**Returns name of the session.
   */
  inline const QString& getName() const;
  
  /**Returns network suffix.
   */
  inline const QString& getNetworkSuffix() const;

  /**Returns session code.
   */
  inline const QString& getSessionCode() const;

  /**Returns network ID.
   */
  inline const QString& getNetworkID() const;

  /**Returns description of the session.
   */
  inline const QString& getDescription() const;

  /**Returns user flag -- a set of char(s) assigned by user.
   */
  inline const QString& getUserFlag() const;

  /**Returns official name of the session (should consult Masterfile).
   */
  inline const QString& getOfficialName() const;

  /**Returns name/ID of the correlator that processed the session.
   */
  inline const QString& getCorrelatorName() const;

  /**Returns name/ID of the correlator that processed the session.
   */
  inline const QString& getCorrelatorType() const;

  /**Returns name/ID of the organization responsible to submit the sessions to IVS.
   */
  inline const QString& getSubmitterName() const;

  /**Returns name/ID of the organization that scheduled the sessions.
   */
  inline const QString& getSchedulerName() const;

  /**Returns name/ID of the organization that scheduled the sessions.
   */
  inline const QString& getPiAgencyName() const;

  inline int getExperimentSerialNumber() const {return experimentSerialNumber_;};
  inline const QString& getRecordingMode() const {return recordingMode_;};

  /**Returns number of stations participated in the session.
   */
  inline int getNumOfStations() const;

  /**Returns number of individual baselines that observed during the session.
   */
  inline int getNumOfBaselines() const;

  /**Returns number of sources which were observed.
   */
  inline int getNumOfSources() const;

  /**Returns number of observations.
   */
  inline int getNumOfObservations() const;
  
  inline CorrelatorPostProcSoftware getCppsSoft() const {return cppsSoft_;};



  // sets:
  /**Sets up type of origin.
   * \param type -- an actual type;
   */
  inline void setOriginType(OriginType type);

  /**Sets up epoch of the creation of original file.
   */
  inline void setTCreation(const SgMJD& t);

  /**Sets up epoch of the first observation.
   */
  inline void setTStart(const SgMJD& t);

  /**Sets up epoch of the last observation.
   */
  inline void setTFinis(const SgMJD& t);

  /**Sets up name of the session.
   */
  inline void setName(const QString& name);
  
  /**Sets up network suffix.
   */
  inline void setNetworkSuffix(const QString& suffix);

  /**Sets up session code.
   */
  inline void setSessionCode(const QString& code);

  /**Sets up network ID.
   */
  inline void setNetworkID(const QString& netID);

  /**Sets up description of the session.
   */
  inline void setDescription(const QString& description);

  /**Sets up user flag -- a set of char(s) assigned by user.
   */
  inline void setUserFlag(const QString& flag);

  /**Sets up official name of the session (should consult Masterfile).
   */
  inline void setOfficialName(const QString& name);

  /**Sets up correlator name for the session.
   */
  inline void setCorrelatorName(const QString& name);

  /**Sets up correlator name for the session.
   */
  inline void setCorrelatorType(const QString& name);

  /**Sets up a name of organization responsible to submit the sessions to IVS.
   */
  inline void setSubmitterName(const QString& name);

  /**Sets up a name of organization that scheduled the sessions.
   */
  inline void setSchedulerName(const QString& name);
  
  /**Sets up a name of organization that scheduled the sessions.
   */
  inline void setPiAgencyName(const QString& name);

  inline void setExperimentSerialNumber(int sn) {experimentSerialNumber_ = sn;};
  inline void setRecordingMode(const QString& mode) {recordingMode_ = mode;};

  /**Sets up number of stations participated in the session.
   */
  inline void setNumOfStations(int n);

  /**Sets up number of individual baselines that observed during the session.
   */
  inline void setNumOfBaselines(int n);

  /**Sets up number of sources which were observed.
   */
  inline void setNumOfSources(int n);

  /**Sets up number of observations.
   */
  inline void setNumOfObservations(int n);
  
  inline void setCppsSoft(CorrelatorPostProcSoftware soft) {cppsSoft_ = soft;};


  //
  // Functions:
  //
  /**
   */
  inline void addDelayWRMS(double o_c, double w);

  /**
   */
  inline void addRateWRMS(double o_c, double w);

  /** Returns weighted root mean squares for delays.
   */
  inline double delayWRMS() const;

  /** Returns weighted root mean squares for relay rates.
   */
  inline double rateWRMS() const;

  /**
   */
  inline void clearRMSs();

  /**
   */
  void guessNetworkId();


  //
  // Friends:
  //

  //
  // I/O:
  //
  // ...

protected:
  OriginType                    originType_;      //!< type of origin of the imported file(s);
  SgMJD                         tCreation_;       //!< date of creation of imported file(s);
  SgMJD                         tStart_;          //!< first epoch of the observations;
  SgMJD                         tFinis_;          //!< last epoch of the observations;
  SgMJD                         tMean_;           //!< mean epoch of the observations;

  QString                       name_;            //!< name of the session (e.g., 10JUL22XE);
  QString                       networkSuffix_;   //!< a char that specifies network (e.g., A, E, U, etc.);
  QString                       sessionCode_;     //!< official session code (from Masterfile);
  QString                       networkID_;       //!< Network ID (a key);
  QString                       description_;     //!< Experiment description;
  QString                       userFlag_;        //!< A flag assigned by user;
  QString                       officialName_;    //!< Official Name (from MasterFile)
  QString                       correlatorName_;  //!< Correlator Name (from MasterFile)
  QString                       correlatorType_;  //!< Type of the correlator;
  QString                       submitterName_;   //!< Name of resposible organization (from MasterFile)
  QString                       schedulerName_;   //!< Name of resposible organization (from MasterFile)
  QString                       piAgencyName_;    //!< Name of P.I. organization
  QString                       recordingMode_;
  int                           experimentSerialNumber_;


  int                           numOfStations_;
  int                           numOfBaselines_;
  int                           numOfSources_;
  int                           numOfObservations_;
  CorrelatorPostProcSoftware    cppsSoft_;

  double                        delaySumRMS2_;
  double                        rateSumRMS2_;
  double                        delaySumW_;
  double                        rateSumW_;
  
  virtual inline void calcTMean();
};
/*=====================================================================================================*/





/*=====================================================================================================*/
/*                                                                                                     */
/* SgVlbiSessionInfo inline members:                                                                   */
/*                                                                                                     */
/*=====================================================================================================*/
//
//
// INTERFACES:
//
//
inline SgVlbiSessionInfo::OriginType SgVlbiSessionInfo::getOriginType() const
{
  return originType_;
};



//
inline const SgMJD& SgVlbiSessionInfo::getTCreation() const
{
  return tCreation_;
};



//
inline const SgMJD& SgVlbiSessionInfo::getTStart() const
{
  return tStart_;
};



//
inline const SgMJD& SgVlbiSessionInfo::getTFinis() const
{
  return tFinis_;
};



//
inline const SgMJD& SgVlbiSessionInfo::getTMean() const
{
  return tMean_;
};



//
inline const QString& SgVlbiSessionInfo::getName() const
{
  return name_;
};



//
inline const QString& SgVlbiSessionInfo::getNetworkSuffix() const
{
  return networkSuffix_;
};



//
inline const QString& SgVlbiSessionInfo::getSessionCode() const
{
  return sessionCode_;
};



//
inline const QString& SgVlbiSessionInfo::getNetworkID() const
{
  return networkID_;
};



//
inline const QString& SgVlbiSessionInfo::getDescription() const
{
  return description_;
};



//
inline const QString& SgVlbiSessionInfo::getUserFlag() const
{
  return userFlag_;
};



//
inline const QString& SgVlbiSessionInfo::getOfficialName() const
{
  return officialName_;
};



//
inline const QString& SgVlbiSessionInfo::getCorrelatorName() const
{
  return correlatorName_;
};



//
inline const QString& SgVlbiSessionInfo::getCorrelatorType() const
{
  return correlatorType_;
};



//
inline const QString& SgVlbiSessionInfo::getSubmitterName() const
{
  return submitterName_;
};



//
inline const QString& SgVlbiSessionInfo::getSchedulerName() const
{
  return schedulerName_;
};



//
inline const QString& SgVlbiSessionInfo::getPiAgencyName() const
{
  return piAgencyName_;
};



//
inline int SgVlbiSessionInfo::getNumOfStations() const
{
  return numOfStations_;
};



//
inline int SgVlbiSessionInfo::getNumOfBaselines() const
{
  return numOfBaselines_;
};



//
inline int SgVlbiSessionInfo::getNumOfSources() const
{
  return numOfSources_;
};



//
inline int SgVlbiSessionInfo::getNumOfObservations() const
{
  return numOfObservations_;
};






//
inline void SgVlbiSessionInfo::setOriginType(OriginType type)
{
  originType_ = type;
};



//
inline void SgVlbiSessionInfo::setTCreation(const SgMJD& t)
{
  tCreation_ = t;
};



//
inline void SgVlbiSessionInfo::setTStart(const SgMJD& t)
{
  tStart_ = t;
};



//
inline void SgVlbiSessionInfo::setTFinis(const SgMJD& t)
{
  tFinis_ = t;
};



//
inline void SgVlbiSessionInfo::setName(const QString& name)
{
  name_ = name;
};



//
inline void SgVlbiSessionInfo::setNetworkSuffix(const QString& suffix)
{
  networkSuffix_ = suffix;
};



//
inline void SgVlbiSessionInfo::setSessionCode(const QString& code)
{
  sessionCode_ = code;
};



//
inline void SgVlbiSessionInfo::setNetworkID(const QString& netID)
{
  networkID_ = netID;
};



//
inline void SgVlbiSessionInfo::setDescription(const QString& description)
{
  description_ = description;
};



//
inline void SgVlbiSessionInfo::setUserFlag(const QString& flag)
{
  userFlag_ = flag;
};



//
inline void SgVlbiSessionInfo::setOfficialName(const QString& name)
{
  officialName_ = name;
};



//
inline void SgVlbiSessionInfo::setCorrelatorName(const QString& name)
{
  correlatorName_ = name;
};



//
inline void SgVlbiSessionInfo::setCorrelatorType(const QString& type)
{
  correlatorType_ = type;
};



//
inline void SgVlbiSessionInfo::setSubmitterName(const QString& name)
{
  submitterName_ = name;
};



//
inline void SgVlbiSessionInfo::setSchedulerName(const QString& name)
{
  schedulerName_ = name;
};



//
inline void SgVlbiSessionInfo::setPiAgencyName(const QString& name)
{
  piAgencyName_ = name;
};



//
inline void SgVlbiSessionInfo::setNumOfStations(int n)
{
  numOfStations_ = n;
};



//
inline void SgVlbiSessionInfo::setNumOfBaselines(int n)
{
  numOfBaselines_ = n;
};



//
inline void SgVlbiSessionInfo::setNumOfSources(int n)
{
  numOfSources_ = n;
};



//
inline void SgVlbiSessionInfo::setNumOfObservations(int n)
{
  numOfObservations_ = n;
};


//
// FUNCTIONS:
//
//
//
//
inline void SgVlbiSessionInfo::calcTMean()
{
  tMean_ = (tStart_.toDouble() + tFinis_.toDouble())/2.0;
};



//
inline void SgVlbiSessionInfo::addDelayWRMS(double o_c, double w)
{
  delaySumRMS2_ += o_c*o_c*w;
  delaySumW_ += w;
};



//
inline void SgVlbiSessionInfo::addRateWRMS(double o_c, double w) 
{
  rateSumRMS2_ += o_c*o_c*w;
  rateSumW_ += w;
};



//
inline double SgVlbiSessionInfo::delayWRMS() const 
{
  return delaySumW_ > 0.0 ? sqrt(delaySumRMS2_/delaySumW_) : 0.0;
};



//
inline double SgVlbiSessionInfo::rateWRMS() const 
{
  return rateSumW_ > 0.0 ? sqrt(rateSumRMS2_/rateSumW_) : 0.0;
};



//
inline void SgVlbiSessionInfo::clearRMSs() 
{
  delaySumRMS2_ = delaySumW_ = rateSumRMS2_ = rateSumW_ = 0.0;
};



//
// FRIENDS:
//
//
//


/*=====================================================================================================*/





/*=====================================================================================================*/
//
// aux functions:
//
bool lessThan4_FirstEpochSortingOrder(SgVlbiSessionInfo*, SgVlbiSessionInfo*);

/*=====================================================================================================*/
#endif // SG_VLBI_SESSION_INFO_H
