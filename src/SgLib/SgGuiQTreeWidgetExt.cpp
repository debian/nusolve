/*
 *
 *    This file is a part of Space Geodetic Library. The library is used by
 *    nuSolve, a part of CALC/SOLVE system, and designed to make analysis of
 *    geodetic VLBI observations.
 *    Copyright (C) 2010-2020 Sergei Bolotin.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <SgGuiQTreeWidgetExt.h>
#include <SgLogger.h>


#if QT_VERSION >= 0x050000
#   include <QtWidgets/QApplication>
#else
#   include <QtGui/QApplication>
#endif

#include <QtGui/QMouseEvent>


/*==================================================================================================
*
*                           METHODS:
* 
*=================================================================================================*/
SgGuiQTreeWidgetExt::SgGuiQTreeWidgetExt(QWidget* parent)
  : QTreeWidget(parent)
{
  isMoving_ = false;
  mItem_ = NULL;
  mCol_ = -1;
  mouseButtonState_ = Qt::NoButton;
  connect(this, 
    SIGNAL(itemPressed(QTreeWidgetItem*, int)),
    SLOT(movingStarted(QTreeWidgetItem*, int)));
};



//
void SgGuiQTreeWidgetExt::movingStarted(QTreeWidgetItem* item, int c)
{
  if (item)
  {
    mItem_ = item;
    mCol_ = c;
    isMoving_ = true;
    emit moveUponItem(mItem_, mCol_, mouseButtonState_, QApplication::keyboardModifiers());
  };
};



//
void SgGuiQTreeWidgetExt::mouseMoveEvent(QMouseEvent* e)
{
  if (e && isMoving_)
  {
    //QPoint vp = contentsToViewport(e->pos());
    QTreeWidgetItem *item = itemAt(e->pos());
    if (item && item!=mItem_)
    {
      mItem_ = item;
      setCurrentItem(mItem_);
      emit moveUponItem(mItem_, mCol_, mouseButtonState_, e->modifiers());
    };
  };
  QTreeWidget::mouseMoveEvent(e);
};



//
void SgGuiQTreeWidgetExt::mousePressEvent(QMouseEvent* e)
{
  if (e)
    mouseButtonState_ = e->button();
  QTreeWidget::mousePressEvent(e);
};



//
void SgGuiQTreeWidgetExt::mouseReleaseEvent(QMouseEvent* e)
{
  mItem_ = NULL;
  mCol_ = -1;
  isMoving_ = false;
  mouseButtonState_ = e->button();
  QTreeWidget::mouseReleaseEvent(e);
};
/*================================================================================================*/
